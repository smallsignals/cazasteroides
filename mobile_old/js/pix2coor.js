//from pixel to coordinate

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

deg2degminsec=function (degin){
      var deg = Math.floor(degin);
      var m = Math.floor((degin-deg)*60);
      var s = (((degin-deg)*60) - m )*60;

	return ""+pad(deg,2)+" "+pad(m,2)+" "+s.toFixed(2);//fixme cuidado con el +
}
deg2hour=function (deg){
	var hour = Math.floor(deg*24.0/360.0);
	var mdec = ((deg*24.0/360.0)-hour)*60.0;

	var m = Math.floor(mdec);
	var s = (mdec - m )*60;
	return ""+pad(hour,2)+" "+pad(m,2)+" "+s.toFixed(2);
}
deg2degminsecPretty=function (min){
	var hour = Math.floor(min/60);
	var m = Math.floor(min-(hour*60));
	var s = (min-(hour*60) - m )*60;
	return ""+hour+"º "+m+"m "+s.toFixed(2);
}
deg2hourPretty=function (deg){
	var hour = Math.floor(deg*24.0/360.0);
	var mdec = ((deg*24.0/360.0)-hour)*60.0;

	var m = Math.floor(mdec);
	var s = (mdec - m )*60;
	return ""+hour+"h "+m+"m "+s.toFixed(2);
}
pix2coord = function (x_pix, y_pix, metadata){
	/*
	 |x|  | CD1_1 CD1_2 |   | f(u,v)+u |
	 | |= |             | * |          |
	 |y|  | CD2_1 CD2_2 |   | g(u,v)+v |
	*/

	u = x_pix - parseFloat(metadata.CRPIX1);
	v = y_pix -  parseFloat(metadata.CRPIX2);

	var t1 = f_func(u,v,metadata)+u;
	var t2 = g_func(u,v,metadata)+v;
    var x =  parseFloat(metadata.CD1_1)*t1+ parseFloat(metadata.CD1_2)*t2;
    var y =  parseFloat(metadata.CD2_1)*t1+ parseFloat(metadata.CD2_2)*t2;
    x = x +  parseFloat(metadata.CRVAL1);
    y = y +  parseFloat(metadata.CRVAL2);
    return [x, y];
}
f_func = function (u , v,metadata){
	var res =  parseFloat(metadata.A_0_0) + 
	           parseFloat(metadata.A_1_0) * u + 
	           parseFloat(metadata.A_0_1) * v +
			   parseFloat(metadata.A_2_0) *u*u + 
			   parseFloat(metadata.A_0_2)*v*v + 
			   parseFloat(metadata.A_1_1)*u*v 
			  // A_2_1*u*u*v + A_1_2*u*v*v + A_3_0*u*u*u+ A_0_3*v*v*v
	return res;
}
g_func = function (u , v,metadata){
	var res =  parseFloat(metadata.B_0_0) + 
	           parseFloat(metadata.B_1_0) * u + 
	           parseFloat(metadata.B_0_1) * v +
			   parseFloat(metadata.B_2_0) *u*u + 
			   parseFloat(metadata.B_0_2)*v*v + 
			   parseFloat(metadata.B_1_1)*u*v 
	return res;
			  
}

