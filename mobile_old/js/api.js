//api.js

var Api = function () {
	this.server = SERVER_URL_BASE;
};

Api.prototype.getQueryPic= function (n){

}
Api.prototype.star=function (RA, DEC,cb){
	var identData = {"RA":RA, "DEC":DEC};
	$.ajax({
		url: this.server + '/images/star',
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		//dataType: 'json',
		data: JSON.stringify(identData),
        success: function(data){
        	cb(data);
          //$.mobile.navigate("#mainpage");
      },
      error: function(jqXHR) {
      	if (jqXHR.status == 401){
      		console.log('Invalid user or password');
      		cb('Invalid User or password');
      	}else{
      		console.log('Error connecting to server');
      		cb('Error connecting to server');
      	}
      },
      failure: function(errMsg) {
      	console.log(errMsg);
      	cb(errMsg);
      }
  });//fin ajax
}

Api.prototype.identify=function (RA, DEC,cb){
	var identData = {"RA":RA, "DEC":DEC};
	$.ajax({
		url: this.server + '/images/mpc',
		type: 'POST',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		data: JSON.stringify(identData),
        success: function(data){
        	cb(data);
          //$.mobile.navigate("#mainpage");
      },
      error: function(jqXHR) {
      	if (jqXHR.status == 401){
      		console.log('Invalid user or password');
      		cb('Invalid User or password');
      	}else{
      		console.log('Error connecting to server');
      		cb('Error connecting to server');
      	}
      },
      failure: function(errMsg) {
      	console.log(errMsg);
      	cb(errMsg);
      }
  });//fin ajax
}
Api.prototype.getHistogram=function (){

	$.ajax({
		url: this.server + '/images/kk/histogram',
		type: 'GET',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success: function(data){
      //SERVER_TOKEN=data.token;
      data.min = data.min + 32768;
      data.max = data.max + 32768;
      m_hexcel = new Histogram();
      m_hexcel.setData(data);
      m_hexcel.draw();
          //$.mobile.navigate("#mainpage");
      },
      error: function(jqXHR) {
      	if (jqXHR.status == 401){
      		console.log('Invalid user or password');
      		alert('Invalid User or password');
      	}else{
      		console.log('Error connecting to server');
      		alert('Error connecting to server');
      	}
      },
      failure: function(errMsg) {
      	console.log(errMsg);
      	alert(errMsg);
      }
  });//fin ajax
}
Api.prototype.setMeatdata=function(data) { 
		 	console.log("obj"+obj)
			console.log(data);	
			this.metadata = data;
}
Api.prototype.getMetadata=function (){
	obj = this;
	$.ajax({
		url: this.server + '/images/kk/coeff',
		type: 'GET',
		contentType: 'application/json; charset=utf-8',
		dataType: 'json',
		success:function(data){ obj.setMeatdata(data)},
		error: function(jqXHR) {
			if (jqXHR.status == 401){
				console.log('Invalid user or password');
				alert('Invalid User or password');
			}else{
				console.log('Error connecting to server');
				alert('Error connecting to server');
			}
		},
		failure: function(errMsg) {
			console.log(errMsg);
			alert(errMsg);
		}
  });//fin ajax
}
