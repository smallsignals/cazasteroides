import os
import pymongo
import shutil
import requests
import subprocess
import gzip
import time
import inspect
import glob

from xml.etree import ElementTree
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timedelta

try:
    import pwd
except ImportError:
    try:
        import winpwd as pwd
    except ImportError as err:
        print("ImportErroras error: {0}".format(err))
    
try:
    uid = pwd.getpwnam('root')[2]
    os.setuid(uid)
except Exception as err:
    print("Exception error: {0}".format(err))

#Eliminar los directorios de las imagenes que ya no estan en la base de datos
#Descargar las imagenes que estan en la base de datos pero no existe su directorio

#Configuration

exclude_dir_names_on_data = ['dump']

#my_api_url = 'http://localhost:3000/' #Test url
my_api_url = 'http://api.cazasteroides.org/'
my_api_gif_url = my_api_url+'obs/get_obs_gif/'


#my_mongo_client = 'mongodb://web:web@ds143980.mlab.com:43980/cazasteroides' #Test database
my_mongo_client = 'mongodb://web:web@ds129641.mlab.com:29641/cazasteroides' # Nueva Base de Datos Propiedad de AstroAula
my_mongo_db = 'cazasteroides';

my_src_data = os.path.normpath(os.path.join((os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))),'../data/'))
my_src_gif = os.path.normpath(os.path.join((os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))),'../server/cazasteroides/gif/'))
my_mapfile_map = os.path.join(my_src_data,'mapfile.map')
my_tv_key = '40680253ab417e043ee14810f70b990008a9c01a' #IAC
#my_tv_key = '85ab4f72b2c8e71988d47d1fabfb0f163c8b5f76' #UPM
my_tv_url = 'http://www.telescopiovirtual.com:80/services/remote/'+my_tv_key

#print('Connecting...')
client = MongoClient(my_mongo_client)

#print('Open database')
db = client[my_mongo_db]

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name)) and name not in exclude_dir_names_on_data]

def get_gif_startswith(det_id):
    return [name for name in os.listdir(my_src_gif)
            if os.path.isfile(os.path.join(my_src_gif, name)) and name.startswith(str(det_id))]

def exist_gif(det_id):
    return len([name for name in os.listdir(my_src_gif)
            if os.path.isfile(os.path.join(my_src_gif, name)) and name.startswith(str(det_id))])>0

def remove_readonly(func, path, excinfo):
    #os.chmod(path, stat.S_IWRITE)
    #func(path)
    if os.path.exists(path):
        print(str(path)+' '+str(excinfo))
    
def generate_findasteroid_uri(d):
    date = str(d.year)+'-'+str(d.month).zfill(2) +'-'+str(d.day).zfill(2) 
    url =my_tv_url+'/findasteroids/extendedheaders/'+date
    #print(url)
    return url

#2017-04-22T00:00:00Z
def generate_findasteroid_uri_string(d):
    date = d[0:10] 
    url =my_tv_url+'/findasteroids/extendedheaders/'+date
    #print(url)
    return url

def generate_findimages_uri(name):
    date = name[0:4]+'-'+name[4:6]+'-'+name[6:8]
    telescopio = name[14:]
    url = my_tv_url+'/findimages/headers/'+date+'/'+telescopio+'/asteroid('+name+')'
    #print(url)
    return url

def generate_findimages_uri_end_numeric(name):
    date = name[0:4]+'-'+name[4:6]+'-'+name[6:8]
    telescopio = name[14:len(name)-2]
    url = my_tv_url+'/findimages/headers/'+date+'/'+telescopio+'/asteroid('+name+')'
    #print(url)
    return url

def unzip_file(fileGz,fileFit):
    if os.path.isfile(fileGz):
        inF = gzip.open(fileGz, 'rb')            
        outF = open(fileFit, 'wb')
        outF.write( inF.read() )
        inF.close()
        outF.close()

        os.remove(fileGz)

def download_file(url,fileGz):
    if not os.path.isfile(fileGz):
        #print("Download: "+url)
        #print("File: "+fileGz)
        response = requests.get(url, stream=True)
        handle = open(fileGz, "wb")
        for chunk in response.iter_content(chunk_size=512):
            if chunk:  # filter out keep-alive new chunks
                handle.write(chunk)

def download_and_unzip(url,fileGz,fileFit,forceDownload):
    if not forceDownload and os.path.isfile(fileFit):
        return
    
    download_file(url,fileGz)
    unzip_file(fileGz,fileFit)

def parseTimestamp(str):
    if len(str) != 14 and not str.isdigit():
        return "invalid date";

    y = int(str[0: 4])
    m = int(str[4: 6])
    d =int(str[6: 8])
    h = int(str[8: 10])
    mm = int(str[10: 12])
    s = int(str[12:])

    return datetime(y, m, d, h, mm, s);


def update_database(name,fitImages):
    #Create Image on database
    fileNames = ['','','','','']
    fileNames[0] = name+'/'+os.path.basename(fitImages[0])
    fileNames[1] = name+'/'+os.path.basename(fitImages[1])
    fileNames[2] = name+'/'+os.path.basename(fitImages[2])
    fileNames[3] = name+'/'+os.path.basename(fitImages[3])
    fileNames[4] = name+'/'+os.path.basename(fitImages[4])

    values = {'name': name, 'fecha': parseTimestamp(name[0: 14]), 'mapfile': name+'/mapfile.map', 'filename': fileNames, 'active' : True}

    id = None 
    if db.images.find_one({'name': name}):
        db.images.update_one({'name': name},{'$set': values})
    else:       
        result = db.images.insert_one(values)
        #print(my_api_url+'images/'+str(result.inserted_id)+'/0/coeff')
        requests.get(my_api_url+'images/'+str(result.inserted_id)+'/0/coeff')

        
def generate_mapfile(name,fitImages):
    dir = os.path.join(my_src_data,name)

    isNewQuintet = True
    
    fileMap = os.path.join(dir,'mapfile.map')
    if os.path.isfile(fileMap):
        #os.remove(fileMap)
        isNewQuintet = False

    with open(my_mapfile_map, "rt") as fin:
        with open(fileMap, "wt") as fout:
            for line in fin:
                line = line.replace('AAAA1', fitImages[0])
                line = line.replace('AAAA2', fitImages[1])
                line = line.replace('AAAA3', fitImages[2])
                line = line.replace('AAAA4', fitImages[3])
                line = line.replace('AAAA5', fitImages[4])
                fout.write(line)

            fout.close()
        fin.close()

    return isNewQuintet;       

def generate_xml(telescop,ecliplat,proba,quintet,quintetn,process):
    xml = '<asteroid_image>'
    xml += "<image><telescop>"+telescop+"</telescop></image>"
    xml += "<ecliplat>"+ecliplat+"</ecliplat>"
    xml += "<proba>"+proba+"</proba>"
    xml += "<quintet>"+quintet+"</quintet>"
    xml += "<quintetn>"+quintetn+"</quintetn>"
    xml += "<process>"+process+"</process>"
    xml += "</asteroid_image>"    
    return xml

def remove_unused_images():
    count = 0
    #Obtenemos todas las imagenes que no estan activas y las guardamos en una lista
    resultImgs = db.images.find({"active":{"$eq":False}},{"_id":1,"name":1})
    #Eliminamos las imagenes del servidor siempre que no existan detecciones para ese quinteto
    for el in resultImgs:

        #if db.candidates.find_one({"coordenates.quintet":{"$eq":el['_id']},"mpcReportState":{"$ne":1},"isKnown":False}):
        #    continue
        obs = db.observations.find_one({'image':ObjectId(el['_id'])})
        if obs is not None:
            continue
        
        path = os.path.join(my_src_data,str(el['name']))
        if os.path.exists(path):
            shutil.rmtree(path, onerror=remove_readonly)
            count += 1
            
    return count        
'''    lstImageID = []
    for result in resultImgs:
        lstImageID.append(result)

    #Eliminar de la lista las imagenes que tengan observaciones
    resultObs = db.observations.distinct("image")
    for result in resultObs:
        if(len(lstImageID)==0):
                resultObs = []
                break
        if any(x['_id'] == result for x in lstImageID):
            for x in lstImageID:
                if(x['_id'] == result):
                    lstImageID.remove(x)
                    break

    #Eliminamos las imagenes del servidor y de la bbdd
    for el in lstImageID:
        #print(my_src_data+str(el['name']))
        shutil.rmtree(my_src_data+str(el['name']), onerror=remove_readonly)
        db.images.delete_one({'_id': ObjectId(el['_id'])})
        count += 1
    return count
'''

    
def remove_unused_directories():
    count = 0
    directories = get_immediate_subdirectories(my_src_data)

    for name in directories:
        image = db.images.find_one({'name':name})        
        if image is None:
            shutil.rmtree(os.path.join(my_src_data,name), onerror=remove_readonly)
            count += 1
    return count

            
def disable_images():
    count = 0
    resultImgs = db.images.find({"active":{"$eq":True}},{"_id":1,"fecha":1,"name":1})
    for image in resultImgs:
        response = requests.post(generate_findasteroid_uri(image['fecha']), data=generate_xml('','','',image['name'],'','true'), headers={'Content-Type': 'application/xml'})
        tree = ElementTree.fromstring(response.content)
        if len(tree.findall('asteroid_image')) == 0 :
            #Need check that exist at least one active:True
            if (db.images.find_one({"active":{"$eq":True},'_id':{"$ne": ObjectId(image['_id'])}})):
                db.images.update_one({'_id': ObjectId(image['_id'])},{'$set': {'active':False}})
                count += 1

    return count

def disable_images_by_threshold():
    count = 0
    threshold = db.setups.find({"config.key":"threshold_to_deactivate_quintet"},{"config.$":1})
    if threshold.count()>0:
        resultImgs = db.images.find({"active":{"$eq":True}},{"_id":1,"fecha":1,"name":1,"info":1})
        for image in resultImgs:
            if 'info' in image and 'total_area_shown' in  image['info'] and 'total_area' in  image['info'] and image['info']['total_area_shown'] and image['info']['total_area'] and (float(image['info']['total_area_shown'])/float(image['info']['total_area']))>float(threshold[0]['config'][0]['value']):
                #Need check that exist at least one active:True
                if (db.images.find_one({"active":{"$eq":True},'_id':{"$ne": ObjectId(image['_id'])}})):
                    db.images.update_one({'_id': ObjectId(image['_id'])},{'$set': {'active':False,'disabled_by_threshold':True}})
                    count += 1
        
    return count

def load_process_images():
    count = 0
    fitImages = []
    responseProcess = requests.get(my_tv_url+'/quintetos')
    treeProcess = ElementTree.fromstring(responseProcess.content)
    for numquinteto in treeProcess.findall('numquinteto'):
        if int(numquinteto.find('num').text) < 5:
            continue
        
        response = requests.post(generate_findasteroid_uri_string(numquinteto.find('fecha').text), data=generate_xml('','','','','','true'), headers={'Content-Type': 'application/xml'})
        tree = ElementTree.fromstring(response.content)
        lstQuintet = {}
        for publicImage in tree.findall('asteroid_image'):
            
            quintet = publicImage.find('quintet').text

            if quintet is None or db.images.find_one({'name': quintet}):
                continue

            dir = os.path.join(my_src_data,quintet)        
            
            if not os.path.exists(dir):
                os.makedirs(dir)
                #print('Create Dir: '+dir)
            
            idImagen = publicImage.find('image').find('idimagen').text
            
            fileGz = os.path.join(dir,idImagen + '.fits.gz')
            fileFits = os.path.join(dir,idImagen + '.fits')
            
            download_and_unzip(publicImage.find('image').find('fitsgz').text,fileGz,fileFits,False)       
            
            
            if quintet not in lstQuintet:
                lstQuintet.setdefault(quintet,[])
            lstQuintet[quintet].append(fileFits)
                    
        for x in lstQuintet:
            #print("Total: "+str(len(lstQuintet[x])))
            if len(lstQuintet[x]) == 5:
                if generate_mapfile(x,lstQuintet[x]):
                    count += 1
                update_database(x,lstQuintet[x])
                
            else:
                dir = os.path.join(my_src_data,x)
                shutil.rmtree(dir, onerror=remove_readonly)   
               
    return count
 
    
def load_new_images(days_ago):
    count = 0
    fitImages = []
    for day in range(days_ago):
        d = datetime.today() - timedelta(days=day)
        response = requests.post(generate_findasteroid_uri(d), data=generate_xml('','','','','','true'), headers={'Content-Type': 'application/xml'})
        tree = ElementTree.fromstring(response.content)

        lstQuintet = {}
        for publicImage in tree.findall('asteroid_image'):
            
            quintet = publicImage.find('quintet').text

            if db.images.find_one({'name': quintet}):
                continue

            dir = os.path.join(my_src_data,quintet)        
            
            if not os.path.exists(dir):
                os.makedirs(dir)
                #print('Create Dir: '+dir)
            
            idImagen = publicImage.find('image').find('idimagen').text
            
            fileGz = os.path.join(dir,idImagen + '.fits.gz')
            fileFits = os.path.join(dir,idImagen + '.fits')
            
            download_and_unzip(publicImage.find('image').find('fitsgz').text,fileGz,fileFits,False)       
            
            
            if quintet not in lstQuintet:
                lstQuintet.setdefault(quintet,[])
            lstQuintet[quintet].append(fileFits)
                    
        for x in lstQuintet:
            #print("Total: "+str(len(lstQuintet[x])))
            if len(lstQuintet[x]) == 5:
                if generate_mapfile(x,lstQuintet[x]):
                    count += 1
                update_database(x,lstQuintet[x])                
            else:
                dir = os.path.join(my_src_data,x)
                shutil.rmtree(dir, onerror=remove_readonly)   
                
    return count

def sync_loss_dirs():
    count = 0
    directories = get_immediate_subdirectories(my_src_data)

    resultImgs = db.images.find({},{"_id":1,"name":1,"active":1})
    lstImageName = []
    for result in resultImgs:
        dirImage = os.path.join(my_src_data,result['name'])
        if result['name'] not in directories or os.listdir(dirImage) == [] or not any(File.endswith(".map") for File in os.listdir(dirImage)) or len(glob.glob1(dirImage,"*.fits")) < 5:
            if result['active']:
                lstImageName.append(result['name'])
                #print('Added Active: '+result['name'])
            else:
                obs = db.observations.find_one({'image':ObjectId(result['_id'])})
                if obs is not None:
                    lstImageName.append(result['name'])
                    #print('Added With Detection: '+result['name'])
                                  
    for name in lstImageName:  
        #Download Header
        response = requests.get(generate_findimages_uri(name))
        tree = ElementTree.fromstring(response.content)

        if not tree:
            response = requests.get(generate_findimages_uri_end_numeric(name))
            tree = ElementTree.fromstring(response.content)
        
        fitImages = []
        dir = os.path.join(my_src_data,name)
        for publicImage in tree.findall('public_image'):
            
            if not os.path.exists(dir):
                os.makedirs(dir)
                print('Create Dir: '+dir)

            idImagen = publicImage.find('idimagen').text
            
            fileGz = os.path.join(dir,idImagen + '.fits.gz')
            fileFits = os.path.join(dir,idImagen + '.fits')
            
            download_and_unzip(publicImage.find('fitsgz').text,fileGz,fileFits,True)         

            fitImages.append(fileFits)

        if not os.path.exists(dir):
            continue

        # Create mapfile.map
        if len(fitImages) == 5:
            generate_mapfile(name,fitImages)
            count += 1
    return count

def increase_user_contests_points(contestsList,user_id,points):
    user = db.users.find_one({'_id':ObjectId(user_id)})
    index = 0

    if user and user['contests']:
        for contest in user['contests']:
            if any(str(contest['contest']) in s for s in contestsList):
                #print("update score "+str(contest['contest'])+" "+str(contest['points'])+" "+str(index))
                db.users.find_one_and_update({"_id":ObjectId(user_id)},{'$inc': {'contests.'+str(index)+'.points': points}})
            index +=1
    

def delete_rejecteds(downvotePoints):
    count = 0

    resultImgs = db.images.find({"active":{"$eq":False}},{"_id":1})
    for image in resultImgs:
        #Delete Rejecteds        
        resultDetections = db.observations.find({"image":ObjectId(image['_id']),"status":"rejected"},{'_id':1})
        for detection in resultDetections:
            for gif in get_gif_startswith(detection['_id']):
                shutil.rmtree(my_src_gif+gif, onerror=remove_readonly)        
        
        result = db.observations.delete_many({"image":ObjectId(image['_id']),"status":"rejected"})
        count += result.deleted_count
        
        # Delete pendings with only one detection and without votes or more downvotes that votes
        resultDetections = db.observations.find({"image":ObjectId(image['_id']),"status":"pending","$or":[{"revisit": {"$size": 0}},{"revisit": {"$exists": False}}]},{'_id':1,'votes':1,'downvotes':1,'user':1,'revisit':1})
        for detection in resultDetections:
            if len(detection['downvotes']) > len(detection['votes']) or len(detection['votes']) == 0:                
                for gif in get_gif_startswith(detection['_id']):
                    shutil.rmtree(my_src_gif+gif, onerror=remove_readonly)        


                timeNow = datetime.now()
                contests = db.contests.find({"time_start": {'$lte': timeNow},"time_end": {'$gte': timeNow}},{'_id':1,'name':1})

                contestsList = []
                for contest in contests:
                    contestsList.append(str(contest['_id']))
                    #print ("Active contest: "+str(contest['name'])+" "+str(contest['_id']))

                contestsUsersList = []
                contestsUsersList.append(str(detection['user']))
                updateResult = db.users.find_one_and_update({"_id":ObjectId(detection['user'])},{'$inc': {'sDetections.rejected': 1}})

                for user in detection['revisit']:
                    updateResult = db.users.find_one_and_update({"_id":ObjectId(user['user'])},{'$inc': {'sDetections.rejected': 1}})
                    contestsUsersList.append(str(user['user']))

                for user in detection['votes']:
                    updateResult = db.users.find_one_and_update({"_id":ObjectId(user)},{'$inc': {'sVotes.wrongs': 1}})
                    contestsUsersList.append(str(user))                    
                
                for user in detection['downvotes']:                  
                    
                    #Dar puntos a los usuarios que votaron negativo siempre que no esten ya en la lista, y aadir puntos al concurso                   
                    if not any(str(user) in s for s in contestsUsersList):
                        #print(user)
                        updateResult = db.users.find_one_and_update({"_id":ObjectId(user)},{'$inc': {'points': downvotePoints, 'sVotes.corrects': 1, 'sVotes.points': downvotePoints}})                        
                        increase_user_contests_points(contestsList,user,downvotePoints)
                    contestsUsersList.append(str(user))



                #Eliminar la deteccion
                result = db.observations.delete_one({"_id":ObjectId(detection['_id'])})
                count += result.deleted_count
                
    return count

def generate_gif():
    count = 0
    countError = 0

    resultImgs = db.images.find({"active":{"$eq":False}},{"_id":1})
    for image in resultImgs:
        resultDetec = db.observations.find({"image":ObjectId(image['_id'])},{"_id":1})
        for detection in resultDetec:
            if not exist_gif(detection['_id']):
                r = requests.get(my_api_gif_url+str(detection['_id']))
                count += 1 if r.status_code==200 else 0
                countError += 0 if r.status_code==200 else 1

    #
    #for name in directories:

    return 'Generated: '+str(count)+' Fails: '+str(countError)

def remove_gif():
    count = 0
    
    for file in os.listdir(my_src_gif):
        olderThanDays = 15*60*60 # convert days to seconds
        present = time.time()
        if file.endswith(".gif") and (present - os.path.getmtime(os.path.join(my_src_gif, file))) > olderThanDays:
            id = ''
            if file.find("_") > 0:
                id = file[:file.find("_")]
            elif file.find(".gif") > 0:
                id = file[:file.find(".gif")]

            if db.observations.find_one({'_id':ObjectId(id)}) is None:
                os.remove(os.path.join(my_src_gif, file))
                count += 1

    return count

def delete_zones():
    count = 0

    resultImgs = db.images.find({"active":{"$eq":False}},{"_id":1})
    for image in resultImgs:
        result = db.rectangles.delete_many({"image":ObjectId(image['_id'])})
        count += result.deleted_count
            
    return count

def generateCandidates():
    response = requests.get(my_api_url+'admin/candidate/generate')


if not os.path.exists(my_src_data):
    print ("No existe la ruta: "+os.path.abspath(my_src_data))
    exit()

if not os.path.exists(my_src_gif):
    print ("No existe la ruta: "+os.path.abspath(my_src_gif))
    exit()

    
'''
recentImage = list(db.images.find({"active":{"$eq":True}}).limit(1).sort([('fecha', -1)]))
if recentImage:
    print (str(recentImage[0]['fecha']))
else:
    dayFrom = d = datetime.today() - timedelta(days=30)
'''

daysToLoad = 30
threshold = db.setups.find({"config.key":"days_to_load_quintets"},{"config.$":1})
if threshold.count()>0:
   daysToLoad = int(threshold[0]['config'][0]['value'])

downvotePoints = 5
downvote_points = db.setups.find({"config.key":"downvote_points"},{"config.$":1})
if downvote_points.count()>0:
   downvotePoints = int(downvote_points[0]['config'][0]['value'])             


timeStart = datetime.now()
total_dis = disable_images()
total_dit = disable_images_by_threshold()
total_loa = load_process_images()#load_new_images(daysToLoad)
total_dde = delete_rejecteds(downvotePoints)
total_gif = generate_gif()
total_rmg = remove_gif()
total_dzo = delete_zones()
total_rmi = remove_unused_images()
total_rmd = remove_unused_directories()
total_syn = sync_loss_dirs()
timeEnd = datetime.now()
timeTotal = timeEnd - timeStart
timeTotalMinSec = divmod(timeTotal.days * 86400 + timeTotal.seconds, 60)

print(
    'total_dis: '+str(total_dis)+
    ',total_dit: '+str(total_dit)+
    ',total_dde: '+str(total_dde)+
    ',total_gif: '+str(total_gif)+
    ',total_rmg: '+str(total_rmg)+
    ',total_dzo: '+str(total_dzo)+
    ',total_loa: '+str(total_loa)+
    ',total_rmi: '+str(total_rmi)+
    ',total_rmd: '+str(total_rmd)+
    ',total_syn: '+str(total_syn)
    )
print ("%d minutes and %d seconds" % (timeTotalMinSec[0], timeTotalMinSec[1]))

generateCandidates();

if total_loa > 0:
    print("Enviar email: "+str(total_loa)+" nuevos quintetos")
    response = requests.get(my_api_url+'admin/users/notifyNewQuintets?total='+str(total_loa))

client.close()
