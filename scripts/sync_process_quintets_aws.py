import os
import pymongo
import shutil
import requests
import subprocess
import gzip
import time
import inspect
import glob

from xml.etree import ElementTree
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import datetime, timedelta
'''
try:
    import pwd
except ImportError:
    try:
        import winpwd as pwd
    except ImportError as err:
        print("ImportErroras error: {0}".format(err))
    
try:
    uid = pwd.getpwnam('root')[2]
    os.setuid(uid)
except Exception as err:
    print("Exception error: {0}".format(err))
'''
#Eliminar los directorios de las imagenes que ya no estan en la base de datos
#Descargar las imagenes que estan en la base de datos pero no existe su directorio

#Configuration

exclude_dir_names_on_data = ['dump']

#my_mongo_client = 'mongodb://web:web@ds143980.mlab.com:43980/cazasteroides' #Test database
my_mongo_client = 'mongodb://web:web@ds129641.mlab.com:29641/cazasteroides' # Nueva Base de Datos Propiedad de AstroAula
my_mongo_db = 'cazasteroides';

my_src_data = os.path.normpath(os.path.join((os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))),'../data/'))
my_src_gif = os.path.normpath(os.path.join((os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))),'../server/cazasteroides/gif/'))
my_mapfile_map = os.path.join(my_src_data,'mapfile.map')
my_tv_key = '40680253ab417e043ee14810f70b990008a9c01a' #IAC
#my_tv_key = '85ab4f72b2c8e71988d47d1fabfb0f163c8b5f76' #UPM
#my_tv_key = 'f83815230806f44f6a4fe2e6d7ec311412e1ed61' #AWS
my_tv_url = 'http://www.telescopiovirtual.com:80/services/remote/'+my_tv_key

#print('Connecting...')
client = MongoClient(my_mongo_client)

#print('Open database')
db = client[my_mongo_db]

def get_immediate_subdirectories(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name)) and name not in exclude_dir_names_on_data]

def exist_gif(det_id):
    return len([name for name in os.listdir(my_src_gif)
            if os.path.isfile(os.path.join(my_src_gif, name)) and name.startswith(str(det_id))])>0

def remove_readonly(func, path, excinfo):
    #os.chmod(path, stat.S_IWRITE)
    #func(path)
    if os.path.exists(path):
        print(str(path)+' '+str(excinfo))

def generate_findimages_uri(name):
    date = name[0:4]+'-'+name[4:6]+'-'+name[6:8]
    telescopio = name[14:]
    url = my_tv_url+'/findimages/headers/'+date+'/'+telescopio+'/asteroid('+name+')'
    #print(url)
    return url

def generate_findimages_uri_end_numeric(name):
    date = name[0:4]+'-'+name[4:6]+'-'+name[6:8]
    telescopio = name[14:len(name)-2]
    url = my_tv_url+'/findimages/headers/'+date+'/'+telescopio+'/asteroid('+name+')'
    #print(url)
    return url

def unzip_file(fileGz,fileFit):
    if os.path.isfile(fileGz):
        inF = gzip.open(fileGz, 'rb')            
        outF = open(fileFit, 'wb')
        outF.write( inF.read() )
        inF.close()
        outF.close()

        os.remove(fileGz)

def download_file(url,fileGz):
    if not os.path.isfile(fileGz):
        #print("Download: "+url)
        #print("File: "+fileGz)
        response = requests.get(url, stream=True)
        handle = open(fileGz, "wb")
        for chunk in response.iter_content(chunk_size=512):
            if chunk:  # filter out keep-alive new chunks
                handle.write(chunk)

def download_and_unzip(url,fileGz,fileFit,forceDownload):
    if not forceDownload and os.path.isfile(fileFit):
        return
    
    download_file(url,fileGz)
    unzip_file(fileGz,fileFit)
        
def generate_mapfile(name,fitImages):
    dir = os.path.join(my_src_data,name)

    isNewQuintet = True
    
    fileMap = os.path.join(dir,'mapfile.map')
    if os.path.isfile(fileMap):
        #os.remove(fileMap)
        isNewQuintet = False

    with open(my_mapfile_map, "rt") as fin:
        with open(fileMap, "wt") as fout:
            for line in fin:
                line = line.replace('AAAA1', fitImages[0])
                line = line.replace('AAAA2', fitImages[1])
                line = line.replace('AAAA3', fitImages[2])
                line = line.replace('AAAA4', fitImages[3])
                line = line.replace('AAAA5', fitImages[4])
                fout.write(line)

            fout.close()
        fin.close()

    return isNewQuintet;       

def remove_unused_images():
    count = 0
    #Obtenemos todas las imagenes que no estan activas y las guardamos en una lista
    resultImgs = db.images.find({"active":{"$eq":False}},{"_id":1,"name":1})
    #Eliminamos las imagenes del servidor siempre que no existan detecciones para ese quinteto
    for el in resultImgs:

        obs = db.observations.find_one({'image':ObjectId(el['_id'])})
        if obs is not None:
            continue
        
        path = os.path.join(my_src_data,str(el['name']))
        if os.path.exists(path):
            shutil.rmtree(path, onerror=remove_readonly)
            count += 1
            
    return count     

    
def remove_unused_directories():
    count = 0
    directories = get_immediate_subdirectories(my_src_data)

    for name in directories:
        image = db.images.find_one({'name':name})
        if image is None:
            shutil.rmtree(os.path.join(my_src_data,name), onerror=remove_readonly)
            count += 1
    return count

            
def sync_loss_dirs():
    count = 0
    directories = get_immediate_subdirectories(my_src_data)

    resultImgs = db.images.find({},{"_id":1,"name":1,"active":1})
    lstImageName = []
    for result in resultImgs:
        dirImage = os.path.join(my_src_data,result['name'])
        if result['name'] not in directories or os.listdir(dirImage) == [] or not any(File.endswith(".map") for File in os.listdir(dirImage)) or len(glob.glob1(dirImage,"*.fits")) < 5:
            if result['active']:
                lstImageName.append(result['name'])
                #print('Added Active: '+result['name'])
            else:
                obs = db.observations.find_one({'image':ObjectId(result['_id'])})
                if obs is not None:
                    lstImageName.append(result['name'])
                    #print('Added With Detection: '+result['name'])
                                  
    for name in lstImageName:  
        #Download Header
        response = requests.get(generate_findimages_uri(name))
        tree = ElementTree.fromstring(response.content)

        if not tree:
            response = requests.get(generate_findimages_uri_end_numeric(name))
            tree = ElementTree.fromstring(response.content)
        
        fitImages = []
        dir = os.path.join(my_src_data,name)
        for publicImage in tree.findall('public_image'):
            
            if not os.path.exists(dir):
                os.makedirs(dir)
                print('Create Dir: '+dir)

            idImagen = publicImage.find('idimagen').text
            
            fileGz = os.path.join(dir,idImagen + '.fits.gz')
            fileFits = os.path.join(dir,idImagen + '.fits')
            
            download_and_unzip(publicImage.find('fitsgz').text,fileGz,fileFits,True)         

            fitImages.append(fileFits)

        if not os.path.exists(dir):
            continue

        # Create mapfile.map
        if len(fitImages) == 5:
            generate_mapfile(name,fitImages)
            count += 1
    return count

def remove_gif():
    count = 0
    
    for file in os.listdir(my_src_gif):
        olderThanDays = 15*60*60 # convert days to seconds
        present = time.time()
        if file.endswith(".gif") and (present - os.path.getmtime(os.path.join(my_src_gif, file))) > olderThanDays:
            id = ''
            if file.find("_") > 0:
                id = file[:file.find("_")]
            elif file.find(".gif") > 0:
                id = file[:file.find(".gif")]

            if db.observations.find_one({'_id':ObjectId(id)}) is None:
                os.remove(os.path.join(my_src_gif, file))
                count += 1

    return count


if not os.path.exists(my_src_data):
    print ("No existe la ruta: "+os.path.abspath(my_src_data))
    exit()


if not os.path.exists(my_src_gif):
    print ("No existe la ruta: "+os.path.abspath(my_src_gif))
    exit()


timeStart = datetime.now()
total_rmg = remove_gif()
total_rmi = remove_unused_images()
total_rmd = remove_unused_directories()
total_syn = sync_loss_dirs()
timeEnd = datetime.now()
timeTotal = timeEnd - timeStart
timeTotalMinSec = divmod(timeTotal.days * 86400 + timeTotal.seconds, 60)

print(
    'total_rmg: '+str(total_rmg)+
    ',total_rmi: '+str(total_rmi)+
    ',total_rmd: '+str(total_rmd)+
    ',total_syn: '+str(total_syn)
    )
print ("%d minutes and %d seconds" % (timeTotalMinSec[0], timeTotalMinSec[1]))

client.close()
