(function () {
    'use strict';

    angular
            .module('app')
            .controller('ApprovedController', ApprovedController);

    ApprovedController.$inject = ['$rootScope', 'CandidateServerService', '$scope', 'domain_img'];
    function ApprovedController($rootScope, CandidateServerService, $scope, domain_img) {
        //var domain = 'http://api.cazasteroides.org';
        var vm = this;

        vm.user = $rootScope.globals.currentUser;
        vm.known = [];
        vm.unknown = [];
        vm.unknownIncomplete = [];

        $scope.selected = 'unknown';

        $scope.isSelected = function (tabName) {
            return $scope.selected === tabName;
        };

        $scope.openTab = function (tabName) {
            //console.log('tabName: ' + tabName);
            $scope.selected = tabName;

            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";

            switch (tabName) {
                case 'unknown':
                    loadUnknown();
                    generateUnknownHtml();
                    break;

                case 'known':
                    loadKnon();
                    generateKnownHtml();
                    break;

                case 'unknownIncomplete':
                    loadUnknownIncomplete();
                    generateUnknownIncompleteHtml();
                    break;
            }
        };

        initController();

        function initController() {
            loadKnon();
            loadUnknown();
            loadUnknownIncomplete();
        }
        function loadKnon() {
            CandidateServerService.GetKnown()
                    .then(function (response) {
                        if (!response.error) {
                            vm.known = response;
                            generateKnownHtml();
                        }
                    });
        }
        function loadUnknown() {
            CandidateServerService.GetUnknown()
                    .then(function (response) {
                        if (!response.error) {
                            vm.unknown = response;
                            generateUnknownHtml();
                        }
                    });
        }
        function loadUnknownIncomplete() {
            CandidateServerService.GetUnknownIncomplete()
                    .then(function (response) {
                        if (!response.error) {
                            vm.unknownIncomplete = response;
                            generateUnknownIncompleteHtml();
                        }
                    });
        }



        function getMPCstate(state) {
            switch (state) {
                case - 1:
                    return "Not reported to MPC.";
                case 0:
                    return "Waiting for the response from the MPC.";
                case 1:
                    return "MPC responded correctly";
                case 2:
                    return "MPC responded error";
            }
        }

        function generateUnknownHtml() {

            //console.log("generateUnknownHtml");
            $("#unknown_total").text("Total Candidates: " + vm.unknown.length);
            $("#unknown_list").html('');


            for (var i = 0; i < vm.unknown.length; i++) {
                var candidate = vm.unknown[i];

                var totalLines = 0;
                var lines = '';
                for (var l = 0; l < candidate.coordenates.length; l++) {
                    if (candidate.coordenates[l].line) {
                        lines += candidate.coordenates[l].line + "<br>";
                        totalLines++;
                    }
                }

                var style = 'height: auto ;width: auto;';
                var htmlImagesPNG = '';
                var htmlImageGif = undefined;

                for (var l = candidate.coordenates.length - 1; l >= 0; l--) {
                    var q = candidate.coordenates[l].quintetn;
                    //var imageUrl = domain + "/obs/get_obs_png/" + candidate.coordenates[l].gif + "/" + q;
                    var coord = candidate.coordenates[l].coord;
                    var ra = coord[0];
                    var dec = coord[1];
                    var increase = ' ' + " ~[" + (ra).toFixed(5) + "," + (dec).toFixed(5) + "]";

                    htmlImagesPNG +=
                            '<div id="img_' + candidate._id + "_" + q + '_container" style="vertical-align: top; display: inline-block; text-align: center; width: auto; margin: 10px 5px 0 0;">'
                            //+ '  <img id="img_' + candidate._id + "_" + q + '" class="thumb" src="' + imageUrl + '" style="' + style + '" />'
                            + '  <canvas id="canvas_' + candidate._id + "_" + q + '" style="' + style + '" />'
                            + '  <span id="img_' + candidate._id + "_" + q + '_text" style="display: block">Quintet: ' + q + increase + '</span>'
                            + '</div>';

                    if (!htmlImageGif && l === Math.floor(candidate.coordenates.length / 2)) {
                        var imageGifUrl = domain_img + "/obs/get_obs_gif/" + candidate.coordenates[l].gif;
                        htmlImageGif = '<div style="vertical-align: top; display: inline-block; text-align: center; width: auto; margin: 10px 5px 0 0;">'
                                + '  <img class="thumb" src="' + imageGifUrl + '" style="' + style + '" />'
                                + '  <span style="display: block">Gif</span>'
                                + '</div>';
                    }

                }

                var htmlImages = '<div class="row"><div class="col-sm-12">'
                        + htmlImageGif
                        + htmlImagesPNG
                        + '</div>'
                        + '     <div style="margin: 20px;" id="update_container_' + candidate._id + '"><a id="update_' + candidate._id + '" class="btn btn-primary" role="button" >Update Coordenates</a></div>'
                        + '</div>';

                candidate.lines = lines;
                candidate.images = htmlImages;

                var html =
                        '<div class="row" id="row_' + candidate._id + '">'
                        + ' <div class="thumbnail" style="padding:10px">'
                        + '     <div class="media">'
                        + '             <h4 class="media-heading">CazastDesignation: ' + candidate.designation + '</h4>'
                        + '             <h5 >MPC tempDesignation: C' + candidate.tmpDesignation + '</h5>'
                        + '             <div >'
                        + '                 <table>'
                        + '                     <tr>'
                        + '                         <td class="shrink" id="status_' + candidate._id + '" >Current MPC Status: ' + getMPCstate(candidate.mpcReportState) + '</td>';
                if (candidate.mpcReportState < 0) {
                    html += '                     <td class="expand" id="report_container_' + candidate._id + '"><a id="report_' + candidate._id + '" class="btn btn-primary" role="button" >Report to MPC</a></td>';
                }
                html += '                      </tr>'
                        + '                 </table>'
                        + '         </div>'
                        + '             <div >'
                        + '                 <table>'
                        + '                     <tr>'
                        + '                         <td class="shrink" >Total Users: ' + (candidate.users.length) + '</td>'
                        + '                     </tr>'
                        + '                 </table>'
                        + '         </div>'
                        + '      <div ><a id="candidate_lines_button_' + candidate._id + '" >Coordenates (' + totalLines + ') <i id="candidate_lines_icon_' + candidate._id + '" class="fa fa-caret-down" aria-hidden="true"></i></a></div>'
                        + '      <div style=" white-space: pre;" id="candidate_lines_' + candidate._id + '"></div>'
                        + '         <div id="candidate_images_' + candidate._id + '" ></div>'
                        + '     </div>'
                        + '     </div>'
                        + '     </div>'
                        + '</div>';

                $("#unknown_list").append(html);


                $("#candidate_lines_button_" + candidate._id).click((function () {
                    var mCandidate = candidate;
                    var mIndex = i;
                    var isOpen = false;
                    return function () {
                        if (isOpen) {
                            $("#candidate_lines_" + mCandidate._id).html('');
                            $("#candidate_images_" + mCandidate._id).html('');
                            $("#candidate_lines_icon_" + mCandidate._id).removeClass('fa-caret-up').addClass('fa-caret-down');
                        } else {
                            $("#candidate_lines_" + mCandidate._id).html(mCandidate.lines);
                            $("#candidate_images_" + mCandidate._id).html(mCandidate.images);
                            $("#candidate_lines_icon_" + mCandidate._id).removeClass('fa-caret-down').addClass('fa-caret-up');


                            for (var l = mCandidate.coordenates.length - 1; l >= 0; l--) {

                                var canvasId = 'canvas_' + mCandidate._id + "_" + mCandidate.coordenates[l].quintetn;
                                var tmpCanvas = document.getElementById(canvasId);
                                var tmpCtx = tmpCanvas.getContext("2d");

                                var tmpImg = new Image();
                                tmpImg.onload = function () {
                                    var canvas = tmpCanvas;
                                    var ctx = tmpCtx;
                                    var img = tmpImg;

                                    var tmpCoordPosition = l;
                                    var tmpCandidatePosition = mIndex;
                                    var tmpQuintetn = mCandidate.coordenates[l].quintetn;
                                    var tmpCandidate = mCandidate;
                                    var increase = mCandidate.coordenates[tmpCoordPosition].increase;

                                    return function () {
                                        var radius = 3;
                                        var stokeColor = '#FE0202';
                                        var centerX = img.width / 2 + increase[0];
                                        var centerY = img.height / 2 + increase[1];

                                        canvas.width = img.width;
                                        canvas.height = img.height;
                                        ctx.drawImage(img, 0, 0);

                                        ctx.beginPath();
                                        ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
                                        ctx.lineWidth = 1;
                                        ctx.strokeStyle = stokeColor;
                                        ctx.stroke();

                                        //console.log("Center [" + centerX + "," + centerY + "]");

                                        $('#canvas_' + tmpCandidate._id + "_" + tmpQuintetn).click(function (e) {


                                            if (!$("#update_" + tmpCandidate._id) || !$("#update_" + tmpCandidate._id).length) {
                                                return;
                                            }


                                            var offset = $(this).offset();
                                            var x = (Math.ceil(e.pageX - offset.left) - $(this).width() / 2) - increase[0];
                                            var y = (Math.ceil(e.pageY - offset.top) - $(this).height() / 2) - increase[1];

                                            var coord = tmpCandidate.coordenates[tmpCoordPosition].coord;

                                            //console.log('#img_' + i + '_' + tmpCandidate._id + '_' + tmpQuintetn + " [" + increase[0] + "," + increase[1] + "]");

                                            var meta = tmpCandidate.coordenates[tmpCoordPosition].quintet.meta;

                                            var scale = meta ? parseFloat(meta.SCALE) : 1;

                                            var dec = coord[1] - (y * scale) / 3600;
                                            var ra = coord[0] - ((x * scale) / 3600) / Math.cos(dec * Math.PI / 180);

                                            vm.unknown[tmpCandidatePosition].coordenates[tmpCoordPosition].newIncrease = [x, y];
                                            $('#img_' + tmpCandidate._id + "_" + tmpQuintetn + "_text").text('Quintet: ' + tmpQuintetn + ' ' + " ~[" + (ra).toFixed(5) + "," + (dec).toFixed(5) + "]");


                                            var mouseX = Math.ceil(e.pageX - offset.left);
                                            var mouseY = Math.ceil(e.pageY - offset.top);
                                            //clean
                                            ctx.clearRect(0, 0, canvas.width, canvas.height);
                                            ctx.drawImage(img, 0, 0);
                                            //draw circle on initial position
                                            ctx.beginPath();
                                            ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
                                            ctx.lineWidth = 1;
                                            ctx.strokeStyle = stokeColor;
                                            ctx.stroke();
                                            //draw lines
                                            ctx.beginPath();
                                            ctx.moveTo(mouseX, 0);
                                            ctx.lineTo(mouseX, canvas.height);
                                            ctx.moveTo(0, mouseY);
                                            ctx.lineTo(canvas.width, mouseY);
                                            ctx.strokeStyle = '#000000';
                                            ctx.stroke();
                                        });
                                    };
                                }();

                                var q = mCandidate.coordenates[l].quintetn;
                                var imageUrl = domain_img + "/obs/get_obs_png/" + mCandidate.coordenates[l].gif + "/" + q;
                                tmpImg.src = imageUrl;

                            }

                            $("#update_" + mCandidate._id).click((function () {
                                var tmpCandidatePosition = mIndex;
                                return function () {

                                    var mCandidateUpdate = vm.unknown[tmpCandidatePosition];

                                    //console.log("update Server Side");
                                    $("#update_" + mCandidateUpdate._id).hide();
                                    CandidateServerService.Update(mCandidateUpdate).then(function (response) {
                                        //console.log(JSON.stringify(response));
                                        if (!response.error) {
                                            //$("#row_" + mCandidate._id).remove();
                                            loadUnknown();
                                        } else {
                                            $("#update_" + mCandidateUpdate._id).show();
                                        }
                                    });

                                };
                            })());


                        }
                        isOpen = !isOpen;
                    };
                })());

//                for (var l = candidate.coordenates.length - 1; l >= 0; l--) {
//
//                    var canvasId = 'canvas_' + candidate._id + "_" + candidate.coordenates[l].quintetn;
//                    var tmpCanvas = document.getElementById(canvasId);
//                    var tmpCtx = tmpCanvas.getContext("2d");
//
//                    var tmpImg = new Image();
//                    tmpImg.onload = function () {
//                        var canvas = tmpCanvas;
//                        var ctx = tmpCtx;
//                        var img = tmpImg;
//
//                        var tmpCoordPosition = l;
//                        var tmpCandidatePosition = i;
//                        var tmpQuintetn = candidate.coordenates[l].quintetn;
//                        var tmpCandidate = candidate;
//                        var increase = candidate.coordenates[tmpCoordPosition].increase;
//
//                        return function () {
//                            var radius = 3;
//                            var stokeColor = '#FE0202';
//                            var centerX = img.width / 2 + increase[0];
//                            var centerY = img.height / 2 + increase[1];
//
//                            canvas.width = img.width;
//                            canvas.height = img.height;
//                            ctx.drawImage(img, 0, 0);
//
//                            ctx.beginPath();
//                            ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
//                            ctx.lineWidth = 1;
//                            ctx.strokeStyle = stokeColor;
//                            ctx.stroke();
//
//                            //console.log("Center [" + centerX + "," + centerY + "]");
//
//                            $('#canvas_' + tmpCandidate._id + "_" + tmpQuintetn).click(function (e) {
//
//
//                                if (!$("#update_" + tmpCandidate._id) || !$("#update_" + tmpCandidate._id).length) {
//                                    return;
//                                }
//
//
//                                var offset = $(this).offset();
//                                var x = (Math.ceil(e.pageX - offset.left) - $(this).width() / 2) - increase[0];
//                                var y = (Math.ceil(e.pageY - offset.top) - $(this).height() / 2) - increase[1];
//
//                                var coord = tmpCandidate.coordenates[tmpCoordPosition].coord;
//
//                                //console.log('#img_' + i + '_' + tmpCandidate._id + '_' + tmpQuintetn + " [" + increase[0] + "," + increase[1] + "]");
//
//                                var meta = tmpCandidate.coordenates[tmpCoordPosition].quintet.meta;
//
//                                var scale = meta ? parseFloat(meta.SCALE) : 1;
//
//                                var dec = coord[1] - (y * scale) / 3600;
//                                var ra = coord[0] - ((x * scale) / 3600) / Math.cos(dec * Math.PI / 180);
//
//                                vm.unknown[tmpCandidatePosition].coordenates[tmpCoordPosition].newIncrease = [x, y];
//                                $('#img_' + tmpCandidate._id + "_" + tmpQuintetn + "_text").text('Quintet: ' + tmpQuintetn + ' ' + " ~[" + (ra).toFixed(5) + "," + (dec).toFixed(5) + "]");
//
//
//                                var mouseX = Math.ceil(e.pageX - offset.left);
//                                var mouseY = Math.ceil(e.pageY - offset.top);
//                                //clean
//                                ctx.clearRect(0, 0, canvas.width, canvas.height);
//                                ctx.drawImage(img, 0, 0);
//                                //draw circle on initial position
//                                ctx.beginPath();
//                                ctx.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
//                                ctx.lineWidth = 1;
//                                ctx.strokeStyle = stokeColor;
//                                ctx.stroke();
//                                //draw lines
//                                ctx.beginPath();
//                                ctx.moveTo(mouseX, 0);
//                                ctx.lineTo(mouseX, canvas.height);
//                                ctx.moveTo(0, mouseY);
//                                ctx.lineTo(canvas.width, mouseY);
//                                ctx.strokeStyle = '#000000';
//                                ctx.stroke();
//                            });
//                        };
//                    }();
//
//                    var q = candidate.coordenates[l].quintetn;
//                    var imageUrl = domain_img + "/obs/get_obs_png/" + candidate.coordenates[l].gif + "/" + q;
//                    tmpImg.src = imageUrl;
//
//                }
//
//                $("#update_" + candidate._id).click((function () {
//                    var tmpCandidatePosition = i;
//                    return function () {
//
//                        var mCandidate = vm.unknown[tmpCandidatePosition];
//
//                        //console.log("update Server Side");
//                        $("#update_" + mCandidate._id).hide();
//                        CandidateServerService.Update(mCandidate).then(function (response) {
//                            //console.log(JSON.stringify(response));
//                            if (!response.error) {
//                                //$("#row_" + mCandidate._id).remove();
//                                loadUnknown();
//                            } else {
//                                $("#update_" + mCandidate._id).show();
//                            }
//                        });
//
//                    };
//                })());

                $("#report_" + candidate._id).click((function () {
                    //persist variable on closure
                    var mCandidate = candidate;
                    return function () {

                        CandidateServerService.Report(mCandidate._id).then(function (data) {
                            //console.log(JSON.stringify(data));
                            if (data) {
                                $('#status_' + mCandidate._id).html('Current MPC Status: ' + getMPCstate(data.reportState));
                                mCandidate.mpcReportState = data.reportState;

                                if (data.reportState >= 0) {
                                    $('#report_container_' + mCandidate._id).remove();
                                }
                            }
                        });

                    };
                })());
            }
        }

        function generateUnknownIncompleteHtml() {

            //console.log("generateUnknownHtml");
            var totalNotComplete = vm.unknownIncomplete.length;
            $("#unknownIncomplete_total").text("Total Candidates: " + totalNotComplete);
            $("#unknownIncomplete_list").html('');


            for (var i = 0; i < vm.unknownIncomplete.length; i++) {
                var candidate = vm.unknownIncomplete[i];

                var totalLines = 0;
                var lines = '';
                for (var l = 0; l < candidate.coordenates.length; l++) {
                    if (candidate.coordenates[l].line) {
                        lines += candidate.coordenates[l].line + "<br>";
                        totalLines++;
                    }
                }

                var style = 'height: auto ;width: auto;';
                var htmlImagesPNG = '';
                var htmlImageGif = undefined;

                for (var l = candidate.coordenates.length - 1; l >= 0; l--) {
                    if (!candidate.coordenates[l].line) {
                        var q = candidate.coordenates[l].quintetn;
                        var imageUrl = domain_img + "/obs/get_obs_png/" + candidate.coordenates[l].gif + "/" + q;
                        lines += "No coordenates for the quintet " + q + "<br>";

                        var increase = ''
                        if (candidate.coordenates[l].increase[0] != 0 || candidate.coordenates[l].increase[1] != 0) {
                            var meta = candidate.coordenates[l].quintet.meta;
                            var scale = meta ? parseFloat(meta.SCALE) : 1;
                            var coord = candidate.coordenates[l].coord;

                            var dec = coord[1] - (candidate.coordenates[l].increase[1] * scale) / 3600;
                            var ra = coord[0] - ((candidate.coordenates[l].increase[1] * scale) / 3600) / Math.cos(dec * Math.PI / 180);
                            increase = ' ' + " ~[" + (ra).toFixed(5) + "," + (dec).toFixed(5) + "]";
                        }
                        htmlImagesPNG +=
                                '<div id="img_' + candidate._id + "_" + q + '_container" style="vertical-align: top; display: inline-block; text-align: center; width: auto; margin: 10px 5px 0 0;">'
                                + '  <img id="img_' + candidate._id + "_" + q + '" class="thumb" src="' + imageUrl + '" style="' + style + '" />'
                                + '  <span id="img_' + candidate._id + "_" + q + '_text" style="display: block">Quintet: ' + q + increase + '</span>'
                                + '</div>';


                        if (!htmlImageGif) {
                            var imageGifUrl = domain_img + "/obs/get_obs_gif/" + candidate.coordenates[l].gif;
                            htmlImageGif = '<div style="vertical-align: top; display: inline-block; text-align: center; width: auto; margin: 10px 5px 0 0;">'
                                    + '  <img class="thumb" src="' + imageGifUrl + '" style="' + style + '" />'
                                    + '  <span style="display: block">Gif</span>'
                                    + '</div>';
                        }
                    }
                }

                var htmlImages = '<div class="row"><div class="col-sm-12">'
                        + htmlImageGif
                        + htmlImagesPNG
                        + '</div>'
                        + '     <div style="margin: 20px;" id="update_container_' + candidate._id + '"><a id="update_' + candidate._id + '" class="btn btn-primary" role="button" >Set Coordenates</a></div>'
                        + '</div>';

                candidate.lines = lines;
                candidate.images = htmlImages;

                var html = '<div class="row" id="row_' + candidate._id + '">'
                        + ' <div class="thumbnail" style="padding:10px">'
                        + '     <div class="media">'
                        + '             <h4 class="media-heading">CazastDesignation: ' + candidate.designation + '</h4>'
                        //+ '             <h5 >MPC tempDesignation: C' + candidate.tmpDesignation + '</h5>'
                        + '             <div >'
                        + '                 <table>'
                        + '                     <tr>'
                        + '                         <td class="shrink" id="status_' + candidate._id + '" >Current MPC Status: ' + getMPCstate(candidate.mpcReportState) + '</td>';
//                if (candidate.mpcReportState < 0) {
//                    html += '                     <td class="expand" id="report_container_' + candidate._id + '"><a id="report_' + candidate._id + '" class="btn btn-primary" role="button" >Report to MPC</a></td>';
//                }
                html += '                      </tr>'
                        + '                 </table>'
                        + '         </div>'
                        + '             <div >'
                        + '                 <table>'
                        + '                     <tr>'
                        + '                         <td class="shrink" >Total Users: ' + (candidate.users.length) + '</td>'
                        + '                     </tr>'
                        + '                 </table>'
                        + '         </div>'
                        + '      <div ><a id="candidate_lines_button_' + candidate._id + '" >Coordenates (' + totalLines + ') <i id="candidate_lines_icon_' + candidate._id + '" class="fa fa-caret-down" aria-hidden="true"></i></a></div>'
                        + '      <div style=" white-space: pre;" id="candidate_lines_' + candidate._id + '"></div>'
                        + '<div id="candidate_images_' + candidate._id + '" ></div>'
                        + '     </div>'
                        + '     </div>'
                        + '     </div>'
                        + '</div>';

                $("#unknownIncomplete_list").append(html);

                $("#candidate_lines_button_" + candidate._id).click((function () {
                    var mCandidate = candidate;
                    var mIndex = i;
                    var isOpen = false;
                    return function () {
                        if (isOpen) {
                            $("#candidate_lines_" + mCandidate._id).html('');
                            $("#candidate_images_" + mCandidate._id).html('');
                            $("#candidate_lines_icon_" + mCandidate._id).removeClass('fa-caret-up').addClass('fa-caret-down');
                        } else {
                            $("#candidate_lines_" + mCandidate._id).html(mCandidate.lines);
                            $("#candidate_images_" + mCandidate._id).html(mCandidate.images);
                            $("#candidate_lines_icon_" + mCandidate._id).removeClass('fa-caret-down').addClass('fa-caret-up');


                            for (var l = mCandidate.coordenates.length - 1; l >= 0; l--) {
                                if (!mCandidate.coordenates[l].line) {
                                    $('#img_' + mCandidate._id + "_" + mCandidate.coordenates[l].quintetn).click(function () {
                                        var tmpCoordPosition = l;
                                        var tmpCandidatePosition = mIndex;
                                        var tmpQuintetn = mCandidate.coordenates[l].quintetn;
                                        var tmpCandidate = mCandidate;

                                        return function (e) {

                                            var offset = $(this).offset();
                                            var x = Math.ceil(e.pageX - offset.left) - $(this).width() / 2;
                                            var y = Math.ceil(e.pageY - offset.top) - $(this).height() / 2;

                                            var coord = tmpCandidate.coordenates[tmpCoordPosition].coord;

                                            //console.log('#img_' + i + '_' + tmpCandidate._id + '_' + tmpQuintetn + " [" + x + "," + y + "]");

                                            var meta = tmpCandidate.coordenates[tmpCoordPosition].quintet.meta;

                                            var scale = meta ? parseFloat(meta.SCALE) : 1;

                                            var dec = coord[1] - (y * scale) / 3600;
                                            var ra = coord[0] - ((x * scale) / 3600) / Math.cos(dec * Math.PI / 180);

                                            vm.unknownIncomplete[tmpCandidatePosition].coordenates[tmpCoordPosition].newIncrease = [x, y];
                                            $('#img_' + tmpCandidate._id + "_" + tmpQuintetn + "_text").text('Quintet: ' + tmpQuintetn + ' ' + " ~[" + (ra).toFixed(5) + "," + (dec).toFixed(5) + "]");
                                        };
                                    }());
                                }
                            }

                            $("#update_" + mCandidate._id).click((function () {
                                var tmpCandidatePosition = mIndex;
                                return function () {

                                    var mCandidateUpdate = vm.unknownIncomplete[tmpCandidatePosition];
                                    var mCoordenates = mCandidateUpdate.coordenates;

                                    var canUpdate = true;

                                    for (var j = 0; j < mCoordenates.length; j++) {
                                        if (!mCoordenates[j].line) {
                                            //console.log("[" + mCoordenates[j].increase[0] + "," + mCoordenates[j].increase[1] + "]");
                                            if (mCoordenates[j].newIncrease[0] == 0 && mCoordenates[j].newIncrease[1] == 0) {
                                                canUpdate = false;
                                                $('#img_' + mCandidateUpdate._id + "_" + mCoordenates[j].quintetn + '_container').css('background-color', '').css('background-color', 'red');
                                            } else {
                                                $('#img_' + mCandidateUpdate._id + "_" + mCoordenates[j].quintetn + '_container').css('background-color', '').css('background-color', 'white');
                                            }
                                        }
                                    }

                                    if (canUpdate) {
                                        //console.log("update Server Side");
                                        $("#update_" + mCandidateUpdate._id).hide();
                                        CandidateServerService.Update(mCandidateUpdate).then(function (response) {
                                            //console.log(JSON.stringify(response));
                                            if (!response.error) {
                                                $("#row_" + mCandidateUpdate._id).remove();
                                                totalNotComplete--;
                                                $("#unknownIncomplete_total").text("Total Candidates: " + totalNotComplete);
                                            } else {
                                                $("#update_" + mCandidateUpdate._id).show();
                                            }
                                        });
                                    }
                                };
                            })());


                        }
                        isOpen = !isOpen;
                    };
                })());

//                for (var l = candidate.coordenates.length - 1; l >= 0; l--) {
//                    if (!candidate.coordenates[l].line) {
//                        $('#img_' + candidate._id + "_" + candidate.coordenates[l].quintetn).click(function () {
//                            var tmpCoordPosition = l;
//                            var tmpCandidatePosition = i;
//                            var tmpQuintetn = candidate.coordenates[l].quintetn;
//                            var tmpCandidate = candidate;
//
//                            return function (e) {
//
//                                var offset = $(this).offset();
//                                var x = Math.ceil(e.pageX - offset.left) - $(this).width() / 2;
//                                var y = Math.ceil(e.pageY - offset.top) - $(this).height() / 2;
//
//                                var coord = tmpCandidate.coordenates[tmpCoordPosition].coord;
//
//                                //console.log('#img_' + i + '_' + tmpCandidate._id + '_' + tmpQuintetn + " [" + x + "," + y + "]");
//
//                                var meta = tmpCandidate.coordenates[tmpCoordPosition].quintet.meta;
//
//                                var scale = meta ? parseFloat(meta.SCALE) : 1;
//
//                                var dec = coord[1] - (y * scale) / 3600;
//                                var ra = coord[0] - ((x * scale) / 3600) / Math.cos(dec * Math.PI / 180);
//
//                                vm.unknownIncomplete[tmpCandidatePosition].coordenates[tmpCoordPosition].newIncrease = [x, y];
//                                $('#img_' + tmpCandidate._id + "_" + tmpQuintetn + "_text").text('Quintet: ' + tmpQuintetn + ' ' + " ~[" + (ra).toFixed(5) + "," + (dec).toFixed(5) + "]");
//                            };
//                        }());
//                    }
//                }
//
//                $("#update_" + candidate._id).click((function () {
//                    var tmpCandidatePosition = i;
//                    return function () {
//
//                        var mCandidate = vm.unknownIncomplete[tmpCandidatePosition];
//                        var mCoordenates = mCandidate.coordenates;
//
//                        var canUpdate = true;
//
//                        for (var j = 0; j < mCoordenates.length; j++) {
//                            if (!mCoordenates[j].line) {
//                                //console.log("[" + mCoordenates[j].increase[0] + "," + mCoordenates[j].increase[1] + "]");
//                                if (mCoordenates[j].newIncrease[0] == 0 && mCoordenates[j].newIncrease[1] == 0) {
//                                    canUpdate = false;
//                                    $('#img_' + mCandidate._id + "_" + mCoordenates[j].quintetn + '_container').css('background-color', '').css('background-color', 'red');
//                                } else {
//                                    $('#img_' + mCandidate._id + "_" + mCoordenates[j].quintetn + '_container').css('background-color', '').css('background-color', 'white');
//                                }
//                            }
//                        }
//
//                        if (canUpdate) {
//                            //console.log("update Server Side");
//                            $("#update_" + mCandidate._id).hide();
//                            CandidateServerService.Update(mCandidate).then(function (response) {
//                                //console.log(JSON.stringify(response));
//                                if (!response.error) {
//                                    $("#row_" + mCandidate._id).remove();
//                                } else {
//                                    $("#update_" + mCandidate._id).show();
//                                }
//                            });
//                        }
//                    };
//                })());

            }
        }
        function generateKnownHtml() {             //console.log("generateKnownHtml");
            $("#known_total").text("Total Known: " + vm.known.length);
            $("#known_list").html('');

            for (var i = 0; i < vm.known.length; i++) {
                var candidate = vm.known[i];

                var lines = '';
                for (var l = 0; l < candidate.coordenates.length; l++) {
                    lines += candidate.coordenates[l].line + "<br>";
                }

                var html = '<div class="row" id="row_' + candidate._id + '">'
                        + ' <div class="thumbnail" style="padding:10px">'
                        + '     <div class="media">'
                        + '             <h4 class="media-heading">MPC Designation: <a target="_blank" href="https://ssd.jpl.nasa.gov/sbdb.cgi?name=' + candidate.designation + '"><font color="4137A4">' + candidate.designation + '</font></a></h4>'
                        + '             <h5 >MPC tempDesignation: C' + candidate.tmpDesignation + '</h5>'
                        + '             <div >'
                        + '                 <table>'
                        + '                     <tr>'
                        + '                         <td class="shrink" id="status_' + candidate._id + '" >Current MPC Status: ' + getMPCstate(candidate.mpcReportState) + '</td>';
                if (candidate.mpcReportState < 0) {
                    html += '                     <td class="expand" id="report_container_' + candidate._id + '"><a id="report_' + candidate._id + '" class="btn btn-primary" role="button" >Report to MPC</a></td>';
                }
                html += '                      </tr>'
                        + '                 </table>'
                        + '         </div>'
                        + '             <div >'
                        + '                 <table>'
                        + '                     <tr>'
                        + '                         <td class="shrink" >Total Users: ' + (candidate.users.length) + '</td>'
                        + '                     </tr>'
                        + '                 </table>'
                        + '         </div>'
                        + '      <div ><a id="candidate_lines_button_' + candidate._id + '" >Coordenates (' + candidate.coordenates.length + ') <i id="candidate_lines_icon_' + candidate._id + '" class="fa fa-caret-down" aria-hidden="true"></i></a></div>'
                        + '      <div id="candidate_lines_' + candidate._id + '" style=" white-space: pre;" id="coordenates_' + candidate._id + '"></div>'
                        + '     </div>'
                        + '     </div>'
                        + '</div>';

                $("#known_list").append(html);

                $("#candidate_lines_button_" + candidate._id).click((function () {                     //persist variable on closure
                    var mCandidate = candidate;
                    var isOpen = false;
                    return function () {
                        if (isOpen) {
                            $("#candidate_lines_" + mCandidate._id).html('');


                            $("#candidate_lines_icon_" + mCandidate._id).removeClass('fa-caret-up').addClass('fa-caret-down');
                        } else {
                            var lines = '';
                            for (var l = 0; l < mCandidate.coordenates.length; l++) {
                                lines += mCandidate.coordenates[l].line + "<br>";
                            }
                            $("#candidate_lines_" + mCandidate._id).html(lines);

                            $("#candidate_lines_icon_" + mCandidate._id).removeClass('fa-caret-down').addClass('fa-caret-up');

                        }
                        isOpen = !isOpen;
                    };
                })());

                $("#report_" + candidate._id).click((function () {                     //persist variable on closure
                    var mCandidate = candidate;
                    return function () {
                        CandidateServerService.Report(mCandidate._id).then(function (data) {
                            //console.log(JSON.stringify(data));
                            if (data) {
                                $('#status_' + mCandidate._id).html('Current MPC Status: ' + getMPCstate(data.reportState));
                                mCandidate.mpcReportState = data.reportState;

                                if (data.reportState >= 0) {
                                    $('#report_container_' + mCandidate._id).remove();
                                }
                            }
                        });

                    };
                })());

            }

        }

    }

})();