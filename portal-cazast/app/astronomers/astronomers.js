(function () {
    'use strict';

    angular
            .module('app')
            .controller('AstronomersController', AstronomersController);

    AstronomersController.$inject = ['$rootScope', 'UserServerService', 'QuintetServerSercive', '$location'];
    function AstronomersController($rootScope, UserServerService, QuintetServerSercive, $location) {
        var vm = this;

        vm.user = $rootScope.globals.currentUser;

        vm.selectedAstronomer = null;
        vm.allUsers = [];
        vm.allTelescopes = [];
        vm.changeAstronomer = changeAstronomer;


        initController();

        function initController() {
            //MenuService.ConfigMenu(vm.user);

            loadUsers();
            loadTelescopes();
        }

        function loadUsers() {
            UserServerService.AllUsers()
                    .then(function (response) {
                        if (!response.error) {
                            vm.allUsers = response;
                            for (var k in  vm.allUsers) {
                                if (vm.allUsers[k].superAdmin) {
                                    vm.allUsers[k].color = "#808080";
                                } else if (vm.allUsers[k].admin) {
                                    vm.allUsers[k].color = "#cccccc";
                                }
                            }
                        }
                    });
        }

        function loadTelescopes() {
            QuintetServerSercive.AllTelescopes().then(function (response) {
                if (!response.error) {
                    vm.allTelescopes = response;
                }
            });
        }

        function changeAstronomer() {
            var user = JSON.parse(vm.selectedAstronomer);

//http://bootsnipp.com/snippets/featured/checked-list-group
            var html =
                    '<div class="thumbnail" id=row_' + user._id + '" style="margin-top:20px;">'
                    + ' <div class="media" style="padding:10px">'
                    + '     <div class="container">'
                    + '         <div class="row">'
                    + '             <div class="col-xs-6">'
                    + '                 <h2>' + user.username + '</h2>'
                    + '                 <h5>' + user.email + '</h4>'
                    + '                 <h3 class="text-center">Rol</h3>'
                    + '                 <div class="well" style="max-height: 300px;overflow: auto;">'
                    + '                     <ul class="list-group checked-list-box">'
                    + '                       <li id="li_superAdmin" class="list-group-item" data-style="button" data-color="danger">Super Admin</li>'
                    + '                       <li id="li_admin" class="list-group-item" data-style="button">Astronomer</li>'
                    + '                     </ul>'
                    + '                 </div>'
                    + '             </div>'
                    + '             <div class="col-xs-6">'
                    + '                 <h3 class="text-center">Telescopes</h3>'
                    + '                 <div class="well" style="max-height: ' + parseInt($(window).height() / 2) + 'px;overflow: auto;">'
                    + '                     <ul id="check-list-box" class="list-group checked-list-box">';

            var allTelescopes = arrayUnique(vm.allTelescopes.concat(user.adminTelescopes));
            for (var k in  allTelescopes) {
                var li = '<li id="telescope_li_' + k + '" class="list-group-item">' + allTelescopes[k] + '</li>';
                html += li;
            }

            html += '                   </ul>'
                    + '                     <br />'
                    + '                     <button class="btn btn-primary col-xs-12" id="check-all-telescopes">Check All</button>'
                    + '                 </div>'
                    + '             </div>'
                    + '         </div>'
                    + '         <div class="row">'
                    + '             <button class="btn btn-primary col-xs-12" id="update-astronomer-info">Update Astronomer Info</button>'
                    + '<pre id="display-json"></pre>'
                    + '         </div>'
                    + '     </div>'
                    + ' </div>'
                    + '</div>'
                    ;
            $("#astronomer_info").html(html);

//Configure List groups
            $('.list-group.checked-list-box .list-group-item').each(function () {

                // Settings
                var $widget = $(this),
                        $checkbox = $('<input type="checkbox" class="hidden" />'),
                        color = ($widget.data('color') ? $widget.data('color') : "primary"),
                        style = ($widget.data('style') === "button" ? "btn-" : "list-group-item-"),
                        settings = {
                            on: {
                                icon: 'glyphicon glyphicon-check'
                            },
                            off: {
                                icon: 'glyphicon glyphicon-unchecked'
                            }
                        };

                $widget.css('cursor', 'pointer');
                $widget.append($checkbox);

                // Event Handlers
                $widget.on('click', function () {
                    console.log('id: ' + $widget.attr('id'));

                    if ($widget.attr('id') && $widget.attr('id') === 'li_superAdmin' && !$checkbox.is(':checked')) {
                        $('#li_admin input').prop('checked', true);
                        $('#li_admin input').triggerHandler('change');
                    }
                    if ($widget.attr('id') && $widget.attr('id') === 'li_admin' && $checkbox.is(':checked') && $('#li_superAdmin input').is(':checked')) {
                        alert("Need to remove the Super Admin permission first.");
                        return;
                    }

                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    updateDisplay();
                });
                $checkbox.on('change', function () {
                    updateDisplay();
                });


                // Actions
                function updateDisplay() {
                    var isChecked = $checkbox.is(':checked');

                    // Set the button's state
                    $widget.data('state', (isChecked) ? "on" : "off");

                    // Set the button's icon
                    $widget.find('.state-icon')
                            .removeClass()
                            .addClass('state-icon ' + settings[$widget.data('state')].icon);

                    // Update the button's color
                    if (isChecked) {
                        $widget.addClass(style + color + ' active');
                    } else {
                        $widget.removeClass(style + color + ' active');
                    }
                }

                // Initialization
                function init() {

                    if ($widget.data('checked') === true) {
                        $checkbox.prop('checked', !$checkbox.is(':checked'));
                    }

                    $checkbox.prop('checked', user.adminTelescopes.includes($widget.text()));

                    updateDisplay();

                    // Inject the icon if applicable
                    if ($widget.find('.state-icon').length === 0) {
                        $widget.prepend('<span class="state-icon ' + settings[$widget.data('state')].icon + '"></span>');
                    }
                }
                init();
            });
            //Check All Functionality 
            $('#check-all-telescopes').on('click', function (event) {
                $("#check-list-box li input").each(function (idx, li) {
                    $(li).prop('checked', true);
                    $(li).triggerHandler('change');
                });
            });

            //Update Admin Info 
            $('#update-astronomer-info').on('click', function (event) {
                event.preventDefault();
                var checkedItems = {};

                checkedItems['_id'] = user._id;

                if ($('#li_superAdmin input').is(':checked')) {
                    checkedItems['superAdmin'] = true;
                }

                if (checkedItems.superAdmin || $('#li_admin input').is(':checked')) {
                    checkedItems['admin'] = true;
                }

                var telescopeSelected = [];
                $("#check-list-box li.active").each(function (idx, li) {
                    telescopeSelected.push($(li).text());
                });
                checkedItems['adminTelescopes'] = telescopeSelected;

                if (!checkedItems.admin && !checkedItems.superAdmin) {
                    checkedItems['adminTelescopes'] = [];
                }

                $('#display-json').html(JSON.stringify(checkedItems, null, '\t'));


                if (user.username === vm.user.username && !checkedItems.superAdmin) {
                    var r = confirm("Surely you want to stop being superAdmin");
                    if (r === false) {
                        return;
                    } else {
                        $location.path('/login');
                    }
                }
                UserServerService.UpdateAdmin(checkedItems).then(function () {

                    vm.selectedAstronomer = user;
                    vm.selectedAstronomer.suerAdmin = checkedItems.superAdmin;
                    vm.selectedAstronomer.admin = checkedItems.admin;
                    vm.selectedAstronomer.adminTelescopes = checkedItems.adminTelescopes;
                    loadUsers();
                });

            });


            //Configure Admin checked state
            $('#li_admin input').prop('checked', user.admin);
            $('#li_admin input').triggerHandler('change');

            //Configure superAdmin checked state
            $('#li_superAdmin input').prop('checked', user.superAdmin);
            $('#li_superAdmin input').triggerHandler('change');
        }

        function arrayUnique(array) {
            var a = array.concat();
            for (var i = 0; i < a.length; ++i) {
                for (var j = i + 1; j < a.length; ++j) {
                    if (a[i] === a[j])
                        a.splice(j--, 1);
                }
            }

            return a;
        }
    }

})();