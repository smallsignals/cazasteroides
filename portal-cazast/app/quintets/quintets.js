(function () {
    'use strict';

    angular
            .module('app')
            .controller('QuintetsController', QuintetsController);

    QuintetsController.$inject = ['MenuService', '$rootScope', '$location', 'QuintetServerSercive', 'myService'];
    function QuintetsController(MenuService, $rootScope, $location, QuintetServerSercive, myService) {
        var vm = this;

        vm.user = $rootScope.globals.currentUser;
        vm.allStatuses = ['Active', 'Inactive'];
        vm.allQuintets = [];
        vm.selectedStatus = vm.allStatuses[0];
        vm.changeStatus = changeStatus;

        initController();

        function initController() {
            MenuService.ConfigMenu(vm.user);

            loadQuintets(vm.selectedStatus);
            displayQuintets();
        }

        function changeStatus() {
            loadQuintets(vm.selectedStatus);
        }

        function loadQuintets(state) {
            var data = {

            };

            if (state) {
                data.active = vm.allStatuses[0] == state;
            }

            QuintetServerSercive.AllQuintets(data)
                    .then(function (quintets) {
                        if (!quintets || quintets.length <= 0) {
                            $("#quintet_info").html("No Quintets Found!");
                            return;
                        }
                        vm.allQuintets = quintets;

                        displayQuintets();

                    });
        }

        function displayQuintets() {
            $("#quintet_info").html(
                    '<div class="container" style="padding-top: 1em;">'
                    + '<div class="container">'
                    + '  <div class="panel panel-default">'
                    + '     <div class="panel-heading">Found ' + vm.allQuintets.length + ' quintets ' + (vm.selectedStatus ? 'for the ' + vm.selectedStatus + ' quintets' : '') + '</div>'
                    + '     <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">'
                    + '         <thead>'
                    + '             <tr>'
                    + '                 <th>Name</th>'
                    + '                 <th>Date</th>'
                    + '                 <th>Telescope</th>'
                    + '                 <th>State</th>'
                    + '                 <th>Area</th>'
                    + '                 <th>Area Show</th>'
                    + '                 <th>Total DET</th>'
                    + '                 <th>Approved DET</th>'
                    + '                 <th>Pending DET</th>'
                    + '                 <th>Know Objets</th>'
                    + '                 <th></th>'
                    + '             </tr>'
                    + '         </thead>'
                    + '         <tfoot>'
                    + '             <tr>'
                    + '                 <th>Name</th>'
                    + '                 <th>Date</th>'
                    + '                 <th>Telescope</th>'
                    + '                 <th>State</th>'
                    + '                 <th>Area</th>'
                    + '                 <th>Area Shown</th>'
                    + '                 <th>Total DET</th>'
                    + '                 <th>Appr. DET</th>'
                    + '                 <th>Pend. DET</th>'
                    + '                 <th>Know Objets</th>'
                    + '                 <th></th>'
                    + '             </tr>'
                    + '         </tfoot>'
                    + '         <tbody id="quintet_table_body" />'
                    + '     </table>'
                    + ' </div>'
                    + '</div>'
                    + '</div>');

            for (var i = 0; i < vm.allQuintets.length; i++) {
                var quintet = vm.allQuintets[i];

                var telescopio = quintet.meta && quintet.meta.TELESCOP ? quintet.meta.TELESCOP : quintet.name.substring(14);

                var html =
                        '  <tr>'
                        + '     <td>' + quintet.name + '</td>'
                        + '     <td>' + quintet.fecha + '</td>'
                        + '     <td>' + telescopio + '</td>'
                        + '     <td>' + (quintet.active ? 'Active' : 'Inactive') + '</td>'
                        + '     <td>' + (quintet.info && quintet.info.total_area ? quintet.info.total_area.toLocaleString() : 'unknow') + '</td>'
                        + '     <td>' + (quintet.info && quintet.info.total_area_shown && quintet.info.total_area ? (parseFloat(quintet.info.total_area_shown) / parseFloat(quintet.info.total_area)).toFixed(3).toLocaleString() : '0') + '</td>'
                        + '     <td>' + (quintet.info && quintet.info.total_detections ? quintet.info.total_detections : '0') + '</td>'
                        + '     <td>' + (quintet.info && quintet.info.total_approved ? quintet.info.total_approved : '0') + '</td>'
                        + '     <td>' + (quintet.info && quintet.info.total_pending ? quintet.info.total_pending : '0') + '</td>'
                        + '     <td>' + (quintet.info && quintet.info.total_know ? quintet.info.total_know : '0') + '</td>'
                        + '     <td><a id="more_info_' + quintet._id + '" class="btn btn-primary" role="button" >More Info</a></td>'
                        + ' </tr>'
                        ;

                $("#quintet_table_body").append(html);



                $("#more_info_" + quintet._id).click((function () {
                    //persist variable on closure
                    var mQuintet = quintet;
                    return function () {
                        console.log(JSON.stringify(mQuintet, null, '\t'));
                        myService.set(mQuintet);
                        $location.path('/quintet');
                        $rootScope.$apply();
                    };
                })());

            }

            $('#example').DataTable({
                "columnDefs": [
                    {"orderable": false, "searchable": false, "targets": 10}
                ]
            });
        }

    }

})();