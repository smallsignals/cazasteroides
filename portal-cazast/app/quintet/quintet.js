(function () {
    'use strict';

    angular
            .module('app')
            .controller('QuintetController', QuintetController);

    QuintetController.$inject = ['MenuService', '$rootScope', 'QuintetServerSercive', 'myService', 'domain', 'domain_img'];
    function QuintetController(MenuService, $rootScope, QuintetServerSercive, myService, domain, domain_img) {
        var vm = this;

        vm.user = $rootScope.globals.currentUser;
        vm.quintet = myService.get();

        initController();

        function initController() {
            MenuService.ConfigMenu(vm.user);
            loadQuintet();
            displayQuintet();
        }

        function loadQuintet() {
            if (vm.quintet && vm.quintet._id)
                QuintetServerSercive.QuintetInfo(vm.quintet._id)
                        .then(function (quintet) {
                            if (!quintet) {
                                $("#quintet_info").html("No Quintet Found!");
                                return;
                            }
                            vm.quintet = quintet;
                            displayQuintet();
                        });
        }

        function displayQuintet() {
            if (!vm.quintet)
                return;

            var qintet = vm.quintet;


            $("#quintet_title").html(qintet.name + '<p class="lead">' + qintet.fecha + '</p>');

            var jpgUrl = domain + '/admin/quintets/jpg/' + qintet.name;
            $("#quintet_image").html('<a><img src="' + jpgUrl + '" class="thumbnail img-responsive"></a>');

            var coverageUrl = domain + '/admin/quintets/' + qintet._id + '/coverage';
            $("#quintet_coverage").html('<a><img src="' + coverageUrl + '" class="thumbnail img-responsive"></a>');



            var approvedBadge = 0, rejectedBadge = 0, pendingBadge = 0;
            for (var k in qintet.detections) {

                var detectionUrl = domain_img + "/obs/get_obs_gif/" + qintet.detections[k]._id;
                var totalVotesUp = qintet.detections[k].votes.length;
                var totalVotesDown = qintet.detections[k].downvotes.length;
                var totalVotes = totalVotesUp + totalVotesDown;
                var totalReDetec = qintet.detections[k].revisit.length + 1;
                var designation = undefined;

                var quintetn = qintet.detections[k].quintetn ? qintet.detections[k].quintetn : "?";

                if (qintet.detections[k].object) {
                    if (qintet.detections[k].object.cazastDesignation) {
                        designation = qintet.detections[k].object.cazastDesignation.replace(qintet.name + '-', '');
                    } else if (qintet.detections[k].object.mpcDesignation) {
                        designation = qintet.detections[k].object.mpcDesignation;
                    }
                }

                var hover =
                        'Total Detections: ' + totalReDetec
                        + '\nQuintet n : ' + quintetn
                        + '\nVOTES'
                        + '\nTotal    : ' + totalVotes
                        + '\nPostive  : ' + totalVotesUp
                        + '\nNegative : ' + totalVotesDown
                        ;
                var detectionHtml =
                        '<div class="col-xs-2">'
                        + (designation ? '<p><font size="3">' + designation + '</font></p>' : '')
                        + ' <a class="thumbnail"><img title="' + hover + '"  src="' + detectionUrl + '" class="img-responsive"></a>'
                        + '</div>';
                switch (qintet.detections[k].status) {
                    case 'approved':
                        $("#approved_grid").append(detectionHtml);
                        approvedBadge++;
                        break;
                    case 'rejected':
                        $("#rejected_grid").append(detectionHtml);
                        rejectedBadge++;
                        break;
                    default:
                        $("#pending_grid").append(detectionHtml);
                        pendingBadge++;
                        break;
                }
            }
            $("#approved_badge").html(approvedBadge);
            $("#pending_badge").html(pendingBadge);
            $("#rejected_badge").html(rejectedBadge);

        }
    }

})();