(function () {
    'use strict';

    angular
            .module('app')
            .controller('SetupController', SetupController);

    SetupController.$inject = ['$rootScope', '$scope', 'SetupServerService'];
    function SetupController($rootScope, $scope, SetupServerService) {
        var vm = this;

        vm.user = $rootScope.globals.currentUser;
        vm.me = null;

        $scope.tempOption = {};
        $scope.optionArray = [];
        $scope.addOption = function () {
            var temp = {
                key: $scope.tempOption.key,
                value: $scope.tempOption.value
            };
            $scope.optionArray.push(temp);
            $scope.tempOption = {};
        };

        $scope.updateOption = function () {
            var data = {config: $scope.optionArray};
            SetupServerService.Post(data);
        };

        $scope.refreshOption = function () {
            initController();
        };

        initController();

        function initController() {
            SetupServerService.Get().then(function (response) {
                if (!response.error) {
                    $scope.optionArray = response;
                    $scope.apply();
                }
            });
        }

    }

})();