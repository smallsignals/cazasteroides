(function () {
    'use strict';

    angular
            .module('app')
            .factory('UserServerService', UserServerService);

    UserServerService.$inject = ['$http', 'domain', '$location'];
    function UserServerService($http, domain, $location) {
        var service = {};


        service.AllUsers = AllUsers;
        service.UpdateAdmin = UpdateAdmin;
        service.Me = Me;

        return service;

        function AllUsers() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/users', config).then(handleSuccess, handleError('Error Loading Users'));
        }

        function UpdateAdmin(user) {

            var data = {
                user: user
            };

            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/users/update', data, config).then(handleSuccess, handleError('Error Login'));
        }

        function Me() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/users/me', config).then(handleSuccess, handleError('Error Loading Users'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }
                return {error: true, message: error};
            };
        }
    }

})();