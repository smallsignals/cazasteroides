(function () {
    'use strict';

    angular
            .module('app')
            .factory('MenuService', MenuService);

    function MenuService() {
        var service = {};

        service.ConfigMenu = ConfigMenu;
        service.Hide = Hide;
        service.Show = Show;
        return service;


        function Show() {
            $('#main_menu').show();
        }
        function Hide() {
            $('#main_menu').hide();
        }


        function ConfigMenu(currentUser) {

            if (!currentUser) {
                Hide();
                return;
            }

            Show();
            //Set current user
            $('#main_menu_brand').html((currentUser.isSuperAdmin ? "SuperAdmin" : "Astronomer") + " " + currentUser.username);

            var currentUserMenu = "";

            currentUserMenu += '<li id="main_menu_user_menu_detections" ><a href="#!/detections">Detections</a></li>';
            currentUserMenu += '<li id="main_menu_user_menu_quintets" ><a href="#!/quintets">Quintets</a></li>';
            if (currentUser.isSuperAdmin) {
                currentUserMenu += '<li id="main_menu_user_menu_approved" ><a href="#!/approved">Approved</a></li>';
                currentUserMenu += '<li id="main_menu_user_menu_astronomers" ><a href="#!/astronomers">Astronomers</a></li>';
                currentUserMenu += '<li id="main_menu_user_menu_contests" ><a href="#!/contests">Competitions</a></li>';
                currentUserMenu += '<li id="main_menu_user_menu_setups" ><a href="#!/setups">Configurations</a></li>';
            }
            $('#main_menu_user_menu').html(currentUserMenu);
        }
    }

})();