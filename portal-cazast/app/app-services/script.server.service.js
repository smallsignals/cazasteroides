(function () {
    'use strict';

    angular
            .module('app')
            .factory('ScriptServerService', ScriptServerService);

    ScriptServerService.$inject = ['$http', 'domain', '$location'];
    function ScriptServerService($http, domain, $location) {
        var service = {};

        service.Sync = Sync;
        service.avgCoordSync = avgCoordSync;
        service.infoSync = infoSync;
        service.newCandidates = newCandidates;

        return service;

        function Sync() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/scripts/sync', config).then(handleSuccess, handleError('Error Loading Users'));
        }

        function avgCoordSync() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/detections/avgCoordSync', config).then(handleSuccess, handleError('Error Loading Users'));
        }

        function infoSync() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/quintets/infoSync', config).then(handleSuccess, handleError('Error Loading Users'));
        }

        function newCandidates() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/candidate/generate', config).then(handleSuccess, handleError('Error Loading Users'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }
                return {error: true, message: error};
            };
        }
    }

})();