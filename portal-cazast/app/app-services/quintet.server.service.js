(function () {
    'use strict';

    angular
            .module('app')
            .factory('QuintetServerSercive', QuintetServerSercive);

    QuintetServerSercive.$inject = ['$http', 'domain', '$location'];
    function QuintetServerSercive($http, domain, $location) {
        var service = {};

        service.AllQuintets = AllQuintets;
        service.AllQuintetsList = AllQuintetsList;
        service.AllTelescopes = AllTelescopes;
        service.QuintetInfo = QuintetInfo;
        service.NearThresholdToDesactivate = NearThresholdToDesactivate;
        service.QuintetsWithPendings = QuintetsWithPendings;
        return service;

        function AllQuintets(data) {
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/quintets', config).then(handleSuccess, handleError('Error Loading Quintets'));
        }
        function AllQuintetsList() {

            var config = {
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/quintets/list', config).then(handleSuccess, handleError('Error Loading Quintets'));
        }

        function AllTelescopes() {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/quintets/telescopes', config).then(handleSuccess, handleError('Error Loading Telescopes'));
        }

        function QuintetInfo(id) {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/quintets/' + id, config).then(handleSuccess, handleError('Error Loading Quintet'));
        }

        function NearThresholdToDesactivate() {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/quintets/alert/nearThresholdToDesactivate', config).then(handleSuccess, handleError('Error Loading nearThresholdToDesactivate'));
        }

        function QuintetsWithPendings() {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/quintets/alert/quintetsWithPendings', config).then(handleSuccess, handleError('Error Loading quintetsWithPendings'));
        }
        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }
                return {error: true, message: error};
            };
        }
    }

})();