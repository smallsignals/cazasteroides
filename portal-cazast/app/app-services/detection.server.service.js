(function () {
    'use strict';

    angular
            .module('app')
            .factory('DetectionsServerSercive', DetectionsServerSercive);

    DetectionsServerSercive.$inject = ['$http', 'domain', '$location'];
    function DetectionsServerSercive($http, domain, $location) {
        var service = {};

        // AuthenticationService.SetHeaderAuthToken();

        service.AllDetections = AllDetections;
        service.AllStatus = AllStatus;
        service.UpdateDetectionStatus = UpdateDetectionStatus;

        return service;

        function AllStatus() {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            return $http.get(domain + '/admin/detections/status', config).then(handleSuccess, handleError('Error Loading Status'));
        }

        function AllDetections(status, quintet, min, pag) {
            var data = {
                status: status,
                quintet: quintet,
                min: min
            };

            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };
            console.log(JSON.stringify(data));
            return $http.get(domain + '/admin/detections?skip=' + pag.skip + '&limit=' + pag.limit, config).then(handleSuccess, handleError('Error Loading Detections'));
        }

        function UpdateDetectionStatus(id, newState, rejectedCode) {
            var data = {
                obs: {_id: id, status: newState}
            };

            if (rejectedCode) {
                data.obs.rejectedCode = rejectedCode;
            }

            var config = {
                headers: {'Accept': 'application/json'}
            };
            console.log(JSON.stringify(data));
            return $http.post(domain + '/admin/detections/update', data, config).then(handleSuccess, handleError('Error Loading Detections'));
        }

        // private functions

        function handleSuccess(res) {
            //console.log(JSON.stringify(res.data));
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }
                return {error: true, message: error};
            };
        }
    }

})();