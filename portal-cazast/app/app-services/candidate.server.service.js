(function () {
    'use strict';

    angular
            .module('app')
            .factory('CandidateServerService', CandidateServerService);

    CandidateServerService.$inject = ['$http', 'domain', '$location'];
    function CandidateServerService($http, domain, $location) {
        var service = {};

        service.GetKnown = GetKnown;
        service.GetUnknown = GetUnknown;
        service.GetUnknownIncomplete = GetUnknownIncomplete;
        service.Report = Report;
        service.Update = Update;

        return service;

        function GetKnown() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/candidate/known', config).then(handleSuccess, handleError('Error Loading candidate known'));
        }

        function GetUnknown() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/candidate/unknown', config).then(handleSuccess, handleError('Error Loading candidate candidate'));
        }

        function GetUnknownIncomplete() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/candidate/unknownIncomplete', config).then(handleSuccess, handleError('Error Loading candidate incomplete'));
        }

        function Report(_id) {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            var data = {
                _id: _id
            };
            return $http.post(domain + '/admin/candidate/report', data, config).then(handleSuccess, handleError('Error Reporting to MPC'));
        }

        function Update(candidate) {
            var config = {
                headers: {'Accept': 'application/json'}
            };
            var data = {
                _id: candidate._id,
                coordenates: candidate.coordenates
            };
            return $http.post(domain + '/admin/candidate/updateIncomplete', data, config).then(handleSuccess, handleError('Error Updating Incomplete Candidate'));
        }

        // private functions
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }

                if (response.data) {
                    if (response.data.errmsg) {
                        error = response.data.errmsg;
                    }
                }

                return {error: true, message: error};
            };
        }
    }

})();