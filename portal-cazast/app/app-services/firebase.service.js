(function () {
    'use strict';

    angular
            .module('app')
            .factory('FirebaseService', FirebaseService);

    FirebaseService.$inject = ['ContestServerService'];
    function FirebaseService(ContestServerService) {
        var service = {};


        service.UploadContestFiles = UploadContestFiles;
        service.DeleteFiles = authAndDeleteFiles;

        return service;

        function uploadContestFile(id, dir, file) {
            var storageRef = firebase.storage().ref();
// File or Blob named mountains.jpg
//            var file = new File([""], path);

// Create the file metadata
            var metadata = {
                contentType: file.type ? file.type : 'image/*'
            };

// Upload file and metadata to the object 'images/mountains.jpg'
            var uploadTask = storageRef.child('contest/' + dir + '/' + file.name + new Date().getTime()).put(file, metadata);

// Listen for state changes, errors, and completion of the upload.
            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
                    function (snapshot) {
                        // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                        var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                        console.log('Upload is ' + progress + '% done');
                        switch (snapshot.state) {
                            case firebase.storage.TaskState.PAUSED: // or 'paused'
                                console.log('Upload is paused');
                                break;
                            case firebase.storage.TaskState.RUNNING: // or 'running'
                                console.log('Upload is running');
                                break;
                        }
                    }, function (error) {

                // A full list of error codes is available at
                // https://firebase.google.com/docs/storage/web/handle-errors
                console.log(JSON.stringify(error));

            }, function () {
                // Upload completed successfully, now we can get the download URL
                var downloadURL = uploadTask.snapshot.downloadURL;
                console.log(downloadURL);
                var data = {
                    _id: id,
                    url: downloadURL
                };
                ContestServerService.AddImage(data).then(function (response) {

                });
            });
        }

        function UploadContestFiles(id, name, files) {
            if (files.length > 0) {
                authFirebase(function (firebaseUser) {
                    console.log(JSON.stringify(firebaseUser));
                    // Success 
                    for (var i = 0; i < files.length; i++)
                        uploadContestFile(id, name, files[i]);

                }, function (error) {
                    console.log(JSON.stringify(error));

                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // ...
                });
            }

        }

        function deleteFile(url) {
            var desertRef = firebase.storage().refFromURL(url);

            desertRef.delete().then(function () {
                console.log("Success");
                // File deleted successfully
            }).catch(function (error) {
                console.log(JSON.stringify(error));
                // Uh-oh, an error occurred!
            });
        }


        function authAndDeleteFiles(files) {
            if (files.length > 0) {
                authFirebase(function (firebaseUser) {
                    //console.log(JSON.stringify(firebaseUser));
                    // Success 
                    for (var i = 0; i < files.length; i++)
                        deleteFile(files[i]);

                }, function (error) {
                    //console.log(JSON.stringify(error));

                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    // ...
                });
            }

        }


        function authFirebase(successCB, errorCB) {
            firebase.auth().signInWithEmailAndPassword('astroaula@gmail.com', 'cazasteroidesPortalC4z4573r01d35')
                    .then(function (firebaseUser) {
                        if (successCB)
                            successCB(firebaseUser);
                    })
                    .catch(function (error) {
                        if (errorCB)
                            errorCB(error);
                    });
        }
    }

})();