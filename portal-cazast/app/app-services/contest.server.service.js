(function () {
    'use strict';

    angular
            .module('app')
            .factory('ContestServerService', ContestServerService);

    ContestServerService.$inject = ['$http', 'domain', '$location'];
    function ContestServerService($http, domain, $location) {
        var service = {};

        service.Create = Create;
        service.AddImage = AddImage;
        service.DeleteImages = DeleteImages;
        service.Delete = Delete;
        service.Update = Update;
        service.All = All;

        return service;

        function Create(data) {

//    name: {unique: true, type: String, required: true}
//    time_start: {type: Date, required: true}
//    time_end: {type: Date, required: true}
//    description: {type: String}
//    url: {type: String}
//    logos:[String]
            console.log(JSON.stringify(data));
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/contest', data, config).then(handleSuccess, handleError('Error Creating Contests'));
        }

        function All() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/contest', config).then(handleSuccess, handleError('Error Loading Contests'));
        }

        function Delete(data) {
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            return $http.delete(domain + '/admin/contest', config).then(handleSuccess, handleError('Error Deleting Contest'));
        }

        function Update(id, data) {
            var config = {
                //params: data,
                headers: {'Accept': 'application/json'}
            };

            return $http.put(domain + '/admin/contest/' + id, data, config).then(handleSuccess, handleError('Error Deleting Contest'));
        }

        function AddImage(data) {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/contest/logo', data, config).then(handleSuccess, handleError('Error Adding Image Contests'));
        }
        function DeleteImages(data) {
            var config = {
                params: data,
                headers: {'Accept': 'application/json'}
            };

            return $http.delete(domain + '/admin/contest/logo', config).then(handleSuccess, handleError('Error Adding Image Contests'));
        }
        // private functions
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }

                if (response.data) {
                    if (response.data.errmsg) {
                        error = response.data.errmsg;
                    }
                }

                return {error: true, message: error};
            };
        }
    }

})();