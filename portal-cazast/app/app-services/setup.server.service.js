(function () {
    'use strict';

    angular
            .module('app')
            .factory('SetupServerService', SetupServerService);

    SetupServerService.$inject = ['$http', 'domain', '$location'];
    function SetupServerService($http, domain, $location) {
        var service = {};

        service.Get = Get;
        service.Post = Post;

        return service;

        function Post(data) {
            console.log(JSON.stringify(data));
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.post(domain + '/admin/setup', data, config).then(handleSuccess, handleError('Error Creating setup'));
        }

        function Get() {
            var config = {
                headers: {'Accept': 'application/json'}
            };

            return $http.get(domain + '/admin/setup', config).then(handleSuccess, handleError('Error Loading setup'));
        }
        // private functions
        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function (response) {
                if (response && response.status == 401) {
                    $location.path('/login');
                }

                if (response.data) {
                    if (response.data.errmsg) {
                        error = response.data.errmsg;
                    }
                }

                return {error: true, message: error};
            };
        }
    }

})();