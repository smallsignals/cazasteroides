(function () {
    'use strict';

    angular
            .module('app')
            .factory('LoginServerService', LoginServerService);

    LoginServerService.$inject = ['$http', 'domain'];
    function LoginServerService($http, domain) {
        var service = {};

        service.BasicLogin = BasicLogin;
//        service.CreateAdmin = CreateAdmin;
//        service.DeleteAdmin = DeleteAdmin;
//        service.UpdateObservatory = UpdateObservatory;

        return service;

        function BasicLogin(authdata) {

            var config = {
                headers: {'Authorization': 'Basic ' + authdata}
            };

            return $http.get(domain + '/auth/basicAdmin', config).then(handleSuccess, handleError('Error Login'));
        }
        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {error: true, message: error};
            };
        }
    }

})();