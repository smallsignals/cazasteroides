(function () {
    'use strict';

    angular
            .module('app')
            .controller('DetectionsController', DetectionsController);

    DetectionsController.$inject = ['DetectionsServerSercive', 'QuintetServerSercive', '$rootScope', 'domain_img', 'FlashService'];
    function DetectionsController(DetectionsServerSercive, QuintetServerSercive, $rootScope, domain_img, FlashService) {
        var vm = this;
        var LIMIT = 25;
        var skip = 0;
        var total = 0;

        vm.user = $rootScope.globals.currentUser;

        vm.allStatus = [];
        vm.allQuintets = [];
        vm.allMin = ['0', '1', '2', '3', '4', '5'];
        vm.allRejectedCode = [
            {code: 1, name: 'Cosmic rays'},
            {code: 2, name: 'Dead pixels'},
            {code: 3, name: 'Object not centered'},
            {code: 4, name: 'Object without movement'},
            {code: 5, name: 'Hot pixel'},
            {code: 6, name: 'Empty field'}
        ];

        vm.selectedDetectionStatus = 'pending';
        vm.selectedDetectionQuintets = null;
        vm.selectedDetectionMin = '2';

        vm.displayQuintet = function (item) {
            return item.name + " - " + item.active;
        };

        vm.changeDetectionStatus = changeDetectionStatus;
        vm.changeDetectionQuintets = changeDetectionQuintets;
        vm.changeDetectionMin = changeDetectionMin;

        initController();

        function initController() {
//            MenuService.ConfigMenu(vm.user);

            loadStatus();
            loadQuintets();
            loadDetections('pending', vm.selectedDetectionQuintets, vm.selectedDetectionMin, {skip: 0, limit: LIMIT});
        }

        function loadStatus() {
            DetectionsServerSercive.AllStatus()
                    .then(function (decStatus) {
                        console.log('Status: ' + JSON.stringify(decStatus));
                        if (!vm.selectedDetectionStatus && !vm.selectedDetectionQuintets && !vm.selectedDetectionMin) {
                            setTimeout(function () {
                                $("#detections_status").val($("#detections_status option:first").val());
                                $("#detections_images").val($("#detections_images option:first").val());
                                $("#detections_min").val($("#detections_min option:first").val());
                            }, 200);
                        }
                        if (!decStatus.error)
                            vm.allStatus = decStatus.sort();
                    });
        }

        function loadQuintets() {
            QuintetServerSercive.AllQuintetsList()
                    .then(function (decStatus) {
                        if (!vm.selectedDetectionStatus && !vm.selectedDetectionQuintets && !vm.selectedDetectionMin) {
                            setTimeout(function () {
                                $("#detections_status").val($("#detections_status option:first").val());
                                $("#detections_images").val($("#detections_images option:first").val());
                                $("#detections_min").val($("#detections_min option:first").val());
                            }, 200);
                        }
                        if (!decStatus.error)
                            vm.allQuintets = decStatus.sort();
                    });
        }
        function loadDetections(status, quintet, min, pag) {
            //console.log($("#detections_reports").val());
            DetectionsServerSercive.AllDetections(status, quintet, min ? min : 0, pag)
                    .then(function (data) {
                        var detections = data.result ? data.result : data;



                        if (pag.skip === 0) {
                            $("#dectections_list").html('');

                            if (!detections || detections.length <= 0) {
                                $("#dectections_total").html("No Detections Found!");
                                return;
                            }
                            total = (data.total ? data.total : detections.length);



                            $("#dectections_total").html(
                                    "Found " + total + " " + (vm.selectedDetectionStatus ? vm.selectedDetectionStatus : '') + " detections"
                                    //+ ((vm.selectedDetectionQuintets ? " from " + vm.selectedDetectionQuintets : ''))
                                    );
                        }

                        for (var i = 0; i < detections.length; i++) {
                            var detection = detections[i];
                            console.log(detection);
                            if (detection.image) {
                                var title = "";
                                if (detection.image.name) {
                                    title = detection.image.name;
                                } else {
                                    var lis = detection.image.filename[0].split("/");
                                    title = lis[lis.length - 2];
                                }

                                var errra = 0;
                                var errdec = 0;
                                var meanra = parseFloat(detection.avgCoord[0]);
                                var meandec = parseFloat(detection.avgCoord[1]);
//                                for (var n = 0; n < detection.revisit.length; n++) {
//                                    meanra += detection.revisit[n].x;
//                                    meandec += detection.revisit[n].y;
//                                }
//                                meanra /= (detection.revisit.length + 1);
//                                meandec /= (detection.revisit.length + 1);

                                var sqrra = (parseFloat(detection.Coord[0]) - meanra) * (parseFloat(detection.Coord[0]) - meanra);
                                var sqrdec = (parseFloat(detection.Coord[1]) - meandec) * (parseFloat(detection.Coord[1]) - meandec);
                                for (var n = 0; n < detection.revisit.length; n++) {
                                    sqrra += (parseFloat(detection.revisit[n].x) - meanra) * (parseFloat(detection.revisit[n].x) - meanra);
                                    sqrdec += (parseFloat(detection.revisit[n].y) - meandec) * (parseFloat(detection.revisit[n].y) - meandec);
                                }
                                sqrra /= (detection.revisit.length + 1);
                                sqrdec /= (detection.revisit.length + 1);
                                errra = Math.sqrt(sqrra);
                                errdec = Math.sqrt(sqrdec);

                                var upvotes = 0;
                                var downvotes = 0;
                                if (detection.votes)
                                    upvotes = detection.votes.length;
                                if (detection.downvotes)
                                    downvotes = detection.downvotes.length;

                                var designation = undefined;
                                if (detection.object) {
                                    if (detection.object.cazastDesignation) {
                                        designation = detection.object.cazastDesignation;
                                    } else if (detection.object.mpcDesignation) {
                                        designation = detection.object.mpcDesignation;
                                    }
                                }

                                var imageUrl = domain_img + "/obs/get_obs_gif/" + detection._id;


                                var quintetTime = detection.image.fecha;
                                if (detection.quintetn && detection.image.coeff && detection.image.coeff[detection.quintetn] && detection.image.coeff[detection.quintetn].TIMESTAM) {
                                    quintetTime = detection.image.coeff[detection.quintetn].TIMESTAM;
                                } else if (detection.image.meta && detection.image.meta.TIMESTAM) {
                                    quintetTime = detection.image.meta.TIMESTAM;
                                }

                                var html =
                                        '<div class="row" id="row_' + detection._id + '">'
                                        + ' <div class="thumbnail" style="padding:10px">'
                                        + '     <div class="media">'
                                        + '         <div class="media-left media-middle"><img class="media-object" style="margin-right: 10px;" src=' + imageUrl + ' alt="' + title + '"/></div>'
                                        + '         <div class="media-body">'
                                        + '             <h4 class="media-heading">' + title + '</h4>'
                                        + '             <h5>' + quintetTime + ' - Quintet Active? ' + detection.image.active + '</h5>'
                                        + '             <h5> Quintet Number: ' + (detection.quintetn ? detection.quintetn : '0') + '</h5>'
                                        + '             <div >'
                                        + '                 <table>'
                                        + '                     <tr>'
                                        + '                         <td class="shrink" id="status_' + detection._id + '" >Current Status: ' + detection.status + '</td>';
                                if (vm.user.isSuperAdmin || detection.status == 'pending') {
                                    html +=
                                            '                       <td class="shrink"><select id="new_status_' + detection._id + '" style="color: black;"></select></td>'
                                            + '                     <td class="shrink"><select id="rejected_status_' + detection._id + '" style="color: black;display:none;"></select></td>'
                                            + '                     <td class="expand"><a id="change_status_' + detection._id + '" class="btn btn-primary" role="button" >Update Status</a></td>';
                                }
                                html += '                      </tr>'
                                        + '                 </table>'
                                        + '             </div>'
                                        + (designation ? ('             <div >'
                                                + '                 <table>'
                                                + '                     <tr>'
                                                + '                         <td class="shrink">Object: ' + designation + '</td>'
                                                + '                     </tr>'
                                                + '                 </table>'
                                                + '             </div>') : '')

                                        + '             <div >'
                                        + '                 <table>'
                                        + '                     <tr>'
                                        + '                         <td class="shrink">x: ' + detection.pixCoordAbs[0].toFixed(2) + ' [pix]</td>'
                                        + '                         <td class="expand">y: ' + detection.pixCoordAbs[1].toFixed(2) + ' [pix]</td>'
                                        + '                     </tr>'
                                        + '                 </table>'
                                        + '             </div>'
                                        + '             <div >'
                                        + '                 <table>'
                                        + '                     <tr>'
                                        + '                         <td class="shrink">RA  (avg): ' + deg2hour(meanra) + ' [ hms ]</td>'
                                        + '                         <td class="shrink">DEC (avg): ' + deg2degminsec(meandec) + ' [ º \' " ]</td>'
                                        + '                         <td class="shrink">Error RA   : ' + (errra * 3600).toFixed(1) + ' [ " ]</td>'
                                        + '                         <td class="expand">Error DEC  : ' + (errdec * 3600).toFixed(1) + ' [ " ]</td>'
                                        + '                     </tr>'
                                        + '                 </table>'
                                        + '             </div>'
                                        + '             <div >'
                                        + '                 <table>'
                                        + '                     <tr>'
                                        + '                         <td class="expand">Total Reports: ' + (detection.revisit.length + 1) + '</td>'
                                        + '                     </tr>'
                                        + '                 </table>'
                                        + '             </div>'
                                        + '             <div >'
                                        + '                 <table>'
                                        + '                     <tr>'
                                        + '                         <td class="shrink">Votes: ' + (upvotes + downvotes) + '</td>'
                                        + '                         <td class="shrink">Up votes: ' + upvotes + '</td>'
                                        + '                         <td class="expand">Down Votes: ' + downvotes + '</td>'
                                        + '                     </tr>'
                                        + '                 </table>'
                                        + '             </div>'
                                        + '         </div>'
                                        + '     </div>'
                                        + ' </div>'
                                        + '</div>';

                                $("#dectections_list").append(html);

                                var seloption = "";
                                $.each(vm.allStatus, function (i) {
                                    seloption += '<option value="' + vm.allStatus[i] + '" ' + ((detection.status === vm.allStatus[i]) ? 'selected' : '') + '  >' + vm.allStatus[i] + '</option>';
                                });
                                $('#new_status_' + detection._id).append(seloption);

                                $("#new_status_" + detection._id).change((function () {
                                    //persist variable on closure
                                    var mDetection = detection;
                                    return function () {
                                        var newStatus = $('#new_status_' + mDetection._id + " option:selected").text();
                                        if (newStatus.toString() === 'rejected') {
                                            $('#rejected_status_' + mDetection._id).show();
                                        } else {
                                            $('#rejected_status_' + mDetection._id).hide();
                                        }
                                    };
                                })());


                                var seloption = "<option selected value> -- select Rejected code --</option>";
                                $.each(vm.allRejectedCode, function (i) {
                                    seloption += '<option value="' + vm.allRejectedCode[i].code + '" ' + '  >' + vm.allRejectedCode[i].name + '</option>';
                                });
                                $('#rejected_status_' + detection._id).append(seloption);



                                $("#change_status_" + detection._id).click((function () {
                                    //persist variable on closure
                                    var mDetection = detection;
                                    return function () {
                                        var newStatus = $('#new_status_' + mDetection._id + " option:selected").text();

                                        if (newStatus !== mDetection.status) {
                                            var rejectedCode = $('#rejected_status_' + mDetection._id + " option:selected").val();

                                            DetectionsServerSercive.UpdateDetectionStatus(mDetection._id, newStatus, rejectedCode).then(function (data) {

                                                if (data.affected) {
                                                    $('#status_' + mDetection._id).html('Current Status: ' + newStatus);
                                                    mDetection.status = newStatus;

                                                    if (!vm.user.isSuperAdmin) {
                                                        $('#new_status_' + mDetection._id).remove();
                                                        $('#rejected_status_' + mDetection._id).remove();
                                                        $('#change_status_' + mDetection._id).remove();
                                                    }
                                                    $('#row_' + mDetection._id).remove();

                                                    total--;
                                                    skip--;
                                                    $("#dectections_total").html(
                                                            "Found " + total + " " + (vm.selectedDetectionStatus ? vm.selectedDetectionStatus : '') + " detections"
                                                            //+ ((vm.selectedDetectionQuintets ? " from " + vm.selectedDetectionQuintets : ''))
                                                            );
                                                    if (skip < total) {
                                                        loadMore(1);
                                                    }
                                                } else {
                                                    FlashService.Error(data.message);
                                                }
                                            });
                                        }
                                    };
                                })());

                            }

                        }

                        $("#load_more").remove();

                        skip = pag.skip + detections.length;
                        if (detections.length === pag.limit && skip < total) {
                            $('#dectections_list').append(
                                    '<button type="button" class="btn" style="margin:5px;min-width:80px"  id="load_more">Load More</button>'
                                    );
                            $('#load_more').click(function () {
                                loadMore(LIMIT);
                            });

                        }

                    });
        }

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        function deg2degminsec(degin) {
            var deg = Math.trunc(degin);
            var m = Math.abs(Math.trunc((degin - deg) * 60));
            var s = ((Math.abs(degin - deg) * 60) - m) * 60;
            var z = "";
            if (s < 10)
                z = "0";
            return "" + pad(deg, 2) + " " + pad(m, 2) + " " + z + s.toFixed(1);//fixme cuidado con el +
        }

        function deg2hour(deg) {
            var hour = Math.trunc(deg * 24.0 / 360.0);
            var mdec = Math.abs((deg * 24.0 / 360.0) - hour) * 60.0;

            var m = Math.trunc(mdec);
            var s = (mdec - m) * 60;
            var z = "";
            if (s < 10)
                z = "0";
            return "" + pad(hour, 2) + " " + pad(m, 2) + " " + z + s.toFixed(1);
        }

        function changeDetectionStatus() {
            loadDetections(vm.selectedDetectionStatus, vm.selectedDetectionQuintets, vm.selectedDetectionMin, {skip: 0, limit: LIMIT});
        }

        function changeDetectionQuintets() {
            loadDetections(vm.selectedDetectionStatus, vm.selectedDetectionQuintets, vm.selectedDetectionMin, {skip: 0, limit: LIMIT});
        }

        function changeDetectionMin() {
            loadDetections(vm.selectedDetectionStatus, vm.selectedDetectionQuintets, vm.selectedDetectionMin, {skip: 0, limit: LIMIT});
        }

        function loadMore(limit) {
            loadDetections(vm.selectedDetectionStatus, vm.selectedDetectionQuintets, vm.selectedDetectionMin, {skip: skip, limit: limit});
        }


    }

})();