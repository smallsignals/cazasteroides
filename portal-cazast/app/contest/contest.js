(function () {
    'use strict';

    angular
            .module('app')
            .controller('ContestController', ContestController);

    ContestController.$inject = ['$rootScope', 'ContestServerService', 'FlashService', '$scope', 'FirebaseService'];
    function ContestController($rootScope, ContestServerService, FlashService, $scope, FirebaseService) {
        var vm = this;


        vm.showList = false;
        vm.files = [];

        vm.user = $rootScope.globals.currentUser;


        vm.createContest = createContest;
        vm.changeView = changeView;

        vm.time_format = 'MM/DD/YYYY hh:mm A';

        vm.contests = null;
        vm.contest = {};

        initController();

        function initController() {
            configDatepicker();

            configDrop();

            if (vm.showList)
                loadContests();
        }

        function configDrop() {
            function handleFileSelect(evt) {
                evt.stopPropagation();
                evt.preventDefault();

                vm.files = evt.dataTransfer.files; // FileList object.

                // Loop through the FileList and render image files as thumbnails.
                for (var i = 0, f; f = vm.files[i]; i++) {

                    // Only process image files.
                    if (!f.type.match('image.*')) {
                        continue;
                    }

                    var reader = new FileReader();

                    // Closure to capture the file information.
                    reader.onload = (function (theFile) {
                        return function (e) {
                            // Render thumbnail.
                            var span = document.createElement('span');

                            span.innerHTML = ['<img class="thumb" src="', e.target.result,
                                '" title="', escape(theFile.name), '"/>'].join('');
                            document.getElementById('list').insertBefore(span, null);
                        };
                    })(f);

                    // Read in the image file as a data URL.
                    reader.readAsDataURL(f);
                }
            }
            function handleDragOver(evt) {
                evt.stopPropagation();
                evt.preventDefault();
                evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
            }

            // Setup the dnd listeners.
            var dropZone = document.getElementById('drop_zone');
            dropZone.addEventListener('dragover', handleDragOver, false);
            dropZone.addEventListener('drop', handleFileSelect, false);
        }

        function configDatepicker() {

            $('#datetimepicker_start').datetimepicker({
                sideBySide: true,
                format: vm.time_format
            });
            $('#datetimepicker_end').datetimepicker({
                sideBySide: true,
                useCurrent: false,
                format: vm.time_format
            });

            var d = new Date();
            var input = (d).toISOString();
            var fmt = vm.time_format;  // must match the input
            var zone = "UTC";

            var s = moment().tz(input, fmt, zone).utc().format(fmt);

            $('#datetimepicker_start').data("DateTimePicker").minDate(new Date(s));
            $('#datetimepicker_end').data("DateTimePicker").minDate(new Date(s));

            $("#datetimepicker_start").on("dp.change", function (e) {
                $('#datetimepicker_end').data("DateTimePicker").minDate(e.date);

                if (e.timeStamp !== undefined) {
                    var picker = $("#datetimepicker_start").data("DateTimePicker");
                    var d = picker.date();
                    vm.contest.time_start = d.format(vm.time_format);
                }

            });

            $("#datetimepicker_end").on("dp.change", function (e) {
                $('#datetimepicker_start').data("DateTimePicker").maxDate(e.date);

                if (e.timeStamp !== undefined) {
                    var picker = $("#datetimepicker_end").data("DateTimePicker");
                    var d = picker.date();
                    vm.contest.time_end = d.format(vm.time_format);
                }

            });

        }

        function loadContests() {
            delete $rootScope.flash;
            ContestServerService.All()
                    .then(function (response) {
                        if (!response.error) {
                            vm.contests = response;
                            generateHtml();
                        } else {
                            FlashService.Error(response.message);
                        }
                    });
        }

        function createContest() {
            delete $rootScope.flash;
            vm.dataLoading = true;

            var contest = Object.assign({}, vm.contest);

            var date_start = moment(vm.contest.time_start).format(vm.time_format);
            var date_end = moment(vm.contest.time_end).format(vm.time_format);

            contest.time_start = moment.utc(date_start).valueOf();//moment().tz(new Date(vm.contest.time_start), vm.time_format, 'UTC').format();
            contest.time_end = moment.utc(date_end).valueOf();//            moment().tz(new Date(vm.contest.time_end), vm.time_format, 'UTC').format();

            ContestServerService.Create(contest).then(function (response) {
                vm.dataLoading = false;
                console.log(JSON.stringify(response));
                if (!response.error) {
                    if (response._id) {
                        console.log("Contest ID: " + response._id);
                        FirebaseService.UploadContestFiles(response._id, vm.contest.name, vm.files);
                    }
                    changeView(true);
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        }

        function changeView(show) {
            delete $rootScope.flash;
            vm.showList = show;

            if (show)
                loadContests();
            else
            {
                $('#list').html('');
                vm.contest = {};
                $scope.form = {};
                $scope.form.$setUntouched();
                $scope.form.$setPristine();
            }
        }

        function generateContestView(contest) {
            var style = 'height: 60px;width: auto; border: 1px;  margin: 10px 5px 0 0;';
            var htmlImages = '<div class="row"><div class="col-sm-12">';
            if (contest.logos) {
                for (var i = 0; i < contest.logos.length; i++) {
                    htmlImages += '<img id="img_' + i + '_' + contest._id + '" class="thumb" src="' + contest.logos[i] + '" style="' + style + '" />';
                }
            }
            htmlImages += '</div></div>';

            var contentEditable = !(new Date() > new Date(contest.time_end));

            var drop_zone = contentEditable ? '<div class="example"><div class="drop_zone" id="drop_zone_' + contest._id + '">Drop files here</div><output id="list_' + contest._id + '"></output></div>' : '';

            return  '<div class="row" id="row_' + contest._id + '">'
                    + ' <div class="thumbnail" style="padding:10px">'
                    + '     <div class="media">'
                    + '         <div class="media-left media-middle">'
                    + (contentEditable ? '             <a id="update_' + contest._id + '" class="btn btn-primary" role="button" style="margin: 3px;">Update</a>' : '')
                    + '             <a id="delete_' + contest._id + '" class="btn btn-danger" role="button"  style="margin: 3px;">Delete</a>'
                    + '         </div>'
                    + '         <div class="media-body">'
                    //return '<div class="thumbnail luke-text-box" style="padding-bottom:5px">'
                    + '     <table style="margin: 2px auto;table-layout: fixed;">'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">Name:</td>'
                    + '             <td style="padding: 3px 10px;"  align="left">' + contest.name + '</td>'
                    + '         </tr>' + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">Start (UTC):</td>'
                    + '             <td style="padding: 3px 10px;"  align="left">' + moment.utc(moment(new Date(contest.time_start))).format(vm.time_format) + '</td>'
                    + '         </tr>'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">End (UTC):</td>'
                    + '             <td style="padding: 3px 10px;"  align="left" contenteditable="' + contentEditable + '" id="time_end_' + contest._id + '">' + moment.utc(moment(new Date(contest.time_end))).format(vm.time_format) + '</td>'
                    + '         </tr>'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">Rules:</td>'
                    + '             <td style="padding: 3px 10px;"  align="left" contenteditable="' + contentEditable + '"id="link_' + contest._id + '"><a target="_blank" href="' + contest.link + '">' + contest.link + '</td>'
                    + '         </tr>'
                    + '     </table>'
                    + drop_zone
                    + htmlImages
                    + '     Description<div style="margin: 5px;" contenteditable="' + contentEditable + '" id="description_' + contest._id + '">' + contest.description + '</div>'
                    + '         </div>'
                    + '     </div>'
                    + ' </div>'

                    + '</div>'
                    ;
        }

        function  generateHtml() {

            if (!vm.contests || vm.contests.length <= 0) {
                $("#contest_list").html("No Competitions Found!");
                return;
            }

            $("#contest_list").html(
                    "Found " + vm.contests.length + " " + (vm.selectedDetectionStatus ? vm.selectedDetectionStatus : '') + " competitions"
                    + ((vm.selectedDetectionQuintets ? " from " + vm.selectedDetectionQuintets : ''))
                    );

            for (var i = 0; i < vm.contests.length; i++) {
                var contest = vm.contests[i];
                console.log(contest);
                if (!contest || !contest._id || !contest.name || !contest.time_start || !contest.time_end || !contest.link || !contest.description)
                    continue;
                $("#contest_list").append(generateContestView(contest));

                $("#delete_" + contest._id).click((function () {
                    //persist variable on closure
                    var mContest = contest;
                    return function () {
                        var data = {
                            _id: mContest._id
                        };
                        if (confirm("Are you sure you want to delete the competition?") == true) {
                            ContestServerService.Delete(data).then(function (response) {
                                if (!response.error) {
                                    $('#row_' + mContest._id).remove();
                                    FirebaseService.DeleteFiles(mContest.logos);
                                }
                            });
                        }


                    };
                })());

                $("#update_" + contest._id).click((function () {
                    //persist variable on closure
                    var mContest = contest;
                    return function () {
                        var link = $("#link_" + mContest._id).text();
                        var description = $("#description_" + mContest._id).text();
                        var text_time_end = $("#time_end_" + mContest._id).text().trim();


                        if (link.length <= 0) {
                            alert('Link can\'t be blank');
                            return;
                        }
                        if (description.length <= 0) {
                            alert('Description can\'t be blank');
                            return;
                        }
                        if (text_time_end.length <= 0) {
                            alert('Time end can\'t be blank');
                            return;
                        }

//                        if (text_time_end.length !== vm.time_format.length) {
//                            alert('time end need this format: ' + vm.time_format);
//                            return;
//                        }

                        var format_time_end = moment(text_time_end).format(vm.time_format);
                        var utc_time_end = moment.utc(format_time_end).valueOf();

                        if (isNaN(utc_time_end)) {
                            alert('Please, check that time end (' + text_time_end + ') is correct format: ' + vm.time_format);
                            return;
                        }

                        var data = {};

                        if (description.toString() !== mContest.description.toString()) {
                            console.log("description: " + description);
                            data.description = description;
                        }

                        if (link.toString() !== mContest.link.toString()) {
                            console.log("link: " + link);
                            data.link = link;
                        }

                        if (utc_time_end.toString() !== moment.utc(mContest.time_end).valueOf().toString()) {
                            console.log("old time_end: " + moment.utc(mContest.time_end).valueOf());
                            console.log("new time_end: " + utc_time_end);
                            data.time_end = utc_time_end;
                        }



                        if (Object.keys(data).length !== 0) {
                            console.log(JSON.stringify(data));

                            ContestServerService.Update(mContest._id, data).then(function (response) {
                                if (!response.error) {
                                    mContest.description = description;
                                    mContest.link = link;
                                    mContest.time_end = utc_time_end;
                                }
                            });
                        }

                        var logos = [];
                        var logoUris = [];
                        for (var j = 0; j < mContest.logos.length; j++) {
                            if ($('#img_' + j + '_' + mContest._id).css("opacity") < 1) {
                                logos.push(j);
                                logoUris.push(mContest.logos[j]);
                            }
                        }
                        if (logoUris.length !== 0) {
                            console.log(JSON.stringify(logoUris));
                            var dataLogo = {};
                            dataLogo._id = mContest._id;
                            dataLogo.logos = logoUris;

                            ContestServerService.DeleteImages(dataLogo).then(function (response) {
                                if (!response.error) {
                                    FirebaseService.DeleteFiles(logoUris);
                                    for (var j = 0; j < logos.length; j++) {
                                        $('#img_' + j + '_' + mContest._id).remove();
                                    }
                                }
                            });
                        }

                        function dataURItoBlob(dataURI, name) {
                            // convert base64/URLEncoded data component to raw binary data held in a string
                            var byteString;
                            if (dataURI.split(',')[0].indexOf('base64') >= 0)
                                byteString = atob(dataURI.split(',')[1]);
                            else
                                byteString = unescape(dataURI.split(',')[1]);

                            // separate out the mime component
                            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                            // write the bytes of the string to a typed array
                            var ia = new Uint8Array(byteString.length);
                            for (var i = 0; i < byteString.length; i++) {
                                ia[i] = byteString.charCodeAt(i);
                            }

                            return new Blob([ia], {type: mimeString, name: name});
                        }

                        var newLogos = [];
                        $('#list_' + mContest._id + ' span img').each(function (index) {
                            console.log(index + " " + $(this).attr('title'));
                            newLogos.push(dataURItoBlob($(this).attr('src'), $(this).attr('title')));
                            $(this).remove();
                        });

                        if (newLogos.length !== 0) {
                            FirebaseService.UploadContestFiles(mContest._id, mContest.name, newLogos);
                            $('#list_' + mContest._id).html('');
                        }


                        if (Object.keys(data).length === 0 && logoUris.length === 0 && newLogos.length === 0) {
                            alert('Nothing to update.');
                            return;
                        }


                    };
                })());

                if (contest.logos) {
                    for (var j = 0; j < contest.logos.length; j++) {
                        $('#img_' + j + '_' + contest._id).click((function () {
                            //persist variable on closure
                            var mContest = contest;
                            var pos = j;
                            return function () {
                                var id = '#img_' + pos + '_' + mContest._id;
                                $(id).css({opacity: $(id).css("opacity") == 1 ? 0.5 : 1});
                            };
                        })());

                    }
                }


                //////////////

                // Setup the dnd listeners.
                var dropZone = document.getElementById('drop_zone_' + contest._id);
                if (dropZone) {
                    dropZone.addEventListener('dragover', function (evt) {
                        evt.stopPropagation();
                        evt.preventDefault();
                        evt.dataTransfer.dropEffect = 'copy';
                    }, false);

                    dropZone.addEventListener('drop', (function () {
                        //persist variable on closure
                        var mContest = contest;
                        return  function (evt) {
                            evt.stopPropagation();
                            evt.preventDefault();

                            mContest.files = evt.dataTransfer.files; // FileList object.

                            // Loop through the FileList and render image files as thumbnails.
                            for (var i = 0, f; f = mContest.files[i]; i++) {

                                // Only process image files.
                                if (!f.type.match('image.*')) {
                                    continue;
                                }

                                var reader = new FileReader();

                                // Closure to capture the file information.
                                reader.onload = (function (theFile) {
                                    return function (e) {
                                        // Render thumbnail.
                                        var span = document.createElement('span');

                                        span.innerHTML = ['<img class="thumb" src="', e.target.result,
                                            '" title="', escape(theFile.name), '"/>'].join('');
                                        document.getElementById('list_' + mContest._id).insertBefore(span, null);
                                    };
                                })(f);

                                // Read in the image file as a data URL.
                                reader.readAsDataURL(f);
                            }
                        }
                        ;
                    })(), false);

                }
                ///////////////

            }

        }
    }

})();