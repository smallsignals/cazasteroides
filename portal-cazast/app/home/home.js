(function () {
    'use strict';

    angular
            .module('app')
            .controller('HomeController', HomeController);

    HomeController.$inject = ['$rootScope', 'UserServerService', 'ScriptServerService', 'QuintetServerSercive'];
    function HomeController($rootScope, UserServerService, ScriptServerService, QuintetServerSercive) {
        var vm = this;

        vm.user = $rootScope.globals.currentUser;
        vm.me = null;

        initController();

        function initController() {
//            MenuService.ConfigMenu(vm.user);
            loadMe();
            generateHtml();
        }

        function loadMe() {
            UserServerService.Me()
                    .then(function (response) {
                        if (!response.error) {
                            vm.me = response;
                            generateHtml();
                        }
                    });
        }

        function generateHtml() {
            if (!vm.me) {
                $("#home_info").html('');
                return;
            }
            var user = vm.me;

            var html =
                    '<div class="thumbnail" id=row_' + user._id + '" style="margin-top:20px;">'
                    + ' <div class="media" style="padding:10px">'
                    + '     <div class="container">'
                    + '         <div class="row">'
                    + '             <h2>' + user.username + '</h2>'
                    + '             <h5>' + user.email + '</h4>'
                    + '             <div class="col-xs-4">'
                    + '                 <h3 class="text-center">Rol</h3>'
                    + '                 <div class="well" style="max-height: 300px;overflow: auto;">'
                    + '                     <ul class="list-group checked-list-box">';

            if (user.superAdmin)
                html += '                     <li id="li_superAdmin" class="list-group-item" data-style="button" data-color="danger">Super Admin</li>';
            if (user.admin)
                html += '                     <li id="li_admin" class="list-group-item" data-style="button">Astronomer</li>';

            html += '                     </ul>'
                    + '                 </div>'
                    + '             </div>';

            if (user.superAdmin)
                html += '           <div class="col-xs-4">'
                        + '             <h3 class="text-center">Scripts</h3>'
                        + '             <div class="well" style="max-height: 300px;overflow: auto;">'
                        + '                 <a id="script_sync" class="btn btn-primary btn-large btn-block" role="button" >Sync Quintets</a>'
                        + '                 <a id="script_infoSync" class="btn btn-primary btn-large btn-block" role="button" >Sync Quintets Info</a>'
                        + '                 <a id="script_avgCoordSync" class="btn btn-primary btn-large btn-block" role="button" >Sync Avg Coord Detections</a>'
                        + '                 <a id="script_generateCandidates" class="btn btn-primary btn-large btn-block" role="button" >Generate New Approved</a>'
                        + '             </div>'
                        + '         </div>';

            html += '             <div class="col-xs-4">'
                    + '                 <h3 class="text-center">Telescopes</h3>'
                    + '                 <div class="well" style="max-height: ' + parseInt($(window).height() / 2) + 'px;overflow: auto;">'
                    + '                     <ul id="check-list-box" class="list-group checked-list-box">';

            for (var k in  user.adminTelescopes) {
                var li = '<li id="telescope_li_' + k + '" class="list-group-item">' + user.adminTelescopes[k] + '</li>';
                html += li;
            }

            html += '                   </ul>'
                    + '                 </div>'
                    + '             </div>'

                    + '           <div class="col-xs-4">'
                    + '             <h3 class="text-center">Quintets with Pendings</h3>'
                    + '             <div id="quintets_with_pendings" class="well" style="max-height: 300px;overflow: auto;">'
                    + '             </div>'
                    + '          </div>'

                    + '           <div class="col-xs-4">'
                    + '             <h3 class="text-center">Quintets near disabled</h3>'
                    + '             <div id="quintets_neas_disabled" class="well" style="max-height: 300px;overflow: auto;">'
                    + '             </div>'
                    + '          </div>'

                    + '         </div>'
                    + '     </div>'
                    + ' </div>'
                    + '</div>'
                    ;

            $("#home_info").html(html);

            $('#script_sync').on('click', function (event) {
                event.preventDefault();

                ScriptServerService.Sync();

            });
            $('#script_avgCoordSync').on('click', function (event) {
                event.preventDefault();

                ScriptServerService.avgCoordSync();

            });
            $('#script_infoSync').on('click', function (event) {
                event.preventDefault();

                ScriptServerService.infoSync();

            });

            $('#script_generateCandidates').on('click', function (event) {
                event.preventDefault();

                ScriptServerService.newCandidates();

            });


            QuintetServerSercive.QuintetsWithPendings().then(function (quintet) {
                if (!quintet) {
                    return;
                }
                var html = '<ul class="list-group checked-list-box">';
                for (var i = 0; i < quintet.length; i++) {
                    html += '   <li class="list-group-item" data-style="button">' + (quintet[i].active ? ' ' : '*') +quintet[i].name + ':  ' + quintet[i].total_pending + '</li>';
                }
                html += '</ul>';
                $("#quintets_with_pendings").html(html);
            });

            QuintetServerSercive.NearThresholdToDesactivate().then(function (quintet) {
                if (!quintet) {
                    return;
                }
                var html = '<ul class="list-group checked-list-box">';
                for (var i = 0; i < quintet.length; i++) {
                    html += '   <li class="list-group-item" data-style="button">' + quintet[i].name + ': ' + quintet[i].percent.toFixed(2) + '%</li>';
                }
                html += '</ul>';
                $("#quintets_neas_disabled").html(html);
            });
        }
    }

})();