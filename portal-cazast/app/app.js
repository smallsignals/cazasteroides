(function () {
    'use strict';
    angular
            .module('app', ['ngRoute', 'ngCookies'])
            .config(config)
            .constant('domain', /*'http://161.72.206.47:3000')// */ 'http://api.cazasteroides.org')
            .constant('domain_img', 'http://api.cazasteroides.org')
            .constant('sync_script', '../../scripts/sync_process_quintets.py')
            .factory('myService', function () {
                var savedData = {};
                function set(data) {
                    savedData = data;
                }
                function get() {
                    return savedData;
                }
                return {set: set, get: get};
            })
            .run(run);
    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {

        $routeProvider

                .when('/login', {
                    controller: 'LoginController',
                    templateUrl: 'login/login.html',
                    controllerAs: 'vm'
                })

                .when('/', {
                    controller: 'HomeController',
                    templateUrl: 'home/home.html',
                    controllerAs: 'vm'
                })

                .when('/detections', {
                    controller: 'DetectionsController',
                    templateUrl: 'detections/detections.html',
                    controllerAs: 'vm'
                })

                .when('/quintets', {
                    controller: 'QuintetsController',
                    templateUrl: 'quintets/quintets.html',
                    controllerAs: 'vm'
                })

                .when('/astronomers', {
                    controller: 'AstronomersController',
                    templateUrl: 'astronomers/astronomers.html',
                    controllerAs: 'vm'
                })

                .when('/quintet', {
                    controller: 'QuintetController',
                    templateUrl: 'quintet/quintet.html',
                    controllerAs: 'vm'
                })

                .when('/contests', {
                    controller: 'ContestController',
                    templateUrl: 'contest/contest.html',
                    controllerAs: 'vm'
                })

                .when('/setups', {
                    controller: 'SetupController',
                    templateUrl: 'setup/setup.html',
                    controllerAs: 'vm'
                })

                .when('/approved', {
                    controller: 'ApprovedController',
                    templateUrl: 'approved/approved.html',
                    controllerAs: 'vm'
                })

                .otherwise({redirectTo: '/login'});
    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http', 'MenuService'];
    function run($rootScope, $location, $cookies, $http, MenuService) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'BEARER ' + $rootScope.globals.currentUser.token;
        }

        $http.defaults.headers.common['Accept'] = 'application/json';
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            MenuService.ConfigMenu($rootScope.globals.currentUser);
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && (loggedIn && !loggedIn.token)) {
                $location.path('/login');
            }
        });
    }

})();