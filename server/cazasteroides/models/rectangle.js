var mongoose = require('mongoose');


// Project Schema
var rectangleSchema = mongoose.Schema({
    "image": {"type": mongoose.Schema.Types.ObjectId, ref: 'Image'},
    "minx": Number,
    "miny": Number,
    "maxx": Number,
    "maxy": Number,
    "scalemin": Number,
    "scalemax": Number,
    "scaleminArr": [],
    "scalemaxArr": [],
    "user": {"type": mongoose.Schema.Types.ObjectId, ref: 'User'}
});

module.exports = mongoose.model('Rectangle', rectangleSchema);
