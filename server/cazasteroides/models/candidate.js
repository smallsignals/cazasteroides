var mongoose = require('mongoose');
require("./user.js");
require("./image.js");

var telescopeSchema = mongoose.Schema({
    "name": String,
    "ocn": String,
    "observer": String
}, {_id: false});

var coordSchema = mongoose.Schema({
    "line": String,
    "telescope": telescopeSchema,
    "coord": [Number],
    "timestamp": String,
    "magnitude": Number,
    "quintet": {"type": mongoose.Schema.Types.ObjectId, ref: 'Image'},
    "quintetn": Number,
    "increase": {type: [Number], default: [0, 0]},
    "gif": String //gif -> detection _id
}, {_id: false});

var candidateSchema = mongoose.Schema({
    "designation": {
        type: String,
        unique: true,
        required: true
    },
    "coordenates": [coordSchema],
    "users": [{"type": mongoose.Schema.Types.ObjectId, ref: 'User'}],
    "isKnown": Boolean,
    "tmpDesignation": {
        type: String,
        unique: true,
        required: true
    },
    "mpcReportState": {type: Number, default: -1}, //-1 Not Reported, 0 Reported, 1 mpc Response(200,ok), 2 mpc Response (!=200,fail)
    "discoverer": {"type": mongoose.Schema.Types.ObjectId, ref: 'User'}
});
module.exports = mongoose.model('Candidate', candidateSchema);
