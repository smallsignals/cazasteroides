var mongoose = require('mongoose');
require("./user.js");
// Project Schema
var coordSchema = mongoose.Schema({
    "x": Number, //ar
    "y": Number, //dec
    "user": {"type": mongoose.Schema.Types.ObjectId, ref: 'User'},
    "points": Number,
    "time": {type: Date, default: Date.now},
    "displayToUser": {type: Boolean, default: true},
    magnitude: Number
}, {_id: false});

var observationSchema = mongoose.Schema({
    "image": {"type": mongoose.Schema.Types.ObjectId, ref: 'Image'},
    "rect": {"type": mongoose.Schema.Types.ObjectId, ref: 'Rectangle'},
    "pixCoord": [Number],
    "pixCoordAbs": [Number],
    "revisit": [coordSchema],
    "votes": [{"type": mongoose.Schema.Types.ObjectId, ref: 'User'}],
    "downvotes": [{"type": mongoose.Schema.Types.ObjectId, ref: 'User'}],
    "status": {"type": String, "enum": ["approved", "rejected", "pending"]},
    "object": Object,
    "rejectedCode": Number,
    "quintetn": Number,
    "avgCoord": [Number],
    "Coord": [Number], //[ar,dec]
    "user": {"type": mongoose.Schema.Types.ObjectId, ref: 'User'},
    "points": Number,
    "time": {type: Date, default: Date.now},
    "displayToUser": {type: Boolean, default: true},
    magnitude: Number,
    avgMagnitude: Number
});

module.exports = mongoose.model('Observation', observationSchema);
