var mongoose = require('mongoose');

var GCMSchema = new mongoose.Schema({
    token: {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true}
});

module.exports = mongoose.model('GCM', GCMSchema);
