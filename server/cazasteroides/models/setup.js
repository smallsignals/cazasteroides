var mongoose = require('mongoose');

var setupSchema = mongoose.Schema({
    config: [{
            key: String,
            value: String
        }]
});

module.exports = mongoose.model('Setup', setupSchema);
