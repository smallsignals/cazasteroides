var mongoose = require('mongoose');

// Project Schema
var telescopeSchema = mongoose.Schema({
    name: {unique: true, type: String, required: true},
    lat: {type: String},
    long: {type: String},
    alt: {type: String},
    ocn: {type: String},
    detections: {type: Number, default: 0},
    candidates: {type: Number, default: 0},
    newAst: {type: Number, default: 0}
});

module.exports = mongoose.model('Telescope', telescopeSchema);
