// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var detectionsSchema = mongoose.Schema({
    total: {type: Number, default: 0},
    approved: {type: Number, default: 0},
    points: {type: Number, default: 0},
    rejected: {type: Number, default: 0}
}, {_id: false});

var votesSchema = mongoose.Schema({
    total: {type: Number, default: 0},
    corrects: {type: Number, default: 0},
    points: {type: Number, default: 0},
    wrongs: {type: Number, default: 0}
}, {_id: false});

var notificationsSendSchema = mongoose.Schema({
    "newQuintetsAvailableEmail": {type: Date}
}, {_id: false});

var constestSchema = mongoose.Schema({
    points: {type: Number, default: 0},
    contest: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Contest'
    }
}, {_id: false});
// Define our user schema
var UserSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    admin: {type: Boolean},
    superAdmin: {type: Boolean},
    adminTelescopes: [String],
    provider: {type: String},
    icon: {type: String},
    password: {type: String},
    email: {type: String},
    country: {type: String},
    active: {type: Boolean},
    points: {type: Number},
    badges: [String],
    providerId: {type: String},
    contests: [constestSchema],
//    contests: [{
//            points: {type: Number, default: 0},
//            contest: {
//                type: mongoose.Schema.Types.ObjectId,
//                ref: 'Contest'
//            }
//        }],
    sDetections: detectionsSchema,
    sVotes: votesSchema,
    notification: notificationsSendSchema,
    last_auth: {type: Date},
    accept_language: {type: String}
});


//var UserModel = mongoose.model('User', UserSchema);
// Execute before each user.save() call
UserSchema.pre('save', function (callback, req) {
    var user = this;
    //console.log(req);
    if (req && req.facebookProfile) {
        var profile = req.facebookProfile;

        /**
         * Update old system to new system
         */
        if (user.username == profile.id && user.provider == 'fiware') {
            user.providerId = profile.id;
            user.provider = 'facebook';
        }


        var providerName = undefined;
        if (!providerName) {
            try {
                if (profile.displayName && profile.displayName.replace(/\s+/g, '').length > 0) {
                    providerName = profile.displayName.replace(/\s+/g, '');
                } else if (profile.name && (profile.name.givenName + profile.name.middleName + profile.name.familyName).replace(/\s+/g, '').length > 0) {
                    providerName = (profile.name.givenName + profile.name.middleName + profile.name.familyName).replace(/\s+/g, '');
                } else if (profile._json && profile._json.name && profile._json.name.replace(/\s+/g, '').length > 0) {
                    providerName = profile._json.name.replace(/\s+/g, '');
                } else if (profile._json && profile._json.first_name && profile._json.first_name.replace(/\s+/g, '').length > 0) {
                    providerName = profile._json.first_name.replace(/\s+/g, '');
                }
            } catch (exception) {

            }
        }

        //Fix Facebook Usernames
        if (providerName && profile.id == user.username) {
            var UserModel = require('./user');
            UserModel.find({"username": {$regex: "^" + providerName}}, {username: 1})
                    .sort({"username": -1})
                    .exec(function (err, docs) {
                        if (docs.length <= 0) {
                            user.username = providerName;
                        } else {
                            var count = -1;
                            var existEqual = false;
                            for (var i = 0; i < docs.length; i++) {
                                var sub = docs[i].username.substr(providerName.length);
                                if (isNaN(sub) && parseInt(sub) > count) {
                                    count = parseInt(sub);
                                }
                                if (providerName == docs[i].username) {
                                    existEqual = true;
                                }
                            }
                            user.username = providerName + "" + (existEqual ? (count + 1) : "");
                        }
                        return  callback();
                    });
        } else {
            return callback();
        }
    } else {
        // Break out if the password hasn't changed
        if (!user.isModified('password'))
            return callback();
        // Password changed so we need to hash it
        bcrypt.genSalt(5, function (err, salt) {
            if (err)
                return callback(err);
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err)
                    return callback(err);
                user.password = hash;
                callback();
            });
        });
    }
});
UserSchema.methods.verifyPassword = function (password, cb) {
    bcrypt.compare(password, this.password, function (err, isMatch) {
        if (err)
            return cb(err);
        cb(null, isMatch);
    });
};
// Export the Mongoose model
module.exports = mongoose.model('User', UserSchema);
