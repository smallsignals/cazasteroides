var mongoose = require('mongoose');

// Project Schema
var contestSchema = mongoose.Schema({
    name: {unique: true, type: String, required: true},
    time_start: {type: Date, required: true},
    time_end: {type: Date, required: true},
    description: {type: String, required: true},
    link: {type: String, required: true},
    logos: [String]
});

module.exports = mongoose.model('Contest', contestSchema);
