var mongoose = require('mongoose');

// Project Schema
var imageSchema = mongoose.Schema({
    "name": String,
    "filename": [String],
    "mapfile": String,
    "fecha": Date,
    "height": Number,
    "width": Number,
//    "xpoly": [Number],
//    "ypoly": [Number],
//    "minx": Number,
//    "miny": Number,
//    "maxx": Number,
//    "maxy": Number,
    "active": Boolean,
    "meta": Object,
    "histo": Object,
    "coeff": [],
    "info": Object,
    "generatedCandidates": {type: Boolean, default: false},
    "disabled_by_threshold": Boolean
});

module.exports = mongoose.model('Image', imageSchema);
