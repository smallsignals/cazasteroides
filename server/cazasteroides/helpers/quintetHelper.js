/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Quintet = require('../models/image.js');

exports.increaseAreaShown = function (id, area) {
    Quintet.findById(id)
            .exec()
            .then(function (doc) {
                if (doc) {
                    if (doc['info'] && doc['info']['total_area_shown']) {
                        var query = {$inc: {}};
                        query['$inc']['info.total_area_shown'] = area;
                        Quintet.findOneAndUpdate({"_id": id}, query, {new : true}, function (err, image) {
                            //console.log("inc " + JSON.stringify(err));
                            //console.log("inc " + JSON.stringify(image));
                            //TODO Notify astronomers that the quintet is about to be deactivated
                        });
                    } else {
                        var query = {$set: {}};
                        query['$set']['info.total_area_shown'] = area;
                        Quintet.findOneAndUpdate({"_id": id}, query, {new : true}, function (err, image) {
                            //console.log("set " + JSON.stringify(err));
                            //console.log("set " + JSON.stringify(image));
                        });
                    }
                }
            });
};

exports.increaseDetections = function (id) {
    Quintet.findById(id)
            .exec()
            .then(function (doc) {
                if (doc) {
                    if (doc['info'] && doc['info']['total_detections']) {
                        var query = {$inc: {}};
                        query['$inc']['info.total_detections'] = 1;
                        Quintet.findOneAndUpdate({"_id": id}, query, function (err, image) {
                            //console.log("inc " + JSON.stringify(err));
                            //console.log("inc " + JSON.stringify(image));
                        });
                    } else {
                        var query = {$set: {}};
                        query['$set']['info.total_detections'] = 1;
                        Quintet.findOneAndUpdate({"_id": id}, query, function (err, image) {
                            //console.log("set " + JSON.stringify(err));
                            //console.log("set " + JSON.stringify(image));
                        });
                    }
                }
            });
};

exports.increaseKnow = function (id) {
    Quintet.findById(id)
            .exec()
            .then(function (doc) {
                if (doc) {
                    if (doc['info'] && doc['info']['total_know']) {
                        var query = {$inc: {}};
                        query['$inc']['info.total_know'] = 1;
                        Quintet.findOneAndUpdate({"_id": id}, query, function (err, image) {
                            //console.log("inc " + JSON.stringify(err));
                            //console.log("inc " + JSON.stringify(image));
                        });
                    } else {
                        var query = {$set: {}};
                        query['$set']['info.total_know'] = 1;
                        Quintet.findOneAndUpdate({"_id": id}, query, function (err, image) {
                            //console.log("set " + JSON.stringify(err));
                            //console.log("set " + JSON.stringify(image));
                        });
                    }
                }
            });
};

exports.increaseApproved = function (id) {
    Quintet.findById(id)
            .exec()
            .then(function (doc) {
                if (doc) {
                    if (doc['info'] && doc['info']['total_approved']) {
                        var query = {$inc: {}};
                        query['$inc']['info.total_approved'] = 1;
                        Quintet.findOneAndUpdate({"_id": id}, query, function (err, image) {
                            //console.log("inc " + JSON.stringify(err));
                            //console.log("inc " + JSON.stringify(image));
                        });
                    } else {
                        var query = {$set: {}};
                        query['$set']['info.total_approved'] = 1;
                        Quintet.findOneAndUpdate({"_id": id}, query, function (err, image) {
                            //console.log("set " + JSON.stringify(err));
                            //console.log("set " + JSON.stringify(image));
                        });
                    }
                }
            });
};