/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var User = require('../models/user.js');

exports.increaseDetection = function (id) {
    User.findOneAndUpdate({"_id": id}, {$inc: {'sDetections.total': 1}}, function (err, u) {
        console.log("increaseDetection " + JSON.stringify(err));
    });
};

exports.increaseDetectionApproved = function (id, points) {
    User.findOneAndUpdate({"_id": id}, {$inc: {'sDetections.approved': 1, 'sDetections.points': points}}, function (err, u) {
        console.log("increaseDetectionApproved " + JSON.stringify(err));
    });
};

exports.increaseDetectionRejected = function (id) {
    User.findOneAndUpdate({"_id": id}, {$inc: {'sDetections.rejected': 1}}, function (err, u) {
        console.log("increaseDetectionRejected " + JSON.stringify(err));
    });
};

exports.increaseVote = function (id) {
    User.findOneAndUpdate({"_id": id}, {$inc: {'sVotes.total': 1}}, function (err, u) {
        console.log("increaseVote " + JSON.stringify(err));
    });
};

exports.increaseVoteCorrect = function (id, points) {
    User.findOneAndUpdate({"_id": id}, {$inc: {'sVotes.corrects': 1, 'sVotes.points': points}}, function (err, u) {
        console.log("increaseVoteCorrect " + JSON.stringify(err));
    });
};

exports.increaseVoteWrong = function (id) {
    User.findOneAndUpdate({"_id": id}, {$inc: {'sVotes.wrongs': 1}}, function (err, u) {
        console.log("increaseVoteWrong " + JSON.stringify(err));
    });
};
