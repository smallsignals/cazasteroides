/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const Score = require('../helpers/scoreCalculation.js');
var path = require('path');
var request = require('request');
var GIFEncoder = require('gifencoder');
var fs = require('fs');
var gm = require('gm');
var configConst = require("../config_template");
var Setup = require('../models/setup.js');

exports.rebuild_gif = function (obs, res, callback) {
    //console.log(JSON.stringify(obs));

    if (!fs.existsSync(configConst.SRC_FOLDER_GIF)) {
        fs.mkdirSync(configConst.SRC_FOLDER_GIF);
    }
    //console.log("SRC_FOLDER_GIF: " + configConst.SRC_FOLDER_GIF);
    if (!obs || !obs._id || !obs.image || !obs.pixCoord || !obs.pixCoordAbs || !obs.Coord || !obs.revisit) {
        console.log("Observation: " + (obs && obs._id ? obs._id : "undefined") + " not exists image");
        if (callback) {
            callback();
        }
        return;
    }

    var gifWidth = 200;
    var gifHeight = 200;

    var imgheight = obs.image.height;

    var center = [parseFloat(gifWidth / 2), parseFloat(gifHeight / 2)];

    var encoder = new GIFEncoder(gifWidth, gifHeight);
    // stream the results as they are available into myanimated.gif

    var pixCoordAbsX = obs.pixCoordAbs && obs.pixCoordAbs[0] ? obs.pixCoordAbs[0] : undefined;
    var pixCoordAbsY = obs.pixCoordAbs && obs.pixCoordAbs[1] ? obs.pixCoordAbs[1] : undefined;

    try {

        var metadata = obs.image.meta;
        if (obs.image.coeff && (obs.image.coeff[obs.quintetn] || obs.image.coeff[0])/* && img.coeff[index].BZERO*/) {
            try {
                var arr = obs.image.coeff[obs.quintetn];
                if (obs.image.coeff[0] && obs.image.coeff[0][obs.quintetn]) {
                    metadata = obs.image.coeff[0][obs.quintetn];
                }
            } catch (e) {
                console.log("Ups", e);
            }
        }

        var pix2coorHelper = require('../helpers/pix2coor.js');

        var repix = pix2coorHelper.coord2pix(parseFloat(obs.avgCoord[0]), parseFloat(obs.avgCoord[1]), metadata);

        pixCoordAbsX = repix[0];
        pixCoordAbsY = repix[1];
    } catch (e) {

    }

    if (isNaN(pixCoordAbsX)) {
        pixCoordAbsX = obs.pixCoordAbs && obs.pixCoordAbs[0] ? obs.pixCoordAbs[0] : undefined;
    }
    if (isNaN(pixCoordAbsY)) {
        pixCoordAbsY = obs.pixCoordAbs && obs.pixCoordAbs[1] ? obs.pixCoordAbs[1] : undefined;
    }

    //console.log("Gif[00] pixCoordAbs = " + pixCoordAbsX + "  " + pixCoordAbsY);

    /**
     * 
     */
    var out_filename = path.join(configConst.SRC_FOLDER_GIF, obs._id + '_' + pixCoordAbsX.toFixed(5) + '-' + pixCoordAbsY.toFixed(5) + '.gif');
    if (fs.existsSync(out_filename)) {
        console.log("Observation: " + obs._id + " gif already exists");
        if (callback) {
            callback(out_filename);
        }
        return;
    }

    var glob = require("glob");
    glob(path.join(configConst.SRC_FOLDER_GIF, obs._id + '*'), null, function (er, files) {
        for (var i = 0; i < files.length; i++) {
            if (out_filename != files[i]) {
                try {
                    fs.unlinkSync(files[i]);
                } catch (e) {

                }
            }

        }
    });

    //This values need get from setup
    var sigma_detection = 0.0028;
    var res_image_end = 0.6;
    Setup.findOne({}, function (err, setup) {
        if (setup) {
            var indexResImageEnd = setup.config.findIndex(function (element) {
                return element.key.toString() === "res_image_end";
            });
            var indexSigmaDetection = setup.config.findIndex(function (element) {
                return element.key.toString() === "sigma_detection";
            });

            if (indexResImageEnd >= 0 && indexResImageEnd < setup.config.length) {
                res_image_end = parseFloat(setup.config[indexResImageEnd].value);
            }

            if (indexSigmaDetection >= 0 && indexSigmaDetection < setup.config.length) {
                sigma_detection = parseFloat(setup.config[indexSigmaDetection].value);
            }
        }

        //console.log("Gif[00] filename = " + out_filename);
        encoder.createReadStream().pipe(fs.createWriteStream(out_filename));
        encoder.start();
        encoder.setRepeat(0); // 0 for repeat, -1 for no-repeat
        encoder.setDelay(500); // frame delay in ms
        encoder.setQuality(10); // image quality. 10 is default.

        var spatial_scale = obs.image.meta && obs.image.meta.SCALE ? parseFloat(obs.image.meta.SCALE) : 1;
        if (!spatial_scale)
            spatial_scale = 1;

        var w = parseFloat(gifWidth / 2) * res_image_end / spatial_scale;
        var h = parseFloat(gifHeight / 2) * res_image_end / spatial_scale;
        var rect = {};
        rect.minx = pixCoordAbsX - w;
        rect.maxx = pixCoordAbsX + w;
        rect.miny = imgheight - pixCoordAbsY - h;
        rect.maxy = imgheight - pixCoordAbsY + h;

        var sigma_detection_value = parseFloat(sigma_detection * 3600);

//        console.log("Gif[" + obs._id + "] obs.image.meta.SCALE:" + obs.image.meta.SCALE);
//        console.log("Gif[" + obs._id + "] spatial_scale:" + spatial_scale);
//        console.log("Gif[" + obs._id + "] res_image_end:" + res_image_end);
//        console.log("Gif[" + obs._id + "] gifWidth:" + gifWidth);
//        console.log("Gif[" + obs._id + "] gifHeight:" + gifHeight);
//        console.log("Gif[" + obs._id + "] pixCoordAbsX:" + pixCoordAbsX);
//        console.log("Gif[" + obs._id + "] w:" + w);
//        console.log("Gif[" + obs._id + "] imgheight:" + imgheight);
//        console.log("Gif[" + obs._id + "] pixCoordAbsY:" + pixCoordAbsY);
//        console.log("Gif[" + obs._id + "] h:" + h);
//        console.log("Gif[" + obs._id + "] rect:" + JSON.stringify(rect));
//        console.log("Gif[" + obs._id + "] sigma_detection:" + sigma_detection);
//        console.log("Gif[" + obs._id + "] sigma_detection_value:" + sigma_detection_value);

        var count = 0;
        var add = (function () {
            return function (err, resp, buffer) {
                fs.writeFile(path.join(configConst.SRC_FOLDER_GIF, obs._id + "_" + count + '.png'),
                        buffer, 'binary', function (err)
                        {
                            gm(path.join(configConst.SRC_FOLDER_GIF, obs._id + "_" + count + '.png'))
                                    .stroke((!obs.quintetn && 0 == count) || (obs.quintetn == count) ? '#218D3F' : '#ffb2b2')
                                    .strokeWidth(2)
                                    .fill("None")
                                    .drawRectangle(center[0] - sigma_detection_value, center[1] - sigma_detection_value,
                                            center[0] + sigma_detection_value, center[1] + sigma_detection_value, 3, 3)
                                    .stroke("#10ff10")
                                    .font("Helvetica.ttf", 12)
                                    .toBuffer('RGBA', function (error, data) {
                                        try {
                                            //console.log("gm returns: " + error);
                                            //console.log("Gif[2]: add" + count);
                                            //console.log("Gif[2]: buff length:" + buffer.length);
                                            //console.log("Gif[2]: " + buffer.toString("utf-8", 0, 12));
                                            encoder.addFrame(data);
                                            count++;
                                            if (count < 5) {
                                                nextfunc();
                                            } else {
                                                encoder.finish();
                                                //console.log("*************************************************************************");
                                                console.log("Gif[" + obs._id + "]: finised");
                                                for (var ind = 0; ind < 6; ind++) {
                                                    var pathpng = path.join(configConst.SRC_FOLDER_GIF, obs._id + "_" + ind + '.png');
                                                    if (fs.existsSync(pathpng)) {
                                                        try {
                                                            fs.unlinkSync(pathpng);
                                                        } catch (e) {

                                                        }
                                                    }

                                                }
                                                if (callback) {
                                                    callback(out_filename);
                                                }
                                            }
                                        } catch (err) {
                                            console.log("Gif[" + obs._id + "]: error GIF: " + err.message);
                                            if (count < 5) {
                                                count++;
                                                nextfunc();
                                            } else {
                                                encoder.finish();
                                                //console.log("*************************************************************************");
                                                console.log("Gif[" + obs._id + "]: finised with error: " + err.message);
                                                for (var ind = 0; ind < 6; ind++) {
                                                    var pathpng = path.join(configConst.SRC_FOLDER_GIF, obs._id + "_" + ind + '.png');
                                                    if (fs.existsSync(pathpng)) {
                                                        try {
                                                            fs.unlinkSync(pathpng);
                                                        } catch (e) {

                                                        }
                                                    }
                                                }

                                                if (fs.existsSync(out_filename)) {
                                                    try {
                                                        fs.unlinkSync(out_filename);
                                                    } catch (e) {

                                                    }
                                                }

                                                if (callback) {
                                                    callback();
                                                }
                                            }
                                        }
                                    }); //fin parse png
                        }); //fin wscribearchivotemp
            };
        });
        var nextfunc = function () {
            try {
                var mapFileUri = obs.image.mapfile;
                var pathSearch = '/cazasteroides/data/';
                if (mapFileUri.indexOf(pathSearch) >= 0) {
                    mapFileUri = configConst.SRC_DATA + mapFileUri.substring(mapFileUri.indexOf(pathSearch) + pathSearch.length, mapFileUri.length);
                } else {
                    mapFileUri = configConst.SRC_DATA + obs.image.mapfile;
                }
                //console.log(obs.image.mapfile + " conver to " + mapFileUri);
                var map = 'map=' + mapFileUri + '&';

                var r = obs.rect ? obs.rect : {};

                if (r.scaleminArr && r.scaleminArr.length === 5) {
                    r.scalemin = r.scaleminArr[count];
                }
                if (r.scalemaxArr && r.scalemaxArr.length === 5) {
                    r.scalemax = r.scalemaxArr[count];
                }

                try {

                    var metadata = obs.image.meta;
                    if (obs.image.coeff && (obs.image.coeff[count] || obs.image.coeff[0])) {
                        try {
                            if (obs.image.coeff[0] && obs.image.coeff[0][count]) {
                                metadata = obs.image.coeff[0][count];
                            }
                        } catch (e) {
                            console.log("Ups", e);
                        }
                    }

                    var bzero = 0;
                    if (metadata.BZERO) {
                        bzero = parseFloat(metadata.BZERO);
                    }
                    metadata.SKY = parseFloat(metadata.SKY) - bzero;

                    var sig = 5;

                    if (!r.scalemin) {
                        r.scalemin = parseFloat(metadata.SKY) - (sig * parseFloat(metadata.SIGMA));
                    }
                    if (!r.scalemax) {
                        r.scalemax = parseFloat(metadata.SKY) + (sig * parseFloat(metadata.SIGMA));
                    }
                } catch (e) {

                }

                console.log("scalemin: " + r.scalemin + "scalemax" + r.scalemax);
                var tscalemin = 2000;
                if (r.scalemin)
                    tscalemin = r.scalemin;
                var tscalemax = 2500;
                if (r.scalemax)
                    tscalemax = r.scalemax;

//            var imsizex = 200;
//            var imsizey = 200;

                var mapsize = '&MSCALE=' + tscalemin + ',' + tscalemax + '&mapsize=' + gifWidth + '%20' + gifHeight + '&';
                var mapext = "MAPEXT=" + rect.minx + "%20" + rect.miny + "%20" + rect.maxx + "%20" + rect.maxy + "&";
                var layer = 'layer=img' + (count + 1);
                var reqstr = configConst.URL_MAPSERV_CGI_BIN + map + 'mode=map&' + mapsize + mapext + layer;
                //console.log(reqstr);
                request.get(reqstr, {encoding: 'binary'}, add());
            } catch (err) {
                console.log("Gif[6]: error request:" + err.message);
                if (count < 5) {
                    count++;
                    nextfunc();
                } else {
                    encoder.finish();
                    console.log("finised with error" + err.message);
                    if (res) {
                        res.json({"res": err.message});
                    }
                }
            }
        };
        nextfunc();
    });
};

