'use strict';

class CoordHelper {
    pad(n, width, z) {
        z = z || '0';
        n = n + '';
        return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
    }

    /**
     * decl
     * @param {type} degin
     * @param {int} precision
     * @param {bool} showPlusSign
     * @returns {String}
     */
    deg2degminsec(degin, precision, showPlusSign) {
        var deg = Math.trunc(degin);
        var m = Math.abs(Math.trunc((degin - deg) * 60));
        var s = ((Math.abs(degin - deg) * 60) - m) * 60;
        var z = "";
        if (deg > -10 && deg < 0)
            deg = '-0' + Math.abs(deg);
        else if (deg >= 0 && deg < 10)
            deg = '0' + deg;
        if (m < 10)
            m = "0" + m;
        if (s < 10)
            z = "0";

        var sign = "";
        if (showPlusSign) {
            if (parseInt(deg) >= 0) {
                sign = "+";
            }
        }

        return sign + this.pad(deg, 2) + " " + this.pad(m, 2) + " " + z + s.toFixed(precision ? precision : 0); //fixme cuidado con el +
    }

    /**
     * ra
     * @param {type} deg
     * @param {int} precision
     * @returns {String}
     */
    deg2hour(deg, precision) {
        var hour = Math.trunc(deg * 24.0 / 360.0);
        var mdec = Math.abs((deg * 24.0 / 360.0) - hour) * 60.0;
        var m = Math.trunc(mdec);
        var s = (mdec - m) * 60;
        var z = "";
        if (hour < 10)
            hour = "0" + hour;
        if (m < 10)
            m = "0" + m;
        if (s < 10)
            z = "0";
        return "" + this.pad(hour, 2) + " " + this.pad(m, 2) + " " + z + s.toFixed(precision ? precision : 0);
    }
}
;

module.exports = CoordHelper;