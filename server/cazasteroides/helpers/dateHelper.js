'use strict';

class DateHelper {
    dateAdd(date, interval, units) {
        var ret = new Date(date); //don't change original date
        var checkRollover = function () {
            if (ret.getDate() != date.getDate())
                ret.setDate(0);
        };
        switch (interval.toLowerCase()) {
            case 'year':
                ret.setFullYear(ret.getFullYear() + units);
                checkRollover();
                break;
            case 'quarter':
                ret.setMonth(ret.getMonth() + 3 * units);
                checkRollover();
                break;
            case 'month'  :
                ret.setMonth(ret.getMonth() + units);
                checkRollover();
                break;
            case 'week'   :
                ret.setDate(ret.getDate() + 7 * units);
                break;
            case 'day'    :
                ret.setDate(ret.getDate() + units);
                break;
            case 'hour'   :
                ret.setTime(ret.getTime() + units * 3600000);
                break;
            case 'minute' :
                ret.setTime(ret.getTime() + units * 60000);
                break;
            case 'second' :
                ret.setTime(ret.getTime() + units * 1000);
                break;
            default       :
                ret = undefined;
                break;
        }
        return ret;
    }

    timestam2date(t) {
        return new Date(t.replace(
                /^(\d{4})(\d\d)(\d\d)(\d\d)(\d\d)(\d\d)$/,
                '$4:$5:$6 $2/$3/$1'
                ));
    }

    date2timestam(date) {
        function pad2(n) {
            return n < 10 ? '0' + n : n;
        }

        return date.getFullYear().toString() + pad2(date.getMonth() + 1) + pad2(date.getDate()) + pad2(date.getHours()) + pad2(date.getMinutes()) + pad2(date.getSeconds());
    }

    timestamp2YYYYMMDDprecision(timestamp, precision) {
        //console.log(timestamp);
        var fecha = this.timestam2date(timestamp);
        //console.log(fecha);
        var year = fecha.getFullYear();
        var month = fecha.getMonth() + 1;
        var day = fecha.getDate() + fecha.getHours() / 24.0 + fecha.getMinutes() / (24 * 60.0) + fecha.getSeconds() / (24 * 60.0 * 60.0);

        var regex = new RegExp("/^-?\d+(?:\.\d{0," + precision + "})?/");

        var match = day.toString().match(regex);
        if (match) {
            day = Number(match[0]);
        } else {
            day = day.toFixed(precision);
        }

        return {
            year: year,
            month: month,
            day: day
        };
    }

    getDOY(t) {
        var now = this.timestam2date(t);
        var start = new Date(now.getFullYear(), 0, 0);
        var diff = now - start;
        var oneDay = 1000 * 60 * 60 * 24;
        var day = Math.floor(diff / oneDay);
        return day;
    }

}
;

module.exports = DateHelper;