var User = require('../models/user.js');
var Candidate = require('../models/candidate.js');
var Telescope = require('../models/telescope.js');
var Contest = require('../models/contest.js');
var Quintet = require('../models/image.js');
var Detection = require('../models/observation.js');
exports.pointsForVotes = function (limit, callback) {

    User.find({"sVotes.points": {$gt: 0}}, {_id: 0, "username": 1, "sVotes.points": 1}, {limit: parseInt(limit)})
            .sort({'sVotes.points': -1})
            .lean()
            .exec(function (err, users) {
                if (err) {
                    callback([]);
                } else {
                    for (var i = 0; i < users.length; i++) {
                        //users[i]['points'] = users[i].sVotes.points;
                        delete users[i].sVotes;
                    }
                    callback(users);
                }
            });
};
exports.pointsForDetections = function (limit, callback) {

    User.find({"sDetections.points": {$gt: 0}}, {_id: 0, "username": 1, "sDetections.points": 1}, {limit: parseInt(limit)})
            .sort({'sDetections.points': -1})
            .lean()
            .exec(function (err, users) {
                if (err) {
                    callback([]);
                } else {
                    for (var i = 0; i < users.length; i++) {
                        //users[i]['points'] = users[i].sDetections.points;
                        delete users[i].sDetections;
                    }
                    callback(users);
                }
            });
};
exports.moreVotes = function (limit, callback) {

    User.find({"sVotes.corrects": {$gt: 0}}, {_id: 0, "username": 1, "sVotes": 1}/*, {limit: parseInt(limit)}*/)
            //.sort({'sVotes.total': -1})
            .lean()
            .exec(function (err, users) {
                if (err) {
                    callback([]);
                } else {
                    for (var i = 0; i < users.length; i++) {
                        var appro = users[i].sVotes.corrects;
                        var rejec = (users[i].sVotes.wrongs ? users[i].sVotes.wrongs : 0);
                        var pendi = users[i].sVotes.total - (appro + rejec);
                        var total = users[i].sVotes.total - pendi;
                        users[i]['total'] = total;
                        users[i]['corrects'] = appro;
                        var percent = ((appro / total) * 100);
                        users[i]['info'] = appro + " / " + total + " ( " + percent.toFixed(0) + "% )";
                        delete users[i].sVotes;
                    }
//                    for (var i = 0; i < users.length; i++) {
//                        users[i]['total'] = users[i].sVotes.total;
//                        users[i]['corrects'] = users[i].sVotes.corrects;
//                        var percent = ((users[i].sVotes.corrects / users[i].sVotes.total) * 100);
//                        users[i]['info'] = users[i].sVotes.corrects + " / " + users[i].sVotes.total + " ( " + percent.toFixed(0) + "% )";
//                        delete users[i].sVotes;
//                    }

                    users.sort(function (a, b) {
                        if (a.total === b.total) {
                            return b.corrects - a.corrects;
                        }
                        return b.total - a.total;
                    });
                    var result_users = users.slice(0, limit);
                    callback(result_users);
                }
            });
};
exports.moreDetections = function (limit, callback) {

    User.find({"sDetections.approved": {$gt: 0}}, {_id: 0, "username": 1, "sDetections": 1}/*, {limit: parseInt(limit)}*/)
            //.sort({'sDetections.total': -1})
            .lean()
            .exec(function (err, users) {
                if (err) {
                    callback([]);
                } else {
                    for (var i = 0; i < users.length; i++) {
                        var appro = users[i].sDetections.approved;
                        var rejec = (users[i].sDetections.rejected ? users[i].sDetections.rejected : 0);
                        var pendi = users[i].sDetections.total - (appro + rejec);
                        var total = users[i].sDetections.total - pendi;
                        users[i]['total'] = total;
                        users[i]['approved'] = appro;
                        var percent = ((appro / total) * 100);
                        users[i]['info'] = appro + " / " + total + " ( " + percent.toFixed(0) + "% )";
                        delete users[i].sDetections;
                    }


//                    for (var i = 0; i < users.length; i++) {
//                        users[i]['total'] = users[i].sDetections.total;
//                        users[i]['approved'] = users[i].sDetections.approved;
//                        var percent = ((users[i].sDetections.approved / users[i].sDetections.total) * 100);
//                        users[i]['info'] = users[i].sDetections.approved + " / " + users[i].sDetections.total + " ( " + percent.toFixed(0) + "% )";
//                        delete users[i].sDetections;
//                    }
                    users.sort(function (a, b) {
                        if (a.total === b.total) {
                            return b.approved - a.approved;
                        }
                        return b.total - a.total;
                    });
                    var result_users = users.slice(0, limit);
                    callback(result_users);
                }
            });
};
exports.moreEffectiveDetections = function (limit, callback) {
    User.find({"sDetections.approved": {$gt: 0}}, {_id: 0, "username": 1, "sDetections": 1})
            .lean()
            .exec(function (err, users) {
                if (err) {
                    callback([]);
                } else {

                    var max = 0;
                    var maxTotal = 0;
                    for (var i = 0; i < users.length; i++) {
                        var appro = users[i].sDetections.approved;
                        var rejec = (users[i].sDetections.rejected ? users[i].sDetections.rejected : 0);
                        var pendi = users[i].sDetections.total - (appro + rejec);
                        var total = users[i].sDetections.total - pendi;
                        if (users[i].sDetections.approved > max) {
                            max = users[i].sDetections.approved;
                        }

                        if (total > maxTotal) {
                            maxTotal = total;
                        }
                    }

                    for (var i = 0; i < users.length; i++) {
                        var total = users[i].sDetections.total;
                        var appro = users[i].sDetections.approved;
                        var rejec = (users[i].sDetections.rejected ? users[i].sDetections.rejected : 0);
                        var pendi = total - (appro + rejec);
                        var total_without_pendings = (total - pendi);
                        users[i]['errors'] = total_without_pendings - appro;
                        users[i]['percent'] = ((appro / total_without_pendings) + (appro / maxTotal)) * 100 / 2;
                        users[i]['info'] = appro + " / " + total_without_pendings;
                        delete users[i].sDetections;
                    }

                    users.sort(function (a, b) {
                        return b.percent - a.percent;
                    });
                    var result_users = users.slice(0, limit);
                    callback(result_users);
                }
            });
};
exports.moreEffectiveVotes = function (limit, callback) {
    User.find({"sVotes.corrects": {$gt: 50}}, {_id: 0, "username": 1, "sVotes": 1})
            .lean()
            .exec(function (err, users) {
                if (err) {
                    callback([]);
                } else {
                    var max = 0;
                    var maxTotal = 0;
                    for (var i = 0; i < users.length; i++) {

                        var appro = users[i].sVotes.corrects;
                        var rejec = (users[i].sVotes.wrongs ? users[i].sVotes.wrongs : 0);
                        var pendi = users[i].sVotes.total - (appro + rejec);
                        var total = users[i].sVotes.total - pendi;
                        if (total > maxTotal) {
                            maxTotal = total;
                        }

                        if (users[i].sVotes.corrects > max) {
                            max = users[i].sVotes.corrects;
                        }
                    }

                    for (var i = 0; i < users.length; i++) {

                        var total = users[i].sVotes.total;
                        var appro = users[i].sVotes.corrects;
                        var wrong = (users[i].sVotes.wrongs ? users[i].sVotes.wrongs : 0);
                        var pendi = total - (appro + wrong);
                        var total_without_pendings = (total - pendi);
                        users[i]['errors'] = total_without_pendings - appro;
                        users[i]['percent'] = ((appro / total_without_pendings) + (appro / maxTotal)) * 100 / 2;
                        users[i]['info'] = appro + " / " + total_without_pendings;
                        delete users[i].sVotes;
                    }

                    users.sort(function (a, b) {
                        return b.percent - a.percent;
                    });
                    var result_users = users.slice(0, limit);
                    callback(result_users);
                }
            });
};
exports.totalApproved = function (callback) {
    Candidate.count({}, function (err, total) {
        callback({total_approved: total});
    });
};
exports.totalUserActions = function (callback) {

    const aggregatorOpts = ([
        {"$group": {
                _id: null,
                "tota_detections": {"$sum": "$sDetections.total"},
                "tota_detections_approved": {"$sum": "$sDetections.approved"},
                "tota_votes": {"$sum": "$sVotes.total"},
                "tota_votes_corrects": {"$sum": "$sVotes.corrects"}
            }
        }
    ]);
    User.aggregate(aggregatorOpts)
            .exec(function (err, users) {
                console.log(JSON.stringify(err));
                console.log(JSON.stringify(users));
                if (err) {
                    callback({});
                } else {
                    delete(users[0]._id);
                    callback(users[0]);
                }
            });
};
exports.telescopesResume = function (callback) {
    Telescope.find({}, {name: 1, detections: 1, candidates: 1, _id: 0}).sort({candidates: -1, detections: -1}).exec(function (err, telescopes) {
        callback(telescopes);
    });
};
exports.totalParticipants = function (name_encode, callback) {
    if (!name_encode) {
        callback({});
        return;
    }

    var name = decodeURI(name_encode);
    Contest.findOne({name: name})
            .exec(function (err, contest) {
                if (err) {
                    callback({});
                } else if (!contest) {
                    callback({});
                } else {
                    User.count({
                        "contests.contest": contest._id
                    }, function (err, total) {
                        callback({name: name, total_participants: total});
                    });
                }
            });
};
exports.totalQuintetActions = function (callback) {

    const aggregatorOpts = ([
        {"$group": {
                _id: null,
                "total_detections": {"$sum": "$info.total_detections"},
                "total_quintets": {"$sum": 1}
            }
        }
    ]);
    Quintet.aggregate(aggregatorOpts)
            .exec(function (err, users) {
                console.log(JSON.stringify(err));
                console.log(JSON.stringify(users));
                if (err) {
                    callback({});
                } else {
                    delete(users[0]._id);
                    callback(users[0]);
                }
            });
};
function totalDetectionAction(condition, callback) {
    console.log(JSON.stringify(condition));
    const aggregatorOpts = ([
        condition,
        {"$group": {
                _id: null,
                "total_unknown": {"$sum": {
                        $cond: [{$eq: ["$object.isCadidate", true]}, 1, 0]
                    }
                },
                "total_known": {"$sum": {
                        $cond: [{$eq: ["$object.isCadidate", true]}, 0, 1]
                    }
                },
                "total_pending": {"$sum": {
                        $cond: [{$eq: ["$status", "pending"]}, 1, 0]
                    }
                },
                "total_approved": {"$sum": {
                        $cond: [{$eq: ["$status", "approved"]}, 1, 0]
                    }
                },
                "total_rejected": {"$sum": {
                        $cond: [{$eq: ["$status", "rejected"]}, 1, 0]
                    }
                },
                "total_up_votes": {"$sum": {$size: "$votes"}},
                "total_down_votes": {"$sum": {$size: "$downvotes"}},
                "total": {"$sum": 1}
            }
        }
    ]);
    Detection.aggregate(aggregatorOpts)
            .exec(function (err, users) {
                console.log(JSON.stringify(err));
                console.log(JSON.stringify(users));
                if (err) {
                    callback({});
                } else if (users.length <= 0) {
                    callback(users);
                } else {
                    delete(users[0]._id);
                    callback(users[0]);
                }
            });
}

exports.totalDetectionsActions = function (callback) {
    totalDetectionAction({}, callback);
};

exports.totalContestDetectionsActions = function (name_encode, callback) {
    if (!name_encode) {
        callback({});
        return;
    }

    var name = decodeURI(name_encode);
    Contest.findOne({name: name})
            .exec(function (err, contest) {
                if (err) {
                    callback({});
                } else if (!contest) {
                    callback({});
                } else {
                    Detection.find({
                        "time": {$gte: new Date(contest.time_start), $lte: new Date(contest.time_end)}
                    }).exec(function (err, detections) {
                        console.log(JSON.stringify(err));
                        console.log(JSON.stringify(detections));

                        if (err) {
                            callback({});
                        } else {

                            var result = {
                                "total_unknown": 0,
                                "total_known": 0,
                                "total_pending": 0,
                                "total_approved": 0,
                                "total_rejected": 0,
                                "total_up_votes": 0,
                                "total_down_votes": 0,
                                "total": 0
                            };
                            for (var i = 0; i < detections.length; i++) {
                                result.total += 1;
                                result.total_down_votes += detections[i].downvotes.length;
                                result.total_up_votes += detections[i].votes.length;
                                if (detections[i].object && detections[i].object.isCandidate) {
                                    result.total_unknown += 1;
                                } else {
                                    result.total_known += 1;
                                }
                                switch (detections[i].status) {
                                    case "approved":
                                        result.total_approved += 1;
                                        break;
                                    case "rejected":
                                        result.total_rejected += 1;
                                        break;

                                    default:
                                        result.total_pending += 1;
                                        break;
                                }

                            }

                            callback(result);
                        }
                    });
                }
            });
};