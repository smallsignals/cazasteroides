/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

exports.isSuperAdmin = function (user) {
    return user && user.superAdmin && user.superAdmin === true;
};

exports.isAdmin = function (user) {
    return this.isSuperAdmin(user)
            || (user && user.admin && user.admin === true);
};

exports.errorNoAdmin = function (res) {
    res.status(401);
    res.header('Cazast_error', 'error_invalid_admin');
    res.send('error_invalid_admin');
};

exports.errorNoSuperAdmin = function (res) {
    res.status(401);
    res.header('Cazast_error', 'error_invalid_superadmin');
    res.send('error_invalid_superadmin');
};