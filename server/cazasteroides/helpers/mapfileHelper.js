/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var configConst = require("../config_template");
exports.getMapfilePath = function (mapfile) {
    var mapFileUri = mapfile;
    var pathSearch = '/cazasteroides/data/';
    if (mapFileUri.indexOf(pathSearch) >= 0) {
        mapFileUri = configConst.SRC_DATA + mapFileUri.substring(mapFileUri.indexOf(pathSearch) + pathSearch.length, mapFileUri.length);
    } else {
        if (mapFileUri.indexOf(configConst.SRC_DATA) < 0) {
            mapFileUri = configConst.SRC_DATA + mapfile;
        }
    }

    return mapFileUri;
};
