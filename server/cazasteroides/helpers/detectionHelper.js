var configConst = require("../config_template");

const DEFAULT_MAGNITUDE = 19;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
exports.compareDetection = function (a, b) {
    var upvotes = 0;
    var downvotes = 0;
    if (a.votes)
        upvotes = a.votes.length;
    if (a.downvotes)
        downvotes = a.downvotes.length;
    var avotes = upvotes - downvotes;
    upvotes = 0;
    downvotes = 0;
    if (b.votes)
        upvotes = b.votes.length;
    if (b.downvotes)
        downvotes = b.downvotes.length;
    var bvotes = upvotes - downvotes;
    if (avotes < bvotes)
        return 1;
    else if (avotes > bvotes)
        return -1;
    else if (a.revisit.length < b.revisit.length)
        return 1;
    else if (a.revisit.length > b.revisit.length)
        return -1;
    else
        return 0;

};

exports.getMagnitude = function (image_id, quintetn, coord, callback) {
    //callback(DEFAULT_MAGNITUDE);//TODO use default while finish of configurate the formula
    //return;

    var Quintet = require('../models/image.js');
    Quintet.findOne({'_id': image_id}, {'meta': 1, 'coeff': 1, 'mapfile': 1}, function (err, image) {
        try {
            if (err) {
                callback(DEFAULT_MAGNITUDE);
            } else {
                calculateMagnitude(image.meta, quintetn, image.mapfile, getMetadata(quintetn, image), coord, callback);
            }
        } catch (e) {
            //console.log("Error " + image_id, JSON.stringify(e));
            callback(DEFAULT_MAGNITUDE);
        }
    });


};

function getMetadata(quintetn, image) {
    try {
        if (quintetn === 0) {
            return image.meta;
        }
        return image.coeff[0][quintetn];
    } catch (e) {
        if (image)
            return {};
        return image.meta;
    }

}
/**
 * Calculate magnitude
 * 
 * @param {type} meta0      -> Metadata from quintet 0 -> This quintet have FWHM 
 * @param {type} i          -> layer index 
 * @param {type} mapfile    -> mapfile path
 * @param {type} metadata   -> metadata from quintet i
 * @param {type} coord      -> coord [ra,dec]
 * @param {type} callback   -> callback function with param magnitude
 * @returns {undefined}
 */
function calculateMagnitude(meta0, i, mapfile, metadata, coord, callback) {

    var fs = require('fs');

    var mapFileUri = mapfile;
    var pathSearch = '/cazasteroides/data/';
    if (mapFileUri.indexOf(pathSearch) >= 0) {
        mapFileUri = configConst.SRC_DATA + mapFileUri.substring(mapFileUri.indexOf(pathSearch) + pathSearch.length, mapFileUri.length);
    } else {
        mapFileUri = configConst.SRC_DATA + mapfile;
    }

    if (!fs.existsSync(mapFileUri)) {
        callback(DEFAULT_MAGNITUDE);
        return;
    }


    var getPixels = require("get-pixels");

    generateImagePath(i, mapFileUri, metadata, coord, function (url) {

        //console.log(url);

        getPixels(url, function (err, pixels) {
            if (err) {
                callback(DEFAULT_MAGNITUDE);
                console.log("Bad image path " + url);
                return;
            }
            //console.log("got data", JSON.stringify(pixels.data));
            //console.log("got shape", JSON.stringify(pixels.shape));

            var results = [];
            for (var i = 0; i < pixels.data.length; i += 4) {
                var R = pixels.data[i];
                var G = pixels.data[i + 1];
                var B = pixels.data[i + 2];
                var A = pixels.data[i + 3];
                var b = 0.2126 * R + 0.7152 * G + 0.0722 * B;
                results.push(b);
            }
            results.sort(function (a, b) {
                return a - b;
            });
            var mediana = undefined;
            if (results.length % 2 === 0) {
                var c = results.length / 2;
                mediana = (results[c] + results[c + 1]) / 2;
            } else {
                mediana = results[Math.ceil(results.length / 2)];
            }

            /**
             * Center Pixel
             */
            var index = parseInt(pixels.shape[0]) * parseInt(pixels.shape[2]) * Math.floor((parseInt(pixels.shape[1]) / 2)) + Math.floor((parseInt(pixels.shape[0]) / 2)) * parseInt(pixels.shape[2]) /** Math.floor((parseInt(pixels.shape[0]) / 2))*/;
            //console.log(index);
            var R = pixels.data[index];
            var G = pixels.data[index + 1];
            var B = pixels.data[index + 2];
            var b = 0.2126 * R + 0.7152 * G + 0.0722 * B;
            if (pixels.shape[0] % 2 === 0) {
                R = pixels.data[index - 1];
                G = pixels.data[index - 2];
                B = pixels.data[index - 3];
                b += 0.2126 * R + 0.7152 * G + 0.0722 * B;
                index = parseInt(pixels.shape[0]) * parseInt(pixels.shape[2]) * (Math.floor((parseInt(pixels.shape[1]) / 2)) - 1) + Math.floor((parseInt(pixels.shape[0]) / 2)) * parseInt(pixels.shape[2]) /** Math.floor((parseInt(pixels.shape[0]) / 2))*/;
                R = pixels.data[index];
                G = pixels.data[index + 1];
                B = pixels.data[index + 2];
                b += 0.2126 * R + 0.7152 * G + 0.0722 * B;
                R = pixels.data[index - 1];
                G = pixels.data[index - 2];
                B = pixels.data[index - 3];
                b += 0.2126 * R + 0.7152 * G + 0.0722 * B;
                b = b / 4;
            }


            if (!metadata.BZERO) {
                metadata.BZERO = meta0.BZERO;
            }
            if (!metadata.SKY) {
                metadata.SKY = meta0.SKY;
            }
            if (!metadata.ZMAG) {
                metadata.ZMAG = meta0.ZMAG;
            }
            if (!metadata.EXPTIME) {
                metadata.EXPTIME = meta0.EXPTIME;
            }
            if (!metadata.VEXT) {
                metadata.VEXT = meta0.VEXT;
            }
            if (!metadata.AIRMASS) {
                metadata.AIRMASS = meta0.AIRMASS;
            }

            var bzero = 0;
            if (metadata.BZERO) {
                bzero = parseFloat(metadata.BZERO);
            }
            metadata.SKY = parseFloat(metadata.SKY) - bzero;

            meta0.FWHM = parseFloat(meta0.FWHM ? meta0.FWHM : meta0.FWHMORI);

            var imax = b;// * parseFloat(metadata.SKY) / mediana;
            var ftot = 1.14 * Math.pow(parseFloat(meta0.FWHM), 2) * (imax === mediana ? (parseFloat(metadata.SKY)) : (imax - mediana/*parseFloat(metadata.SKY)*/));
            var magnitude = parseFloat(metadata.ZMAG) - 2.5 * Math.log10(Math.abs(ftot) / parseFloat(metadata.EXPTIME)) - parseFloat(metadata.VEXT) * parseFloat(metadata.AIRMASS);

            //console.log("Current", b.toFixed(2) + " " + mediana.toFixed(2) + " " + (!magnitude ? '?'.repeat(5) : magnitude.toFixed(2)) + " " + url);

            callback(!magnitude ? DEFAULT_MAGNITUDE : magnitude);
        });
    });
}

/**
 * 
 * @param {type} index          -> layer index 
 * @param {type} mapfile    -> mapfile path
 * @param {type} meta   -> metadata from quintet i
 * @param {type} coord      -> coord [ra,dec]
 * @returns {undefined}
 */
function generateImagePath(index, mapfile, meta, coord, callback) {
    var pix2coorHelper = require('../helpers/pix2coor.js');

    var Setup = require('../models/setup.js');

    Setup.findOne({}, function (err, setup) {
        var res_image_end = 0.6;
        var sigma_detection = 0.0028;

        if (setup) {
            var indexResImageEnd = setup.config.findIndex(function (element) {
                return element.key.toString() === "res_image_end";
            });
            if (indexResImageEnd >= 0 && indexResImageEnd < setup.config.length) {
                res_image_end = parseFloat(setup.config[indexResImageEnd].value);
            }

            var indexSigmaDetection = setup.config.findIndex(function (element) {
                return element.key.toString() === "sigma_detection";
            });
            if (indexSigmaDetection >= 0 && indexSigmaDetection < setup.config.length) {
                sigma_detection = parseFloat(setup.config[indexSigmaDetection].value);
            }
        }


        //var meta = metadata[index];
        var sigma_detection_value = parseFloat(sigma_detection * 3600);
        var gifWidth = (sigma_detection_value * 2);
        var gifHeight = (sigma_detection_value * 2);
        var imgheight = parseFloat(meta.IMAGEH);
        var spatial_scale = parseFloat(meta.SCALE);
        if (!spatial_scale)
            spatial_scale = 1;
        var w = parseFloat(gifWidth / 2) * res_image_end / spatial_scale;
        var h = parseFloat(gifHeight / 2) * res_image_end / spatial_scale;
        var rect = {};
        var repix = pix2coorHelper.coord2pix(parseFloat(coord[0]), parseFloat(coord[1]), meta);
        var pixCoordAbsX = repix[0];
        var pixCoordAbsY = repix[1];
        rect.minx = pixCoordAbsX - w;
        rect.maxx = pixCoordAbsX + w;
        rect.miny = imgheight - pixCoordAbsY - h;
        rect.maxy = imgheight - pixCoordAbsY + h;



        var r = {};
        var bzero = 0;
        if (meta.BZERO) {
            bzero = parseFloat(meta.BZERO);
        }
        meta.SKY = parseFloat(meta.SKY) - bzero;
        var sig = 5;
        if (!r.scalemin) {
            r.scalemin = parseFloat(meta.SKY) - (sig * parseFloat(meta.SIGMA));
        }
        if (!r.scalemax) {
            r.scalemax = parseFloat(meta.SKY) + (sig * parseFloat(meta.SIGMA));
        }

        var map = 'map=' + mapfile + '&mode=map';
        var mapsize = '&MSCALE=' + r.scalemin + ',' + r.scalemax + '&mapsize=' + gifWidth + '%20' + gifHeight;
        var mapext = "&MAPEXT=" + rect.minx + "%20" + rect.miny + "%20" + rect.maxx + "%20" + rect.maxy;
        var layer = '&layer=img' + (index + 1);

        //callback('http://api.cazasteroides.org/images/mapserv?' + map + mapsize + mapext + layer);
        callback(configConst.URL_MAPSERV_CGI_BIN + map + mapsize + mapext + layer);
    });
}
