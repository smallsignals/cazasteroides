'use strict';
var Detection = require('../models/observation.js');
var Candidate = require('../models/candidate.js');
var Quintet = require('../models/image.js');
var detectionHelper = require('../helpers/detectionHelper.js');
var pix2coorHelper = require('../helpers/pix2coor.js');
const DateHelper = require('../helpers/dateHelper.js');
const dateHelper = new DateHelper();
const CoordHelper = require('../helpers/coordHelper.js');
const coordHelper = new CoordHelper();

function coord2pix(obs) {
    var metadata = getMetadata(obs);
    return pix2coorHelper.coord2pix(parseFloat(obs.avgCoord[0]), parseFloat(obs.avgCoord[1]), metadata);
}

function getTimestamp(p) {
    return getMetadata(p)["TIMESTAM"];
}

function getMetadata(p) {
    try {
        if (p.quintetn === 0) {
            return p.image.meta;
        }
        return p.image.coeff[0][p.quintetn];
    } catch (e) {
        if (!p || !p.image)
            return {};
        return p.image.meta;
    }
}

function getMetadataFromQuintet(image, quintetn) {
    try {
        if (quintetn === 0) {
            return image.meta;
        }
        return image.coeff[0][quintetn];
    } catch (e) {
        if (!image)
            return {};
        return image.meta;
    }
}

function speed(p1, p2) {
    var t1 = getTimestamp(p1), t2 = getTimestamp(p2);
    var d1 = new Date(t1.substring(0, 4), t1.substring(4, 6), t1.substring(6, 8), t1.substring(8, 10), t1.substring(10, 12), t1.substring(12, 14), 0);
    var d2 = new Date(t2.substring(0, 4), t2.substring(4, 6), t2.substring(6, 8), t2.substring(8, 10), t2.substring(10, 12), t2.substring(12, 14), 0);
    var t = Math.abs(d1.getTime() - d2.getTime()) / 1000;
    var pix1 = p1.pix;
    var pix2 = p2.pix;
    var vx = Math.abs(pix2[0] - pix1[0]) / (t);
    var vy = Math.abs(pix2[1] - pix1[1]) / (t);
    return Math.sqrt(Math.pow(vx, 2) + Math.pow(vy, 2));
}

function angle(p1, p2) {
    var pix1 = p1.pix;
    var pix2 = p2.pix;
    var a = (pix1[1] - pix2[1]);
    var b = (pix1[0] - pix2[0]);
    var angle = Math.atan(Math.abs(a) / Math.abs(b));
    if (a > 0 && b < 0) {
        return Math.PI - angle;
    } else if (a < 0 && b < 0) {
        return Math.PI + angle;
    } else if (a < 0 && b > 0) {
        return 2 * Math.PI - angle;
    }
    return angle;
}

function sameSpeed(s1, s2) {
    var err = 0.1;
    var maxSpeed = 0.5;
    return Math.abs(s1 - s2) < err && s1 < maxSpeed && s2 < maxSpeed;
}

function sameAngle(a1, a2) {
    var err = 0.05 * 180 / Math.PI;
    return Math.abs(a1 - a2) < err;
}

function isCollinear(p1, p2, p3) {

    var s1 = speed(p1, p2), s2 = speed(p2, p3);
    var a1 = angle(p1, p2), a2 = angle(p2, p3);
    if (sameSpeed(s1, s2) && sameAngle(a1, a2)) {
        return true;
    }

    return false;
}

function generateCandidates(quintet_id, detections) {
    var q0 = [];
    var q1 = [];
    var q2 = [];
    var q3 = [];
    var q4 = [];
    /*
     * Separar detecciones por quintetos
     */
    for (var i = 0; i < detections.length; i++) {
        detections[i].pix = coord2pix(detections[i]);
        switch (detections[i].quintetn) {
            case 0:
                q0.push(detections[i]);
                break;
            case 1:
                q1.push(detections[i]);
                break;
            case 2:
                q2.push(detections[i]);
                break;
            case 3:
                q3.push(detections[i]);
                break;
            case 4:
                q4.push(detections[i]);
                break;
            default:
                console.log("Error: " + detections[i].quintetn);
                break;
        }
    }

    /**
     * Unir quintetos en grupos de tres
     */
    var qList = [q0, q1, q2, q3, q4];
    var candidates_3 = [];
    var combinations = k_combinations([0, 1, 2, 3, 4], 3);
    for (var i = 0; i < combinations.length; i++) {
        var response = joinDetections(combinations[i], qList[combinations[i][0]], qList[combinations[i][1]], qList[combinations[i][2]]);
        if (response.length > 0) {
            candidates_3 = candidates_3.concat(response);
        }
    }

    /**
     * Crear grupos de 5 
     */
    var candidates_5 = mergeCandidates(candidates_3);
    /*
     * Generar fichas para cada candidato
     */
    for (var i = 0; i < candidates_5.length; i++) {
//console.log("convertToCandidateStructure index: " + i);
        convertToCandidateStructure(candidates_5[i], function (candidate) {
            console.log("Generated candidate: " + JSON.stringify(candidate.designation));//, null, 3));
            insertCandidate(candidate, function () {
                markQuintetAsCandidateGenerated(quintet_id);
            });
        });

        for (var a = 0; a < candidates_5[i].length; a++) {
            var det = candidates_5[i][a];
            if (det) {
                detections = detections.filter(function (el) {
                    return det.indexOf(el) < 0;
                });
            }
        }
    }

    for (var i = 0; i < detections.length; i++) {
        var list = undefined;
        switch (detections.quintetn) {
            case 1:
                list = [[], [detections[i]], [], [], []];
                break;
            case 2:
                list = [[], [], [detections[i]], [], []];
                break;
            case 3:
                list = [[], [], [], [detections[i]], []];
                break;
            case 4:
                list = [[], [], [], [], [detections[i]]];
                break;
            default:
                list = [[detections[i]], [], [], [], []];
                break;
        }
        convertToCandidateStructure(list, function (candidate) {
            console.log("Generated candidate: " + JSON.stringify(candidate.designation));//, null, 3));
            insertCandidate(candidate, function () {
                markQuintetAsCandidateGenerated(quintet_id);
            });
        });

    }

    if (candidates_5.length <= 0) {
        markQuintetAsCandidateGenerated(quintet_id);
    }
}

function markQuintetAsCandidateGenerated(quintet_id) {
    //console.log("Set Quintet : " + quintet_id + " generatedCandidates=true");
    Quintet.findOneAndUpdate(
            {_id: quintet_id},
            {$set: {generatedCandidates: true}
            }, function (e) {

    });
}

function insertCandidate(candidate, callback) {
    Candidate.findOne({$or: [{"designation": candidate.designation}, {"tmpDesignation": candidate.tmpDesignation}]}, function (err, cOriginal) {
        if (cOriginal && cOriginal.designation.toString() === candidate.designation.toString() && candidate.isKnown) {
            updateCandidate(cOriginal, candidate, callback);
        } else if (cOriginal && cOriginal.tmpDesignation.toString() === candidate.tmpDesignation.toString()) {
            updateTempDesignation(candidate, callback);
        } else {
            var c = new Candidate(candidate);
            c.save(function (err) {
                if (err) {
                    updateTempDesignation(candidate, callback);
                } else {
                    console.log("Insert candidate: " + c.tmpDesignation + " " + JSON.stringify(c.designation));//, null, 3));
                    callback();
                    sendMPC(c);
                }
            });
        }
    });

}

//function insertCandidate(candidate, callback) {
//    var c = new Candidate(candidate);
//    c.save(function (err) {
//        if (err) {
//            console.log(JSON.stringify(err));
//            if (err.code === 11000) {
//                Candidate.findOne({"designation": candidate.designation}, function (err, cOriginal) {
//                    if (cOriginal && cOriginal.designation.toString() === candidate.designation.toString()) {
//                        updateCandidate(cOriginal, candidate, callback);
//                    } else {
//                        updateTempDesignation(candidate, callback);
//                    }
//                });
//            } else {
//                console.log(err);
//            }
//        } else {
//            console.log("Insert candidate: " + c.tmpDesignation + " " + JSON.stringify(c.designation));//, null, 3));
//            callback();
//            sendMPC(c);
//        }
//    });
//}

function updateTempDesignation(c, callback) {
//console.log('updateTempDesignation');
    Candidate.findOne({}, {"tmpDesignation": 1})
            .sort({"tmpDesignation": -1})
            .exec(function (err, doc) {
                //console.log(JSON.stringify(doc));
                var maxIndex = undefined;
                if (doc) {
                    //console.log(doc.tmpDesignation);
                    maxIndex = doc.tmpDesignation;
                }

                function pad(num, size) {
                    var s = num + "";
                    while (s.length < size)
                        s = "0" + s;
                    return s;
                }

                c.tmpDesignation = pad(!maxIndex ? 0 : parseInt(maxIndex) + 1, 6);
                for (var j = 0; j < c.coordenates.length; j++) {
                    c.coordenates[j].line = changeLineTempDesignation(c.coordenates[j].line, c.tmpDesignation);
                }
                insertCandidate(c, callback);
            });
}

function updateCandidate(cOriginal, c, callback) {
//Set same temporal designation
    c.tmpDesignation = cOriginal.tmpDesignation;
    for (var j = 0; j < c.coordenates.length; j++) {
        c.coordenates[j].line = changeLineTempDesignation(c.coordenates[j].line, cOriginal.tmpDesignation);
    }

    Candidate.findOneAndUpdate(
            {_id: cOriginal._id},
            {$addToSet: {
                    coordenates: {$each: c.coordenates},
                    users: {$each: c.users}
                }
            },
            {returnNewDocument: true},
            function (e, cNew) {
                if (!e) {
                    console.log("Update candidate: " + cOriginal.tmpDesignation + " " + JSON.stringify(cOriginal.designation));//, null, 3));
                    callback();
                    if (cNew.coordenates.length !== cOriginal.coordenates.length) {
                        sendMPC(c);
                    }
                } else {
                    console.log(e);
                }
            });
}

function changeLineTempDesignation(line, tmpDesignation) {
    if (!line)
        return line;
    return  line.substring(0, 6) + tmpDesignation + line.substring(12);
}

function sendMPC(c) {
    //TODO uncoment this block to report MPC
    //const MPC = require('../helpers/mpcHelper.js');
    //new MPC().reportCandidate(c);
}

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}

function convertToCandidateStructure(candidate, callback) {

    var time = undefined;
    var designation = undefined;
    var allUsers = [];
    var coord = [undefined, undefined, undefined, undefined, undefined];
    var timestamp = [undefined, undefined, undefined, undefined, undefined];
    var magnitude = [undefined, undefined, undefined, undefined, undefined];
    var isKnown = false;
    var telescope = undefined;
    var ocn = undefined;
    var observer = undefined;
    var quintet = undefined;
    var gif = [undefined, undefined, undefined, undefined, undefined];
    var metadatas = [undefined, undefined, undefined, undefined, undefined];
    var discoverer = undefined;
    var mapfile = [undefined, undefined, undefined, undefined, undefined];
    var detectionObject = undefined;
//    var needRegenerateGif = [false, false, false, false, false];
//    var coordInitial = [undefined, undefined, undefined, undefined, undefined];
//    var scale = [undefined, undefined, undefined, undefined, undefined];

    for (var i = 0; i < candidate.length; i++) {
        var detections = candidate[i];
        if (detections) {
            for (var j = 0; j < detections.length; j++) {
                var d = detections[j];
                if (!d)
                    continue;

                if (!time) {
                    time = d['time'];
                    discoverer = d['user'];
                    if (!isKnown) {
                        designation = d['object']['mpcDesignation'] ? d['object']['mpcDesignation'] : d['object']['cazastDesignation'];
                        detectionObject = d['object'];
                    }
                } else if (time > d['time']) {
                    time = d['time'];
                    discoverer = d['user'];
                    if (!isKnown) {
                        designation = d['object']['mpcDesignation'] ? d['object']['mpcDesignation'] : d['object']['cazastDesignation'];
                        detectionObject = d['object'];
                    }
                }

                //console.log(JSON.stringify(d['object']));
                if (d['object']['mpcDesignation']) {
                    isKnown = true;
                    designation = d['object']['mpcDesignation'];
                    detectionObject = d['object'];
                }

                allUsers.push(d['user']);
                for (var u = 0; u < d['revisit'].length; u++) {
                    allUsers.push(d['revisit'][u]['user']);
                }

                var n = d['quintetn'];
                if (!coord[n]) {
                    coord[n] = d['avgCoord'];
                    gif[n] = d._id;
//                    coordInitial[n] = d['avgCoord'];
                    //console.log(d['image']['name'] + " " + n + " " + JSON.stringify(d['avgCoord']));
                } else if (isKnown) {
                    coord[n][0] = (coord[n][0] + d['avgCoord'][0]) / 2;
                    coord[n][1] = (coord[n][1] + d['avgCoord'][1]) / 2;
                }

//                if (!needRegenerateGif[n] && JSON.stringify(d['avgCoord']).toString() !== JSON.stringify(coord[n]).toString()) {
//                    needRegenerateGif[n] = true;
//                    //console.log(d['image']['name'] + " " + n + " " + JSON.stringify(d['avgCoord']) + " " + JSON.stringify(coord[n]));
//                }

                if (!magnitude[n]) {
                    magnitude[n] = d['avgMagnitude'];
                } else {
                    magnitude[n] = (magnitude[n] + d['avgMagnitude']) / 2;
                }

                if (!timestamp[n]) {
                    //timestamp[n] = getTimestamp(d);
                    for (var q = 0; q < timestamp.length; q++) {
                        if (!timestamp[q]) {
                            timestamp[q] = getMetadataFromQuintet(d.image, q)["TIMESTAM"];
                        }
                    }
                }

                var cMetadata = getMetadata(d);

                if (!metadatas[n]) {
                    metadatas[n] = cMetadata;
                }

                if (!telescope) {
                    telescope = cMetadata["TELESCOP"];
                }

//                if (!scale[n]) {
//                    scale[n] = cMetadata["SCALE"];
//                }

                if (!ocn) {
                    ocn = cMetadata["OCN"];
                }
                if (!ocn && d.image && d.image.meta) {
                    ocn = d.image.meta["OCN"];
                }

                if (!observer && cMetadata["OBSERVER"]) {
                    observer = cMetadata["OBSERVER"];
                }
                if (!observer && d.image && d.image.meta && d.image.meta["OBSERVER"]) {
                    observer = d.image.meta["OBSERVER"];
                }

                if (!quintet) {
                    quintet = d.image._id;
                }

//                if (!gif[n]) {
//                    gif[n] = d._id;
//                }

                if (!mapfile[n]) {
                    mapfile[n] = d.image.mapfile;
                }
            }
        }
    }

//    var increase = [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0]];
//    for (var b = 0; b < scale.length; b++) {
//        if (needRegenerateGif[b]) {
//            console.log(JSON.stringify(coordInitial[b]) + " " + JSON.stringify(coord[b]) + " " + JSON.stringify(scale[b]));
//
//        }
//        if (scale[b] && coordInitial[b] && coord[b]) {
//            if (coordInitial[b][0] != coord[b][0] || coordInitial[b][1] != coord[b][1]) {
//                var iy = 3600 * (coordInitial[b][1] - coord[b][1]) / scale[b];
//                var ix = (3600 / scale[b]) * (coordInitial[b][0] - coord[b][0]) * Math.cos(coord[b][1] * Math.PI * 180);
//
//                console.log(coordInitial[b][0] + " " + coord[b][0] + " " + ix);
//                console.log(coordInitial[b][1] + " " + coord[b][1] + " " + iy);
//            }
//        }
//    }

    //Update avgCoord from detection gif and create newly the gif
//    for (var i = 0; i < needRegenerateGif.length; i++) {
//        if (needRegenerateGif[i]) {
//            Detection.findOneAndUpdate(
//                    {_id: gif[i]},
//                    {$set: {
//                            avgCoord: coord[i]
//                        }
//                    },
//                    {returnNewDocument: true},
//                    function (e, obs) {
//                        if (!e) {
//                            try {
//                                require('../helpers/gifHelper').rebuild_gif(obs);
//                            } catch (e) {
//
//                            }
//                        }
//                    });
//        }
//    }

    var users = allUsers.reduce(function (p, c, i, a) {
        if (p.indexOf(c) === -1)
            p.push(c);
        return p;
    }, []);
    var magnitudeTest = [undefined, undefined, undefined, undefined, undefined];
    var checked = 0;
    var callAll = false;
    var len = 5;
    var magToRequest = cleanArray(coord).length;
    for (var i = 0; i < len; i++) {

        if (!callAll)
            callAll = (magToRequest - 1) === i;

        if (!coord[i] || !timestamp[i]) {
            continue;
        }

        checked++;
        detectionHelper.getMagnitude(quintet, i, coord[i], function () {
            var index = i;
            return function (m) {
                magnitudeTest[index] = m;
                checked--;
                if (checked <= 0 && callAll) {

                    Candidate.findOne({}, {"tmpDesignation": 1})
                            .sort({"tmpDesignation": -1})
                            .exec(function (err, doc) {
                                //console.log(JSON.stringify(doc));
                                var maxIndex = undefined;
                                if (doc) {
                                    //console.log(doc.tmpDesignation);
                                    maxIndex = doc.tmpDesignation;
                                }

                                function pad(num, size) {
                                    var s = num + "";
                                    while (s.length < size)
                                        s = "0" + s;
                                    return s;
                                }

                                var tmpDesignation = pad(!maxIndex ? 0 : parseInt(maxIndex) + 1, 6);
                                //var tmpDesignation = '000000';
                                var coordenates = [];
                                var isAsteriskSet = false;

                                var emptyLines = [];
                                for (var j = 0; j < 5; j++) {
                                    if (!coord[j] || !timestamp[j] || !(magnitudeTest[j] || magnitude[j])) {
                                        emptyLines.push(j);
                                        continue;
                                    }

                                    var mag = (magnitudeTest[j] ? magnitudeTest[j] : magnitude[j]);
                                    var line = generateLine(timestamp[j], coord[j], mag, tmpDesignation, isKnown, isAsteriskSet, ocn);

                                    coordenates.push({
                                        line: line,
                                        telescope: {name: telescope, ocn: ocn, observer: observer},
                                        coord: coord[j],
                                        timestamp: timestamp[j],
                                        magnitude: mag,
                                        quintet: quintet,
                                        quintetn: j,
                                        gif: gif[j]
                                    });
                                    isAsteriskSet = true;
                                }

                                if (!isKnown && emptyLines.length > 0) {
                                    for (var q = 0; q < emptyLines.length; q++) {
                                        var coordenatesReference = JSON.parse(JSON.stringify(coordenates[0]));
                                        if (coordenatesReference) {
                                            delete coordenatesReference.line;
                                            delete coordenatesReference.magnitude;
                                            coordenatesReference.timestamp = timestamp[emptyLines[q]];
                                            coordenatesReference.quintetn = emptyLines[q];
                                            coordenates.push(coordenatesReference);
                                        }
                                    }
                                }

                                callback({
                                    designation: designation,
                                    coordenates: coordenates,
                                    users: users,
                                    isKnown: isKnown,
                                    tmpDesignation: tmpDesignation,
                                    discoverer: discoverer
                                });

                                for (var a = 0; a < candidate.length; a++) {
                                    var detections = candidate[a];
                                    if (detections) {

                                        var tmpDetections = detections.reduce(function (p, c, i, a) {
                                            if (p.indexOf(c) === -1)
                                                p.push(c);
                                            return p;
                                        }, []);

                                        for (var j = 0; j < tmpDetections.length; j++) {
                                            var d = tmpDetections[j];
                                            if (!d)
                                                continue;
//                                            console.log(JSON.stringify(detectionObject) + " " + JSON.stringify(d.object));
                                            Detection.findOneAndUpdate(
                                                    {_id: d._id},
                                                    {$set: {object: detectionObject}
                                                    }, function (e) {

                                            });
                                        }
                                    }
                                }

                            });
                }

            };
        }());
    }
}

function generateLine(timestamp, coord, magnitude, tmpDesignation, isKnown, isAsteriskSet, ocn) {
    var date = dateHelper.timestamp2YYYYMMDDprecision(timestamp, 5);
    var ra = coordHelper.deg2hour(coord[0], 2) + ' ';
    var dec = coordHelper.deg2degminsec(coord[1], 1, true) + ' ';
    var mag = magnitude;
    var tmpMag = parseFloat(mag.toFixed(2));
    //http://www.minorplanetcenter.net/iau/info/OpticalObs.html
    //http://www.minorplanetcenter.net/iau/MPEph/NewObjEphems.html
    return "     "//(isKnown ? designation : "     ")                                           //1 -  5       A5     Minor planet number
            + "C" + tmpDesignation//(isKnown ? "       " : "C" + temDesign)                         //6 - 12       A7     Provisional or temporary designation
            + (isKnown || isAsteriskSet ? " " : "*")                                                //13           A1     Discovery asterisk
            + " "                                                                                   //14           A1     Note 1
            + "C"                                                                                   //15           A1     Note 2
            + date.year + " " + (date.month < 10 ? "0" : "") + date.month + " " + date.day + " "    //16 - 32             Date of observation
            + ra                                                                                    //33 - 44             Observed RA (J2000.0)
            + dec                                                                                   //45 - 56             Observed Decl. (J2000.0)
            + "         "                                                                           //57 - 65      9X     Must be blank
            + (!tmpMag ? ' '.repeat(6) : tmpMag + ' '.repeat(5 - tmpMag.toString().length) + 'V')   //66 - 71   F5.2,A1   Observed magnitude and band (or nuclear/total flag for comets) 
            + "      "                                                                              //72 - 77      X      Must be blank
            + ocn                                                                                   //78 - 80      A3     Observatory code
            ;
}

//
//const SYMBOLS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+*/¿?¡!@()[]{}~$%&|_^<>=,;.:-';
//function generateTempDesignation(t) {
//    var doy = dateHelper.getDOY(t);
//    return base_converter(t.substring(0, 4) + doy + t.substring(8, 12), 10, SYMBOLS.length);
//}
//
//function base_converter(nbasefrom, basefrom, baseto) {
//    //var SYMBOLS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+*/¿?¡!@()[]{}~$%&|_^<>=,;.:-';
//    if (basefrom <= 0 || basefrom > SYMBOLS.length || baseto <= 0 || baseto > SYMBOLS.length) {
//        console.log("Base unallowed");
//        return null;
//    }
//    var i, nbaseten = 0;
//    if (basefrom != 10) {
//        var sizenbasefrom = nbasefrom.length;
//        for (i = 0; i < sizenbasefrom; i++) {
//            var mul, mul_ok = -1;
//            for (mul = 0; mul < SYMBOLS.length; mul++) {
//                if (nbasefrom[i] == SYMBOLS[mul]) {
//                    mul_ok = 1;
//                    break;
//                }
//            }
//            if (mul >= basefrom) {
//                console.log("Symbol unallowed in basefrom");
//                return null;
//            }
//            if (mul_ok == -1) {
//                console.log("Symbol not found");
//                return null;
//            }
//            var exp = (sizenbasefrom - i - 1);
//            if (exp == 0)
//                nbaseten += mul;
//            else
//                nbaseten += mul * Math.pow(basefrom, exp);
//        }
//    } else
//        nbaseten = parseInt(nbasefrom);
//    if (baseto != 10) {
//        var nbaseto = [];
//        while (nbaseten > 0) {
//            var mod = nbaseten % baseto;
//            if (mod < 0 || mod >= SYMBOLS.length) {
//                console.log("Out of bounds error");
//                return null;
//            }
//            nbaseto.push(SYMBOLS[mod]);
//            nbaseten = parseInt(nbaseten / baseto);
//        }
//        return nbaseto.reverse().toString().replace(/,/g, '');
//    } else {
//        return nbaseten.toString();
//    }
//    return "0";
//}

function mergeCandidates(candidates) {
    var output = [];
    for (var i = 0; i < candidates.length; i++) {
        var c = candidates[i];
        var list = [[c[0]], [c[1]], [c[2]], [c[3]], [c[4]]];
        for (var j = i + 1; j < candidates.length; j++) {

            var c1 = candidates[j];
            if (
                    containsDetection(list[0], c1[0]) ||
                    containsDetection(list[1], c1[1]) ||
                    containsDetection(list[2], c1[2]) ||
                    containsDetection(list[3], c1[3]) ||
                    containsDetection(list[4], c1[4]))
            {
                if (c1[0])
                    list[0].push(c1[0]);
                if (c1[1])
                    list[1].push(c1[1]);
                if (c1[2])
                    list[2].push(c1[2]);
                if (c1[3])
                    list[3].push(c1[3]);
                if (c1[4])
                    list[4].push(c1[4]);
                candidates.splice(j, 1);
            }
        }

        var concatLists = false;
        for (var q = 0; q < output.length; q++) {
            if (!concatLists && list[0] && output[q][0]) {
                for (var r = 0; r < list[0].length; r++) {
                    if (containsDetection(output[q][0], list[0][r])) {
                        output[q] = concat(output[q], list);
                        concatLists = true;
                        break;
                    }
                }
            }

            if (!concatLists && list[1] && output[q][1]) {
                for (var r = 0; r < list[1].length; r++) {
                    if (containsDetection(output[q][1], list[1][r])) {
                        output[q] = concat(output[q], list);
                        concatLists = true;
                        break;
                    }
                }
            }

            if (!concatLists && list[2] && output[q][2]) {
                for (var r = 0; r < list[2].length; r++) {
                    if (containsDetection(output[q][2], list[2][r])) {
                        output[q] = concat(output[q], list);
                        concatLists = true;
                        break;
                    }
                }
            }

            if (!concatLists && list[3] && output[q][3]) {
                for (var r = 0; r < list[3].length; r++) {
                    if (containsDetection(output[q][3], list[3][r])) {
                        output[q] = concat(output[q], list);
                        concatLists = true;
                        break;
                    }
                }
            }

            if (!concatLists && list[4] && output[q][4]) {
                for (var r = 0; r < list[4].length; r++) {
                    if (containsDetection(output[q][4], list[4][r])) {
                        output[q] = concat(output[q], list);
                        concatLists = true;
                        break;
                    }
                }
            }

            if (concatLists) {
                break;
            }
        }

        if (!concatLists) {
            output.push(list);
        }
    }
    return output;
}

function concat(output, list) {
    var finalOutput = [undefined, undefined, undefined, undefined, undefined];
    finalOutput[0] = output[0].concat(list[0]);
    finalOutput[1] = output[1].concat(list[1]);
    finalOutput[2] = output[2].concat(list[2]);
    finalOutput[3] = output[3].concat(list[3]);
    finalOutput[4] = output[4].concat(list[4]);
    return finalOutput;
}

function containsDetection(list, d) {
    if (!d || !list)
        return false;
    if (list.length <= 0)
        return false;
    if (Array.isArray(list)) {
        return list.some(function (element) {
            return element && element['_id'] && d['_id'] && element['_id'].toString() === d['_id'].toString();
        });
    } else {
        //console.log(list["_id"] + "==" + d["_id"] + "? " + (list["_id"] == d["_id"]));
        return list["_id"] == d["_id"];
    }
}

function k_combinations(set, k) {
    var i, j, combs, head, tailcombs;
    // There is no way to take e.g. sets of 5 elements from
    // a set of 4.
    if (k > set.length || k <= 0) {
        return [];
    }

// K-sized set has only one K-sized subset.
    if (k == set.length) {
        return [set];
    }

// There is N 1-sized subsets in a N-sized set.
    if (k == 1) {
        combs = [];
        for (i = 0; i < set.length; i++) {
            combs.push([set[i]]);
        }
        return combs;
    }

    combs = [];
    for (i = 0; i < set.length - k + 1; i++) {
// head is a list that includes only our current element.
        head = set.slice(i, i + 1);
        // We take smaller combinations from the subsequent elements
        tailcombs = k_combinations(set.slice(i + 1), k - 1);
        for (j = 0; j < tailcombs.length; j++) {
            combs.push(head.concat(tailcombs[j]));
        }
    }
    return combs;
}

function joinDetections(comb, q0, q1, q2) {
    var candidates = [];
    for (var i0 = 0; i0 < q0.length; i0++) {
        for (var i1 = 0; i1 < q1.length; i1++) {
            for (var i2 = 0; i2 < q2.length; i2++) {
                if (isCollinear(q0[i0], q1[i1], q2[i2])) {

                    var c = [null, null, null, null, null];
                    c[comb[0]] = q0[i0];
                    c[comb[1]] = q1[i1];
                    c[comb[2]] = q2[i2];
                    candidates.push(c);
                    console.log("Group: " + JSON.stringify(q0[i0].object) + " " + JSON.stringify(q1[i1].object) + " " + JSON.stringify(q2[i2].object));
                }
            }
        }
    }


    return candidates;
}

function checkQuintet(quintet_id) {

    Detection.find({'image': quintet_id, "status": 'approved'},
            {"user": 1, "image": 1, "avgCoord": 1, "quintetn": 1, "object": 1, "revisit": 1, "time": 1, pixCoordAbs: 1})
            .populate('image', 'meta coeff name mapfile filename')
            .exec(function (err, detections) {
                if (!err) {
                    if (detections.length > 0) {
                        //console.log(detections[0].image.name + " " + detections.length);
                        generateCandidates(quintet_id, detections);
                    }
                }
            });
}

exports.findCandidates = function () {

    Quintet.find({'active': false,
        $or: [{'generatedCandidates': false}, {'generatedCandidates': {$exists: false}}]
    }, {'_id': 1, 'name': 1}, function (err, quintet) {
        for (var o = 0; o < quintet.length; o++) {
            const aggregatorOpts = ([
                {$match: {
                        image: quintet[o]._id
                    }},
                {"$group": {
                        "_id": "$image",
                        "total_pending": {"$sum": {
                                $cond: [
                                    {$eq: ["$status", 'pending']}, 1, 0]
                            }
                        },
                        "total_approved": {"$sum": {
                                $cond: [
                                    {$eq: ["$status", 'approved']}, 1, 0]
                            }
                        }
                    }
                }
            ]);
            Detection.aggregate(aggregatorOpts).exec(function (err, detections) {
                if (detections && detections[0] && detections[0]['total_pending'] === 0 && detections[0]['total_approved'] > 0) {
                    checkQuintet(detections[0]['_id']);
                }
            });
        }
    });
};

exports.sortCandidates = function (a, b) {
    return a.timestamp < b.timestamp;
};

exports.generateLine = function (timestamp, coord, magnitude, tmpDesignation, isKnown, isAsteriskSet, ocn) {
    return generateLine(timestamp, coord, magnitude, tmpDesignation, isKnown, isAsteriskSet, ocn);
};

exports.test = function (q_id) {
    checkQuintet(q_id);
};
