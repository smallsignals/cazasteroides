'use strict';
var configConst = require("../config_template");
//Documentations: http://www.minorplanetcenter.net/cgi-bin/checkmp.cgi
module.exports = class mpcHelper {
    reportCandidate(candidate, callback) {
        var Candidate = require('../models/candidate.js');
        var mail = require("../mail.js");

        //http://www.minorplanetcenter.net/iau/info/Astrometry.html
        //http://www.minorplanetcenter.net/iau/ContactUs.html
        var obsCode = [];
        console.log("Send MPC: " + candidate.designation);// + JSON.stringify(c.coordenates, null, 3));
        for (var j = 0; j < candidate.coordenates.length; j++) {
            obsCode.push(candidate.coordenates[j].telescope.ocn);
        }

        obsCode = obsCode.reduce(function (p, c, i, a) {
            if (p.indexOf(c) === -1)
                p.push(c);
            return p;
        }, []);

        var breakLine = "\r\n";
        for (var i = 0; i < obsCode.length; i++) {
            var header = "COD " + obsCode[i] + breakLine;
            header += "CON M. Serra-Ricart, IAC, 38200 Tenerife, Spain" + breakLine;
            header += "CON [mserra@iac.es]" + breakLine;
            header += "OBS M. Serra-Ricart" + breakLine;//OBSERVER -> metadatas
            header += "ACK " + candidate.designation + breakLine;
            header += "AC2 mserra@iac.es,slemes@iac.es" + breakLine;
            //;// obs@cfa.harvard.edu

            var body = '';
            for (var j = 0; j < candidate.coordenates.length; j++) {
                body += candidate.coordenates[j].line + breakLine;
            }
            console.log(header + body);
            mail.sendMPCCandidate('obs@cfa.harvard.edu', '', header + body);//TODO subject: If you do not receive the acknowledgement within 24 hours you should resend the original message, noting in the subject line that the message is a retransmission (ensuring your subject line contains `RESEND' is a good way). 
        }

        Candidate.findOneAndUpdate(
                {_id: candidate._id},
                {$set: {
                        mpcReportState: -1//TODO change to 0
                                //need store current time and resend with RESEND subject
                    }
                },
                function (e, cNew) {
                    if (!e) {
                        if (callback)
                            callback(0);
                    } else {
                        if (callback)
                            callback(-1);
                    }
                });
    }

    check(obs_id) {
        var Detection = require('../models/observation.js');
        Detection.findById(obs_id,
                {"image": 1, "quintetn": 1, "user": 1, "avgCoord": 1})
                .populate('image', 'meta coeff.0.TIMESTAM coeff.1.TIMESTAM coeff.2.TIMESTAM coeff.3.TIMESTAM coeff.4.TIMESTAM')
                .populate('user', 'username')
                .exec(function (err, obs) {

                    if (!obs || !obs.avgCoord || !obs.image || !obs.image.meta || !obs.image.meta.TIMESTAM || !obs.image.meta.OCN) {
                        console.log('MPC ERROR: Faltan datos en la obs: ' + obs_id);
                        return;
                    }

                    console.log('obs: ' + obs_id);
                    console.log('avgCoord: ' + obs.avgCoord[0] + "," + obs.avgCoord[1]);
                    const DateHelper = require('../helpers/dateHelper.js');
                    var dateHelper = new DateHelper();

                    const CoordHelper = require('../helpers/coordHelper.js');
                    var coordHelper = new CoordHelper();


                    var timestam = obs.image.meta.TIMESTAM;
                    if (obs.quintetn && obs.quintetn != 0 && obs.image.coeff && obs.image.coeff[0] && obs.image.coeff[0][obs.quintetn] && obs.image.coeff[0][obs.quintetn].TIMESTAM) {
                        timestam = obs.image.coeff[0][obs.quintetn].TIMESTAM;
                    }
//
//                    var fecha = dateHelper.timestam2date(timestam);
//                    var year = fecha.getFullYear();
//                    var month = fecha.getMonth() + 1;
//                    var day = fecha.getDate() + fecha.getHours() / 24.0 + fecha.getMinutes() / (24 * 60.0) + fecha.getSeconds() / (24 * 60.0 * 60.0);
//                    day = Number(day.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0]);

                    var date = dateHelper.timestamp2YYYYMMDDprecision(timestam, 2);
                    var year = date.year;
                    var month = date.month;
                    var day = date.day;
                    var form = {
                        "year": year, //'2003', 
                        "month": month, //'11', 
                        "day": day, //'23.62', 
                        "which": "pos",
                        "ra": coordHelper.deg2hour(obs.avgCoord[0]), //'04 20 38.5',
                        "decl": coordHelper.deg2degminsec(obs.avgCoord[1]), // '+53 01 10', 
                        "TextArea": "",
                        "radius": 1,
                        "limit": 22,
                        "oc": obs.image.meta.OCN, //'500', 
                        "sort": "d",
                        "mot": "h",
                        "tmot": "s",
                        "pdes": "p",
                        "needed": "f",
                        "ps": "n",
                        "type": "p"
                    };
                    var request = require('request');
                    request.debug = true;
                    //console.log(new Date().getTime());
                    request.post({
                        url: configConst.URL_MINORPLANETCENTER,
                        form: form
                    }, function (error, response, body) {
                        //console.log(new Date().getTime());
                        if (!error && response.statusCode == 200) {
                            var updateObject = {};
                            updateObject.object = {};
                            /**
                             * Actualizar el estado mpcChecked 
                             */
                            updateObject.object.mpcChecked = true;
                            var cheerio = require('cheerio'),
                                    $ = cheerio.load(body);
                            console.log(body);
                            var raw = $("pre").html();
                            if (raw) {
                                var start = raw.search('\n\n') + 2;
                                var temp = raw.substr(start, raw.length);
                                var lines = temp.split("\n");
                                var designation = undefined;
                                for (var l = 0; l < lines.length; l++) {

                                    if (lines[l].substr(0, 25).trim()) {
                                        designation = lines[l].substr(0, 25).trim();
                                        break;
                                    }
                                }
                                updateObject.object.mpcDesignation = designation;
//                        Detection.findOneAndUpdate({"_id": obs_id}, {$set: {'object.mpcDesignation': designation}}, function (err, doc) {
//                        });
                                console.log(JSON.stringify(designation));
                                var QuintetHelper = require('../helpers/quintetHelper.js');
                                QuintetHelper.increaseKnow(obs.image._id);
                            } else {
                                console.log("no mpc found");
                                updateObject.object.isCadidate = true;
                            }
                            console.log(JSON.stringify(updateObject));
                            Detection.findOneAndUpdate({"_id": obs_id}, {$set: updateObject}, function (err, doc) {
                                console.log(JSON.stringify(err));
                                //Update Telescope Table
                                var TelescopeHelper = require('../helpers/telescopeHelper.js');
                                TelescopeHelper.increase(obs.image.meta, 0, updateObject.object.isCadidate ? 1 : 0);

                                if (updateObject.object.isCadidate) {
                                    var username = '';
                                    if (obs.user && obs.user.username) {
                                        username = obs.user.username.replace(/\s+/g, '');
                                    }
                                    var cazastDesignation = obs.image.meta.TELESCOP + '-' + username + '-';
                                    Detection.findOne({"object.cazastDesignation": {$regex: "^" + cazastDesignation}}, {"object.cazastDesignation": 1})
                                            .sort({"object.cazastDesignation": -1})
                                            .exec(function (err, doc) {
                                                console.log(JSON.stringify(doc));
                                                var maxIndex = undefined;
                                                if (doc) {
                                                    console.log(doc.object.cazastDesignation);
                                                    var name = doc.object.cazastDesignation;
                                                    var n = name.lastIndexOf("-");
                                                    if (n >= 0) {
                                                        var index = name.substr(n + 1);
                                                        if (!maxIndex || index > maxIndex) {
                                                            maxIndex = index;
                                                        }
                                                    }
                                                }

                                                function pad(num, size) {
                                                    var s = num + "";
                                                    while (s.length < size)
                                                        s = "0" + s;
                                                    return s;
                                                }

                                                var finalIndex = !maxIndex ? '000' : parseInt(maxIndex) + 1;
                                                //var updateObject = {};
                                                //updateObject.object = {};
                                                updateObject.object.cazastDesignation = cazastDesignation + pad(finalIndex, 3);
                                                console.log(updateObject.object.cazastDesignation);
                                                Detection.findOneAndUpdate({"_id": obs_id}, {$set: updateObject}, function (err, doc) {});
                                            });

                                }
                            });
                        } else {
                            console.log("status: " + response.statusCode + error);
                        }
                    });
                });
    }

};