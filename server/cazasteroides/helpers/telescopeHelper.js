/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var increase = function (meta, incDetection, incCadidate/*, incNewAsteroid*/) {
    var Telescope = require('../models/telescope.js');

    if (!incDetection)
        incDetection = 0;
    if (!incCadidate)
        incCadidate = 0;
//    if (!incNewAsteroid)
//        incNewAsteroid = 0;

    var telescopeQuery = {
        name: meta.TELESCOP,
        lat: meta.SITELAT,
        long: meta.SITELONG,
        alt: meta.SITEALT,
        ocn: meta.OCN
    };
    Telescope.findOneAndUpdate(
            telescopeQuery,
            {
                $inc: {
                    detections: incDetection,
                    candidates: incCadidate
                            //newAst: incNewAsteroid
                }
            },
            {upsert: true},
            function (err, doc) {
                //console.log("err: " + JSON.stringify(err));
                //console.log("doc: " + JSON.stringify(doc));
            }
    );
};

exports.increaseDetection = function (det) {
    var Detection = require('../models/observation.js');

    Detection.findById(det._id,
            {"image": 1})
            .populate('image', 'meta').exec(function (err, detection) {
        if (!err) {
            increase(detection.image.meta, 1);
        }
    });

};

exports.increase = increase;

