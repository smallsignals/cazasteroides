/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
const isDateInRage = (startDate, endDate) => (dateToCheck) => {
        return dateToCheck >= startDate && dateToCheck <= endDate;
    };

exports.compareContest = function (a, b) {
    if (a.contest) {
        a = a.contest;
    }
    if (b.contest) {
        b = b.contest;
    }

    var startA = new Date(a.time_start);
    var startB = new Date(b.time_start);
    var endA = new Date(a.time_end);
    var endB = new Date(b.time_end);
    const isInRangeA = isDateInRage(startA, endA)(new Date());
    const isInRangeB = isDateInRage(startB, endB)(new Date());

    const isFinishedA = (new Date() > endA);
    const isFinishedB = (new Date() > endB);

    if (isInRangeA && isInRangeB) {
        if (endA > endB) {
            return 1;
        } else if (endA < endB) {
            return -1;
        } else {
            return 0;
        }
    } else if (isInRangeA) {
        return -1;
    } else if (isInRangeB) {
        return 1;
    } else if (isFinishedA && isFinishedB) {
        if (endA > endB) {
            return 1;
        } else if (endA < endB) {
            return -1;
        } else {
            return 0;
        }
    } else if (isFinishedA) {
        return 1;
    } else if (isFinishedB) {
        return -1;
    } else {
        if (startA > startB) {
            return 1;
        } else if (startA < startB) {
            return -1;
        } else {
            return 0;
        }
    }

};
exports.sortContestPoints = function (a, b) {
    if (a.contests[0].points > b.contests[0].points) {
        return -1;
    } else {
        return 1;
    }
};
