function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}

/**
 * Muy importante comprobar que esta variable este a false en los entornos de producción
 */
define("DEBUG", false);//FIXME poner a false para produccion!!!!!!!

/**
 * API KEY
 */
define('API_KEY_TELESCOPIO', 'f83815230806f44f6a4fe2e6d7ec311412e1ed61');

/**
 * FACEBOOK
 */
define('FACEBOOK_APP_ID', 1068871259896373);
define('FACEBOOK_APP_SECRET', '5e88376b25ca1144e5ab4060481f82dc');
define('FACEBOOK_URL_CALLBACK', 'http://13.59.126.52/auth/facebook/callback');

/**
 * LAB-FIWARE
 */
define('URL_LAB_FIWARE', 'https://account.lab.fiware.org');

define('URL_LAB_FIWARE_ID', 'f42ddb9df48f4e1c9007e3402d9df146');
define('URL_LAB_FIWARE_ID_SECRET', 'ff0406535a494ec7abe8b61a277ec7f0');
define('URL_LAB_FIWARE_CALLBACK', 'http://localhost:3000/auth/callback');
define('URL_LAB_FIWARE_FRONTEND', 'https://multifab.eu');

define('URL_LAB_FIWARE_OAUTH2_AUTHORIZE', this.URL_LAB_FIWARE + '/oauth2/authorize');
define('URL_LAB_FIWARE_OAUTH2_TOKEN', this.URL_LAB_FIWARE + '/oauth2/token');
define('URL_LAB_FIWARE_USER', this.URL_LAB_FIWARE + '/user?');

/**
 * SMTP
 */
define('SMTP_HOST', 'smtp.1and1.es');
define('SMTP_PORT', 587);
define('SMTP_USER', 'cazasteroides@cazasteroides.org');
define('SMTP_PASS', 'C@z@st3r01d3!');

/**
 * URL
 */
define("URL_TELESCOPIOVIRTUAL", 'http://www.telescopiovirtual.com:80/services/remote/' + this.API_KEY_TELESCOPIO + '/');
define("URL_TELESCOPIOVIRTUAL_IMAGEN", this.URL_TELESCOPIOVIRTUAL + 'imagen/');
define("URL_STELLAR_DATABASE_NEIGHBORS", 'http://www.stellar-database.com/Scripts/find_neighbors.exe?');
define("URL_MINORPLANETCENTER", 'http://www.minorplanetcenter.net/cgi-bin/mpcheck.cgi');

define("URL_MAPSERV_CGI_BIN", 'http://13.59.126.52/cgi-bin/mapserv?');

//define("SRC_PORTAL", this.DEBUG ? 'localhost:3001' : 'http://api.cazasteroides.org:3001');

/**
 * Config
 */
define("MONGODB", this.DEBUG ? 'mongodb://web:web@ds143980.mlab.com:43980/cazasteroides' : 'mongodb://web:web@ds129641.mlab.com:29641/cazasteroides');//'mongodb://web:web@ds135818.mlab.com:35818/cazasteroides'   <- Antigua en producción (de Jorge)
define("SRC_GDALINFO", '/usr/local/bin/gdalinfo ');//Respetar el espacio final!!
define("SRC_PYTHON", '/usr/bin/python ');//Respetar el espacio final!!

define("SRC_MAIN", (require('path').resolve('../../') + '/'));
define("SRC_SERVER", this.SRC_MAIN + 'server/');
define("SRC_FOLDER_GIF", this.SRC_SERVER + 'cazasteroides/gif/');
define("SRC_DATA", this.SRC_MAIN + 'data/');
define("SRC_SCRIPTS", this.SRC_MAIN + 'scripts/');
define("SRC_SCRIPT_CARGAIMG_SH", this.SRC_DATA + 'cargaimg.sh ');//Respetar el espacio final!!
define("SRC_MOBILE_APP", this.SRC_MAIN + 'gloria-mobile/platforms/browser/www');//../../gloria-mobile/platforms/browser/www
