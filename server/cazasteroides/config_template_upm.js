function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}

/**
 * Muy importante comprobar que esta variable este a false en los entornos de producción
 */
define("DEBUG", false);//FIXME poner a false para produccion!!!!!!!

/**
 * API KEY
 */
define('API_KEY_TELESCOPIO', '85ab4f72b2c8e71988d47d1fabfb0f163c8b5f76');

/**
 * FACEBOOK
 */
define('FACEBOOK_APP_ID', 1068871259896373);
define('FACEBOOK_APP_SECRET', '5e88376b25ca1144e5ab4060481f82dc');
define('FACEBOOK_URL_CALLBACK', 'http://138.4.110.153/api/auth/facebook/callback');

/**
 * LAB-FIWARE
 */
define('URL_LAB_FIWARE', 'https://account.lab.fiware.org');

define('URL_LAB_FIWARE_ID', '365a2073c6434168aa3d2ff5f1f062ba');
define('URL_LAB_FIWARE_ID_SECRET', '49fce34030ec413d84e9c2a8e302095b');
define('URL_LAB_FIWARE_CALLBACK', 'https://api.multifab.eu/auth/callback');
define('URL_LAB_FIWARE_FRONTEND', 'https://multifab.eu');

define('URL_LAB_FIWARE_OAUTH2_AUTHORIZE', this.URL_LAB_FIWARE + '/oauth2/authorize');
define('URL_LAB_FIWARE_OAUTH2_TOKEN', this.URL_LAB_FIWARE + '/oauth2/token');
define('URL_LAB_FIWARE_USER', this.URL_LAB_FIWARE + '/user?');

/**
 * SMTP
 */
define('SMTP_HOST', 'smtp.1and1.es');
define('SMTP_PORT', 587);
define('SMTP_USER', 'cazasteroides@cazasteroides.org');
define('SMTP_PASS', 'C@z@st3r01d3!');

/**
 * URL
 */
define("URL_TELESCOPIOVIRTUAL", 'http://www.telescopiovirtual.com:80/services/remote/' + this.API_KEY_TELESCOPIO + '/');
define("URL_TELESCOPIOVIRTUAL_IMAGEN", this.URL_TELESCOPIOVIRTUAL + 'imagen/');
define("URL_STELLAR_DATABASE_NEIGHBORS", 'http://www.stellar-database.com/Scripts/find_neighbors.exe?');
define("URL_MINORPLANETCENTER", 'http://www.minorplanetcenter.net/cgi-bin/mpcheck.cgi');

define("URL_MAPSERV_CGI_BIN", 'http://localhost/cgi-bin/mapserv?');

define("SRC_PORTAL", this.DEBUG ? 'localhost:3001' : 'http://api.cazasteroides.org:3001');

/**
 * Config
 */
define("MONGODB", this.DEBUG ? 'mongodb://web:web@ds143980.mlab.com:43980/cazasteroides' : 'mongodb://web:web@ds135818.mlab.com:35818/cazasteroides');//mongodb://web:web@ds129641.mlab.com:29641/cazasteroides  <- Nueva en producción
define("SRC_GDALINFO", '/usr/bin/gdalinfo ');//Respetar el espacio final!!
define("SRC_PYTHON", '/usr/bin/python ');//Respetar el espacio final!!

define("SRC_MAIN", '/var/opt/cazasteroides/');
define("SRC_SERVER", this.SRC_MAIN + 'server/');
define("SRC_FOLDER_GIF", this.SRC_SERVER + 'cazasteroides/gif/');
define("SRC_DATA", this.SRC_MAIN + 'data/');
define("SRC_SCRIPTS", this.SRC_MAIN + 'scripts/');
define("SRC_SCRIPT_CARGAIMG_SH", this.SRC_DATA + 'cargaimg.sh ');//Respetar el espacio final!!
define("SRC_MOBILE_APP", this.SRC_MAIN + 'gloria-mobile/platforms/browser/www');
