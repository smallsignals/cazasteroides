var SERVER_URL_BASE  = "http://localhost:3000/"
api_createImage = function (filename, cbFn){
    var partData = {
        'image': {
            'filename': filename
        }
    };
    $.ajax({
        url: SERVER_URL_BASE + 'images/',
        type: 'POST',
        data: JSON.stringify(partData),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        /*beforeSend : function( xhr ) {
            xhr.setRequestHeader( 'Authorization', 'BEARER ' + ApiConfig.TOKEN  );
        },*/
        success: function(data){
            if(cbFn) {
                cbFn(data);
            }
        },
        error: function() {
            console.log('Error connecting to server');
            if(cbFn) {
                cbFn("Error");
            }
        },
        failure: function(errMsg) {
            error.log(errMsg);
            if(cbFn) {
                cbFn("Error");
            }
        }
    });
}
