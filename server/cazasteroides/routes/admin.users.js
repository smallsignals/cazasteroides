var express = require('express');
var passport = require('passport');
var User = require('../models/user.js');
var router = express.Router();
var AdminHelper = require('../helpers/adminHelper.js');

router.get('/', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    User.find({provider: 'local'},
            {
                _id: 1,
                username: 1,
                admin: 1,
                superAdmin: 1,
                adminTelescopes: 1,
                email: 1
            }
    ).sort({superAdmin: -1, admin: -1, username: 1}).exec(function (err, users) {
        if (err) {
            res.send(err);
        } else {
            res.json(users);
        }
    });
});

/* Make/Remove user admin/superAdmin/Telescopes. */
router.post('/update', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    if (!req.body.user._id) {
        res.status(400);
        res.header('Cazast_error', 'error_invalid_params');
        res.send('error_invalid_params');
        return;
    }

    //console.log('req.body: ' + JSON.stringify(req.body));
    User.findById(req.body.user._id).exec()
            .then(function (user) {

                if (req.body.user.admin) {
                    user.admin = true;
                } else {
                    if (user.admin) {
                        console.log("Remove Admin property");
                        user.admin = undefined;
                    }
                }

                if (req.body.user.superAdmin) {
                    user.superAdmin = true;
                    user.admin = true;
                } else {
                    if (user.superAdmin) {
                        console.log("Remove superAdmin property")
                        user.superAdmin = undefined;
                    }
                }

                user.adminTelescopes = req.body.user.adminTelescopes;
                //console.log(JSON.stringify(user));
                return user.save();
            }).then(function () {
        res.send("ok");
    });
    // executes
});

router.get('/me', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    if (AdminHelper.isSuperAdmin(req.user)) {
        var Quintet = require('../models/image.js');
        Quintet.find({}, {name: 1, meta: 1})
                .exec(function (err, img) {
                    var arr = [];
                    for (var k in  img) {
                        var telescope = img[k] && img[k].meta && img[k].meta.TELESCOP ? img[k].meta.TELESCOP : img[k].name.substr(14);
                        if (arr.indexOf(telescope) === -1) {
                            arr.push(telescope);
                        }
                    }
                    arr.sort();
                   req.user.adminTelescopes = arr;
                    res.json(req.user);
                });
    } else {
        res.json(req.user);
    }

});

router.get('/notifyNewQuintets', function (req, res, next) {
//notifyNewQuintets?total
    var cutoff = new Date();
    cutoff.setDate(cutoff.getHours() - 8);
    User.find({
        username: {'$in': ['slemesp', 'mserraricart']},
        '$or': [{"notification.newQuintetsAvailableEmail": {$lt: cutoff}}, {"notification.newQuintetsAvailableEmail": {$exists: false}}]
    },
            {
                _id: 1,
                username: 1,
                email: 1
            }
    ).lean().exec(function (err, users) {
        if (err) {
            res.send(err);
        } else {
            var mail = require("../mail.js");
            for (var i = 0; i < users.length; i++) {
                mail.sendNewQuintetsAvailables(users[i]);
            }
            res.send("ok");
        }
    });
});

module.exports = router;
