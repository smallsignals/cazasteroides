var express = require('express');
var router = express.Router();
var passport = require("passport");
var GCM = require('../models/gcm.js');

router.post('/', passport.authenticate('bearer'), function (req, res) {
    console.log(JSON.stringify(req.body));
    if (req.body.token) {
        GCM.find({token: req.body.token}).remove().exec();
        var gcm = new GCM();
        gcm.token = req.body.token;
        gcm.user = req.user._id;
        gcm.save(function (err) {
            console.log(JSON.stringify(err));
        });
    }
    res.send("Ok");

});

router.delete('/', passport.authenticate('bearer'), function (req, res) {
    console.log(JSON.stringify(req.query));
    if (req.query.token) {
        GCM.find({token: req.query.token}).remove().exec();
    }
    res.send("Ok");
});

module.exports = router;