var express = require('express');
var passport = require('passport');
var User = require('../models/user.js');
const Score = require('../helpers/scoreCalculation.js');
var Detection = require('../models/observation.js');
var AdminHelper = require('../helpers/adminHelper.js');
var router = express.Router();
var Setup = require('../models/setup.js');
var UserHelper = require('../helpers/userHelper.js');

// Create endpoint /api/obs for GET
router.get('/', passport.authenticate('bearer'), function (req, res) {//TODO add pagination

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    var query = {};
    var queryTelescopes = {};

    if (!AdminHelper.isSuperAdmin(req.user)) {
        var telescopes = req.user.adminTelescopes;
        if (!telescopes || telescopes.length <= 0)
        {
            res.json([]);
            return;
        }

        var telescope = req.query.telescope;
        if (telescope) {
            telescopes = [telescope];
        }

        for (var k = 0; k < telescopes.length; k++) {
            var expression = telescopes[k] + "$";
            var rx = new RegExp(expression, 'i');
            telescopes[k] = rx;
        }

        queryTelescopes = {'name': {$in: telescopes}};
    }
//
//
//
//    var telescopes = req.user.adminTelescopes;
//    if (!telescopes || telescopes.length <= 0)
//    {
//        res.json([]);
//        return;
//    }

    var status = req.query.status;
    var quintet = req.query.quintet;

    var minDetections = 0;
    if (req.query.min) {
        var minDetections = parseInt(req.query.min);
    }


    if (status) {
        query["status"] = {$eq: status};
    }

    if (quintet) {
        query["image"] = {$eq: quintet};
    }

    if (minDetections) {
        if (minDetections >= 1) {
            query["revisit." + (minDetections - 1)] = {$exists: true};//revisit 4 and creator 1 = 5      
        }
    }


    var condition = {};

    if (req.query.skip && req.query.limit) {
        condition = {skip: parseInt(req.query.skip), limit: parseInt(req.query.limit)};
    }
//    var expression = '(' + telescopes.join('|') + ")$";
//    var rx = new RegExp(expression, 'i');
//    console.log(rx);
//    //query["image.meta.TELESCOP"] = {$in: telescopes};
//    console.log(query);
    Detection
            .find(
                    query
                    , {_id: 1, image: 1, Coord: 1, avgCoord: 1, revisit: 1, votes: 1, downvotes: 1, status: 1, pixCoordAbs: 1, 'revisit.x': 1, 'revisit.y': 1, 'object.mpcDesignation': 1, 'object.cazastDesignation': 1, 'quintetn': 1}
            //, condition
            )
            .lean()
            .populate('image', 'name fecha filename active coeff.0.TIMESTAM coeff.1.TIMESTAM coeff.2.TIMESTAM coeff.3.TIMESTAM coeff.4.TIMESTAM meta.TIMESTAM', queryTelescopes)
            .exec(function (err, obs) {
                if (err) {
                    res.send(err);
                    return;
                }
                //show only observations for the current set of images
                for (var i = obs.length - 1; i >= 0; i--) {
//                    console.log(JSON.stringify(obs[i]));
                    if (!obs[i].image) {
                        obs.splice(i, 1);
                    } else {
                        if (obs[i].votes) {
                            obs[i].votes = new Array(obs[i].votes.length);
                        }
                        if (obs[i].downvotes) {
                            obs[i].downvotes = new Array(obs[i].downvotes.length);
                        }
                    }
                }

                obs.sort(require('../helpers/detectionHelper.js').compareDetection);

                if (condition && condition.skip !== undefined && condition.limit !== undefined) {
                    var limit = condition.skip + condition.limit;
                    obs = obs.slice(condition.skip, limit);
                }


                if (condition && condition.skip === 0) {
                    Detection.count(query, function (err, total) {
                        res.json({result: obs, total: total});
                    });
                } else {
                    res.json({result: obs});
                }
            });
});

/* GET home page. */
router.post('/update', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

//    console.log(JSON.stringify(req.body));
    var ObsID = req.body.obs._id;
    var conditions =
            {
                "_id": req.body.obs._id
            };
//Solo los superadmins pueden cambiar el estado de una deteccion que no esta pendiente
//Los superadmins solo pueden cambiar el estado de una deteccion a otro estado distinto al que ya tiene
    if (!AdminHelper.isSuperAdmin(req.user)) {
        conditions['status'] = 'pending';
    } else {
        conditions['status'] = {$ne: req.body.obs.status};
    }

    var update = req.body.obs;
    var options = {
        multi: false
    };
    delete update._id;
//    console.log(JSON.stringify(conditions));
//    console.log(JSON.stringify(update));

    Setup.findOne({}, function (err, setup) {
        var downvotesPoints = 10;
        var upvotePercentagePoints = 0.3;

        if (setup) {
            var indexDownvotesPoints = setup.config.findIndex(function (element) {
                return element.key.toString() === "downvote_points";
            });

            if (indexDownvotesPoints >= 0 && indexDownvotesPoints < setup.config.length) {
                downvotesPoints = parseInt(setup.config[indexDownvotesPoints].value);
            }

            var indexUpvotesPoints = setup.config.findIndex(function (element) {
                return element.key.toString() === "upvote_percentage_points";
            });

            if (indexUpvotesPoints >= 0 && indexUpvotesPoints < setup.config.length) {
                upvotePercentagePoints = parseFloat(setup.config[indexUpvotesPoints].value);
            }
        }

        Detection.update(conditions, update, options, callback);
        function callback(err, numAffected) {
            // numAffected is the number of updated documents
            //console.log(numAffected + " affected rows ");
            res.send(JSON.stringify({
                "affected": numAffected,
                "error": err
            }));

            if (err || !numAffected || numAffected.ok <= 0 || numAffected.n <= 0 || numAffected.nModified <= 0) {
                return;
            }

            /**
             * Añadir puntos
             */
            Detection.findById(ObsID
                    , {
                        "status": 1,
                        "votes": 1,
                        "downvotes": 1,
                        "revisit": 1,
                        "user": 1,
                        "points": 1,
                        "image": 1
                    })
                    .populate('user', '_id contests')
                    .populate('revisit.user', '_id contests')
                    .populate('votes', '_id contests')
                    .populate('downvotes', '_id contests')
                    .exec(function (err, o)
                    {
                        if (o) {
                            //Obtener los concursos activos
                            var dateNow = Date.now();
                            var Contest = require('../models/contest.js');
                            Contest.find({
                                "time_start": {$lte: dateNow},
                                "time_end": {$gte: dateNow}})
                                    .lean()
                                    .distinct('_id')
                                    .exec(function (err, activeContestsIds) {

                                        function increaseUserContestsPoints(user, points) {
                                            try {
                                                if (user && activeContestsIds && activeContestsIds.length > 0 && user.contests && user.contests.length > 0) {
                                                    for (var i = 0; i < user.contests.length; i++) {
                                                        if (activeContestsIds.some(function (id) {
                                                            return id.toString() === user.contests[i].contest.toString();
                                                        })) {
                                                            var query = {$inc: {}};
                                                            query['$inc']['contests.' + i + '.points'] = points;
                                                            User.findOneAndUpdate({"_id": user._id}, query, function (err, u2) {
                                                            });
                                                        }
                                                    }
                                                }
                                            } catch (e) {

                                            }
                                        }

//                            console.log("Obs datas: " + JSON.stringify(o));
                                        switch (o.status) {
                                            case 'rejected':
                                                console.log("Rejected Case");

                                                var arrUsersDetect = [o.user._id.toString()];

                                                UserHelper.increaseDetectionRejected(o.user._id);


                                                for (var i = 0; i < o.revisit.length; i++) {
                                                    var auxObs = o.revisit[i];
                                                    if (auxObs.user._id) {
                                                        arrUsersDetect.push(auxObs.user._id.toString());
                                                    }
                                                    UserHelper.increaseDetectionRejected(auxObs.user._id);
                                                }

                                                for (var i = 0; i < o.votes.length; i++) {
                                                    UserHelper.increaseVoteWrong(o.votes[i]._id);
                                                }

                                                // Dar puntos a los usuarios que votaron negativamente cuando el state es rejected
                                                for (var i = 0; i < o.downvotes.length; i++) {
                                                    if (arrUsersDetect.indexOf(o.downvotes[i]._id.toString()) >= 0) {
                                                        console.log("User " + o.downvotes[i]._id + " detect and vote");
                                                        continue;
                                                    }
                                                    arrUsersDetect.push(o.downvotes[i]._id.toString());

                                                    User.findOneAndUpdate({"_id": o.downvotes[i]._id}, {$inc: {points: downvotesPoints, 'sVotes.corrects': 1, 'sVotes.points': downvotesPoints}}, function (err, u) {
                                                        if (err)
                                                            console.log("downvotes Error!");
                                                        else
                                                            console.log(u.username + " +" + downvotesPoints + " points");


                                                    });
                                                    increaseUserContestsPoints(o.downvotes[i], downvotesPoints);
                                                }
                                                break;
                                            case 'approved':
                                                console.log("Approved Case");
                                                // Dar puntos a los usuarios que detectaron y a los que votaron positivamente
                                                /**
                                                 * Añadir los puntos al primero
                                                 */
                                                var arrUsersDetect = [o.user._id.toString()];

                                                User.findOneAndUpdate({"_id": o.user._id}, {$inc: {points: o.points, 'sDetections.approved': 1, 'sDetections.points': o.points}}, function (err, u) {
                                                    if (err)
                                                        console.log("user Error!");
                                                    else
                                                        console.log(u.username + " +" + o.points + " points");


                                                });
                                                increaseUserContestsPoints(o.user, o.points);

                                                var maxPoints = o.points;
                                                /**
                                                 * Añadimos los puntos al resto de usuarios que tambien detectaron
                                                 */
                                                for (var i = 0; i < o.revisit.length; i++) {
                                                    var auxObs = o.revisit[i];
                                                    if (auxObs.user._id && auxObs.points) {

                                                        if (arrUsersDetect.indexOf(auxObs.user._id.toString()) >= 0) {
                                                            console.log("User " + auxObs.user._id + " detect and vote");
                                                            continue;
                                                        }

                                                        if (auxObs.points > maxPoints)
                                                            maxPoints = auxObs.points;

                                                        arrUsersDetect.push(auxObs.user._id.toString());
                                                        User.findOneAndUpdate({"_id": auxObs.user._id}, {$inc: {points: auxObs.points, 'sDetections.approved': 1, 'sDetections.points': auxObs.points}}, function (err, u) {
                                                            if (err)
                                                                console.log("revisit Error!");
                                                            else
                                                                console.log(u.username + " +" + auxObs.points + " points");


                                                        });
                                                        increaseUserContestsPoints(auxObs.user, auxObs.points);
                                                    }
                                                }


                                                /**
                                                 * Darle a los votantes que acertaron, la mitad de la maxima puntuación
                                                 */
                                                var vtPoints = (maxPoints * upvotePercentagePoints);
                                                for (var i = 0; i < o.votes.length; i++) {
                                                    if (arrUsersDetect.indexOf(o.votes[i]._id.toString()) >= 0) {
                                                        console.log("User " + o.votes[i]._id + " detect and vote");
                                                        continue;
                                                    }
                                                    arrUsersDetect.push(o.votes[i]._id.toString());

                                                    User.findOneAndUpdate({"_id": o.votes[i]._id}, {$inc: {points: vtPoints, 'sVotes.corrects': 1, 'sVotes.points': vtPoints}}, function (err, u) {
                                                        if (err)
                                                            console.log("votes Error!");
                                                        else
                                                            console.log(u.username + " +" + vtPoints + " points");

                                                    });
                                                    increaseUserContestsPoints(o.votes[i], vtPoints);
                                                }

                                                for (var i = 0; i < o.downvotes.length; i++) {
                                                    UserHelper.increaseVoteWrong(o.downvotes[i]._id);
                                                }

                                                var QuintetHelper = require('../helpers/quintetHelper.js');
                                                QuintetHelper.increaseApproved(o.image);

                                                const MPC = require('../helpers/mpcHelper.js');
                                                new MPC().check(ObsID);
                                                break;
                                            default:
                                                console.log("Default Case");
                                                break;
                                        }
                                    });     //console.log(JSON.stringify(o));
                        }
                    });
        }
    });
});

router.get('/status', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    res.json(["pending", "approved", "rejected"]);
});

router.get('/syncMPCApproved', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }
    Detection.find(
            {status: 'approved', $or: [{'object.mpcChecked': false}, {'object.mpcChecked': {$exists: false}}]}
    ,
            {"_id": 1}
    )
            .exec(function (err, obs) {
                if (!err) {
                    for (var i = 0; i < obs.length; i++) {
                        const MPC = require('../helpers/mpcHelper.js');
                        new MPC().check(obs[i]._id);
                    }
                }

            });
    res.send('ok');
});

router.post('/avgCoordSync', passport.authenticate('bearer'), function (req, res, next) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    Detection.find()
            .exec(function (err, obs) {
                if (obs) {
                    for (var i = 0; i < obs.length; i++) {
                        var d = obs[i];
                        var meanra = parseFloat(d.Coord[0]);
                        var meandec = parseFloat(d.Coord[1]);

//                        coordprec = 0.0007;
//                        if (d.avgCoord && d.avgCoord[0]) {
//                            var diff = Math.abs(d.avgCoord[0] - meanra);
//                            //if (diff > coordprec) {
//                                console.log(d._id + " detection 1 RA diff " + diff);
//                            //}
//                        }
//                        if (d.avgCoord && d.avgCoord[1]) {
//                            var diff = Math.abs(d.avgCoord[1] - meandec);
//                            //if (diff > coordprec) {
//                                console.log(d._id + " detection 1 DEC diff " + diff);
//                            //}
//                        }

                        for (var n = 0; n < d.revisit.length; n++) {
                            meanra += parseFloat(d.revisit[n].x);
                            meandec += parseFloat(d.revisit[n].y);

//                            if (d.avgCoord && d.avgCoord[0]) {
//                                var diff = Math.abs(d.avgCoord[0] - parseFloat(d.revisit[n].x));
//                                //if (diff > coordprec) {
//                                    console.log(d._id + " detection " + (n + 2) + " DEC diff " + diff);
//                                //}
//                            }
//                            if (d.avgCoord && d.avgCoord[1]) {
//                                var diff = Math.abs(d.avgCoord[1] - parseFloat(d.revisit[n].y));
//                                //if (diff > coordprec) {
//                                    console.log(d._id + " detection " + (n + 2) + " DEC diff " + diff);
//                                //}
//                            }

                        }
                        meanra /= (d.revisit.length + 1);
                        meandec /= (d.revisit.length + 1);

                        d.avgCoord = [meanra, meandec];
                        //console.log(d.avgCoord);
                        d.save();
                    }
                }
            }
            );

    res.send('ok');
});

router.get('/resetUsersKarma', passport.authenticate('bearer'), function (req, res, next) {
    //var schema = {sDetections: {total: 0, approved: 0, points: 0}, sVotes: {total: 0, corrects: 0, points: 0}};
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    var allUsers = {};
    Detection.find()
            .populate('user', '_id ')
            .populate('revisit.user', '_id ')
            .populate('votes', '_id ')
            .populate('downvotes', '_id ')
            .exec(function (err, detections) {
                console.log("Total Detections: " + detections.length);
                for (var h = 0; h < detections.length; h++) {
                    //console.log(h + " " + ((h * 100) / parseInt(detections.length)).toFixed(2) + '%');
                    var o = detections[h];

                    var arrUsersDetect = [o.user._id.toString()];

                    if (!allUsers[o.user._id]) {
                        allUsers[o.user._id] = {};// {sDetections: {total: 0, approved: 0, rejected: 0, points: 0}, sVotes: {total: 0, corrects: 0, wrongs: 0, points: 0}};
                    }
                    if (!allUsers[o.user._id].sDetections) {
                        allUsers[o.user._id].sDetections = {};
                    }

                    if (!allUsers[o.user._id].sDetections.total) {
                        allUsers[o.user._id].sDetections.total = 0;
                    }
                    allUsers[o.user._id].sDetections.total++;
                    //approved
                    if (o.status == 'approved') {
                        if (!allUsers[o.user._id].sDetections.approved) {
                            allUsers[o.user._id].sDetections.approved = 0;
                        }
                        allUsers[o.user._id].sDetections.approved++;

                        if (!allUsers[o.user._id].sDetections.points) {
                            allUsers[o.user._id].sDetections.points = 0;
                        }
                        allUsers[o.user._id].sDetections.points += o.points;
                    } else if (o.status == 'rejected') {
                        if (!allUsers[o.user._id].sDetections.rejected) {
                            allUsers[o.user._id].sDetections.rejected = 0;
                        }
                        allUsers[o.user._id].sDetections.rejected++;
                    }

                    var maxPoints = o.points;
                    for (var i = 0; i < o.revisit.length; i++) {
                        var auxObs = o.revisit[i];
                        if (auxObs.user._id && auxObs.points) {
                            if (arrUsersDetect.indexOf(auxObs.user._id.toString()) >= 0) {
                                continue;
                            }
                            arrUsersDetect.push(auxObs.user._id.toString());

                            if (auxObs.points > maxPoints)
                                maxPoints = auxObs.points;

                            //increaseUserContestsPoints(auxObs.user, auxObs.points);
                            if (!allUsers[auxObs.user._id]) {
                                allUsers[auxObs.user._id] = {};// {sDetections: {total: 0, approved: 0, rejected: 0, points: 0}, sVotes: {total: 0, corrects: 0, wrongs: 0, points: 0}};
                            }
                            if (!allUsers[auxObs.user._id].sDetections) {
                                allUsers[auxObs.user._id].sDetections = {};
                            }

                            if (!allUsers[auxObs.user._id].sDetections.total) {
                                allUsers[auxObs.user._id].sDetections.total = 0;
                            }
                            allUsers[auxObs.user._id].sDetections.total++;

                            //approved
                            if (o.status == 'approved') {
                                if (!allUsers[auxObs.user._id].sDetections.approved) {
                                    allUsers[auxObs.user._id].sDetections.approved = 0;
                                }
                                allUsers[auxObs.user._id].sDetections.approved++;
                                if (!allUsers[auxObs.user._id].sDetections.points) {
                                    allUsers[auxObs.user._id].sDetections.points = 0;
                                }
                                allUsers[auxObs.user._id].sDetections.points += auxObs.points;
                            } else if (o.status == 'rejected') {
                                if (!allUsers[auxObs.user._id].sDetections.rejected) {
                                    allUsers[auxObs.user._id].sDetections.rejected = 0;
                                }
                                allUsers[auxObs.user._id].sDetections.rejected++;
                            }
                        }
                    }


                    /**
                     * Darle a los votantes que acertaron, la mitad de la maxima puntuación
                     */
                    var vtPoints = (maxPoints * 0.2);
                    for (var i = 0; i < o.votes.length; i++) {
                        if (arrUsersDetect.indexOf(o.votes[i]._id.toString()) >= 0) {
                            continue;
                        }
                        arrUsersDetect.push(o.votes[i]._id.toString());

                        //increaseUserContestsPoints(o.votes[i], vtPoints);
                        if (!allUsers[o.votes[i]._id]) {
                            allUsers[o.votes[i]._id] = {};// {sDetections: {total: 0, approved: 0, rejected: 0, points: 0}, sVotes: {total: 0, corrects: 0, wrongs: 0, points: 0}};
                        }
                        if (!allUsers[o.votes[i]._id].sVotes) {
                            allUsers[o.votes[i]._id].sVotes = {};
                        }

                        if (!allUsers[o.votes[i]._id].sVotes.total) {
                            allUsers[o.votes[i]._id].sVotes.total = 0;
                        }

                        allUsers[o.votes[i]._id].sVotes.total++;

                        //approved
                        if (o.status == 'approved') {
                            if (!allUsers[o.votes[i]._id].sVotes.corrects) {
                                allUsers[o.votes[i]._id].sVotes.corrects = 0;
                            }
                            allUsers[o.votes[i]._id].sVotes.corrects++;
                            if (!allUsers[o.votes[i]._id].sVotes.points) {
                                allUsers[o.votes[i]._id].sVotes.points = 0;
                            }
                            allUsers[o.votes[i]._id].sVotes.points += vtPoints;
                        } else if (o.status == 'rejected') {
                            if (!allUsers[o.votes[i]._id].sVotes.wrongs) {
                                allUsers[o.votes[i]._id].sVotes.wrongs = 0;
                            }
                            allUsers[o.votes[i]._id].sVotes.wrongs++;
                        }
                    }

                    for (var i = 0; i < o.downvotes.length; i++) {
                        if (arrUsersDetect.indexOf(o.downvotes[i]._id.toString()) >= 0) {
                            continue;
                        }
                        arrUsersDetect.push(o.downvotes[i]._id.toString());

                        if (!allUsers[o.downvotes[i]._id]) {
                            allUsers[o.downvotes[i]._id] = {};// {sDetections: {total: 0, approved: 0, rejected: 0, points: 0}, sVotes: {total: 0, corrects: 0, wrongs: 0, points: 0}};
                        }
                        if (!allUsers[o.downvotes[i]._id].sVotes) {
                            allUsers[o.downvotes[i]._id].sVotes = {};
                        }

                        if (!allUsers[o.downvotes[i]._id].sVotes.total) {
                            allUsers[o.downvotes[i]._id].sVotes.total = 0;
                        }
                        allUsers[o.downvotes[i]._id].sVotes.total++;

                        //rejected               
                        if (o.status == 'rejected') {
                            if (!allUsers[o.downvotes[i]._id].sVotes.corrects) {
                                allUsers[o.downvotes[i]._id].sVotes.corrects = 0;
                            }
                            allUsers[o.downvotes[i]._id].sVotes.corrects++;
                            if (!allUsers[o.downvotes[i]._id].sVotes.points) {
                                allUsers[o.downvotes[i]._id].sVotes.points = 0;
                            }
                            allUsers[o.downvotes[i]._id].sVotes.points += 5;
                        } else if (o.status == 'approved') {
                            if (!allUsers[o.downvotes[i]._id].sVotes.wrongs) {
                                allUsers[o.downvotes[i]._id].sVotes.wrongs = 0;
                            }
                            allUsers[o.downvotes[i]._id].sVotes.wrongs++;
                        }
                    }

                }
                //console.log(JSON.stringify(allUsers, null, 3));

                for (var key in allUsers) {
                    if (key === 'length' || !allUsers.hasOwnProperty(key))
                        continue;
                    var value = allUsers[key];
                    //console.log(key + " " + JSON.stringify(value));
                    User.findOneAndUpdate({"_id": key}, {$set: value}, function (err, u) {
                    });
                }
                res.send("Ok");
            });

});


module.exports = router;
