var express = require('express');
//var passport = require('passport');
var router = express.Router();
//var math = require("mathjs");
//var mongoose = require('mongoose');
var child_process = require('child_process');
//var Observation = require('../models/observation.js');
//var Rectangle = require('../models/rectangle.js');
var Image = require('../models/image.js');
//var Promise = require('mpromise');
//var Q = require('q');
var fs = require('fs');
//var path = require('path');
//var gm = require('gm');
//var querystring = require('querystring');
var configConst = require("../config_template");

var request = require('request');
var Url = require('url');
//var async = require('async');

//function isDirSync(aPath) {
//    try {
//        return fs.statSync(aPath).isDirectory();
//    } catch (e) {
//        if (e.code === 'ENOENT') {
//            return false;
//        } else {
//            throw e;
//        }
//    }
//}
///* GET home page. */
//router.get('/', function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    Image.find({},
//            {'_id': 1, 'name': 1, 'fecha': 1, 'active': 1}
//    ).sort({active: -1, fecha: -1}).exec(function (err, docs) {
//        console.log("length: " + docs.length);
//        //console.log(JSON.stringify(docs));
//        res.send(JSON.stringify(docs));
//    });
//});
///* CREATE. */
//router.post('/', function (req, res, next) {//Not in use
//    var p = new Image(req.body.image);
//    p.save(function (err) {
//        if (err)
//            res.send("error");
//        else
//            res.send("ok");
//    });
//});
/* GET home page. */
router.get('/active', function (req, res, next) {
    Image.find(
            {active: true},
            {'_id': 1, 'name': 1, 'width': 1, 'height': 1, 'mapfile': 1, 'info': 1}
    )
            .lean()
            .sort({active: -1, fecha: -1})
            .exec(function (err, docs) {

//Avoid returning quintets that there is no mapfile on the server
                for (var i = docs.length - 1; i >= 0; i--) {
                    var mapFileUri = docs[i].mapfile;
                    var pathSearch = '/cazasteroides/data/';
                    if (mapFileUri.indexOf(pathSearch) >= 0) {
                        mapFileUri = configConst.SRC_DATA + mapFileUri.substring(mapFileUri.indexOf(pathSearch) + pathSearch.length, mapFileUri.length);
                    } else {
                        mapFileUri = configConst.SRC_DATA + docs[i].mapfile;
                    }
                    if (!fs.existsSync(mapFileUri)) {
                        docs.splice(i, 1);
                    } else {
                        var weight = 100;
                        if (docs[i].info && docs[i].info.total_area_shown && docs[i].info.total_area) {
                            weight = 100 - (parseFloat(docs[i].info.total_area_shown) / parseFloat(docs[i].info.total_area)).toFixed(3);
                            if (weight < 1) {
                                weight = 1;
                            }
                        }
                        docs[i].weight = weight.toFixed(0);
                        if (docs[i].info) {
                            delete docs[i].info;
                        }
                    }
                }
                res.send(JSON.stringify(docs));
            });
});
///* UPDATE. */
//router.post('/update', function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    var conditions = {
//        "_id": req.body.image._id
//    },
//            update = req.body.image,
//            options = {
//                multi: false
//            };
//    delete update._id;
//    console.log(JSON.stringify(conditions));
//    console.log(JSON.stringify(update));
//    Image.update(conditions, update, options, callback);
//
//    function callback(err, numAffected) {
//        // numAffected is the number of updated documents
//        console.log(numAffected + " affected rows ");
//        res.send(JSON.stringify({
//            "affected": numAffected,
//            "error": err
//        }));
//    }
//});
///* DELETE. */
//router.post('/delete', passport.authenticate('bearer'), function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    Image.findByIdAndRemove(req.body.image._id, function () {
//        res.json({"res": "ok"});
//    });
//});
//
//router.get('/telescopes', function (req, res, next) {//Not in use
//    Image.find({},
//            {name: 1}).sort({active: -1, fecha: -1}).exec(function (err, img) {
//        var arr = [];
//        for (var k in  img) {
//            var telescope = img[k].name.substr(14);
//            if (arr.indexOf(telescope) === -1) {
//                arr.push(telescope);
//            }
//        }
//        res.send(arr);
//    });
//});

router.get("/mapserv", function (req, res, next) {
    //console.log("mapserv[1]:" + req.url);
    var url_parts = Url.parse(req.url, false);
    var query = url_parts.query;
    //console.log("mapserv[2]:" + query);

    //Olders images store the absolute path on server, need remove /var/opt/cazasteroides/data/ from path and use new configConst.SRC_DATA
    var finalQuery = '';
    queries = query.split("&");
    for (i = 0, l = queries.length; i < l; i++) {
        temp = queries[i].split('=');
        if (temp[0] === 'map') {
            var imageFileUri = temp[1];
            var pathSearch = '/cazasteroides/data/';
            if (imageFileUri.indexOf(pathSearch) >= 0) {
                imageFileUri = configConst.SRC_DATA + imageFileUri.substring(imageFileUri.indexOf(pathSearch) + pathSearch.length, imageFileUri.length);
            } else {
                imageFileUri = configConst.SRC_DATA + temp[1];
            }
            temp[1] = require('path').resolve(imageFileUri);

        }
        finalQuery += temp[0] + '=' + temp[1] + (i + 1 < queries.length ? '&' : '');
    }

    var reqstr = configConst.URL_MAPSERV_CGI_BIN + finalQuery;
    console.log("mapserv[2]:" + reqstr);
    request.get(reqstr).pipe(res);

});

//
//function generateCoverage(doc, callback) {//Move to gifHelper
//    Rectangle.find({image: doc._id}, function (err, rects) {
//        console.log("   Rectangle.length: " + rects.length);
//
//        if (!doc.width)
//            doc.width = 2000;
//        if (!doc.height)
//            doc.height = 2000;
//        console.log("Coverage[1]: creando : " + doc.width + " " + doc.height);
//        var g = gm(doc.width, doc.height, "#ffffffff");
//        g.fill("#ff0000f0");
//        for (var r = 0; r < rects.length; r++) {
//            //estimate area
//            g.drawRectangle(rects[r].minx, rects[r].miny,
//                    rects[r].maxx, rects[r].maxy);
//        }
//
//        Observation.find({image: doc._id}, function (err, obs) {
//
//            //g.fill("#0000ff00");
//            console.log("   Observation.length:" + obs.length);
//            for (var o = 0; o < obs.length; o++) {//
//                //estimate area
//                switch (obs[o].status) {
//                    case 'rejected':
//                        g.fill("#66000000");
//                        break;
//                    case 'approved':
//                        g.fill("#00800000");
//                    default:
//                        g.fill("#0000ff00");
//                        break;
//                }
//
//                g.drawRectangle(obs[o].pixCoordAbs[0] - 8, obs[o].pixCoordAbs[1] - 8,
//                        obs[o].pixCoordAbs[0] + 8, obs[o].pixCoordAbs[1] + 8);
//
//            }
//
//            callback(g);
//
//        });
//    });
//}
//
//
//router.get('/coverage', function (req, res, next) {//Not in use
//    Image.find({}, function (err, docs) {
//        console.log("Image.length: " + docs.length);
//        async.forEachSeries(docs, function (doc, callback) {
//
//            generateCoverage(doc, function (g) {
//
//                var covPath = path.join(configConst.SRC_FOLDER_GIF, 'cov');
//                if (!fs.existsSync(covPath)) {
//                    fs.mkdirSync(covPath);
//                }
//
//                console.log("writing:" + path.join(covPath, doc._id + '.png'));
//                g.write(path.join(covPath, doc._id + '.png'), function (err) {
//                    console.log(err);
//                    callback();
//                });
//            });
//
//        });
//        res.send();
//    });
//});
//
//router.get('/:id_image', function (req, res, next) {//Not in use (check)
//    Image.findById(req.params.id_image, function (err, doc) {
//        console.log(JSON.stringify(doc));
//        res.send(JSON.stringify(doc));
//    });
//});
//
//router.get('/:id_image/coverage', function (req, res, next) {//Need move to Admin!
//    Image.findById(req.params.id_image, function (err, doc) {
//
//        generateCoverage(doc, function (g) {
//            res.set('Content-Type', 'image/png'); // set the header here
//            g.stream('png', function (err, stdout, stderr) {
//                console.log("Coverage[2]: streaming" + err);
//                if (err) {
//                    res.status(400);
//                    res.header('Cazast_error', 'error_gm_' + err.code);
//                    res.send('error_gm_' + err.code);
//                    return console.dir(arguments);
//                }
//
//                stdout.pipe(res);
//                var writeStreamerr = fs.createWriteStream(configConst.SRC_SERVER + 'err.txt');
//                stderr.pipe(writeStreamerr);
//            });
//        });
//
//    });
//});
//router.get('/:id_image/select_only_one', function (req, res, next) {//Not in use
//    Image.find({}, function (err, docs) {
//        for (var i = 0; i < docs.length; i++) {
//            var doc = docs[i];
//            if (doc._id == req.params.id_image) {
//                doc.active = true;
//                doc.save();
//                console.log("saved active image");
//            } else if (doc.active == true) {
//                doc.active = false;
//                doc.save();
//                console.log("saved old active image");
//            } else if (doc.active == null) {
//                doc.active = false;
//                doc.save();
//            }
//        }
//        res.json({"res": "ok"});
//    });
//});
//
//router.get('/:id_image/select', function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    Image.findById(req.params.id_image, function (err, doc) {
//        doc.active = true;
//        doc.save();
//        console.log("saved active image");
//        res.json({"res": "ok"});
//    });
//});
//
//router.get('/:id_image/deselect', function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    Image.findById(req.params.id_image, function (err, doc) {
//        doc.active = false;
//        doc.save();
//        console.log("saved inactive image");
//        res.json({"res": "ok"});
//    });
//});

router.get("/:id_image/histogram", function (req, res, next) {//Not in use
    Image.findById(req.params.id_image, function (err, img) {

        if (img.histo) {
            res.json(img.histo);
            return;
        }

        var imageFileUri = img.filename[0];
        var pathSearch = '/cazasteroides/data/';
        if (imageFileUri.indexOf(pathSearch) >= 0) {
            imageFileUri = configConst.SRC_DATA + imageFileUri.substring(imageFileUri.indexOf(pathSearch) + pathSearch.length, imageFileUri.length);
        } else {
            imageFileUri = configConst.SRC_DATA + img.filename[0];
        }

        //var image = configConst.SRC_DATA+"obs1/campo\\ 1200-001c120.fit"
        child_process.exec(configConst.SRC_GDALINFO + ' -mm -hist ' + require('path').resolve(imageFileUri),
                function (err, stdout, stderr) {
                    if (err) {
                        console.log("child processes failed with error code: " + err.code + " " + err);
                        res.status(400);
                        res.header('Cazast_error', 'error_gdal_' + err.code);
                        res.send('error_gdal_' + err.code); //FIXME intentar obtener la imagen...
                    } else {
                        var data = {};
                        //console.log(stdout);
                        var start = stdout.search('buckets from ') + 13;
                        var temp = stdout.substr(start, stdout.length);
                        data.min = parseFloat(temp)
                        var start2 = temp.search(" to ") + 4;
                        var temp2 = temp.substr(start2, temp.length);
                        data.max = parseFloat(temp2)
                        var start3 = temp2.search(":") + 4;
                        var temp3 = temp2.substr(start3, temp2.length);
                        var splited = temp3.split(" ");
                        var i = 0;
                        data.buckets = [];
                        for (i = 0; i < 256; i++) {
                            data.buckets[i] = parseFloat(splited[i]);
                        }

                        img.histo = data;
                        img.save();

                        res.json(data);
                    }
                });
    });
});

/**
 * Retornar los matadatos si se trata de la primera imagen del quinteto
 * En otro caso retorna los coeficientes que hacen falta:
 *      SKY
 *      SIGMA
 */
router.get("/:id_image/:ii/coeff", function (req, res, next) {

    var allCoeff = ['PROBA', 'SCALE', /*'FWHM',*/ 'SKY', 'BZERO', 'SIGMA', 'CD1_1', 'CD1_2', 'CD2_1',
        'CD2_2', 'CRVAL1', 'CRVAL2', 'CRPIX1', 'CRPIX2', 'A_0_0', 'A_0_1', 'A_0_2', 'A_1_0', 'A_1_1',
        'A_2_0', 'B_0_0', 'B_1_0', 'B_0_1', 'B_2_0', 'B_0_2', 'B_1_1', 'AP_0_0', 'AP_1_0', 'AP_0_1',
        'AP_2_0', 'AP_0_2', 'AP_1_1', 'BP_0_0', 'BP_1_0', 'BP_0_1', 'BP_2_0', 'BP_0_2', 'BP_1_1',
        'IMAGEH', 'IMAGEW', 'TIMESTAM', 'ZMAG', 'EXPTIME', 'VEXT', 'AIRMASS', 'FWHMORI'];

    Image.findById(req.params.id_image, function (err, img) {
        if (!img.coeff) {
            img.coeff = new Array(5);
            img.save();
        }

        var index = parseInt(req.params.ii);

        if (index == 0) {
            if (img.meta) {
                res.send(img.meta);
                return;
            }
        } else if (img.coeff && (img.coeff[index] || img.coeff[0])/* && img.coeff[index].BZERO*/) {
            try {
                var arr = img.coeff[index];
                if (img.coeff[0] && img.coeff[0][index]) {
                    arr = img.coeff[0][index];
                }
                if (Object.keys(arr).sort().toString() === allCoeff.sort().toString()) {
                    res.send(arr);
                    return;
                }
            } catch (e) {
                console.log("Ups", e);
            }
        }
        //var agre =[];
        //for (var ii = 0; ii < img.filename.length; i++){
        var imageFileUri = img.filename[index];
        var pathSearch = '/cazasteroides/data/';
        if (imageFileUri.indexOf(pathSearch) >= 0) {
            imageFileUri = configConst.SRC_DATA + imageFileUri.substring(imageFileUri.indexOf(pathSearch) + pathSearch.length, imageFileUri.length);
        } else {
            imageFileUri = configConst.SRC_DATA + img.filename[index];
        }
        //var image = configConst.SRC_DATA+"obs1/campo\\ 1200-001c120.fit"
        child_process.exec(configConst.SRC_GDALINFO + require('path').resolve(imageFileUri),
                function (err, stdout, stderr) {
                    if (err) {
                        console.log("child processes failed with error code: " + err.code + " " + err);
                        res.status(400);
                        res.header('Cazast_error', 'error_gdal_' + err.code);
                        res.send('error_gdal_' + err.code);
                    } else {
                        var data = {};
                        //console.log(stdout);
                        var start = stdout.search('Metadata:') + 12;
                        var end = stdout.search('Corner');
                        var temp = stdout.substr(start, end - start - 1);
                        var lines = temp.split("\n");
                        for (var l = 0; l < lines.length; l++) {
                            //console.log("line " + l + ": " + lines[l]);
                            lines[l] = lines[l].replace(/\:/g, "-").replace(/=/, ":").replace(/"/g, "_");
                            var t = lines[l].split(":")
                            lines[l] = '"' + t[0].trim() + '":"' + t[1].trim() + '"';
                        }
                        var splited = "{" + lines.join(",\n") + "}";
                        //var splited = "{\"" + temp.replace(/\:/g,"-").replace(/^([^=]+)=/g,function($1){return ($1+ "\":\"")}).replace(/\n  /g, "\",\n\"") + "\"}";
                        //console.log(splited);

                        var meta = JSON.parse(splited);

                        var updateQuery = {};
                        updateQuery['$set'] = {};

                        if (index == 0) {
                            updateQuery['$set']["meta" ] = meta;
                            updateQuery['$set']["height"] = meta.IMAGEH;
                            updateQuery['$set']["width"] = meta.IMAGEW;
                            updateQuery['$set']['info.total_area'] = parseFloat(meta.IMAGEH) * parseFloat(meta.IMAGEW);
                        }

                        var coeff = {};

                        for (var i = 0; i < allCoeff.length; i++) {
                            if (meta[allCoeff[i]]) {
                                coeff[allCoeff[i]] = meta[allCoeff[i]];
                            } else {
                                coeff[allCoeff[i]] = undefined;
                            }
                        }

                        if (meta['SIGMA']) {
                            coeff['SIGMA'] = meta['SIGMA'];
                        }

                        if (meta['SKY']) {
                            coeff['SKY'] = meta['SKY'];
                        }

                        if (meta['BZERO']) {
                            coeff['BZERO'] = meta['BZERO'];
                        } else {
                            coeff['BZERO'] = 0;
                        }


                        updateQuery['$set']["coeff." + index] = coeff;
                        //console.log(JSON.stringify(updateQuery));
                        Image.findOneAndUpdate({"_id": img._id}, updateQuery, function (err) {
                        });

                        if (index == 0) {
                            res.send(splited);
                        } else {
                            res.send(coeff);
                        }

                        // agre.push(splited);
                    }
                });
        //  }
    });
});
//router.get("/rectangle/:id_rect/:id_layer", function (req, res, next) {//Not in use
//    Rectangle.findById(req.params.id_rect).populate('image').exec(function (err, r) {
//        if (err) {
//            console.log(err);
//            res.json(err);
//            return;
//        }
//
//        var mapFileUri = r.image.mapfile;
//        var pathSearch = '/cazasteroides/data/';
//        if (mapFileUri.indexOf(pathSearch) >= 0) {
//            mapFileUri = configConst.SRC_DATA + mapFileUri.substring(mapFileUri.indexOf(pathSearch) + pathSearch.length, mapFileUri.length);
//        } else {
//            mapFileUri = configConst.SRC_DATA + r.image.mapfile;
//        }
//        console.log(r.image.mapfile + " conver to " + mapFileUri);
//        console.log("map:" + mapFileUri);
//
//
//        var request = require('request');
//        var map = 'map=' + require('path').resolve(mapFileUri) + '&'
//
//
//        if (r.scaleminArr && r.scaleminArr.length === 5) {
//            r.scalemin = r.scaleminArr[req.params.id_layer - 1];
//        }
//        if (r.scalemaxArr && r.scalemaxArr.length === 5) {
//            r.scalemax = r.scalemaxArr[req.params.id_layer - 1];
//        }
//
//
//        var tscalemin = 2000;
//        if (r.scalemin)
//            tscalemin = r.scalemin;
//        var tscalemax = 2500;
//        if (r.scalemax)
//            tscalemax = r.scalemax;
//        var imsizex = r.maxx - r.minx;
//        var imsizey = r.maxy - r.miny;
//        var mapsize = '&MSCALE=' + tscalemin + ',' + tscalemax + '&mapsize=' + imsizex + '%20' + imsizey + '&';
//        var mapext = "MAPEXT=" + r.minx + "%20" + r.miny + "%20" + r.maxx + "%20" + r.maxy + "&";
//        //var mapext = 'MAPEXT=1794.9913681723988%202951.6880229843364%201894.9913681723988%203051.6880229843364&'
//        var layer = 'layer=' + req.params.id_layer;
//        var reqstr = configConst.URL_MAPSERV_CGI_BIN + map + 'mode=map&' + mapsize + mapext + layer;
//        console.log(reqstr);
//        request.get(reqstr).pipe(res);
//    });
//});
//router.get("/televirt/:id_imagen/thumb", function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    var request = require('request');
//    var reqstr = configConst.URL_TELESCOPIOVIRTUAL_IMAGEN + req.params.id_imagen + "/thumbnail"
//    console.log(reqstr);
//    request.get(reqstr).pipe(res);
//});
//router.get("/televirt/:id_imagen/jpeg", function (req, res, next) {//Use only by admin on Mobile App (To Delete)
//    var request = require('request');
//    var reqstr = configConst.URL_TELESCOPIOVIRTUAL_IMAGEN + req.params.id_imagen + "/jpeg"
//    console.log(reqstr);
//    request.get(reqstr).pipe(res);
//});
//router.post("/findmoredate", function (req, res, next) {
//    //      {dateObs} una fecha en formato yyyy-mm-dd
//    //  {telescop} el nombre de un telescopio existente en el Telescopio Virtual o “all” para no filtrar por telescopio
//    //  {object} el nombre o fragmento del nombre del objeto a buscar o “all” para no filtrar por objetos
//    //telescop y object pueden ser all
//
//    var request = require('request');
//
//    var v = req.body;
//    //method : findimages or findasteroids
//    var reqstr = configConst.URL_TELESCOPIOVIRTUAL + 'findimages/headers/' + v.dateObs + '/' + v.telescop + '/' + v.object;
//    console.log(reqstr);
//    //·         configConst.URL_TELESCOPIOVIRTUAL + findasteroids/coordinates/{ra_h},{ra_m},{ra_s},{dec_g},{dec_m},{dec_s},{radio}
//    request({
//        uri: reqstr,
//        method: 'GET'
//    }, function (error, response, body) {
//        if (!error && response.statusCode == 200) {
//            var parseString = require('xml2js').parseString;
//            parseString(body, function (err, result) {
//                if (!err) {
//                    console.log(JSON.stringify(result));
//                    if (result.publicImages) {
//                        if (result.publicImages.public_image.length > 0) {
//                            var image = result.publicImages.public_image[0];
//                            /*"decY": ["22.0273"],
//                             "descripcion": ["Saturno"],
//                             "detector": ["wright"],
//                             "fecha": ["2003-01-03T00:00:00Z"],
//                             "fitsgz": [configConst.URL_TELESCOPIOVIRTUAL_IMAGEN+"21/fitsgz"],
//                             "idimagen": ["21"],
//                             "imagetyp": ["object"],
//                             "insfilte": ["U"],
//                             "instrume": ["wright"],
//                             "jpeg": [configConst.URL_TELESCOPIOVIRTUAL_IMAGEN+"21/jpeg"],
//                             "nombre": ["1815467191.fits.gz"],
//                             "object": ["Saturno"],
//                             "raX": ["5.5781"],
//                             "telescop": ["iac80"],
//                             "thumbnail": [configConst.URL_TELESCOPIOVIRTUAL_IMAGEN+"21/thumbnail"],
//                             "titulo": ["Saturno"]*/
//                            var downloadjpg = image.jpeg;
//                            var http = require('http');
//                            var fs = require('fs');
//                            var file = fs.createWriteStream(configConst.SRC_DATA + image.nombre + ".jpg");
//                            var request = http.get(downloadjpg, function (response) {
//                                response.pipe(file);
//                                console.log("file saved");
//                            });
//                        }
//                    }
//                    console.log("end");
//                    res.send(result);
//                } else {
//                    console.log(JSON.stringify(err));
//                    res.send(err);
//                }
//            });
//        } else {
//            console.log(error);
//            console.log("status: " + response.statusCode)
//            res.send("status: " + response.statusCode + error + response.body)
//        }
//    });
//});
//
//
//function parseTimestamp(str) {
//    if (!/^(\d){14}$/.test(str))
//        return "invalid date";
//    var y = str.substr(0, 4),
//            m = str.substr(4, 2),
//            d = str.substr(6, 2),
//            h = str.substr(8, 2),
//            mm = str.substr(10, 2),
//            s = str.substr(12, 2);
//    return new Date(y, m, d, h, mm, s);
//}
//router.post("/updateim", function (req, res, next) {
//    console.log("req.body:" + JSON.stringify(req.body));
//    var request = require('request');
//    var dir = configConst.SRC_DATA + req.body.fieldname;
//    console.log("creating " + dir);
//    if (!isDirSync(dir)) {
//        fs.mkdirSync(dir, 0777);
//        console.log("dir " + dir + "created");
//    }
//    var sp = [];
//    var filenames = [];
//    for (var i = 0; i < req.body.imgs.length; i++) {
////closure
//        (function () {
//            var reqstr = configConst.URL_TELESCOPIOVIRTUAL_IMAGEN + req.body.imgs[i] + "/fitsgz";
//            console.log(reqstr);
//            var p = Q.defer();
//            sp.push(p.promise);
//            var stream = request.get(reqstr).pipe(fs.createWriteStream(dir + "/" + req.body.imgs[i] + '.fits.gz'));
//            stream.on('finish', function () {
//                console.log("resolved");
//                p.resolve();
//            });
//            stream.on('error', function (error) {
//                p.reject(error);
//            });
//            //TODO esto habrá que eliminar la parte del SRC_DATA
//            filenames.push(dir + "/" + req.body.imgs[i] + ".fits");
//        })()//end closure
//    }//end for
//    Q.all(sp).then(function () {
//        console.log("all images downloaded start processing");
//        var cmd = '/bin/bash ' + configConst.SRC_SCRIPT_CARGAIMG_SH + filenames.join(' ');
//        console.log(cmd);
//        child_process.exec(cmd,
//                function (err, stdout, stderr) {
//                    if (err) {
//                        console.log("child processes failed with error code: " + err.code + " " + err);
//                    } else {
//                        var data = {};
//                        console.log(stdout);
//                        console.log(stderr);
//                        var start = stdout.search('mapfile') - 11;
//                        var result = stdout.substring(start);
//                        console.log("result:" + result);
//                        var resobj = JSON.parse(result);
//                        resobj.image.name = req.body.fieldname;
//                        resobj.image.fecha = parseTimestamp(req.body.fieldname.substr(0, 14));
//                        console.log("Fecha: " + req.body.fieldname.substr(0, 14) + " " + resobj.image.fecha);
//                        var im = new Image(resobj.image);
//
//                        //TODO eliminar configConst.SRC_DATA de mapfile para permitir compatibilidad si se cambia de ruta
//
//                        im.save(function (err) {
//                            if (err) {
//                                res.json(err);
//                            } else {
//                                res.json(resobj);
//                            }
//                        });
//                    }
//                });
//    }).fail(function (error) {
//        console.log(error);
//    });
//});

//router.post("/findmore", function (req, res, next) {
//    var request = require('request');
//    var v = req.body;
//    //method : findimages or findasteroids
//    var reqstr = configConst.URL_TELESCOPIOVIRTUAL + v.method + '/coordinates/' + v.ra_h + ',' + v.ra_m + ',' + v.ra_s + ',' +
//            v.dec_g + ',' + v.dec_m + ',' + v.dec_s + ',' + v.radio;
//    console.log(reqstr);
//    //·         configConst.URL_TELESCOPIOVIRTUAL +findasteroids/coordinates/{ra_h},{ra_m},{ra_s},{dec_g},{dec_m},{dec_s},{radio}
//    request({
//        uri: reqstr,
//        method: 'GET'
//    }, function (error, response, body) {
//        if (!error && response.statusCode == 200) {
//            var parseString = require('xml2js').parseString;
//            parseString(body, function (err, result) {
//                if (!err) {
//                    console.log(JSON.stringify(result));
//                    if (result.publicImages) {
//                        if (result.publicImages.public_image.length > 0) {
//                            var image = result.publicImages.public_image[0];
//                            /*"decY": ["22.0273"],
//                             "descripcion": ["Saturno"],
//                             "detector": ["wright"],
//                             "fecha": ["2003-01-03T00:00:00Z"],
//                             "fitsgz": [configConst.URL_TELESCOPIOVIRTUAL_IMAGEN+"21/fitsgz"],
//                             "idimagen": ["21"],
//                             "imagetyp": ["object"],
//                             "insfilte": ["U"],
//                             "instrume": ["wright"],
//                             "jpeg": [configConst.URL_TELESCOPIOVIRTUAL_IMAGEN+"21/jpeg"],
//                             "nombre": ["1815467191.fits.gz"],
//                             "object": ["Saturno"],
//                             "raX": ["5.5781"],
//                             "telescop": ["iac80"],
//                             "thumbnail": [configConst.URL_TELESCOPIOVIRTUAL_IMAGEN+"21/thumbnail"],
//                             "titulo": ["Saturno"]*/
//                            var downloadjpg = image.jpeg;
//                            var http = require('http');
//                            var fs = require('fs');
//                            var file = fs.createWriteStream(configConst.SRC_DATA + image.nombre + ".jpg");
//                            var request = http.get(downloadjpg, function (response) {
//                                response.pipe(file);
//                                console.log("file saved");
//                            });
//                        }
//                    }
//                    console.log("end");
//                    res.send(result);
//                } else {
//                    console.log(JSON.stringify(err));
//                    res.send(err);
//                }
//            });
//        } else {
//            console.log(error);
//            console.log("status: " + response.statusCode)
//            res.send("status: " + response.statusCode + error + response.body)
//        }
//    });
//});

//router.post("/star", function (req, res, next) {
//    deg2degminsec = function (degin) {
//        console.log("40");
//        var deg = Math.trunc(degin);
//        console.log("41");
//        var m = Math.abs(Math.trunc((degin - deg) * 60));
//        console.log("42");
//        var s = ((Math.abs(degin - deg) * 60) - m) * 60;
//        console.log("43");
//        var z = "";
//        if (s < 10)
//            z = "0";
//        return [deg, m, s];
//    }
//    deg2hour = function (deg) {
//        console.log(JSON.stringify(deg));
//        var kk = math.floor(1.2);
//        var hour = math.trunc(deg * 24.0 / 360.0);
//        var mdec = math.abs((deg * 24.0 / 360.0) - hour) * 60.0;
//        var m = math.trunc(mdec);
//        var s = (mdec - m) * 60;
//        var z = "";
//        if (s < 10)
//            z = "0";
//        return [hour, m, s];
//    }
//    console.log("1");
//    //var request = require('request');
//    var RA = parseFloat(req.body.RA);
//    console.log("2");
//    var DEC = parseFloat(req.body.DEC);
//    console.log("3");
//    var r1 = 1.0 //radius in minutes
//    console.log("4");
//    var r2 = 1.0 //radius in minutes
//    //if (RA < 0) r1 = -r1;
//    //if (DEC < 0) r2 = -r2;
//    var minRA = deg2hour(RA - (r1 / 60.0));
//    console.log("5");
//    var maxRA = deg2hour(RA + (r1 / 60.0));
//    console.log("6");
//    var minDEC = deg2degminsec(DEC - (r2 / 60.0));
//    console.log("7");
//    var maxDEC = deg2degminsec(DEC + (r2 / 60.0));
//    console.log("8");
//    var reqstr = configConst.URL_STELLAR_DATABASE_NEIGHBORS +
//            "ly=15" +
//            "&X=0.0&Y=0.0&Z=0.0" +
//            "&minRAHour=" + minRA[0] +
//            "&minRAMin=" + minRA[1] +
//            "&minRASec=" + minRA[2] +
//            "&maxRAHour=" + maxRA[0] +
//            "&maxRAMin=" + maxRA[1] +
//            "&maxRASec=" + maxRA[2] +
//            "&minDecDeg=+" + minDEC[0] +
//            "&minDecMin=" + minDEC[1] +
//            "&minDecSec=" + minDEC[2] +
//            "&maxDecDeg=+" + maxDEC[0] +
//            "&maxDecMin=" + maxDEC[1] +
//            "&maxDecSec=" + maxDEC[2]
//    console.log("req: " + reqstr);
//    request({
//        uri: reqstr,
//        method: 'GET'
//    }, function (error, response, body) {
//        if (!error && response.statusCode == 200) {
//            //res.send(body)
//            console.log("ok");
//            var cheerio = require('cheerio'),
//                    $ = cheerio.load(body);
//            //console.log(body);
//            $("center").remove();
//            console.log($("body").html());
//            res.send($("body").html());
//        } else {
//            console.log("status: " + response.statusCode + error)
//            res.send("status: " + response.statusCode + error + response.body)
//        }
//    });
//});
/* MPC */
//
//customEncodeURIComponent = function (s) {
//    var r = querystring.stringify(s);
//    var rr = r.replace(/%20/g, "+");
//    console.log(rr);
//    return rr;
//}
//router.post("/mpc", function (req, res, next) {
//    var request = require('request');
//    request.debug = true;
//    //19/12/2014
//    var form = {
//        "year": req.body.year,
//        "month": req.body.month,
//        "day": req.body.day,
//        "which": "pos",
//        "ra": req.body.RA, //.replace(/ /g,"+"), //"05 03 22.6",
//        "decl": req.body.DEC, //.replace(/ /g,"+"), //"+00 20 16.64",
//        "TextArea": "",
//        "radius": 1,
//        "limit": 30, //TODO cambiar a 22
//        "oc": 500, //TODO intentar obtener el valor con el header
//        "sort": "d",
//        "mot": "h",
//        "tmot": "s",
//        "pdes": "p",
//        "needed": "f",
//        "ps": "n",
//        "type": "p"
//    }
//    console.log(form);
////  var reqstr = "http://www.minorplanetcenter.net/cgi-bin/neocheck.cgi"
//    var reqstr = configConst.URL_MINORPLANETCENTER;
//    //  var reqstr = "http://www.minorplanetcenter.net/cgi-bin/checkmp.cgi"
////    var reqstr = "http://requestb.in/rkp2abrk"
//    request.post({
//        url: reqstr,
//        form: form
////      body: customEncodeURIComponent(form)
//    },
//            function (error, response, body) {
//                if (!error && response.statusCode == 200) {
//                    var cheerio = require('cheerio'),
//                            $ = cheerio.load(body);
//                    console.log(body);
//                    //var valores=[];
//                    //$(".small tr").each(function() {
//                    var raw = $("pre").html();
//                    if (raw) {
//                        var start = raw.search('\n\n') + 2;
//                        var temp = raw.substr(start, raw.length);
//                        var lines = temp.split("\n");
//                        var result = [];
//                        for (var l = 0; l < lines.length; l++) {
//                            result.push({});
//                            result[l].designation = lines[l].substr(0, 25);
//                            var resto = lines[l].substr(25, lines[l].length);
//                            var splited = resto.split(/ +/);
//                            result[l].RA = splited[0] + " " + splited[1] + " " + splited[2];
//                            result[l].DEC = splited[3] + " " + splited[4] + " " + splited[5];
//                            result[l].V = splited[6];
//                            result[l].Offsets = {
//                                'RA': splited[7],
//                                'DEC': splited[8]
//                            };
//                            result[l].Motion = {
//                                'RA': splited[9],
//                                'DEC': splited[10]
//                            };
//                            result[l].Orbit = splited[11];
//                            result[l].Comment = splited[12]; //fixme solo saca la ultima palabra
//
//                        }
//                        //"{\""+temp.replace(/=/g,"\":\"").replace(/\n  /g,"\",\n\"")+"\"}";
//                        //ejempl de lo recibido
//                        /* Object designation         R.A.      Decl.     V       Offsets     Motion/hr   Orbit  <a href="http://www.cfa.harvard.edu/iau/info/FurtherObs.html">Further observations?</a>
//                         h  m  s     &#176;  '  "        R.A.   Decl.  R.A.  Decl.        Comment (Elong/Decl/V at date 1)
//                         
//                         2010 KD56       05 02 32.3 +00 25 30  18.8  12.6W   5.2N    37-     3-    1d  Leave for survey recovery.
//                         */
//
//                        //var hora= $(this).find("td").eq(1).html().replace("&#xA0;","");
//                        //var valor= $(this).find("td").eq(2).html().replace(/&#xA0;/g,"");
//                        //var d = new moment(fecha+" "+hora, "DD-MM-YYYY HH:mm");
//                        //valores.push ({"fecha":fecha, "hora":hora,"valor":valor,"ts":d.valueOf()});
//                        //});
//                        res.json(result);
//                    } else {
//                        res.json({
//                            "res": "no mpc found"
//                        });
//                    }
//                } else {
//                    console.log("status: " + response.statusCode + error)
//                    res.send("status: " + response.statusCode + error + response.body)
//                }
//            });
//});
module.exports = router;
