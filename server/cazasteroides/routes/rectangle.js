var express = require('express');
var router = express.Router();
var passport = require("passport");
var Rectangle = require('../models/rectangle.js');

/* CREATE. */
router.post('/', passport.authenticate('bearer'), function (req, res, next) {
    var p = new Rectangle(req.body);
    p.user = req.user._id;
    p.save(function (err) {
        if (err) {
            res.status(400);
            res.send("error");
        } else {
            res.json(p);

            var area = (((Math.abs(parseFloat(p.minx) - parseFloat(p.maxx)))) * (Math.abs(parseFloat(p.miny) - parseFloat(p.maxy))));
            if (!isNaN(area)) {
                var QuintetHelper = require('../helpers/quintetHelper.js');
                QuintetHelper.increaseAreaShown(req.body.image, area);
            }
        }
    });
});

module.exports = router;
