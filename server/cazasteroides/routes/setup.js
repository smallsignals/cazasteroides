var express = require('express');
var passport = require('passport');
var Setup = require('../models/setup.js');
var router = express.Router();


router.get('/:key', passport.authenticate('bearer'), function (req, res) {

    Setup.findOne({'config.key': req.params.key}, {'config.$.value': 1}, function (err, setup) {
        if (setup)
            res.send(setup.config[0].value);
        else
        {
            res.status(401);
            res.header('Cazast_error', 'error_invalid_key');
            res.send('error_invalid_key_' + req.params.key);
        }
    });

});

module.exports = router;
