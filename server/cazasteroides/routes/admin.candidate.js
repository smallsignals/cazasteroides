var express = require('express');
var passport = require('passport');
var router = express.Router();
var Candidate = require('../models/candidate.js');
var configConst = require("../config_template");
var AdminHelper = require('../helpers/adminHelper.js');

router.get('/generate', /* passport.authenticate('bearer'),*/ function (req, res, next) {
    require('../helpers/candidatesHelper.js').findCandidates();
    res.send('ok');
});

router.get('/known', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    var query = {isKnown: true};

    Candidate.find(query,
            {}
    )
            //.populate('users', 'username')
            .lean()
            .sort({designation: 1})
            .exec(function (err, candidates) {

                for (var i = candidates.length - 1; i >= 0; i--) {
                    if (candidates[i].users) {
                        candidates[i].users = new Array(candidates[i].users.length);
                    }
                    candidates[i].coordenates.sort(require('../helpers/candidatesHelper.js').sortCandidates);
                }

                res.json(candidates);
            });
});

router.get('/unknown', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    var query = {isKnown: false,
        "coordenates.0.line": {$exists: true},
        "coordenates.1.line": {$exists: true},
        "coordenates.2.line": {$exists: true},
        "coordenates.3.line": {$exists: true},
        "coordenates.4.line": {$exists: true}
    };

    Candidate.find(query,
            {}
    )
            //.populate('users', 'username')
            .lean()
            .sort({designation: 1})
            .exec(function (err, candidates) {
                for (var i = candidates.length - 1; i >= 0; i--) {
                    if (candidates[i].users) {
                        candidates[i].users = new Array(candidates[i].users.length);
                    }
                    candidates[i].coordenates.sort(require('../helpers/candidatesHelper.js').sortCandidates);
                }

                res.json(candidates);
            });
});

router.get('/unknownIncomplete', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    var query = {isKnown: false,
        $or: [
            {"coordenates.0.line": {$exists: false}},
            {"coordenates.1.line": {$exists: false}},
            {"coordenates.2.line": {$exists: false}},
            {"coordenates.3.line": {$exists: false}},
            {"coordenates.4.line": {$exists: false}}
        ]
    };

    Candidate.find(query, {})
            //.populate('users', 'username')
            .populate('coordenates.quintet', 'meta.SCALE')
            .lean()
            .sort({designation: 1})
            .exec(function (err, candidates) {
                for (var i = candidates.length - 1; i >= 0; i--) {
                    if (candidates[i].users) {
                        candidates[i].users = new Array(candidates[i].users.length);
                    }
                    candidates[i].coordenates.sort(require('../helpers/candidatesHelper.js').sortCandidates);
                }

                res.json(candidates);
            });
});

router.post('/updateIncomplete', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    if (!req.body._id || !req.body.coordenates) {
        res.status(400);
        res.header('Cazast_error', 'error_no_exist_obs');
        res.send('error_no_exist_obs');
        return;
    }

    function response(doc) {
        if (doc.users) {
            doc.users = new Array(doc.users.length);
        }
        doc.coordenates.sort(require('../helpers/candidatesHelper.js').sortCandidates);
        res.json(doc);
    }


    Candidate.findById(req.body._id, {})
            //.populate('users', 'username')
            .populate('coordenates.quintet', 'meta.SCALE')
            .lean()
            .sort({designation: 1})
            .exec(function (err, candidate) {
                if (err) {
                    res.status(400);
                    res.header('Cazast_error', 'error_no_exist_obs');
                    res.send('error_no_exist_obs');
                } else {
                    var detectionHelper = require('../helpers/detectionHelper.js');
                    var candidateHelper = require('../helpers/candidatesHelper.js');

                    var totalToUpdate = 0;
                    var totalNoUpdate = 0;
                    var allChecked = false;
                    for (var i = 0; i < candidate.coordenates.length; i++) {
                        allChecked = (i == candidate.coordenates.length - 1);

                        var coordenate = candidate.coordenates[i];
//
//                        if (/*coordenate.line ||*/ ) {
//                            continue;
//                        }

                        var increase = undefined;
                        for (var j = 0; j < req.body.coordenates.length; j++) {
                            if (req.body.coordenates[j].quintetn == coordenate.quintetn) {
                                increase = req.body.coordenates[j].newIncrease;
                                break;
                            }
                        }

                        if (!coordenate.quintet || !increase || (increase[0] === 0 && increase[1] === 0)) {
                            totalNoUpdate++;
                            continue;
                        }

                        var scale = parseFloat(coordenate.quintet.meta.SCALE);
                        var coord = coordenate.coord;

                        var dec = coord[1] - (increase[1] * scale) / 3600;
                        var ra = coord[0] - ((increase[0] * scale) / 3600) / Math.cos(dec * Math.PI / 180);

                        var newCoord = [ra, dec];
                        totalToUpdate++;
                        detectionHelper.getMagnitude(coordenate.quintet._id, coordenate.quintetn, newCoord, function () {
                            var index = i;
                            var tmpNewCoord = newCoord;
                            var tmpCoordenate = coordenate;
                            var tmpIncrease = increase;
                            return function (m) {
                                if (!m && tmpCoordenate.magnitude) {
                                    m = tmpCoordenate.magnitude;
                                }

                                var isAsteriskLine = tmpCoordenate.line && tmpCoordenate.line.length > 12 && tmpCoordenate.line.charAt(12) === '*';

                                var line = candidateHelper.generateLine(tmpCoordenate.timestamp, tmpNewCoord, m, candidate.tmpDesignation, candidate.isKnown, !isAsteriskLine, tmpCoordenate.telescope.ocn);
                                console.log(line);
                                tmpCoordenate.increase = [
                                    tmpCoordenate.increase[0] + (tmpIncrease ? tmpIncrease[0] : 0),
                                    tmpCoordenate.increase[1] + (tmpIncrease ? tmpIncrease[1] : 0)
                                ];
                                tmpCoordenate.line = line;
                                tmpCoordenate.coord = tmpNewCoord;
                                tmpCoordenate.magnitude = m;

                                var updateCoordenate = {};
                                updateCoordenate['coordenates.' + index] = tmpCoordenate;

                                Candidate.findOneAndUpdate({_id: req.body._id}, {$set: updateCoordenate}, {new : true}, function (err, doc) {
                                    totalToUpdate--;
                                    if (err) {
                                        console.log("Something wrong when updating candidate " + req.body._id);
                                    }

                                    if (totalToUpdate === 0 && allChecked) {
                                        response(doc);
                                    }
                                });


                            };
                        }());
                    }
                    if (candidate.coordenates.length === totalNoUpdate) {
                        response(candidate);
                        //res.send('ok');
                    }
                }
            });
});


router.post('/report', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    if (req.body._id) {
        Candidate.findById(req.body._id, function (err, candidate) {
            if (err) {
                res.status(400);
                res.header('Cazast_error', 'error_no_exist_obs');
                res.send('error_no_exist_obs');
            } else {
                const MPC = require('../helpers/mpcHelper.js');
                new MPC().reportCandidate(candidate, function (reportState) {
                    res.json({'reportState': reportState});
                });
            }
        });
    } else {
        res.status(400);
        res.header('Cazast_error', 'error_no_exist_obs');
        res.send('error_no_exist_obs');
    }
});

module.exports = router;

