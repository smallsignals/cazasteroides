var express = require('express');
var passport = require('passport');
var Setup = require('../models/setup.js');
var router = express.Router();
var AdminHelper = require('../helpers/adminHelper.js');


router.get('/', passport.authenticate('bearer'), function (req, res) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }
    Setup.findOne({}, function (err, config) {
        if (config)
            res.send(config['config']);
        else
            res.send([]);
    });

});

router.post('/', passport.authenticate('bearer'), function (req, res) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    var options = {upsert: true, new : true, setDefaultsOnInsert: true};
    if (!req.body.config) {
        res.status(401);
        res.header('Cazast_error', 'error_invalid_params');
        res.send('error_invalid_params');
        return;
    }

    var config = req.body.config;
    //console.log(JSON.stringify(req.body));
    for (var i = 0; i < config.length; i++) {
        var query = {}, update = {$push: {config: config[i]}};
        if (config[i].key && config[i].value && config[i].key.trim() && config[i].value.trim()) {
            //console.log(JSON.stringify(config[i]));

            Setup.findOneAndUpdate({'config.key': config[i].key}, {'$set': {
                    'config.$.value': config[i].value
                }}, function (err, config) {
                if (!config) {
                    Setup.findOneAndUpdate(query, update, options, function (err, config) {
                    });
                }
            });
        }
    }
    res.send('ok');
});
module.exports = router;
