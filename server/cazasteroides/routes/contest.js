var express = require('express');
var passport = require('passport');
var Contest = require('../models/contest.js');
var router = express.Router();

/**
 * Todos los concursos con los puntos del usuario
 */
router.get('/active', passport.authenticate('bearer'), function (req, res) {

//    var arrContests = [];
//    var userContests = req.user.contests ? req.user.contests : [];
//    for (var i = 0; i < userContests.length; i++) {
//        arrContests.push(userContests[i].contest);
//    }
    //var date = Date.now();
    Contest.find({
        /*"time_start": {$lte: date},*/
        //"time_end": {$gte: date},
        //"_id": {$nin: arrContests}
    })
            .lean()
            .exec(function (err, contests) {
                if (err) {
                    res.status(400);
                    res.send(err);
                } else if (contests) {
                    if (req.user.contests) {
                        for (var i = 0; i < contests.length; i++) {
                            var index = req.user.contests.findIndex(function (element) {
                                return element.contest.toString() === contests[i]._id.toString();
                            });
                            //console.log(index);
                            if (index >= 0 && index < req.user.contests.length) {
                                contests[i]['registered'] = true;
                                if (req.user.contests[index].points)
                                    contests[i]['points'] = req.user.contests[index].points;
                            }
                        }
                    }

                    contests.sort(require('../helpers/contestHelper.js').compareContest);
                    res.json(contests);
                } else {
                    res.json([]);
                }
            });
});
/**
 * Añadir al concurso a un usuario
 */
router.post('/register', passport.authenticate('bearer'), function (req, res) {

    if (!req.user.contests) {
        req.user.contests = [];
    }

    var contest = {};
    contest.contest = req.body.contest;
    if (!req.user.contests.some(function (o) {
        return o["contest"] == req.body.contest;
    })) {
        req.user.contests.push(contest);
    }

    req.user.save(function (err) {
        if (err) {
            res.status(400);
            res.send(err);
        } else {
            res.json({"status": "ok"});
        }
    });
});

/**
 * Añadir al concurso a un usuario
 */
router.delete('/:id', passport.authenticate('bearer'), function (req, res) {

    if (!req.user.contests) {
        res.json({"status": "ok"});
        return;
    }

    var index = req.user.contests.findIndex(function (o) {
        return o["contest"] == req.params.id;
    });

    if (index >= 0 && index < req.user.contests.length) {
        req.user.contests.splice(index, 1);
    }

    req.user.save(function (err) {
        if (err) {
            res.status(400);
            res.send(err);
        } else {
            res.json({"status": "ok"});
        }
    });
});

/**
 * Todos los concursos
 */
router.get('/', passport.authenticate('bearer'), function (req, res) {
    Contest.find({})
            .lean()
            .sort({name: -1})
            .exec(function (err, contests) {
                if (err) {
                    res.status(400);
                    res.send(err);
                } else {
                    if (req.user.contests) {
                        for (var i = 0; i < contests.length; i++) {
                            if (req.user.contests.some(function (o) {
                                return o["contest"].toString() === contests[i]._id.toString();
                            })) {
                                contests[i]['registered'] = true;
                            }
                        }
                    }
                    res.json(contests);
                }
            });
});
/**
 * Info del concurso
 */
router.get('/info/:name', function (req, res) {
    var name = decodeURI(req.params.name);
    Contest.findOne({name: name})
            .exec(function (err, contest) {
                if (err) {
                    res.status(400);
                    res.send(err);
                } else {
                    res.json(contest);
                }
            });
});
/**
 * Ranking del concurso
 */
router.get('/ranking/:name', function (req, res) {


    var condition = {};

    if (req.query.skip && req.query.limit) {
        condition = {skip: parseInt(req.query.skip), limit: parseInt(req.query.limit)};
    }


    var name = decodeURI(req.params.name);
    var User = require('../models/user.js');
    Contest.findOne({name: name})
            .exec(function (err, contest) {
                if (err) {
                    res.status(400);
                    res.send(err);
                } else if (!contest) {
                    res.json([]);
                } else {
                    User.find(
                            {
                                "contests.contest": contest._id
                            }
                    ,
                            {username: 1,
                                "contests.$": 1
                            }
                    , condition
                            )
                            .lean()
                            //.populate("contests.contest")
                            .sort({"contests.points": -1})
                            .exec(function (err, users) {
                                if (err) {
                                    res.send(err);
                                } else {
                                    //users.sort(require('../helpers/contestHelper.js').sortContestPoints);

                                    var index = users.findIndex(function (element) {
                                        return parseInt(element.contests[0].points) <= 0;
                                    });

                                    if (index >= 0 && index < users.length) {
                                        users.splice(index, users.length - index);
                                    }

                                    res.json(users);
                                }
                            });
                }
            });
});
module.exports = router;
