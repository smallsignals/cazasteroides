var express = require('express');
var passport = require('passport');
var Score = require('../helpers/scoreCalculation.js');
var Observation = require('../models/observation.js');
var Quintet = require('../models/image.js');
var router = express.Router();
var fs = require('fs');
var GifHelper = require('../helpers/gifHelper.js');
var Setup = require('../models/setup.js');
//var PNGHelper = require('../helpers/pngHelper.js');
/**
 * Añadir una detección
 * body:
 *  {
 *      "image": id_image,
 *      "rect": id_rect,
 *      "pixCoord": pixcoord,       -> []
 *      "pixCoordAbs": pixcoordabs, -> []
 *      "Coord": coords,            -> []
 *      "clock": clock,             -> Antes con este parametro se enviaba la puntuación
 *      "time": time,               -> Tiempo que tardo en la detección
 *      "meta": JSON.stringify(meta), 
 *      "status": "pending",
 *      "quintetn": quintetn
 *  };
 *  meta = {
 *      "PROBA": metadata.PROBA,
 *      "SCALE": metadata.SCALE,
 *      "FWHM": metadata.FWHM
 *  };
 *        
 * Response:        
 *  {
 *      message: string,                -> ...
 *      "points": number,               -> Puntos finales que obtengo por esta observación
 *      "totalDetect": number,          -> Posición de mi detección 1º,2º,...
 *      "firstOwnDetection": boolean    -> Si es mi primera detección en estas coordenadas
 *  }
 */
// Create endpoint /api/obs for POST
router.post('/', passport.authenticate('bearer'), function (req, res) {
    var TelescopeHelper = require('../helpers/telescopeHelper.js');
    var coordprec = 0.0028; //~10 arcseg;
    var points = 0;
    var detectionTime = Date.now();
    Setup.findOne({}, function (err, setup) {

        if (setup) {
            var indexSigmaDetection = setup.config.findIndex(function (element) {
                return element.key.toString() === "sigma_detection";
            });
            if (indexSigmaDetection >= 0 && indexSigmaDetection < setup.config.length) {
                coordprec = parseFloat(setup.config[indexSigmaDetection].value);
            }
        }


//    Image.findById(req.body.image, function (err, img) {
//        const DateHelper = require('../helpers/dateHelper.js');
//        var dateHelper = new DateHelper();
//        var date = dateHelper.timestam2date(img.meta.TIMESTAM);
//        var gte = dateHelper.date2timestam(dateHelper.dateAdd(date, 'minute', -5));
//        var lt = dateHelper.date2timestam(dateHelper.dateAdd(date, 'minute', 5));
//
//        /**
//         * Obtener todas las imagesnes que esten entre +-5 minutos con la imagen de la observación
//         * Esto nos ayuda a descartar imagenes que no esten en el mismo rango de tiempo
//         */
//        Image.find(
//                {
//                    "meta.TIMESTAM": {
//                        $gte: gte,
//                        $lte: lt
//                    }
//                }
//        ,
//                {
//                    "_id": 1
//                }
//        ).exec(function (err, images) {
//            if (err) {
//                console.log("ERROR: " + JSON.stringify(err));
//            }
//
//
//    console.log("images:" + images.length);
//    var arrImgId = [];
//    for (var i = 0; i < images.length; i++)
//    {
//        arrImgId.push(images[i]._id);
//    }
        //console.log(JSON.stringify(arrImgId));
        /**
         * Obtenemos todas las observaciones de imagenes del rango horario
         * y que tenga las coordenadas entre las coordprec
         */
        var query = {
            "image": req.body.image, //{"$in": arrImgId},
            //"status": {"$ne": "rejected"},
            "rejectedCode": {"$ne": 3}, //3-> Objeto no centrado
            "avgCoord.0": {
                $gt: (req.body.Coord[0] - coordprec),
                $lt: (req.body.Coord[0] + coordprec)
            },
            "avgCoord.1": {
                $gt: (req.body.Coord[1] - coordprec),
                $lt: (req.body.Coord[1] + coordprec)
            }
        }; // añadir el quinteto n => 0 -> {$or:[{ quintetn: { $exists: false }},{ quintetn: 0}]} other -> { quintetn: n}
        if (req.body.quintetn && req.body.quintetn != 0) {
            query['quintetn'] = req.body.quintetn;
        } else {
            query['$or'] = [{quintetn: {$exists: false}}, {quintetn: 0}];
        }

        Observation.findOne(
                query
//                    {
//                        "image": {"$in": arrImgId},
//                        "Coord.0": {
//                            $gt: (req.body.Coord[0] - coordprec),
//                            $lt: (req.body.Coord[0] + coordprec)
//                        },
//                        "Coord.1": {
//                            $gt: (req.body.Coord[1] - coordprec),
//                            $lt: (req.body.Coord[1] + coordprec)
//                        }
//                    }
                )
                .populate('image', '_id meta')
                .exec(function (err, observation) {
                    if (err) {
                        console.log("ERROR: " + JSON.stringify(err));
                    }
                    //console.log("observation" + JSON.stringify(observation));
                    if (!observation) {
                        //Nueva Observacion
                        console.log("new observation");
//                    //if reached here no observation existed so create a new one

                        res.json({
                            message: 'obs added',
                            //"points": points,//No se muestra
                            "totalDetect": 1,
                            "firstOwnDetection": true
                        });
                        require('../helpers/detectionHelper.js').getMagnitude(req.body.image, req.body.quintetn, req.body.Coord, function (magnitude) {
                            Quintet.findOne({_id: req.body.image}).exec(function (err, quintet) {
                                var meta = JSON.parse(req.body.meta);
                                if (quintet && quintet.meta) {
                                    meta = quintet.meta;
                                }

                                var obs = new Observation(req.body);
                                if (req.body.time && meta) {
                                    points = new Score().getPoints(req.body.time, meta, magnitude, 0); //TODO Revisar puntos
                                }

                                obs.user = req.user._id;
                                obs.points = points;
                                obs.time = detectionTime; //Date.now();
                                obs.avgCoord = req.body.Coord;
                                obs.magnitude = magnitude;
                                obs.avgMagnitude = magnitude;
                                obs.save(function (err) {
//                            res.json({
//                                message: 'obs added',
//                                //"points": points,//No se muestra
//                                "totalDetect": 1,
//                                "firstOwnDetection": true
//                            });
                                    console.log("obs err:" + JSON.stringify(err));
                                    console.log("obs saved:" + JSON.stringify(obs));
                                    TelescopeHelper.increaseDetection(obs);
                                    var QuintetHelper = require('../helpers/quintetHelper.js');
                                    QuintetHelper.increaseDetections(req.body.image);
                                    var UserHelper = require('../helpers/userHelper.js');
                                    UserHelper.increaseDetection(req.user._id);
                                });
                            });
                        });
                    } else {
                        /**
                         * Si la observación existe se ha de comprobar:
                         *      Si ya ha enviado esta detección
                         *          Se le retornaran los puntos de su deteccion registrada y se dira que no es la primera vez que lo detecta
                         *      Si aún no habia enviado esta detección
                         */
                        console.log("trying to insert the same obeservation");
                        var d = observation; //observations[0];
                        if (d.user.equals(req.user._id)) {
                            //Ya la ha detectado el primero
                            res.json({
                                "message": "existing",
                                "firstOwnDetection": false
                            });
                        } else if (d.revisit.some(function (o) {
                            if (o["user"]) {
                                return o["user"].equals(req.user._id);
                            }
                            return false;
                        })) {
                            //Ya la ha detectado en otro momento
                            res.json({
                                "message": "existing",
                                "firstOwnDetection": false}
                            );
                        } else {
                            //No la ha detectado anteriormente
                            var response = {
                                "message": "existing",
                                //"points": points,
                                "totalDetect": d.revisit.length + 1 + 1,
                                "firstOwnDetection": true
                            };
                            switch (d.status) {
                                case 'rejected':
                                    response.status = d.status;
                                    if (d.rejectedCode) {
                                        response.rejectedCode = d.rejectedCode;
                                    }
                                    break;
                                case 'approved':
                                    response.status = d.status;
                                    if (d.object) {
                                        response.object = d.object;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            res.json(response);
                            require('../helpers/detectionHelper.js').getMagnitude(req.body.image, req.body.quintetn, req.body.Coord, function (magnitude) {
                                if (req.body.time && /*req.body.meta*/ d.image.meta) {
                                    points = new Score().getPoints(req.body.time, d.image.meta /* JSON.parse(req.body.meta)*/, magnitude, d.revisit.length); //TODO Revisar puntos
                                }
                                d.revisit.push(
                                        {//Necesitamos almacenar los demás usuarios que han detectado con su puntuación
                                            "x": req.body.Coord[0],
                                            "y": req.body.Coord[1],
                                            "user": req.user._id,
                                            "points": points,
                                            "magnitude": magnitude,
                                            "time": detectionTime//new Date()
                                        });
                                if (d.avgCoord) {
                                    d.avgCoord[0] = (parseFloat(d.avgCoord[0]) + parseFloat(req.body.Coord[0])) / 2;
                                    d.avgCoord[1] = (parseFloat(d.avgCoord[1]) + parseFloat(req.body.Coord[1])) / 2;
                                } else {
                                    var meanra = parseFloat(d.Coord[0]);
                                    var meandec = parseFloat(d.Coord[1]);
                                    for (var n = 0; n < d.revisit.length; n++) {
                                        meanra += parseFloat(d.revisit[n].x);
                                        meandec += parseFloat(d.revisit[n].y);
                                    }
                                    meanra /= (d.revisit.length + 1);
                                    meandec /= (d.revisit.length + 1);
                                    d.avgCoord = [meanra, meandec];
                                }

                                if (d.avgMagnitude) {
                                    d.avgMagnitude = (parseFloat(d.avgMagnitude) + parseFloat(magnitude)) / 2;
                                } else {
                                    d.avgMagnitude = magnitude;
                                }


                                d.save(function (err) {
                                    //console.log("obs err:" + JSON.stringify(err));
                                    //console.log("obs updated:" + JSON.stringify(d));
                                    var UserHelper = require('../helpers/userHelper.js');
                                    UserHelper.increaseDetection(req.user._id);
                                    switch (d.status) {
                                        case 'rejected':
                                            UserHelper.increaseDetectionRejected(req.user._id);
                                            break;
                                        case 'approved':
                                            UserHelper.increaseDetectionApproved(req.user._id, 0);
                                            break;
                                        default:
                                            break;
                                    }
                                });
                                Observation.findOneAndUpdate({_id: d._id}, {$set: {avgMagnitude: d.avgMagnitude, avgCoord: d.avgCoord}}, function () {});

                            });
                        }
                        //req.user.save();
                    }
                });
//        });
//    });
    });
});
/**
 * Obtener lista de detecciónes para votar
 * La lista es aleatoria
 * 
 * Response:
 *      Cada observación contiene:
 *          {
 *              image:  {
 *                  name
 *                  filename
 *                  fecha
 *              }
 *              votes
 *              downvotes
 *              Coord
 *              _id
 *              
 *          }
 */
router.get('/rand', passport.authenticate('bearer'), function (req, res) {

    var obsFind = Observation
            .find(
                    {"status": {$eq: 'pending'}
                    }, {"_id": 1, "image": 1, "votes": 1, "downvotes": 1, "Coord": 1}
            );
    if (req.user) {
        obsFind = Observation
                .find(
                        {
                            "user": {"$ne": req.user._id},
                            "revisit.user": {$ne: req.user._id},
                            "votes": {$ne: req.user._id},
                            "downvotes": {$ne: req.user._id},
                            "status": {$eq: 'pending'}
                        }
                , {"image": 1, "votes": 1, "downvotes": 1, "Coord": 1, "_id": 1}
                );
    }

    obsFind.lean()
            //.limit(1000)//FIXME se debe limitar el tamaño de la muestra, puede existir problemas cuando existan muchas detecciones
//            .populate({
//                path: 'image',
//                match: {'name': {$exists: true, "$ne": null}},
//                select: 'name filename fecha',
//            })
            .populate('image', 'name filename fecha', {"active": {$eq: true}})

            .exec(function (err, obs) {
                if (err) {
                    res.send(err);
                    return;
                }
                //show only observations for the current set of images
                for (var i = obs.length - 1; i >= 0; i--) {
                    if (obs[i].image == null) {
                        obs.splice(i, 1);
                    } else {
                        if (obs[i].votes) {
                            obs[i].votes = new Array(obs[i].votes.length);
                        }
                        if (obs[i].downvotes) {
                            obs[i].downvotes = new Array(obs[i].downvotes.length);
                        }
                    }
                }
                var shuffled = obs.slice(0);
                var i = obs.length;
                var temp, index;
                while (i--) {
                    index = Math.floor((i + 1) * Math.random());
                    temp = shuffled[index];
                    shuffled[index] = shuffled[i];
                    shuffled[i] = temp;
                }
                res.json(shuffled.slice(0, 50));
            });
});
router.get('/get_obs_gif/:id_obs', function (req, res) {
    var id = req.params.id_obs;
    Observation.findById(id)
            .populate('image')
            .populate('rect')
            .exec(function (err, obs) {
                if (err) {
                    res.send(err);
                    return;
                } else {
                    GifHelper.rebuild_gif(obs, res, function (pathgif) {
                        if (fs.existsSync(pathgif)) {
                            res.sendFile(pathgif);
                        } else {
                            res.status(400);
                            res.header('Cazast_error', 'error_gif');
                            res.send('error_gif');
                        }
                    });
                }
            });
});
router.get('/get_obs_png/:id_obs/:frame', function (req, res) {
    var id = req.params.id_obs;
    var frame = parseInt(req.params.frame);
    if (frame < 0 || frame > 4) {
        res.status(400);
        res.header('Cazast_error', 'error_gif');
        res.send('error_gif');
        return;
    }

    Observation.findById(id)
            .populate('image')
            .populate('rect')
            .exec(function (err, obs) {
                if (err) {
                    res.send(err);
                    return;
                } else {
                    GifHelper.rebuild_gif(obs, res, function (pathgif) {
                        if (fs.existsSync(pathgif)) {
                            var gm = require('gm');
                            gm(pathgif)
                                    .selectFrame(frame)
                                    .stream(function (err, stdout, stderr) {
                                        if (err) {
                                            res.status(400);
                                            res.header('Cazast_error', 'error_gm_' + err.code);
                                            res.send('error_gm_' + err.code);
                                        } else {
                                            stdout.pipe(res);
                                        }
                                    });
                        } else {
                            res.status(400);
                            res.header('Cazast_error', 'error_gif');
                            res.send('error_gif');
                        }
                    });
                }
            });
//    Observation.findById(id)
//            .populate('image')
//            .populate('rect')
//            .exec(function (err, obs) {
//                if (err) {
//                    res.send(err);
//                    return;
//                } else {
//
//                    var metadata = obs.image.meta;
//                    try {
//                        if (obs.quintetn === 0) {
//                            return obs.image.meta;
//                        }
//                        metadata = obs.image.coeff[0][obs.quintetn];
//                    } catch (e) {
//
//                    }
//
//                    PNGHelper.rebuild_png(id, obs.quintetn ? obs.quintetn : 0, obs.avgCoord, metadata, obs.image.mapfile, function (pathpng) {
//                        if (fs.existsSync(pathpng)) {
//                            res.sendFile(pathpng);
//                        } else {
//                            res.status(400);
//                            res.header('Cazast_error', 'error_png');
//                            res.send('error_png');
//                        }
//                    });
//                }
//            });
});
/**
 * Generar un voto positivo a una detección
 * body:
 *  {
 *      "_id": id_obs,
 *  }
 *  
 *  Response:
 *      Success
 *      Error 400
 *          - error_owner_obs           -> Si es su propia detección
 *          - error_duplicate_obs_vote  -> Si ya ha votado
 */
/* GET home page. */
router.post('/vote', passport.authenticate('bearer'), function (req, res, next) {
    //console.log("*************************vote");
    if (req.body._id) {
        Observation.findById(req.body._id, function (err, obs) {
            if (obs.user.equals(req.user._id)) {
                res.status(400);
                res.header('Cazast_error', 'error_owner_obs');
                res.send('error_owner_obs');
            } else if (obs.revisit.some(function (o) {
                if (o["user"]) {
                    return o["user"].equals(req.user._id);
                }
                return false;
            })) {
                res.status(400);
                res.header('Cazast_error', 'error_owner_obs');
                res.send('error_owner_obs');
            } else if (obs.votes.some(function (o) {
                return o.equals(req.user._id);
            })) {
                res.status(400);
                res.header('Cazast_error', 'error_duplicate_obs_vote');
                res.send('error_duplicate_obs_vote');
            } else if (obs.downvotes.some(function (o) {
                return o.equals(req.user._id);
            })) {
                res.status(400);
                res.header('Cazast_error', 'error_duplicate_obs_vote');
                res.send('error_duplicate_obs_vote');
            } else {
                obs.votes.push(req.user._id);
                obs.save(function (data) {
                    res.json(data);
                    var UserHelper = require('../helpers/userHelper.js');
                    UserHelper.increaseVote(req.user._id);
                });
            }
        });
    } else {
        res.status(400);
        res.header('Cazast_error', 'error_no_exist_obs');
        res.send('error_no_exist_obs');
    }
});
/**
 * Generar un voto negativo a una detección
 * body:
 *  {
 *      "_id": id_obs,
 *  }
 *  
 *  Response:
 *      Success
 *      Error 400
 *          - error_owner_obs           -> Si es su propia detección
 *          - error_duplicate_obs_vote  -> Si ya ha votado
 */
router.post('/downvote', passport.authenticate('bearer'), function (req, res, next) {
    //console.log("*************************downvote");
    if (req.body._id) {
        Observation.findById(req.body._id, function (err, obs) {
            if (obs.user.equals(req.user._id)) {
                res.status(400);
                res.header('Cazast_error', 'error_owner_obs');
                res.send('error_owner_obs');
            } else if (obs.revisit.some(function (o) {
                if (o["user"]) {
                    return o["user"].equals(req.user._id);
                }
                return false;
            })) {
                res.status(400);
                res.header('Cazast_error', 'error_owner_obs');
                res.send('error_owner_obs');
            } else if (obs.votes.some(function (o) {
                return o.equals(req.user._id);
            })) {
                res.status(400);
                res.header('Cazast_error', 'error_duplicate_obs_vote');
                res.send('error_duplicate_obs_vote');
            } else if (obs.downvotes.some(function (o) {
                return o.equals(req.user._id);
            })) {
                res.status(400);
                res.header('Cazast_error', 'error_duplicate_obs_vote');
                res.send('error_duplicate_obs_vote');
            } else {
                obs.downvotes.push(req.user._id);
                obs.save(function (data) {
                    res.json(data);
                    var UserHelper = require('../helpers/userHelper.js');
                    UserHelper.increaseVote(req.user._id);
                });
            }
        });
    } else {
        res.status(400);
        res.header('Cazast_error', 'error_no_exist_obs');
        res.send('error_no_exist_obs');
    }
});
router.get('/me', passport.authenticate('bearer'), function (req, res) {//TODO add pagination

    var user_id = req.user._id;
    var query = {'$or': [
            {'user': user_id, displayToUser: {$exists: false}},
            {'user': user_id, displayToUser: {$eq: true}},
            {"revisit.user": {$eq: user_id}, 'revisit.displayToUser': {$exists: false}},
            {"revisit.user": {$eq: user_id}, 'revisit.displayToUser': {$eq: true}}
        ]};

    var condition = {};

    if (req.query.skip && req.query.limit) {
        condition = {skip: parseInt(req.query.skip), limit: parseInt(req.query.limit)};
    }

    Observation.find(query
            , {revisit: 1, user: 1, votes: 1, downvotes: 1, points: 1, status: 1, time: 1, Coord: 1, image: 1, object: 1, rejectedCode: 1}, condition)
            .lean()
            .populate('image', 'name filename')
            .sort({time: -1})
            .exec(function (err, docs) {
                if (err) {
                    return res.send(err);
                }
                //console.log(JSON.stringify(docs));
                var ord = 0;
                var coord = undefined;
                var points = undefined;
                var time = undefined;
                for (var i = docs.length - 1; i >= 0; i--) {


                    if (!docs[i].image.name) {
                        var lis = docs[i].image.filename[0].split('/');
                        docs[i].image.name = lis[lis.length - 2];
                    }


                    if (docs[i].user.toString() === user_id.toString()) {
                        ord = 1;
                        coord = docs[i]['Coord'];
                        points = docs[i]['points'];
                        time = docs[i]['time'];
                    } else if (docs[i].revisit) {
                        for (var j = docs[i].revisit.length - 1; j >= 0; j--) {
                            if (docs[i].revisit[j].user && docs[i].revisit[j].user.toString() === user_id.toString()) {
                                ord = j + 2;
                                if (docs[i].revisit[j].x && docs[i].revisit[j].y)
                                    coord = [docs[i].revisit[j].x, docs[i].revisit[j].y];
                                if (docs[i].revisit[j].points)
                                    points = docs[i].revisit[j].points;
                                if (docs[i].revisit[j].time)
                                    time = docs[i].revisit[j].time;
                                break;
                            }
                        }
                    }

                    docs[i]['info'] = {};
                    docs[i]['info']['ord'] = ord;
                    docs[i]['info']['Coord'] = coord;
                    docs[i]['info']['time'] = time;
                    docs[i]['info']['points'] = points;
                    docs[i]['info']['detections'] = docs[i].revisit.length + 1;
                    docs[i]['info']['votes'] = docs[i].votes.length;
                    docs[i]['info']['downvotes'] = docs[i].downvotes.length;
                    delete docs[i].image.filename;
                    delete docs[i].Coord;
                    delete docs[i].points;
                    delete docs[i].time;
                    delete docs[i].votes;
                    delete docs[i].downvotes;
                    delete docs[i].revisit;
                    delete docs[i].user;
                }

                res.json(docs);
            });
});
router.post('/hide', passport.authenticate('bearer'), function (req, res, next) {
    var user_id = req.user._id;
    var obs_id = req.body._id;
    console.log(JSON.stringify(req.body));
    if (obs_id) {
        Observation.findById(obs_id, function (err, obs) {
            if (obs.user.equals(user_id)) {
                Observation.findOneAndUpdate({"_id": obs_id}, {$set: {displayToUser: false}}, function (err, u) {
                    res.json("ok");
                });
            } else if (obs.revisit.some(function (o) {
                return o['user'].equals(user_id);
            })) {
                Observation.findOneAndUpdate({"_id": obs_id, "revisit.user": {$eq: user_id}}, {$set: {'revisit.$.displayToUser': false}}, function (err, u) {
                    res.json("ok");
                });
            } else {
                res.status(400);
                res.header('Cazast_error', 'error_no_exist_obs_user');
                res.send('error_no_exist_obs_user');
            }
        });
    } else {
        res.status(400);
        res.header('Cazast_error', 'error_no_exist_obs');
        res.send('error_no_exist_obs');
    }
});

router.get('/updateAvgCoord', function (req, res) {
    var query = {
        "status": "pending"
    };
    Observation.find(query)
            .exec(function (err, observations) {
                for (var i = 0; i < observations.length; i++) {
                    var d = observations[i];
                    var meanra = parseFloat(d.Coord[0]);
                    var meandec = parseFloat(d.Coord[1]);
                    for (var n = 0; n < d.revisit.length; n++) {
                        meanra += parseFloat(d.revisit[n].x);
                        meandec += parseFloat(d.revisit[n].y);
                    }
                    meanra /= (d.revisit.length + 1);
                    meandec /= (d.revisit.length + 1);
                    if (JSON.stringify(d.avgCoord).toString() !== JSON.stringify([meanra, meandec]).toString()) {
                        console.log(JSON.stringify(d.avgCoord) + " " + JSON.stringify([meanra, meandec]));
                        d.avgCoord = [meanra, meandec];
                        Observation.findOneAndUpdate({_id: d._id}, {$set: {avgCoord: d.avgCoord}}, function () {});
                    }
                }
            });
    res.send("ok");
});

//router.get('/testMagnitudes', function (req, res) {
//    Observation.find({}, {}, {limit: 100})
//            .sort({time: -1})
//            .exec(function (err, observations) {
//                for (var i = 0; i < observations.length; i++) {
//                    require('../helpers/detectionHelper.js').getMagnitude(observations[i].image, observations[i].quintetn, observations[i].avgCoord, function (magnitude) {
//
//                    });
//                }
//            });
//    res.send("Ok");
//});

module.exports = router;
