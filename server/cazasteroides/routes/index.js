var express = require('express');
var router = express.Router();
var StatisticsHelper = require('../helpers/statisticsHelper.js');

/* GET home page. */
router.get('/', function (req, res, next) {
    var limit = 10;
    var count = 4;
    var render = {title: 'Cazasteroides'};

    StatisticsHelper.moreEffectiveVotes(limit, function (result) {
        render.moreEffectiveVotes = result;
        count--;
        if (count === 0) {
            res.render('index', render);
        }
    });

    StatisticsHelper.moreVotes(limit, function (result) {
        render.moreVotes = result;
        count--;
        if (count === 0) {
            res.render('index', render);
        }
    });

    StatisticsHelper.moreEffectiveDetections(limit, function (result) {
        render.moreEffectiveDetections = result;
        count--;
        if (count === 0) {
            res.render('index', render);
        }
    });
    StatisticsHelper.moreDetections(limit, function (result) {
        render.moreDetections = result;
        count--;
        if (count === 0) {
            res.render('index', render);
        }
    });



});

module.exports = router;
