var express = require('express');
var router = express.Router();
var StatisticsHelper = require('../helpers/statisticsHelper.js');

var MAX_LIMIT = 500;

router.get('/top/:limit/pointsForVotes', function (req, res) {
    var limit = req.params.limit;
    if (limit < 1 || limit > MAX_LIMIT) {
        res.status(400);
        res.header('Cazast_error', 'error_top_limit');
        res.send('error_top_limit');
    }

    StatisticsHelper.pointsForVotes(limit, function (result) {
        res.json(result);
    });

});

router.get('/top/:limit/pointsForDetections', function (req, res) {
    var limit = req.params.limit;
    if (limit < 1 || limit > MAX_LIMIT) {
        res.status(400);
        res.header('Cazast_error', 'error_top_limit');
        res.send('error_top_limit');
    }

    StatisticsHelper.pointsForDetections(limit, function (result) {
        res.json(result);
    });


});

router.get('/top/:limit/moreVotes', function (req, res) {
    var limit = req.params.limit;
    if (limit < 1 || limit > MAX_LIMIT) {
        res.status(400);
        res.header('Cazast_error', 'error_top_limit');
        res.send('error_top_limit');
    }

    StatisticsHelper.moreVotes(limit, function (result) {
        res.json(result);
    });

});

router.get('/top/:limit/moreDetections', function (req, res) {
    var limit = req.params.limit;
    if (limit < 1 || limit > MAX_LIMIT) {
        res.status(400);
        res.header('Cazast_error', 'error_top_limit');
        res.send('error_top_limit');
    }
    StatisticsHelper.moreDetections(limit, function (result) {
        res.json(result);
    });

});

router.get('/top/:limit/moreEffectiveDetections', function (req, res) {
    var limit = req.params.limit;
    if (limit < 1 || limit > MAX_LIMIT) {
        res.status(400);
        res.header('Cazast_error', 'error_top_limit');
        res.send('error_top_limit');
    }
    StatisticsHelper.moreEffectiveDetections(limit, function (result) {
        res.json(result);
    });

});

router.get('/top/:limit/moreEffectiveVotes', function (req, res) {
    var limit = req.params.limit;
    if (limit < 1 || limit > MAX_LIMIT) {
        res.status(400);
        res.header('Cazast_error', 'error_top_limit');
        res.send('error_top_limit');
    }
    StatisticsHelper.moreEffectiveVotes(limit, function (result) {
        res.json(result);
    });

});

router.get('/totalApproved', function (req, res) {
    StatisticsHelper.totalApproved(function (result) {
        res.json(result);
    });
});

router.get('/totalUserActions', function (req, res) {
    StatisticsHelper.totalUserActions(function (result) {
        res.json(result);
    });
});

router.get('/totalQuintetActions', function (req, res) {
    StatisticsHelper.totalQuintetActions(function (result) {
        res.json(result);
    });
});

router.get('/totalDetectionsActions', function (req, res) {
    StatisticsHelper.totalDetectionsActions(function (result) {
        res.json(result);
    });
});

router.get('/telescopesResume', function (req, res) {
    StatisticsHelper.telescopesResume(function (result) {
        res.json(result);
    });
});
router.get('/totalParticipants/:name', function (req, res) {
    StatisticsHelper.totalParticipants(req.params.name, function (result) {
        res.json(result);
    });
});

router.get('/totalDetections/:name', function (req, res) {
    StatisticsHelper.totalContestDetectionsActions(req.params.name, function (result) {
        res.json(result);
    });
});

module.exports = router;