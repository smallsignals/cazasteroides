var express = require('express');
var passport = require('passport');
var router = express.Router();
var AdminHelper = require('../helpers/adminHelper.js');

var child_process = require('child_process');
var configConst = require("../config_template");
var path = require('path');

router.post('/sync', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    child_process.exec(path.normalize(configConst.SRC_PYTHON) + path.normalize(configConst.SRC_SCRIPTS + 'sync_process_quintets.py'),
            function (err, stdout, stderr) {
                console.log(err);

                if (err)
                    res.send(JSON.stringify(err));
                else
                    res.send("ok");
            });

});

module.exports = router;
