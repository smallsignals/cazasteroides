var express = require('express');
var passport = require('passport');
var router = express.Router();
var Quintet = require('../models/image.js');
var Detection = require('../models/observation.js');
var Zone = require('../models/rectangle.js');
var Setup = require('../models/setup.js');
var configConst = require("../config_template");
var AdminHelper = require('../helpers/adminHelper.js');
//var fs = require('fs');
var gm = require('gm');//.subClass({imageMagick: true});

/* GET home page. */
router.get('/', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    var query = {};

    if (!AdminHelper.isSuperAdmin(req.user)) {
        var telescopes = req.user.adminTelescopes;
        if (!telescopes || telescopes.length <= 0)
        {
            res.json([]);
            return;
        }

        var telescope = req.query.telescope;
        if (telescope) {
            telescopes = [telescope];
        }

        for (var k = 0; k < telescopes.length; k++) {
            var expression = telescopes[k] + "$";
            var rx = new RegExp(expression, 'i');
            telescopes[k] = rx;
        }

        query = {name: {$in: telescopes}};
    }

    if (req.query.active) {
        query = {active: req.query.active};
    }

    const aggregatorOpts = ([
        {"$group": {
                "_id": "$image",
                "total_pending": {"$sum": {
                        $cond: [{$eq: ["$status", 'pending']}, 1, 0]
                    }
                },
                "total_rejected": {"$sum": {
                        $cond: [{$eq: ["$status", 'rejected']}, 1, 0]
                    }
                }
            }
        }
    ]);

    Detection.aggregate(aggregatorOpts).exec(function (err, detections) {
        Quintet.find(query,
                {'_id': 1, 'name': 1, 'fecha': 1, 'active': 1, 'height': 1, 'width': 1, 'info': 1, 'meta.TELESCOP': 1}
        ).sort({fecha: -1, active: -1}).lean().exec(function (err, quintets) {
            if (quintets) {
                for (var i = 0; i < detections.length; i++) {
                    var index = quintets.findIndex(function (element) {
                        return element['_id'].toString() === detections[i]['_id'].toString();
                    });
                    //console.log(index);
                    if (index >= 0) {
                        quintets[index].info.total_pending = detections[i]['total_pending'];
                        quintets[index].info.total_rejected = detections[i]['total_rejected'];
                    }
                }
                res.json(quintets);
            } else {
                res.json([]);
            }
        });

    });
});

router.get('/list', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    var query = {};

    if (!AdminHelper.isSuperAdmin(req.user)) {
        var telescopes = req.user.adminTelescopes;
        if (!telescopes || telescopes.length <= 0)
        {
            res.json([]);
            return;
        }

        var telescope = req.query.telescope;
        if (telescope) {
            telescopes = [telescope];
        }

        for (var k = 0; k < telescopes.length; k++) {
            var expression = telescopes[k] + "$";
            var rx = new RegExp(expression, 'i');
            telescopes[k] = rx;
        }

        query = {name: {$in: telescopes}};
    }

    Quintet.find(query,
            {'_id': 1, 'name': 1, 'fecha': 1, 'active': 1}
    ).sort({fecha: -1, active: -1}).exec(function (err, quintets) {
        //console.log("length: " + quintets.length);
        res.send(JSON.stringify(quintets));
    });
});
router.get('/telescopes', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }
    if (AdminHelper.isSuperAdmin(req.user)) {
        Quintet.find({},
                {name: 1, meta: 1}).exec(function (err, img) {
            var arr = [];
            for (var k in  img) {
                var telescope = img[k] && img[k].meta &&  img[k].meta.TELESCOP ? img[k].meta.TELESCOP : img[k].name.substr(14);
                if (arr.indexOf(telescope) === -1) {
                    arr.push(telescope);
                }
            }
            arr.sort();
            res.send(arr);
        });
    } else {
        var arr = req.user.adminTelescopes ? req.user.adminTelescopes : [];
        arr.sort();
        res.send(arr);
    }
});

router.get('/:id_image', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    if (!req.params.id_image) {
        res.status(400);
        res.header('Cazast_error', 'error_invalid_params');
        res.send('error_invalid_params');
        return;
    }

    Quintet.findById(req.params.id_image, {_id: 1, active: 1, fecha: 1, name: 1, filename: 1, mapfile: 1, height: 1, width: 1}, function (err, doc) {
        if (err || !doc) {
            res.status(400);
            res.send(JSON.stringify(err));
            return;
        }

        var quintet = doc.toObject();

        Detection.find({image: req.params.id_image}, {status: 1, revisit: 1, votes: 1, downvotes: 1, 'object.mpcDesignation': 1, 'object.cazastDesignation': 1, 'quintetn': 1}).exec(function (err, detections) {

            for (var i = detections.length - 1; i >= 0; i--) {

                if (detections[i].votes) {
                    detections[i].votes = new Array(detections[i].votes.length);
                }
                if (detections[i].downvotes) {
                    detections[i].downvotes = new Array(detections[i].downvotes.length);
                }
                if (detections[i].revisit) {
                    detections[i].revisit = new Array(detections[i].revisit.length);
                }
            }

            detections.sort(require('../helpers/detectionHelper.js').compareDetection);
            quintet.detections = detections;

            Zone.count({image: req.params.id_image}, function (err, c) {
                quintet.zones = c;
                res.send(JSON.stringify(quintet));
            });
        });
    });
});

router.get("/jpg/:name", function (req, res, next) {

    if (!req.params.name) {
        res.status(400);
        res.header('Cazast_error', 'error_invalid_params');
        res.send('error_invalid_params');
        return;
    }

    var request = require('request');


    Quintet.findOne({name: req.params.name}, {meta: 1}, function (err, quintet) {

        var name = req.params.name;
        var date = name.substring(0, 4) + '-' + name.substring(4, 6) + '-' + name.substring(6, 8);
        var telescopio = quintet && quintet.meta && quintet.meta.TELESCOP ? quintet.meta.TELESCOP : name.substring(14);

        var reqstr = configConst.URL_TELESCOPIOVIRTUAL + 'findimages/headers/' + date + '/' + telescopio + '/asteroid(' + name + ')';
        //console.log("reqstr: " + reqstr);
        request({
            uri: reqstr,
            method: 'GET'
        }, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                var parseString = require('xml2js').parseString;
                parseString(body, function (err, result) {
                    if (!err) {
                        //console.log("Result: " + JSON.stringify(result));
                        if (result.publicImages) {
                            if (result.publicImages.public_image.length > 0) {
                                var image = result.publicImages.public_image[0];
                                //console.log("image.jpeg: " + String(image.jpeg));
                                require('request').get(String(image.jpeg)).pipe(res);
                                return;
                            } else {
                                res.status(400);
                                res.header('Cazast_error', 'error_empty_public_image');
                                res.send('error_empty_public_image');
                            }
                        } else {
                            res.status(400);
                            res.header('Cazast_error', 'error_empty_publicImages');
                            res.send('error_empty_publicImages');
                        }
                    } else {
                        console.log("err: " + JSON.stringify(err));
                        res.status(400);
                        res.send(err);
                    }
                });
            } else {
                console.log("error: " + error);
                res.status(400);
                res.send(error);
            }
        });
    });
});

function generateCoverage(doc, callback) {//Move to gifHelper

    var pix2coorHelper = require('../helpers/pix2coor.js');
    var metadata = doc.meta;

    Zone.find({image: doc._id}, {minx: 1, miny: 1, maxx: 1, maxy: 1}, function (err, rects) {
        //console.log("   Zones.length: " + rects.length);

        if (!doc.width)
            doc.width = 2000;
        if (!doc.height)
            doc.height = 2000;
        //console.log("Coverage[1]: creando : " + doc.width + " " + doc.height);
        var g = gm(doc.width, doc.height, "#ffffffff");
        g.fill("#ff0000f0");
        for (var r = 0; r < rects.length; r++) {
            //estimate area
            if (rects[r].minx && rects[r].miny && rects[r].maxx && rects[r].maxy) {
                //Evitar pintar zonas fuera del marco
                //console.log("***" + JSON.stringify(rects[r]));

                rects[r].miny = parseFloat(doc.height) - parseFloat(rects[r].miny);
                rects[r].maxy = parseFloat(doc.height) - parseFloat(rects[r].maxy);

                if (rects[r].minx < 0)
                    rects[r].minx = 0;
                if (rects[r].miny < 0)
                    rects[r].miny = 0;
                if (rects[r].maxx > doc.width)
                    rects[r].maxx = doc.width;
                if (rects[r].maxy > doc.height)
                    rects[r].maxy = doc.height;

                //console.log("   " + JSON.stringify(rects[r]));
                g.drawRectangle(
                        Math.min(rects[r].minx, rects[r].maxx),
                        Math.min(rects[r].miny, rects[r].maxy),
                        Math.max(rects[r].minx, rects[r].maxx),
                        Math.max(rects[r].miny, rects[r].maxy)
                        );
            }
        }
        //console.log("Coverage[1]: Zones Created");
        Detection.find({image: doc._id}).sort({status: -1}).lean().exec(function (err, obs) {

            //console.log("   Detections.length:" + obs.length);
            for (var o = 0; o < obs.length; o++) {
                //estimate area
                switch (obs[o].status) {
                    case 'rejected':
                        g.fill("#5E0000");
                        break;
                    case 'approved':
                        g.fill("#00CC00");
                        break;
                    default:
                        g.fill("#0000ff");
                        break;
                }


                if (doc.coeff && (doc.coeff[obs.quintetn] || doc.coeff[0])/* && img.coeff[index].BZERO*/) {
                    try {
                        if (doc.coeff[0] && doc.coeff[0][obs.quintetn]) {
                            metadata = doc.coeff[0][obs.quintetn];
                        }
                    } catch (e) {
                        console.log("Ups", e);
                    }
                }
                var repix = pix2coorHelper.coord2pix(parseFloat(obs[o].avgCoord[0]), parseFloat(obs[o].avgCoord[1]), metadata);

                g.drawRectangle(repix[0] - 8, repix[1] - 8,
                        repix[0] + 8, repix[1] + 8);

            }
            //console.log("Coverage[1]: Detections Created");
            callback(g);

        });
    });
}

router.get('/:id_image/coverage', function (req, res, next) {
    Quintet.findById(req.params.id_image, function (err, doc) {
        if (err) {
            res.status(400);
            res.send(err);
            return;
        }

        generateCoverage(doc, function (g) {
            res.set('Content-Type', 'image/png'); // set the header here
            g.stream('png', function (err, stdout, stderr) {
                //console.log("Coverage[2]: streaming" + err);
                if (err) {
                    res.status(400);
                    res.header('Cazast_error', 'error_gm_' + err.code);
                    res.send('error_gm_' + err.code);
                    //return console.dir(arguments);
                } else {
                    stdout.pipe(res);
                }


//                var writeStreamerr = fs.createWriteStream(configConst.SRC_SERVER + 'err.txt');
//                stderr.pipe(writeStreamerr);
            });
        });

    });
});

router.post('/infoSync', passport.authenticate('bearer'), function (req, res, next) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    Quintet.find({active: true},
            {'_id': 1, 'name': 1, 'fecha': 1, 'active': 1, 'height': 1, 'width': 1, 'info': 1}
    ).sort({fecha: -1, active: -1}).lean().exec(function (err, quintets) {
        //console.log("Quintets length: " + quintets.length);
        if (quintets) {
            var allId = [];
            for (var i = 0; i < quintets.length; i++) {
                //if (!quintets[i].info) {
                allId.push(quintets[i]._id.toString());
                quintets[i]['info'] = {};
                quintets[i]['info']['total_detections'] = 0;
                quintets[i]['info']['total_approved'] = 0;
                quintets[i]['info']['total_know'] = 0;
                quintets[i]['info']['total_area_shown'] = 0;
                quintets[i]['info']['total_area'] = parseFloat(quintets[i].width) * parseFloat(quintets[i].height);
                delete quintets[i].width;
                delete quintets[i].height;
                //}
            }
            Detection.find({image: {$in: allId}}, {status: 1, image: 1, /*'rect': 1,*/ 'object.mpcDesignation': 1})
                    .exec(function (err, detections) {
                        //console.log("Detections length: " + detections.length);

                        for (var j = 0; j < detections.length; j++) {
                            var index = allId.indexOf(detections[j].image.toString());
                            if (index >= 0) {
                                quintets[index]['info']['total_detections'] += 1;
                                quintets[index]['info']['total_approved'] += (detections[j].status == 'approved' ? 1 : 0);
                                quintets[index]['info']['total_know'] += (detections[j].object && detections[j].object.mpcDesignation ? 1 : 0);
                            }
                        }

                        delete detections;

                        Zone.find({image: {$in: allId}}, {image: 1, minx: 1, miny: 1, 'maxx': 1, 'maxy': 1})
                                .exec(function (err, zone) {
                                    //console.log("Zones length: " + zone.length);

                                    for (var j = 0; j < zone.length; j++) {
                                        var index = allId.indexOf(zone[j].image.toString());
                                        if (index >= 0) {
                                            var area = (((Math.abs(parseFloat(zone[j].minx) - parseFloat(zone[j].maxx)))) * (Math.abs(parseFloat(zone[j].miny) - parseFloat(zone[j].maxy))));
                                            if (!isNaN(area)) {
                                                quintets[index]['info']['total_area_shown'] += area;
                                            }
                                        }
                                    }
                                    for (var i = 0; i < quintets.length; i++) {
                                        var query = {$set: {}};
                                        query['$set']['info'] = quintets[i]['info'];
                                        Quintet.findOneAndUpdate({"_id": quintets[i]._id}, query, function (err, image) {
                                            //console.log("set " + JSON.stringify(err));
                                            //console.log("set " + JSON.stringify(image));
                                        });
                                    }
                                    res.json([]);
                                });
                    });
        } else {
            res.json([]);
        }
    });
});

router.get('/alert/nearThresholdToDesactivate', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    var query = {"active": true};

    if (!AdminHelper.isSuperAdmin(req.user)) {
        var telescopes = req.user.adminTelescopes;
        if (!telescopes || telescopes.length <= 0)
        {
            res.json([]);
            return;
        }

        for (var k = 0; k < telescopes.length; k++) {
            var expression = telescopes[k] + "$";
            var rx = new RegExp(expression, 'i');
            telescopes[k] = rx;
        }

        query['name'] = {$in: telescopes};
    }

    Setup.findOne({}, function (err, setup) {

        if (setup) {
            var index = setup.config.findIndex(function (element) {
                return element.key.toString() === "threshold_to_deactivate_quintet";
            });
            if (index >= 0 && index < setup.config.length) {
                var threshold = parseFloat(setup.config[index].value);

                Quintet.find(query, {'name': 1, 'info': 1})
                        .exec(function (err, quintets) {
                            var response = [];
                            for (var i = 0; i < quintets.length; i++) {
                                var q = quintets[i];
                                if (q.info && parseFloat(q.info.total_area_shown) / parseFloat(q.info.total_area) > threshold * 0.85) {
                                    response.push({name: q.name, percent: (parseFloat(q.info.total_area_shown) / parseFloat(q.info.total_area)) * 100 / threshold});
                                }
                            }

                            response.sort(function (a, b) {
                                return b.percent - a.percent;
                            });

                            res.json(response);
                        });

            } else {
                res.json([]);
            }
        } else {
            res.json([]);
        }
    });
});


router.get('/alert/quintetsWithPendings', passport.authenticate('bearer'), function (req, res, next) {

    if (!AdminHelper.isAdmin(req.user)) {
        AdminHelper.errorNoAdmin(res);
        return;
    }

    var query = {};

    if (!AdminHelper.isSuperAdmin(req.user)) {
        var telescopes = req.user.adminTelescopes;
        if (!telescopes || telescopes.length <= 0)
        {
            res.json([]);
            return;
        }

        for (var k = 0; k < telescopes.length; k++) {
            var expression = telescopes[k] + "$";
            var rx = new RegExp(expression, 'i');
            telescopes[k] = rx;
        }

        query['name'] = {$in: telescopes};
    }


    const aggregatorOpts = ([
        {"$group": {
                "_id": "$image",
                "total_pending": {"$sum": {
                        $cond: [{$eq: ["$status", 'pending']}, 1, 0]
                    }
                }
            }
        }
    ]);

    Detection.aggregate(aggregatorOpts).exec(function (err, detections) {
        if (detections) {
            detections.sort(function (a, b) {
                return b.total_pending - a.total_pending;
            });

            var index = detections.findIndex(function (element) {
                return parseInt(element.total_pending) <= 0;
            });

            if (index >= 0 && index < detections.length) {
                detections.splice(index, detections.length - index);
            }

            Quintet.populate(detections, {path: '_id', match: query, select: 'name active -_id'}, function (err, populatedQuintets) {

                var response = [];
                for (var i = 0; i < populatedQuintets.length; i++) {
                    if (populatedQuintets[i]._id) {
                        response.push({name: populatedQuintets[i]._id.name, active: populatedQuintets[i]._id.active, total_pending: populatedQuintets[i].total_pending});
                    }
                }

                response.sort(function (a, b) {
                    if (a.active === b.active) {
                        return b.total_pending - a.total_pending;
                    }
                    return a.active ? 1 : -1;
                });

                res.json(response);
            });
        } else {
            res.json([]);
        }
    });
});

module.exports = router;

