var express = require('express');
//var passport = require('passport');
var User = require('../models/user.js');
var Observation = require('../models/observation.js');
var authController = require("../auth");
var mail = require("../mail.js");
var router = express.Router();

// Create endpoint /api/users for POST
router.post('/', function (req, res) {

    var header = req.headers['authorization'] || '', // get the header
            token = header.split(/\s+/).pop() || '', // and the encoded auth token
            auth = new Buffer(token, 'base64').toString(), // convert from base64
            parts = auth.split(/:/), // split on colon
            username = parts[0],
            password = parts[1],
            email = parts[2];

    if (username && password && email) {
        req.body.username = username;
        req.body.password = password;
        req.body.email = email;
    }

    var user = new User(req.body);
    user.provider = "local";

    user.save(function (err) {
        if (err) {
            res.send(err);
        } else {
            res.json({message: 'user added'});
        }
    });
});

/**
 * Lista ordenada por puntos de los usuarios con mas de 0 puntos
 * 
 * Response:
 *  {
 *      Lista de usuarios donde cada uno contiene
 *      {
 *          username
 *          points
 *          badges
 *      }
 *  }
 */
// Create endpoint /api/users for GET
router.get('/', function (req, res) {//TODO add pagination

    var condition = {};

    if (req.query.skip && req.query.limit) {
        condition = {skip: parseInt(req.query.skip), limit: parseInt(req.query.limit)};
    }

    User.find({"points": {$gt: 0}}, {"username": 1, "points": 1, "badges": 1}, condition)
            .sort({points: -1})
            .exec(function (err, users) {
                if (err) {
                    res.send(err);
                } else {
                    res.json(users);
                }
            });
});
//
//router.get('/all', passport.authenticate('bearer'), function (req, res) {//Not in use
//
//    if (req.user) {
//        if (!req.user.superAdmin) {
//            res.status(401);
//            res.header('Cazast_error', 'error_invalid_superadmin');
//            res.send('error_invalid_superadmin');
//            return;
//        }
//    }
//
//    User.find({provider: 'local'},
//            {
//                _id: 1,
//                username: 1,
//                admin: 1,
//                superAdmin: 1,
//                adminTelescopes: 1,
//                email: 1
//            }
//    ).sort({superAdmin: -1, admin: -1, username: 1}).exec(function (err, users) {
//        if (err) {
//            res.send(err);
//        } else {
//            res.json(users);
//        }
//    });
//});

function makeid()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

// Create endpoint /api/users for GET
router.post('/forgot', function (req, res) {
    var r = new RegExp("^" + req.body.email + "$", 'i');
    User.find({email: r}).exec(function (err, users) {
        if (err) {
            res.json(err);
        } else if (users.length == 0) {
            res.json({res: "the mail doen't exist"});
        } else {
            users[0].password = makeid();
            var u = {username: users[0].username,
                password: users[0].password,
                email: users[0].email};
            users[0].save(function (err) {
                if (err) {
                    res.json(err);
                } else {
                    var lang = undefined;
                    if (req.body.lang) {
                        lang = req.body.lang;
                    }
                    mail.sendPassword(u, lang);
                    res.json({res: "ok"});
                }
            });
        }
    });

});


///* GET home page. */
//router.post('/update', function (req, res, next) {//Not in use
//    var conditions = {"_id": req.body.user._id}
//    , update = req.body.user
//            , options = {multi: false};
//    delete update._id;
//    console.log(JSON.stringify(conditions));
//    console.log(JSON.stringify(update));
//    User.update(conditions, update, options, callback);
//
//    function callback(err, numAffected) {
//        // numAffected is the number of updated documents
//        console.log(numAffected + " affected rows ");
//        res.send(JSON.stringify({
//            "affected": numAffected,
//            "error": err
//        }));
//    }
//});
//
///* GET home page. */
//router.post('/delete', function (req, res, next) {//Not in use
//    User.findByIdAndRemove(req.body.user._id, function () {
//        res.send("ok");
//    }); // executes
//});

///* Make user admin. */
//router.post('/updateadmin', passport.authenticate('bearer'), function (req, res) {
//
//    if (req.user) {
//        if (!req.user.superAdmin) {
//            res.status(401);
//            res.header('Cazast_error', 'error_invalid_superadmin');
//            res.send('error_invalid_superadmin');
//            return;
//        }
//    }
//
//    if (!req.body.user._id) {
//        res.status(400);
//        res.header('Cazast_error', 'error_invalid_params');
//        res.send('error_invalid_params');
//        return;
//    }
//
//    console.log('req.body: ' + JSON.stringify(req.body));
//    User.findById(req.body.user._id).exec()
//            .then(function (user) {
//
//                if (req.body.user.admin) {
//                    user.admin = true;
//                } else {
//                    if (user.admin) {
//                        console.log("Remove Admin property");
//                        user.admin = undefined;
//                    }
//                }
//
//                if (req.body.user.superAdmin) {
//                    user.superAdmin = true;
//                    user.admin = true;
//                } else {
//                    if (user.superAdmin) {
//                        console.log("Remove superAdmin property")
//                        user.superAdmin = undefined;
//                    }
//                }
//
//                user.adminTelescopes = req.body.user.adminTelescopes;
//                console.log(JSON.stringify(user));
//                return user.save();
//            }).then(function () {
//        res.send("ok");
//    });
//    // executes
//});
//
//
//
//router.post('/admin', passport.authenticate('bearer'), function (req, res) {
//
//    if (req.user) {
//        if (!req.user.superAdmin) {
//            res.status(401);
//            res.header('Cazast_error', 'error_invalid_superadmin');
//            res.send('error_invalid_superadmin');
//            return;
//        }
//    }
//
//    if (!req.body.user._id) {
//        res.status(400);
//        res.header('Cazast_error', 'error_invalid_params');
//        res.send('error_invalid_params');
//        return;
//    }
//
//    console.log('req.body: ' + JSON.stringify(req.body));
//    User.findById(req.body.user._id).exec()
//            .then(function (user) {
//                user.admin = true;
//                return user.save();
//            }).then(function () {
//        res.send("ok");
//    });
//    // executes
//});
///* Delete user admin. */
//router.delete('/admin', passport.authenticate('bearer'), function (req, res) {
//
//    if (req.user) {
//        if (!req.user.superAdmin) {
//            res.status(401);
//            res.header('Cazast_error', 'error_invalid_superadmin');
//            res.send('error_invalid_superadmin');
//            return;
//        }
//    }
//
//    var user_id = req.query.id;
//
//    if (!user_id) {
//        res.status(400);
//        res.header('Cazast_error', 'error_invalid_params');
//        res.send('error_invalid_params');
//        return;
//    }
//
//    User.findById(user_id).exec()
//            .then(function (user) {
//                delete user.admin;
//                return user.save();
//            }).then(function () {
//        res.send("ok");
//    });
//// executes
//});

router.get('/me', authController.isBearerAuthenticated, function (req, res, next) {
    var user_id = req.user._id;
    User.findById(user_id
            , {
                "_id": 1,
                "provider": 1,
                "username": 1,
                "sDetections": 1,
                "points": 1,
                "badges": 1,
                "icon": 1//,
                        //"contests": 1
            }
    ).lean()
            .populate('contests.contest')
            .exec(function (err, u) {
                //console.log(JSON.stringify(u, null, "\t"));
                if (err) {
                    res.status(400);
                    res.header('Cazast_error', 'error_user_not_found');
                    res.send('error_user_not_found');
                    return;
                }
                var approved = u.sDetections && u.sDetections.approved ? u.sDetections.approved : 0;
                var total = u.sDetections ? u.sDetections.total : 0;
                u['obs_total'] = approved + "/" + total;
                delete u.sDetections;
                res.json(u);

            });
});
router.put('/updatemail/:email', authController.isBearerAuthenticated, function (req, res, next) {
    req.user.email = req.params.email;
    req.user.save();
    res.send("ok");
});

module.exports = router;
