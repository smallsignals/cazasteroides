var express = require('express');
var passport = require('passport');
var Contest = require('../models/contest.js');
var router = express.Router();
var AdminHelper = require('../helpers/adminHelper.js');

router.post('/', passport.authenticate('bearer'), function (req, res) {

    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    var contest = new Contest(req.body);
    contest.save(function (err) {
        if (err) {
            res.status(400);
            res.send(err);
        } else {
            res.json({_id: contest._id, message: 'contest added'});
        }
    });
});

/**
 * Todos los concursos
 */
router.get('/', passport.authenticate('bearer'), function (req, res) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }


    Contest.find({})
            .sort({time_start: -1})
            .exec(function (err, contests) {
                if (err) {
                    res.status(400);
                    res.send(err);
                } else {
                    contests.sort(require('../helpers/contestHelper.js').compareContest);
                    res.json(contests);
                }
            });
});

router.post('/logo', passport.authenticate('bearer'), function (req, res, next) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }
    //console.log(JSON.stringify(req.body));
    var url = req.body.url;
    Contest.findOneAndUpdate({"_id": req.body._id}, {$push: {logos: url}}, function (err, contest) {
        if (err) {
            res.status(400);
            res.send(err);
        } else {
            res.json({id: contest._id, message: 'contest logo added'});
        }
    });

});

router.delete('/', passport.authenticate('bearer'), function (req, res, next) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    Contest.findByIdAndRemove(req.query._id, function () {
        res.send("ok");
    });
});

router.put('/:id', passport.authenticate('bearer'), function (req, res, next) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    Contest.findOneAndUpdate({"_id": req.params.id}, {$set: req.body}, function (err, doc) {
        res.send("ok");
    });
});

router.delete('/logo', passport.authenticate('bearer'), function (req, res, next) {
    if (!AdminHelper.isSuperAdmin(req.user)) {
        AdminHelper.errorNoSuperAdmin(res);
        return;
    }

    var logos = [].concat(req.query.logos);

    Contest.update({_id: req.query._id}, {$pullAll: {logos: logos}}, function (err, doc) {
        res.send("ok");
    });
});

module.exports = router;
