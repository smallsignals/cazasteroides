var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var authController = require('./auth');
var configConst = require("./config_template");

require('./models/user');

var routes = require('./routes/index');

var adminUsers = require('./routes/admin.users');
var adminQuintets = require('./routes/admin.quintets');
var adminDetections = require('./routes/admin.detections');
var adminContest = require('./routes/admin.contest');
var adminScripts = require('./routes/admin.scripts');
var adminSetup = require('./routes/admin.setup');
var adminCandidate = require('./routes/admin.candidate');

var users = require('./routes/users');
var images = require('./routes/image');
var obs = require('./routes/obs');
var rectangles = require('./routes/rectangle');
var contest = require('./routes/contest');
var setup = require('./routes/setup');
var statistics = require('./routes/statistics');
var gcm = require('./routes/gcm');

var app = express();

var mongoose = require('mongoose');

if (!configConst.DEBUG) {
    console.log('Produccion');
} else {
    console.log('¡¡¡¡¡¡¡¡¡¡ WARNING !!!!!!!!!!');
    console.log('¡¡¡¡¡¡¡¡¡ MODO DEBUG !!!!!!!!');
}
//console.log(JSON.stringify(configConst));

//mongoose.connect('localhost', 'cazasteroides');
mongoose.connect(configConst.MONGODB);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback() {
    console.log('Connected to DB');
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());


app.use(function (req, res, next) {
//CORS middleware
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type');
    next();
});

app.use(express.static(path.join(__dirname, 'public')));

app.use("/mobile", express.static(configConst.SRC_MOBILE_APP));
//app.use("/mobile", express.static(path.join(__dirname, '../../gloria-mobile/platforms/browser/www')));
app.use("/gif", express.static(path.join(__dirname, 'gif')));

// Make our db accessible to our router
app.use(function (req, res, next) {
    req.db = mongoose;
    next();
});
//app.use(authController.isAuthenticated);

app.use(passport.initialize());
app.use(passport.session());


app.use('/', routes);
app.use('/admin/users', adminUsers);
app.use('/admin/quintets', adminQuintets);
app.use('/admin/detections', adminDetections);
app.use('/admin/contest', adminContest);
app.use('/admin/scripts', adminScripts);
app.use('/admin/setup', adminSetup);
app.use('/admin/candidate', adminCandidate);
app.use('/users', users);
app.use('/auth', authController.r);
app.use('/images', images);
app.use('/obs', obs);
app.use('/rectangles', rectangles);
app.use('/contest', contest);
app.use('/setup', setup);
app.use('/statistics', statistics);
app.use('/gcm', gcm);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
console.log("started");
