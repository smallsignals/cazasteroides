var express = require("express");

var EmailTemplate = require('email-templates').EmailTemplate;
var path = require('path');
var nodemailer = require('nodemailer');

var configConst = require("./config_template");

var fs = require('fs');


// create reusable transporter object using the default SMTP transport
var smtpConfig = {
    host: configConst.SMTP_HOST,
    port: configConst.SMTP_PORT,
    //secure: true, // use SSL
    requireTLS: true,
    auth: {
        user: configConst.SMTP_USER,
        pass: configConst.SMTP_PASS
    }
};
//var transporter = nodemailer.createTransport('smtps://no-reply%40multifab.eu:m4$1-FAB@smtp.1and1.es');
var transporter = nodemailer.createTransport(smtpConfig);

//// Create endpoint /api/users for POST
//
//var sendWelcome = function (user) {
//    var templateDir = path.join(__dirname, 'mailtempl', 'welcomemail');
//    var welcome = new EmailTemplate(templateDir);
//    //var user = req.body;
//    welcome.render(user, function (err, result) {
//        // result.html
//        // result.text
//        // setup e-mail data with unicode symbols
//        if (err) {
//            //res.json(err)
//            console.log(err);
//            return err;
//        } else {
//            console.log(result.html);
//            var mailOptions = {
//                from: '"Multifab " <no-reply@multifab.eu>', // sender address
//                to: user.email, // list of receivers
//                subject: result.subject, // Subject line
//                text: result.text, // plaintext body
//                html: result.html // html body
//            };
//
//            // send mail with defined transport object
//            transporter.sendMail(mailOptions, function (error, info) {
//                if (error) {
//                    //res.json(error);
//                    console.log(error);
//                    return error;
//
//                }
//                //res.send('Message sent: ' + info.response);
//                console.log('Message sent: ' + info.response);
//                return info;
//            });
//        }
//    });
//};
//
//
//var sendConfirm = function (user) {
//    var templateDir = path.join(__dirname, 'mailtempl', 'confirmmail');
//    var welcome = new EmailTemplate(templateDir);
//    //var user = req.body;
//    welcome.render(user, function (err, result) {
//        // result.html
//        // result.text
//        // setup e-mail data with unicode symbols
//        if (err) {
//            //res.json(err)
//            console.log(err);
//            return err;
//        } else {
//            console.log(result.html);
//            var mailOptions = {
//                from: '"Multifab " <no-reply@multifab.eu>', // sender address
//                to: user.email, // list of receivers
//                subject: result.subject, // Subject line
//                text: result.text, // plaintext body
//                html: result.html // html body
//            };
//
//            // send mail with defined transport object
//            transporter.sendMail(mailOptions, function (error, info) {
//                if (error) {
//                    //res.json(error);
//                    console.log(error);
//                    return error;
//
//                }
//                //res.send('Message sent: ' + info.response);
//                console.log('Message sent: ' + info.response);
//                return info;
//            });
//        }
//    });
//};

var sendPassword = function (user, lang) {
    var templateDir = path.join(__dirname, 'mailtempl', lang ? lang : "en", 'passwordmail');
    if (!fs.existsSync(templateDir)) {
        templateDir = path.join(__dirname, 'mailtempl', "en", 'passwordmail');
    }

    console.log("template Dir" + templateDir);
    var password = new EmailTemplate(templateDir);
    //var user = req.body;

    password.render(user, function (err, result) {
        // result.html
        // result.text
        // setup e-mail data with unicode symbols
        if (err) {
            //res.json(err)
            console.log("renderer" + err);
            return err;
        } else {

            var mailOptions = {
                from: '"Cazasteroides " <no-reply@cazasteroides.org>', // sender address
                to: user.email, // list of receivers
                subject: result.subject, // Subject line
                text: result.text, // plaintext body
                html: result.html // html body
            };

            // send mail with defined transport object
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    //res.json(error);
                    console.log(error);
                    return error;

                }
                //res.send('Message sent: ' + info.response);
                console.log('Message sent: ' + info.response);
                return info;
            });
        }
    });
};

var sendMPCCandidate = function (to, subject, text) {
    var mailOptions = {
        from: '"Cazasteroides " <no-reply@cazasteroides.org>', // sender address
        to: "slemes@iac.es", // list of receivers  //obs@cfa.harvard.edu
        subject: subject,
        text: text // plaintext body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
            return error;

        }
        console.log('Message sent: ' + info.response);
        return info;
    }
    );
};

var sendNewQuintetsAvailables = function (user, lang) {
    var templateDir = path.join(__dirname, 'mailtempl', lang ? lang : "en", 'newQuintetsAvailables');
    if (!fs.existsSync(templateDir)) {
        templateDir = path.join(__dirname, 'mailtempl', "en", '');
    }
    var newQuintetsAvailables = new EmailTemplate(templateDir);
    newQuintetsAvailables.render(user, function (err, result) {
        if (err) {
            console.log("renderer" + err);
            return err;
        } else {
            var mailOptions = {
                from: '"Cazasteroides " <no-reply@cazasteroides.org>',
                to: user.email,
                subject: result.subject,
                text: result.text,
                html: result.html
            };
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);

                    return error;
                }
                require('./models/user.js').findOneAndUpdate({"_id": user._id}, {$set: {"notification.newQuintetsAvailableEmail": Date.now()}}, function (err, doc) {

                });
                console.log('Message sent: ' + info.response);
                return info;
            });
        }
    });
};


//module.exports.sendWelcome = sendWelcome;
//module.exports.sendConfirm = sendConfirm;
module.exports.sendPassword = sendPassword;
module.exports.sendMPCCandidate = sendMPCCandidate;
module.exports.sendNewQuintetsAvailables = sendNewQuintetsAvailables;
