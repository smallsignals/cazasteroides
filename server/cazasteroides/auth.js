// Load required packages
var passport = require('passport');
var BasicStrategy = require('passport-http').BasicStrategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var FacebookTokenStrategy = require('passport-facebook-token');

var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var Token = require('./models/token');
var User = require('./models/user');
var base64 = require('./base64');

var configConst = require("./config_template");

// used to serialize the user for the session
passport.serializeUser(function (user, done) {
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});


function RegisterFacebookLogin(accessToken, refreshToken, profile, done) {
    /*User.findOrCreate({ facebookId: profile.id }, function (err, user) {
     return cb(err, user);
     });*/
    console.log("here1");
    var queryCondition = {};
    if (profile && profile._json && profile._json.email) {
        queryCondition = {
            $or: [{username: profile.id, provider: 'fiware'},
                {providerId: profile.id, provider: 'facebook'}
                // {email: profile._json.email}
            ]
        };
    } else {
        queryCondition = {
            $or: [{username: profile.id, provider: 'fiware'},
                {providerId: profile.id, provider: 'facebook'}
            ]
        };
    }

    console.log("here");

    try {
        if (profile) {//{"provider":"facebook","id":"10154186369076176","displayName":"Samuel Lemes Perera","name":{"familyName":"Lemes Perera","givenName":"Samuel","middleName":""},"gender":"","emails":[{"value":""}],"photos":[{"value":"https://graph.facebook.com/v2.6/10154186369076176/picture?type=large"}],"_raw":"{\"id\":\"10154186369076176\",\"name\":\"Samuel Lemes Perera\",\"last_name\":\"Lemes Perera\",\"first_name\":\"Samuel\"}","_json":{"id":"10154186369076176","name":"Samuel Lemes Perera","last_name":"Lemes Perera","first_name":"Samuel"}}
            //console.log(JSON.stringify(profile));
        }
    } catch (e) {

    }

    var closureuser;
    User.findOne(queryCondition).exec().then(function (user) {
        if (!user) {
            console.log("creating new user");
            var userData = {
                username: profile.id,
                providerId: profile.id,
                provider: 'facebook'
            };

            if (profile && profile._json && profile._json.email) {
                userData.email = profile._json.email;
            }

            if (profile && profile.photos && profile.photos.length > 0) {
                userData.icon = profile.photos[0].value;
            }

            user = new User(userData);
            return user.save({facebookProfile: profile});
        } else {
            var needSave = false;
            if (!user.email && profile && profile._json && profile._json.email) {
                user.email = profile._json.email;
                needSave = true;
            }

            if (!user.icon && profile && profile.photos && profile.photos.length > 0) {
                user.icon = profile.photos[0].value;
                needSave = true;
            }

            if (profile && user.username == profile.id && user.provider == 'fiware') {
                needSave = true;
            }

            if (needSave) {
                return user.save({facebookProfile: profile});
            } else {
                return user;
            }
        }
    }, function (err) {
        console.log(err);
        return done(err);
    }).then(function (user) {
        console.log("ford:" + JSON.stringify(user));
        closureuser = user;
        return Token.findOne({
            userId: user._id
        }).exec();
    }, function (err) {
        console.log(err);
        return done(err);
    }).then(function (token) {
        //No token was found...
        if (!token) {
            console.log("creating new token");
            token = new Token({
                userId: closureuser._id,
                value: accessToken
            });
        } else {
            token.value = accessToken;
        }
        return token.save();
    }, function (err) {
        console.log(err);
        return done(err);
    }).then(function (token) {
        console.log("done");
        closureuser.token = token;
        return done(null, closureuser);
    }, function (err) {
        console.log(err);
        return done(err);
    });
}

passport.use(new FacebookTokenStrategy({
    clientID: configConst.FACEBOOK_APP_ID,
    clientSecret: configConst.FACEBOOK_APP_SECRET
}, function (accessToken, refreshToken, profile, done) {
    console.log("here2");
    RegisterFacebookLogin(accessToken, refreshToken, profile, done);
}
));

passport.use(new FacebookStrategy({
    clientID: configConst.FACEBOOK_APP_ID,
    clientSecret: configConst.FACEBOOK_APP_SECRET,
    callbackURL: configConst.FACEBOOK_URL_CALLBACK
},
        function (accessToken, refreshToken, profile, cb) {
            RegisterFacebookLogin(accessToken, refreshToken, profile, cb);
        }
));
passport.use(new BasicStrategy(
        function (username, password, callback) {
            User.findOne({
                username: username
            }, function (err, user) {
                if (err) {
                    return callback(err);
                }

                // No user found with that username
                if (!user) {
                    return callback(null, false);
                }

                // Make sure the password is correct
                user.verifyPassword(password, function (err, isMatch) {
                    if (err) {
                        return callback(err);
                    }

                    // Password did not match
                    if (!isMatch) {
                        return callback(null, false);
                    }

                    // Success
                    return callback(null, user);
                });
            });
        }
));

passport.use(new OAuth2Strategy({
    authorizationURL: configConst.URL_LAB_FIWARE_OAUTH2_AUTHORIZE,
    tokenURL: configConst.URL_LAB_FIWARE_OAUTH2_TOKEN,
    clientID: configConst.URL_LAB_FIWARE_ID,
    clientSecret: configConst.URL_LAB_FIWARE_ID_SECRET,
    callbackURL: configConst.URL_LAB_FIWARE_CALLBACK
}, function (accessToken, refreshToken, profile, done) {
    console.log("profile:" + JSON.stringify(profile));
    /*User.findOrCreate({ username: profile.id }, function (err, user) {
     console.log("creating new user");
     return done(err, user);
     Token.findOrCreate({userId: user._id}, function(err, token){
     token.value=accessToken;
     });
     
     });*/
}));

passport.use(new BearerStrategy(
        function (accessToken, callback) {
            //console.log(accessToken);
            Token.findOne({
                value: accessToken
            }, function (err, token) {
                if (err) {
                    return callback(err);
                }

                // No token found
                if (!token) {
                    return callback(null, false);
                }

                User.findOne({
                    _id: token.userId
                }, function (err, user) {
                    if (err) {
                        return callback(err);
                    }

                    // No user found
                    if (!user) {
                        return callback(null, false);
                    }

                    try {
                        console.log(JSON.stringify(user.username));
                        User.findOneAndUpdate({"_id": user._id}, {$set: {'last_auth': Date.now()}}, function (err, u) {
                        });
                    } catch (e) {

                    }

                    // Simple example with no scope
                    callback(null, user, {
                        scope: '*'
                    });
                });
            });
        }
));

///* GET home page. */
//router.get('/anonymous', function (req, res, next) {//TODO not in use
//
//    User.findOne({
//        username: "anonymous"
//    }, function (err, user) {
//        if (err) {
//            res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
//            //return done(err);
//        }
//        //No user was found... so create a new user with values from Facebook (all the profile. stuff)
//        if (!user) {
//            console.log("creating new user");
//            user = new User({
//                username: "anonymous",
//                provider: 'local',
//            });
//            user.save(function (err) {
//                if (err) {
//                    console.log(err);
//                    res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
//                }
//                //return done(err, user);
//            });
//        }
//        Token.findOne({
//            userId: user._id
//        }, function (err, token) {
//            if (err) {
//                console.log(err);
//                res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
//                //return done(err);
//            }
//            //No token was found...
//            if (!token) {
//                console.log("creating new token");
//                token = new Token({
//                    userId: user._id,
//                    value: "abcabc"
//                });
//                token.save(function (err) {
//                    if (err) {
//                        console.log(err);
//                        res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
//                    } else {
//                        res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=' + "abcabc");
//                    }
//                    //return done(err, user);
//                });
//            } else {
//                token.value = "abcabc";
//                token.save(function (err) {
//                    if (err) {
//                        console.log(err);
//                        res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
//                    } else {
//                        res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=' + "abcabc");
//                    }
//                    //return done(err, user);
//                });
//            }
//        });
//
//    });
//
//});

function BasicTokenResponse(user, callback) {
    console.log("weee");
    require('crypto').randomBytes(48, function (err, buffer) {
        var tokenstring = buffer.toString('hex');
        Token.findOne({
            userId: user._id
        }, function (err, token) {
            if (err) {
                console.log(err);
                return done(err);
            }
            //No token was found...
            if (!token) {
                console.log("creating new token");
                token = new Token({
                    userId: user._id,
                    value: tokenstring
                });
                token.save(function (err) {
                    if (err) {
                        console.log(err);
                        callback({
                            'err': err
                        });
                    } else {
                        callback({
                            'token': tokenstring
                        });
                    }
                });
            } else {
                token.value = tokenstring;
                token.save(function (err) {
                    if (err) {
                        console.log(err);
                        callback({
                            'err': err
                        });
                    } else {
                        callback({
                            'token': tokenstring
                        });
                    }
                });
            }
        });

    });

}

router.get('/basic', passport.authenticate('basic', {session: false}), function (req, res) {
    if (!req.user) {
        res.status(400);
        res.header('Cazast_error', 'error_invalid_user');
        res.send('error_invalid_user');
        return;
    }

    BasicTokenResponse(req.user, function (json) {
        if (json && json.token) {
            json.hasEmail = (req.user.email ? true : false);
        }
        res.json(json);
    });


    try {
        User.findOneAndUpdate({"_id": req.user._id}, {$set: {'accept_language': req.headers["accept-language"]}}, function (err, u) {
        });
    } catch (ignored) {
    }
}
);

router.get('/basicAdmin', passport.authenticate('basic', {
    session: false
}),
        function (req, res) {
            if (!req.user) {
                res.status(400);
                res.header('Cazast_error', 'error_invalid_user');
                res.send('error_invalid_user');
                return;
            } else if (!req.user.admin && !req.user.superAdmin) {
                res.status(400);
                res.header('Cazast_error', 'error_invalid_admin');
                res.send('error_invalid_admin');
                return;
            }

            BasicTokenResponse(req.user, function (json) {
                if (json.token) {
                    json.admin = req.user.admin;
                    json.superAdmin = req.user.superAdmin;
                    json.adminObservatories = req.user.adminObservatories;
                }
                console.log(json);
                res.json(json);
            });
            try {
                User.findOneAndUpdate({"_id": req.user._id}, {$set: {'accept_language': req.headers["accept-language"]}}, function (err, u) {
                });
            } catch (ignored) {
            }
        }
);

//router.get('/user',
//        passport.authenticate('bearer', {//TODO Not in use
//            session: false
//        }),
//        function (req, res) {
//            res.json(req.user);
//        });

//router.get('/fiware', passport.authenticate('oauth2'));//TODO Not in use

router.get('/callback', function (req, res, next) {
    console.log("callback");
    var request = require('request');
    var querystring = require('querystring');

    console.log("posting for token:" + req.query.code);
    var auth_code = base64.encode(configConst.URL_LAB_FIWARE_ID + ':' + configConst.URL_LAB_FIWARE_ID_SECRET);
    var form = {
        grant_type: 'authorization_code',
        code: req.query.code,
        redirect_uri: configConst.URL_LAB_FIWARE_CALLBACK
    }
    var formData = querystring.stringify(form);
    console.log("sending: " + formData);
    var contentLength = formData.length;
    request({
        headers: {
            'Content-Length': contentLength,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + auth_code
        },
        uri: configConst.URL_LAB_FIWARE_OAUTH2_TOKEN,
        body: formData,
        method: 'POST'
    }, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            console.log(body)
            var accessToken = JSON.parse(body).access_token;

            request({
                uri: configConst.URL_LAB_FIWARE_USER + 'access_token=' + accessToken,
                method: 'GET'
            },
                    function (error, response, body) {
                        if (!error && response.statusCode == 200) {
                            console.log("profile:" + body);
                            var profile = JSON.parse(body);
                            User.findOne({
                                username: profile.id
                            }, function (err, user) {
                                if (err) {
                                    res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
                                    //return done(err);
                                }
                                //No user was found... so create a new user with values from Facebook (all the profile. stuff)
                                if (!user) {
                                    console.log("creating new user");
                                    user = new User({
                                        username: profile.id,
                                        provider: 'fiware',
                                    });
                                    user.save(function (err) {
                                        if (err) {
                                            console.log(err);
                                            res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
                                        }
                                        //return done(err, user);
                                    });
                                }
                                Token.findOne({
                                    userId: user._id
                                }, function (err, token) {
                                    if (err) {
                                        console.log(err);
                                        res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
                                        //return done(err);
                                    }
                                    //No token was found...
                                    if (!token) {
                                        console.log("creating new token");
                                        token = new Token({
                                            userId: user._id,
                                            value: accessToken
                                        });
                                        token.save(function (err) {
                                            if (err) {
                                                console.log(err);
                                                res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
                                            } else {
                                                res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=' + accessToken);
                                            }
                                            //return done(err, user);
                                        });
                                    } else {
                                        token.value = accessToken;
                                        token.save(function (err) {
                                            if (err) {
                                                console.log(err);
                                                res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=error');
                                            } else {
                                                res.redirect(configConst.URL_LAB_FIWARE_FRONTEND + '/?token=' + accessToken);
                                            }
                                            //return done(err, user);
                                        });
                                    }
                                });

                            });
                        } else {
                            console.log("error" + error + " status:" + response.statusCode);
                            console.log("body:" + body);
                        }
                    });

        } else {
            console.log("error" + error + " status:" + response.statusCode);
            console.log("body:" + body);
        }
    });
    //res.send('hello' );*/
}

);
router.get('/facebook',
        passport.authenticate('facebook'));//TODO Not in use

router.get('/facebook/token',
        passport.authenticate('facebook-token'),
        function (req, res) {
            console.log('/facebook/token');
            if (req.user) {
                res.status(200);
                Token.findOne({
                    userId: req.user._id
                }).exec().then(function (t) {
                    res.json({
                        'token': t.value,
                        'hasEmail': (req.user.email ? true : false)
                    });
                });

                try {
                    User.findOneAndUpdate({"_id": req.user._id}, {$set: {'accept_language': req.headers["accept-language"]}}, function (err, u) {
                    });
                } catch (ignored) {
                }
            } else {
                res.status(400);
                res.header('Cazast_error', 'error_accesstoken_invalid');
                res.send('error_accesstoken_invalid');
            }
        });

router.get('/facebook/callback',
        passport.authenticate('facebook', {
            failureRedirect: '/login'
        }),
        function (req, res) {
            // Successful authentication, redirect home. givme a token!
            //res.redirect('/#main');
            console.log("redirecting" + JSON.stringify(req.user));
            Token.findOne({
                userId: req.user._id
            }).exec().then(function (t) {
                res.redirect("" + '/?token=' + t.value);
            });
        });

//TODO https://www.npmjs.com/package/passport-facebook-token#authenticate-requests 
exports.isAuthenticated = passport.authenticate(['basic', 'oauth2'], {
    session: false
});
exports.isBearerAuthenticated = passport.authenticate('bearer', {
    session: false
});
exports.r = router;
