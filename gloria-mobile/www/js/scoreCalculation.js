/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function getPoints(time, metadata) {
    metadata.PROBA = parseFloat(metadata.PROBA);
    metadata.SCALE = parseFloat(metadata.SCALE);
    metadata.FWHM = parseFloat(metadata.FWHM);

    var p_tiempo = calculateProTime(time);
    var p_prob = 100 - parseFloat(metadata.PROBA);
    var p_brillo = calculateProBright(19);
    var p_turb = calculateProFWHM(metadata.SCALE, metadata.FWHM);
    var p_ord = calculateProOrd(0);
    var p = 0.4 * p_tiempo + 0.1 * p_prob + 0.1 * p_brillo + 0.1 * p_turb + 0.3 * p_ord;

    for (var key in metadata) {
        if (key.includes("PROBA") || key.includes("SCALE") || key.includes("FWH")) {
            console.log(key + " " + metadata[key]);
        }
    }
//    console.log('Valores');
//    console.log('Time: ' + time / 1000 / 60);
//    console.log('PROBA: ' + metadata.PROBA);
//    console.log('SCALE: ' + metadata.SCALE);
//    console.log('FWHM: ' + metadata.FWHM);
//    console.log('FWHM*SCALE: ' + metadata.FWHM * metadata.SCALE);
//    var fixSize = 5;
//    console.log('Probabilidad ' + p.toFixed(fixSize));
//    console.log('   p_tiempo: ' + p_tiempo.toFixed(fixSize) + ' ValorFinal: ' + (0.4 * p_tiempo).toFixed(fixSize));
//    console.log('   p_prob: ' + p_prob.toFixed(fixSize) + ' ValorFinal: ' + (0.1 * p_prob).toFixed(fixSize));
//    console.log('   p_brillo: ' + p_brillo.toFixed(fixSize) + ' ValorFinal: ' + (0.1 * p_brillo).toFixed(fixSize));
//    console.log('   p_turb: ' + p_turb.toFixed(fixSize) + ' ValorFinal: ' + (0.1 * p_turb).toFixed(fixSize));
//    console.log('   p_ord: ' + p_ord.toFixed(fixSize) + ' ValorFinal: ' + (0.3 * p_ord).toFixed(fixSize));
    return p;
}
/**
 * 
 * @param {integer} time -> millisecons
 * @returns {Number}
 */
function calculateProTime(time) {
    if (isNaN(time))
    {
        console.warn("calculateProTime: time is NaN");
        return 100;
    }

    var sigma = 2.1;
    var x = time / 1000 / 60;
    return 526 * (Math.pow(Math.E, -((Math.pow(x, 2)) / (2 * Math.pow(sigma, 2))))) / (sigma * Math.sqrt(2 * Math.PI));
}
/**
 * 
 * @param {type} bright
 * @returns {Number}
 */
function calculateProBright(bright) {
    if (isNaN(bright))
    {
        console.warn("calculateProBright: bright is NaN");
        return 100;
    }

    var b = 0.6;
    return 100 * (1 / (1 + (Math.pow(Math.E, -(b * (bright - 15))))));
}
/**
 * 
 * @param {type} scale -> Resolution
 * @param {type} fwhm -> FWHM
 * @returns {Number}
 */
function calculateProFWHM(scale, fwhm) {
    if (isNaN(scale))
    {
        console.warn("calculateProFWHM: scale is NaN");
        return 100;
    }
    if (isNaN(fwhm))
    {
        console.warn("calculateProFWHM: fwhm is NaN");
        return 100;
    }
    return 100 * (1 / (1 + (Math.pow(Math.E, -(scale * fwhm) + 1))));
}

function calculateProOrd(ord) {
    if (isNaN(ord))
    {
        console.warn("calculateProOrd: ord is NaN");
        return 100;
    }

    var sigma = 0.399;
    var x = ord / 10;
    return 100 * (Math.pow(Math.E, -((Math.pow(x, 2)) / (2 * Math.pow(sigma, 2))))) / (sigma * Math.sqrt(2 * Math.PI));
}
