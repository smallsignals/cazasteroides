toolbox.controller('CazasteroidesController', function ($scope, $location, $route, $rootScope) {

    $scope.login = {};
    $scope.login.email = '';
    $scope.login.password = '';
    var id_image = "";
    var id_rect = "";
    var clock_time;
    function isDateInRage(startDate, endDate, dateToCheck) {
        return dateToCheck >= startDate && dateToCheck <= endDate;
    }

    function addhttp(url) {
        if (!/^(f|ht)tps?:\/\//i.test(url)) {
            url = "http://" + url;
        }
        return url;
    }

    function loadBrowserAnalytics() {
//console.log("loadBrowserAnalytics");
        try {
            if (!window.ga) {
                $.getScript("https://www.google-analytics.com/analytics.js", function (data, textStatus, jqxhr) {
                    if (ga) {
                        ga('create', 'UA-96370001-2', 'auto');
                    }
                });
            }
        } catch (e) {
            console.log("Error", e);
        }
    }

    function updateMenuContests() {
        if (!$rootScope.allContests || $rootScope.allContests.length <= 0)
            return;
        $("#sidemenu_contest").remove();
        $("#sidelistview").append('<li data-role="list-divider" role="heading" class="ui-li ui-li-divider ui-bar-b languagespecificHTML" id="sidemenu_contests" >Contests</li>');
        for (var i = 0; i < $rootScope.allContests.length; i++) {
            $("#menu_contest_" + $rootScope.allContests[i]._id).remove();
            $("#sidelistview").append('<li data-icon="false" id="menu_contest_' + $rootScope.allContests[i]._id + '"><i class="fa fa-circle">  </i><a data-ajax="false" href="#/cazast-competition/' + $rootScope.allContests[i].name + '" class="ui-btn" >' + $rootScope.allContests[i].name + '</a></li>');
        }
    }

    function loadContests() {
        if (!$rootScope.allContests || $rootScope.allContests.length <= 0) {
//api = new Api();
            api.contestAll(function (response) {
                $rootScope.allContests = response;
                updateMenuContests();
            });
        } else {
            updateMenuContests();
        }
    }


    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function initlogin() {
        $scope.inLoginProcess = false;
        function successLogin(data, isAutoLogin) {
            //console.log("Login success: " + JSON.stringify(data));
            if (!data.hasEmail) {
//UpdateEmail!
                if (!isAutoLogin) {
                    dialog.prompt({
                        title: getLanguageValue("dialog_email_required_title"), //"Email required",
                        message: getLanguageValue("dialog_email_required_message"), //"Debes actualizar el email para poder hacer uso de la app",
                        button: getLanguageValue("dialog_email_required_button"), //"Actualizar Email",
                        required: false,
                        input: {
                            type: "email",
                            placeholder: getLanguageValue("dialog_email_required_placeholder")//"Type your email..."
                        }, validate: function (value) {
                            if (!validateEmail(value)) {
                                return false;
                            }
                        }, callback: function (value) {
                            if (validateEmail(value)) {
                                //console.log("New Email: " + value);
                                SERVER_TOKEN = data.token;
                                console.log("Token Updated [" + SERVER_TOKEN + "]");
                                localStorage.isLogin = 'true';
                                $location.path('/cazast-main');
                                $scope.$apply();
                                new Api().updateEmail(value);
                            }
                        }
                    });
                }
            } else {
                SERVER_TOKEN = data.token;
                console.log("Token Updated [" + SERVER_TOKEN + "]");
                localStorage.isLogin = 'true';
                $location.path('/cazast-main');
                $scope.$apply();
            }
        }

//localStorage.isLogin = null;
        initApi();
        api.pushDelete(SERVER_TOKEN, PUSH_TOKEN);
        SERVER_TOKEN = '';
        console.log("Token Updated [" + SERVER_TOKEN + "]");
        $("#facebook_button").click(function () {

            var fbLoginSuccess = function (userData) {
                facebookConnectPlugin.getAccessToken(function (token) {
//console.log("Token: " + token);
                    loginServerFacebook($scope, token, successLogin, function (jqXHR) {
//console.log(JSON.stringify(jqXHR));
                        alert(getLanguageValue("server_error"));
                    });
                });
            };
            if (facebookConnectPlugin)
                facebookConnectPlugin.login(["email,public_profile"], fbLoginSuccess,
                        function (error) {
                            console.error(JSON.stringify(error)); //TODO show message error
                            //alert(getLanguageValue("facebook_error_login_installed_app"));
                        }
                );
            return false;
        });
        $("#gosignup_button").click(function (e) {
            e.preventDefault();
            $location.url("/cazast-signup");
            $scope.$apply();
            return false;
        });
        $("#login_button").click(function (event) {
            event.preventDefault();
//            if ($scope.inLoginProcess)
//                return;

            var user = $("#user_login_txt").val();
            var pass = $("#pass_login_txt").val();
            if (!user || !pass) {
                alert(getLanguageValue("required_user_password"));
                return false;
            }

            loginServer($scope, user, pass, successLogin, function (jqXHR) {
//console.log("Login error");
                parseServerError(jqXHR);
                if (jqXHR.status == 401) {
                    alert(getLanguageValue("invalid_user_password"));
                } else {
                    alert(getLanguageValue("server_error"));
                }
            });
            return false;
        }); //endlogin button   


        if (localStorage.isLogin === 'true') {
            setTimeout(function () {
//Auto Facebook Login
                autoLogin($scope, function (data) {
                    successLogin(data, true);
                }, null);
            }, 200);
        }
    }

    function initsignup() {
        $("#signup_btton").click(function (event) {
            event.preventDefault();
            //console.log("Send");
            var username = $('#username_txt').val();
            var pass = $('#password_txt').val();
            var email = $('#email_txt').val();
            if (!username || !pass || !email) {
                alert(getLanguageValue("all_fields_required"));
                return false;
            }

            if (!validateEmail(email)) {
                alert(getLanguageValue("enter_valid_email"));
                return false;
            }

            if (/^[a-zA-Z0-9- ]*$/.test(username) == false) {
                alert(getLanguageValue("valide_username_error_msg"));
            } else {
//                var userData = {//TODO hide this datas
//                    "username": username, //$("#username_txt").val(),
//                    "password": pass, //$("#password_txt").val(),
//                    "email": email//$("#email_txt").val()
//                };
//console.log(userData);
                $.ajax({
                    url: SERVER_URL_BASE + '/users',
                    type: 'POST',
                    //data: JSON.stringify(userData),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    timeout: REQUEST_TIMEOUT,
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + pass + ":" + email));
                    },
                    success: function (data) {
                        //$.mobile.navigate("#login");
                        if (data.message) {
                            if (data.message == "user added") {
                                loginServer($scope, username, pass, function (data) {
                                    //console.log("Login success");
                                    SERVER_TOKEN = data.token;
                                    console.log("Token Updated [" + SERVER_TOKEN + "]");
                                    localStorage.isLogin = 'true';
                                    $location.path('/cazast-main');
                                    $scope.$apply();
                                }, function (jqXHR) {
                                    //console.log("Login error");
                                    parseServerError(jqXHR);
                                    //console.log(JSON.stringify(jqXHR));
                                    if (jqXHR.status == 401) {
                                        //console.log('Invalid user or password');
                                        alert(getLanguageValue("invalid_user_password"));
                                    } else {
                                        //console.log("Error connecting to server");
                                        alert(getLanguageValue("server_error"));
                                    }
                                });
                            } else {
                                alert(data);
                            }
                        } else if (data.errmsg) {
                            if (data.errmsg.includes("duplicate")) {
                                if (data.errmsg.includes("username_1")) {
                                    alert(getLanguageValue("duplicate_username"));
                                } else {
                                    alert(getLanguageValue("duplicate_email"));
                                }
                            } else {
                                alert(data.errmsg);
                            }
                        } else {
                            alert(data);
                        }

                    },
                    error: function (jqXHR) {
                        parseServerError(jqXHR);
                        //console.log("Error connecting to server");
                        alert(getLanguageValue("server_error"));
                    },
                    failure: function (errMsg) {
                        //console.log(errMsg);
                        alert(errMsg);
                    }
                });
            }
            return false;
        });
    }

    function initforgot() {
        $("#send_forgot_btton").click(function () {

            var email = $('#email_forgot_txt').val();
            if (!email) {
                alert(getLanguageValue("all_fields_required"));
                return false;
            }

            var userData = {
                "email": $("#email_forgot_txt").val(),
                "lang": cLANGUAGE
            };
            $("send_forgot_btton").attr("disabled", true);
//console.log(userData);
            $.ajax({
                url: SERVER_URL_BASE + '/users/forgot',
                type: 'POST',
                data: JSON.stringify(userData),
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                timeout: REQUEST_TIMEOUT,
                success: function (data) {
                    if (data.res) {
                        if (data.res == "ok") {
                            alert(getLanguageValue("message_sent"));
                            $location.url("/cazast-login");
                            $("send_forgot_btton").removeAttr("disabled");
                            $scope.$apply();
                        } else {
                            alert(data);
                        }
                    }
                },
                error: function (jqXHR) {
                    parseServerError(jqXHR);
                    alert(getLanguageValue("server_error"));
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
            return false;
        });
    }

    function setInfowindowPosition(event, content) {
        if (!event) {
            $("#infowindow")
                    .css({
                        top: 50 + "px",
                        left: 10 + "px"
                    });
        } else {
            var nX = (event.clientX || event.changedTouches[0].pageX),
                    nY = (event.clientY || event.changedTouches[0].pageY);
            var yContentClick = (nY - ($(window).height() - content));
//            console.log("Content: " + $(window).width() + "x" + content);
//            console.log("Click: " + nX + "x" + yContentClick);
//            console.log("Info: " + $("#infowindow").width() + "x" + $("#infowindow").height());

            var margin = 25;
            if (nX < $("#infowindow").width() + 10 + margin
                    && yContentClick < $("#infowindow").height() + 50 + margin) {
//                console.log("is hidi");

                if (nX + 10 + margin + $("#infowindow").width() < $(window).width()) {
                    $("#infowindow")
                            .css({
                                top: 50 + "px",
                                left: nX + 10 + margin + "px"
                            });
                } else if (yContentClick + 50 + margin + $("#infowindow").height() < content) {
                    $("#infowindow")
                            .css({
                                top: yContentClick + 50 + margin + "px",
                                left: 10 + "px"
                            });
                } else {
                    $("#infowindow")
                            .css({
                                top: 50 + "px",
                                left: 10 + "px"
                            });
                }
            } else {
                $("#infowindow")
                        .css({
                            top: 50 + "px",
                            left: 10 + "px"
                        });
            }
        }
    }

    function initmain() {
        api = new Api();
        api.getSetupValue('res_image_end', function (value) {
            set_res_image_end(value);
        });
        searchButtonAsSpinner = function () {
            if ($("#header-search-icon")) {
                $("#header-search-icon").attr("class", "fa fa-spinner fa-spin");
            }
            if ($("#header-search-txt")) {
                $("#header-search-txt").html("");
            }
        };
        searchButtonAsSend = function () {
            if ($("#header-search-icon")) {
                $("#header-search-icon").attr("class", "fa fa-send");
            }
            if ($("#header-search-txt")) {
                $("#header-search-txt").html(getLanguageValue("header-search-txt"));
            }
        };
        hideInfoWindow = function () {
            $('#infowindow').hide();
        };
        showInfoWindow = function (html) {
            $('#infowindow').html(html);
            $('#infowindow').show();
        };
// code to execute on each page change
        $("#sidebar").panel();
        $("#sidelistview").listview();
        hideInfoWindow();
        $('#tv_link').hide();
        $('#tv_link2').hide();
        $('#fields_link2').hide();
        $('#review_link').hide();
        var screen = $(window).height();
        var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight() - 1 : $(".ui-header").outerHeight();
        var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();
        var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
        var content_info = $(".main-info").outerHeight() ? $(".main-info").outerHeight() : 0;
        var content = screen - header - footer - contentCurrent - content_info;
        $("#animation").height(content - 15);
        $(".main-image").height(content);
        var $zoom = undefined;
        startChallenge = function (image) {
            set_img_size($(window).width(), content);
            set_map_file(image.mapfile);
            var rect = change_img();
            id_rect = "";
            api.newRect(rect, image._id, function (data) {
                initialize_images();
                $zoom = $('.zoom').magnify();
                $('.magnify').on('click', function (event) {
                    if (!$('#infowindow').is(":visible")) {
                        checkstar(pixcoord, pixcoordabs, coord);
                        if (event.clientX && event.clientY)
                            setInfowindowPosition(event, content);
                    }

                });
                id_rect = data._id;
                updateGUIelements($(window).width() / 2, $(window).height() / 2); //400,20
                $("#nuevo_reto_button_2").show();
            });
        };
        getLastImageData = function () {

            api.getLastImageData(function (image) {
                if (!image) {
                    //TODO show alert message
                    return;
                }
                $("#nuevo_reto_button_2").hide();
                api.metadata = new Array(5);
                Math.seedrandom();
                api.quintentMaster = Math.floor(Math.random() * api.metadata.length);
                console.log("Quintet: " + api.quintentMaster);
                id_image = image._id;
                //console.log('current Image Datas: ' + image.width + 'x' + image.height);
                currendfield = image.name;
                //api.getHistogram(id_image, function () {
                var totalResponses = 0;
                for (var i = 4; i >= 0; i--) {
                    api.getMetadata(id_image, i, function (data, index) {
                        var sig = 5;
                        set_img_scale(parseFloat(data.SKY) - (sig * parseFloat(data.SIGMA)), parseFloat(data.SKY) + (sig * parseFloat(data.SIGMA)), index);
                        totalResponses++;
                        if (totalResponses === 5) {
                            startChallenge(image);
                        }
                    });
                }
                //});
            });
        };
        clock_time = new Stopwatch($("#clock")[0]);
        clock_time.start();
        $("#nuevo_reto_2").click(function () { //FIXME este botón se ha eliminado
            hideInfoWindow();
            getLastImageData();
            $("#sidebar").panel("close");
            clock_time.reset();
            clock_time.set_alarm(2000, function () {
                $("#nuevo_reto_button_2").show();
            });
            return false;
        });
        document.getElementById('magnify-container').addEventListener('touchend', function (e) {
// prevent delay and simulated mouse events
            e.preventDefault();
            if (!$('#infowindow').is(":visible")) {
                e.target.click();
                setInfowindowPosition(e, content);
            }
        });
        var draggable = document.getElementById('draggable');
        if (draggable) {
            draggable.addEventListener('touchstart', function (e) {
                e.preventDefault();
            });
            draggable.addEventListener('touchmove', function (e) {
                e.preventDefault();
            });
        }

        pointSend = function (pixcoord0, pixcoord1, pixcoordabs0, pixcoordabs1, coord0, coord1) {
            var coord = [coord0, coord1];
            var pixcoord = [pixcoord0, pixcoord1];
            var pixcoordabs = [pixcoordabs0, pixcoordabs1];
            var currentTime = clock_time.get_clock();
            var pts = getPoints(currentTime, api.getMetadataQuintet(0));
            api.newObservation(id_image, id_rect,
                    pixcoord, pixcoordabs, coord, pts, currentTime, api.getMetadataQuintet(0), api.quintentMaster,
                    function (data) {
                        hideInfoWindow();
                        //TODO check if exist firstOwnDetection to true
                        if (!data.firstOwnDetection) {
                            var html = "<p>" + getLanguageValue("detection_previously_detected") + "</p>";
                            $('#infowindow').html(html);
                            $("#infowindow").show().delay(3500).fadeOut();
                            setInfowindowPosition(null);
                        } else {//TODO mostrar texto cuando la deteccion ya es un candidato
                            if (data.status) {
                                switch (data.status) {
                                    case 'rejected':
                                        var html = "<p>" + getLanguageValue("detection_previously_rejected") + "</p>";
                                        if (data.rejectedCode) {
                                            html += "<p>" + getLanguageValue("rejected_reason_") + " " + getLanguageValue("rejected_code_" + data.rejectedCode, "rejected_no_code") + "</p>";
                                        }
                                        $('#infowindow').html(html);
                                        $("#infowindow").show().delay(3500).fadeOut();
                                        setInfowindowPosition(null);
                                        break;
                                    case 'approved':
                                        var html = "<p>" + getLanguageValue("detection_previously_approved") + "</p>";
                                        if (data.object && data.object.mpcDesignation) {
                                            if (data.object.mpcDesignation) {
                                                html += ' <p>'
                                                        + getLanguageValue("detection_objec_") + " "
                                                        + '<a target="_blank" href="https://ssd.jpl.nasa.gov/sbdb.cgi?name=' + data.object.mpcDesignation + '"><font color="4137A4">' + data.object.mpcDesignation + '</font></a>' + ' ' + getLanguageValue("detection_known")
                                                        + '</p>';
                                            } else {
                                                html += ' <p>'
                                                        + getLanguageValue("detection_objec_") + " " + getLanguageValue("detection_candidate")
                                                        + '</p>';
                                            }
                                        }
                                        $('#infowindow').html(html);
                                        $("#infowindow").show().delay(3500).fadeOut();
                                        setInfowindowPosition(null);
                                        break;
                                }
                            } else {
//                                if (data.points)
//                                    pts = data.points;
                                var totalDetect = '';
                                if (data.totalDetect) {
                                    totalDetect = '<p>' + replaceTokens(getLanguageValue("detection_orden_message")
                                            , ['ord']
                                            , [ordinal_suffix_of(data.totalDetect, getLanguageValue("language"))]
                                            ) + '</p>';
                                }

                                var html = "<p>" + getLanguageValue("thanks") + "</p>"
                                        + totalDetect;
                                //+"<p>" + getLanguageValue("you_will_earn_+") + "" + pts.toFixed(2) + getLanguageValue("points_if_observation_confirmed") + "</p>";
                                $('#infowindow').html(html);
                                $("#infowindow").show().delay(3500).fadeOut();
                                setInfowindowPosition(null);
                            }
                        }
                    });
        };
        noSend = function () {
            hideInfoWindow();
            searchButtonAsSend();
        };
        logOut = function () {
            api.pushDelete(SERVER_TOKEN, PUSH_TOKEN);
            $rootScope.allContests = [];
            localStorage.isLogin = null;
            $location.path('/cazast-login');
            $scope.$apply();
            if (facebookConnectPlugin)
                facebookConnectPlugin.logout();
            if (window.FirebasePlugin) {
                window.FirebasePlugin.unregister();
            }
        };
        function checkstar(pixcoord, pixcoordabs, coord) {
            /**
             * Evitar comprobar si ya se esta comprobando
             */
            if ($('#infowindow').is(":visible")) {
                return;
            }

            searchButtonAsSend();
            var html = '<p>' + getLanguageValue("new_detection") + '</p>'
                    + '<p style="margin-top:10px">' + getLanguageValue("want_to_report_candidate") + '</p> '
                    + '<div class="btn-group btn-group-lg">'
                    + ' <button type="button" class="btn" style="margin:5px;min-width:74px" onclick="pointSend(' + pixcoord[0] + ',' + pixcoord[1] + ',' + pixcoordabs[0] + ',' + pixcoordabs[1] + ',' + coord[0] + ',' + coord[1] + ')">' + getLanguageValue("yes") + '</button>'
                    + ' <button type="button" class="btn" style="margin:5px;min-width:74px" onclick="noSend()">' + getLanguageValue("no") + '</button>'
                    + '</div>';
            showInfoWindow(html);
        }
        ;
        getLastImageData();
        loadContests();
    }

    function initpremios() {
        var skip = 0;
        var limit = 200;

        var isLoading = false;
        $scope.award_loading = true;
        $scope.award_error = false;
        $scope.award_empty = false;
        initApi();
        function generateHtml(position, user) {
            switch (position) {
                case 0:
                    var html = '<div class="premios_winner" style="padding:8px;">';
                    //html = html + '<H1> Winner </H1>';
                    html = html + '<img src=\"images/winnercup_.png\" width=100 heigh=100 style=\"width:100px\" />';
                    html = html + '<H2>1. ' + user.username + '</H2>';
                    html = html + ' ' + (user.points ? user.points.toFixed(0) : 0) + ' Pts</p>';
                    html = html + '</div>';
                    return html;
                case 1:
                    var html =
                            '<div style="padding:8px;">'
//                                    + ' <table style="margin: 0 auto;">'
                            + '     <tr>'
                            + '         <th align="right">&nbsp;&nbsp;<img src="images/secondcup.png" width=40 heigh=40 style="width:40px"/>&nbsp;&nbsp;</td>'
                            + '         <th align="right">&nbsp;&nbsp;' + (position + 1) + '.&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.username) + '&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.points ? user.points.toFixed(0) : 0) + '&nbsp;&nbsp;</td>'
                            + '     </tr>'
//                                    + ' </table>'
                            + '</div>'
                            ;
                    return html;
                case 2:
                    var html =
                            '<div style="padding:8px;">'
//                                    + ' <table style="margin: 0 auto;">'
                            + '     <tr>'
                            + '         <th align="right">&nbsp;&nbsp;<img src="images/third_cup.png" width=32 heigh=32 style="width:32px" />&nbsp;&nbsp;</td>'
                            + '         <th align="right">&nbsp;&nbsp;' + (position + 1) + '.&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.username) + '&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.points ? user.points.toFixed(0) : 0) + '&nbsp;&nbsp;</td>'
                            + '     </tr>'
//                                    + ' </table>'
                            + '</div>'
                            ;
                    return html;
                default:
                    var html =
                            '<div style="padding:8px;">'
//                                    + ' <table style="margin: 0 auto;">'
                            + '     <tr>'
                            + '         <th align="right">&nbsp;&nbsp;' + '&nbsp;&nbsp;</td>'
                            + '         <th align="right">&nbsp;&nbsp;' + (position + 1) + '.&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.username) + '&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.points ? user.points.toFixed(0) : 0) + '&nbsp;&nbsp;</td>'
                            + '     </tr>'
//                                    + ' </table>'
                            + '</div>'
                            ;
                    return html;
            }
        }

        function addToHTMLList(data) {
            $("#premios_load_more").remove();

            for (i = 0; i < data.length; i++) {
                $("#premioslist").append(generateHtml(i + skip, data[i]));
            }
// $("#premioslist").html(html);

            if (data.length == limit) {
                $('#premioslist').append(
                        //'   <div id="premios_load_more_div" class="luke-text-box row" style="padding-bottom:5px;padding-top:5px;">'
                        '   <button type="button" class="btn" style="margin:5px;min-width:80px"  id="premios_load_more">' + getLanguageValue("load_more") + '</button>'
                        //+ '</div>'
                        );
                $('#premios_load_more').click(function () {
                    callServer(false);
                });
            }

        }

        function callServer(clearPrevious) {
            if (!clearPrevious && isLoading)
                return;
            if (clearPrevious) {
                skip = 0;
            }

            isLoading = true;
            api.getUsers({skip: skip, limit: limit}, function (data) {
                $scope.award_loading = false;
                $scope.award_error = false;
                if (clearPrevious && (!data || data.length <= 0)) {
                    $scope.award_empty = true;
                }
                $scope.$apply();
                if (clearPrevious)
                    $('#premioslist').html('');

                isLoading = false;
                if (!data || data.length < 1) {
                    return;
                }

                addToHTMLList(data);
                skip += data.length;
            }, function () {
                $scope.award_loading = false;
                $scope.award_error = clearPrevious && true;
                $scope.award_empty = false;
                $scope.$apply();
                isLoading = false;
            });
        }


        $scope.refreshAwards = function () {
//console.log("LoadObservations");
            $scope.award_loading = true;
            $scope.award_error = false;
            $scope.award_empty = false;
            callServer(true);
        };
        $scope.refreshAwards();
    }

    function initme() {

        function appendContestView(contest/*, /*addButton,points*/) {
            var style = 'height: 60px;width: auto; border: 1px;  margin: 10px 5px 0 0;';
            var htmlImages = '<div class="container"><div class="row"><div class="col-sm-12 col-md-12">';
            if (contest.logos) {
                for (var i = 0; i < contest.logos.length; i++) {
                    htmlImages += '<img class="thumb" src="' + contest.logos[i] + '" style="' + style + '" />';
                }
            }
            htmlImages += '</div></div></div>';
            //console.log(htmlImages);

            var isInRange = isDateInRage(new Date(contest.time_start), new Date(contest.time_end), new Date());
            var isFinished = (new Date() > new Date(contest.time_end));
            var divStyle = 'padding-bottom:5px;';
            if (isInRange) {
                divStyle += 'background-color:#99cc99;';
            } else if (isFinished) {
                divStyle += 'background-color:#ffb2b2;';
            }

            var addButton = false;
            var buttonText = "contest_join";
            var canClickButton = false;
            if (!contest.registered && !isFinished) {
                addButton = true;
                canClickButton = true;
            } else if (contest.registered && !isFinished) {
                addButton = true;
                buttonText = "contest_leave"; //isInRange ? "contest_competing" : "contest_registered";
                canClickButton = true;
            }

            var html = '<div id="contest_' + contest._id + '" class="luke-text-box" style="' + divStyle + '">'
                    + '     <h5>' + contest.name + '</h5>'
                    + htmlImages
                    + '     <table style="margin: 2px auto;table-layout: fixed;">'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("start_") + '</td>'
                    + '             <td style="padding: 3px 10px;"  align="left">' + new Date(contest.time_start).toLocaleString() + '</td>'
                    + '         </tr>'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("end_") + '</td>'
                    + '             <td style="padding: 3px 10px;"  align="left">' + new Date(contest.time_end).toLocaleString() + '</td>'
                    + '         </tr>'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("contest_rules_") + '</td>'
                    + '             <td style="padding: 3px 10px;"  align="left"><a target="_blank" href="' + addhttp(contest.link) + '">' + contest.link + '</td>'
                    + '         </tr>'

                    + (!contest.points ? '' : ('         <tr>'
                            + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("points_") + '</td>'
                            + '             <td style="padding: 3px 10px;"  align="left">' + contest.points.toFixed(0) + '</td>'
                            + '         </tr>'))

                    + '     </table>'
                    + '     <div>' + contest.description + '</div>'
                    + (addButton ? '     <button type="button" class="btn" style="margin:5px;min-width:80px"  id="but_' + contest._id + '">' + getLanguageValue(buttonText) + '</button>' : '')
                    + '</div>'
                    + '<hr style="margin:1px">'
                    ;
            $("#me_next_contests").append(html);
            if (canClickButton) {
                $('#but_' + contest._id).click((function () {
                    var temp_contest = contest;
                    return function () {
                        if (temp_contest.registered) {
                            api.contestUnregister(temp_contest._id, function () {
                                //$("#but_" + temp_contest._id).remove();

                                //var isInRange = isDateInRage(new Date(temp_contest.time_start), new Date(temp_contest.time_end))(new Date());
                                var isFinished = (new Date() > new Date(temp_contest.time_end));
                                var buttonText = "contest_join";
                                if (!isFinished) {
                                    $('#but_' + temp_contest._id).text(getLanguageValue(buttonText));
                                } else {
                                    $("#but_" + temp_contest._id).remove();
                                }

                                var index = $rootScope.allContests.findIndex(function (element) {
                                    return element._id.toString() === temp_contest._id.toString();
                                });
                                if (index >= 0 && index < $rootScope.allContests.length) {
                                    $rootScope.allContests[index].registered = false;
                                }

                                temp_contest.registered = false;
                            });
                        } else {
                            api.contestRegister(temp_contest._id, function () {
                                //$("#but_" + temp_contest._id).remove();

                                //var isInRange = isDateInRage(new Date(temp_contest.time_start), new Date(temp_contest.time_end))(new Date());
                                var isFinished = (new Date() > new Date(temp_contest.time_end));
                                var buttonText = "contest_leave";
                                if (!isFinished) {
                                    $('#but_' + temp_contest._id).text(getLanguageValue(buttonText));
                                } else {
                                    $("#but_" + temp_contest._id).remove();
                                }

                                var index = $rootScope.allContests.findIndex(function (element) {
                                    return element._id.toString() === temp_contest._id.toString();
                                });
                                if (index >= 0 && index < $rootScope.allContests.length) {
                                    $rootScope.allContests[index].registered = true;
                                }

                                temp_contest.registered = true;
                            });
                        }
                    };
                })());
            }
        }

//        function addToMyContests(contest, points) {
//            if ($("#me_me_contests").text().length <= 0) {
//                var html = '<h3>' + getLanguageValue("me_competitions") + '</h3>';
//                $("#me_me_contests").append(html);
//            }
//
//            $("#contest_me_" + contest._id).remove();
//            $("#contest_b_" + contest._id).remove();
//            $("#me_me_contests").append(generateContestView(contest, false, points));
//        }

        initApi();
        api.getMe(function (me) {
            var html = '<div class="luke-text-box">'
                    + '     <img  style="width:60%;max-width: 512px;" src="' + (me.icon ? me.icon : 'img/icon-user-default.png') + '" />'
                    + '<table style="margin: 10px auto;table-layout: fixed;font-size:18px;">'
                    + '  <tr>'
                    + '     <td style="padding: 3px 10px;" align="right">' + getLanguageValue("username_") + '</td>'
                    + '     <td style="padding: 3px 10px;"  align="left">' + me.username + '</td>'
                    + '  </tr>'
                    + '  <tr>'
                    + '     <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("points_") + '</td>'
                    + '     <td style="padding: 3px 10px;"  align="left">' + (me.points ? me.points.toFixed(0) : 0) + '</td>'
                    + '  </tr>'
                    + (me.admin ? ('  <tr>'
                            + '     <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("role_") + '</td>'
                            + '     <td style="padding: 3px 10px;"  align="left">' + getLanguageValue("admin") + '</td>'
                            + '  </tr>') : '')
                    + '</table>'

                    + '     <h3>' + getLanguageValue("observations") + '</h3>'
                    + '     <p>' + (me.obs_total ? me.obs_total : (me.obs ? me.obs.length : 0)) + '</p>'
                    + '</div>';
            $("#me_content").append(html);
//            if (me.contests && me.contests.length > 0) {
//                for (var i = 0; i < me.contests.length; i++) {
//                    var contest = me.contests[i].contest;
//
//                    if (!contest || !contest._id || !contest.name || !contest.time_start || !contest.time_end || !contest.link || !contest.description)
//                        continue;
//
//                    addToMyContests(contest, me.contests[i].points);
//                }
//            }

        });
        api.contestActive(function (contests) {
            if (contests && contests.length > 0) {
                for (var i = 0; i < contests.length; i++) {

                    if (!contests[i]._id || !contests[i].name || !contests[i].time_start || !contests[i].time_end || !contests[i].link || !contests[i].description)
                        continue;
                    if ($("#me_next_contests").text().length <= 0) {
                        var html = '<h3>' + getLanguageValue("competitions") + '</h3>';
                        $("#me_next_contests").append(html);
                    }

                    appendContestView(contests[i]);
                }
            }
        });
    }

    function initvote() {
        $scope.vote_loading = true;
        $scope.vote_error = false;
        $scope.vote_empty = false;
        initApi();
        $scope.getObservationRand = function () {
            //console.log("LoadObservations");
            $scope.vote_loading = true;
            $scope.vote_error = false;
            $scope.vote_empty = false;
            api.getObservationRand(function (data) {
                $scope.vote_loading = false;
                $scope.vote_error = false;
                if (!data || data.length <= 0) {
                    $scope.vote_empty = true;
                }
                $scope.$apply();
                $('#obslist').html('');
                //console.log(data);
                for (i = 0; i < data.length; i++) {
                    var obs = data[i];
                    if (obs.image) {
                        var title = '';
                        if (obs.image.name)
                            title = obs.image.name;
                        else {
                            var lis = obs.image.filename[0].split('/');
                            title = lis[lis.length - 2];
                        }
                        var upvotes = 0;
                        var downvotes = 0;
                        if (obs.votes)
                            upvotes = obs.votes.length;
                        if (obs.downvotes)
                            downvotes = obs.downvotes.length;
                        var html = '<div class="luke-text-box" style="padding-bottom:5px">'
                                + '     <table style="margin: 2px auto;table-layout: fixed;">'
                                + '         <tr>'
                                + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue("image") + '</td>'
                                + '             <td style="padding: 3px 10px;"  align="left">' + title + '</td>'
                                + '         </tr>'
                                + '         <tr>'
                                + '             <td style="padding: 3px 10px;"  align="right">T:&nbsp;</td>'
                                + '             <td style="padding: 3px 10px;"  align="left">' + obs.image.fecha + '</td>'
                                + '         </tr>'
                                + '         <tr>'
                                + '           <td style="padding: 3px 10px;"  align="right">RA:&nbsp;</td>'
                                + '            <td style="padding: 3px 10px;"  align="left">' + deg2hour(obs.Coord[0]) + '</td>'
                                + '         </tr>'
                                + '         <tr>'
                                + '            <td style="padding: 3px 10px;"  align="right">DEC:&nbsp;</td>'
                                + '            <td style="padding: 3px 10px;"  align="left">' + deg2degminsec(obs.Coord[1]) + '</td>'
                                + '          </tr>'
                                + '          <tr id="tr_votes_' + obs._id + '" style="white-space: nowrap;">'
                                + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("votes_") + '</td>'
                                + '             <td id="total_votes_' + obs._id + '" style="padding: 3px 10px;"  align="left">' + (upvotes + downvotes) + '</td>'
                                + '          </tr>'
                                + '          </tr>'
                                + '     </table>'
                                + '     <div id="total_up_votes_' + obs._id + '"    style ="display: none; ">' + upvotes + ' </div>'
                                + '     <div id="total_down_votes_' + obs._id + '"  style ="display: none; ">' + downvotes + '</div>'
                                + '     <button type="button" class="btn" style="margin:5px;min-width:80px"  id="but_' + obs._id + '">' + getLanguageValue("see_image_and_vote") + '</button>'
                                + '     <div id="div_' + obs._id + '"></div>'
                                + '</div>'
                                + '<hr style="margin:2px">';
                        $('#obslist').append(html);
                        $('#but_' + obs._id).click((function () {
                            var temp_closure = obs._id;
                            return function () {
                                if (!$('#div_' + temp_closure).html()) {
                                    $('#but_' + temp_closure).text(getLanguageValue("hide"));
                                    var url = SERVER_URL_BASE + '/obs/get_obs_gif/' + temp_closure;//+'?nochache='+(new Date).getTime();
                                    $('#div_' + temp_closure).html('<img class="vote_img" id="img_' + temp_closure + '" src="' + url + '"/>');
                                    $('#div_' + temp_closure).append('<div id="div_but_container_' + temp_closure + ' class="btn-group btn-group-lg">');
                                    $('#div_' + temp_closure).append('<button type="button" class="btn" style="margin:5px;min-width:80px" id="but_yes_' + temp_closure + '">' + getLanguageValue("yes") + '</button>');
                                    $('#div_' + temp_closure).append('<button type="button" class="btn" style="margin:5px;min-width:80px" id="but_no_' + temp_closure + '">' + getLanguageValue("no") + '</button>');
                                    $('#div_' + temp_closure).append('</div>');
                                    $('#but_yes_' + temp_closure).click(function () {

                                        api.voteObservation(temp_closure, function (success) {
                                            if (success) {
                                                var upVotes = parseInt($('#total_up_votes_' + temp_closure).text()) + 1;
                                                var downVotes = parseInt($('#total_down_votes_' + temp_closure).text());
                                                $('#tr_votes_' + temp_closure).html(
                                                        '   <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("votes_") + '</td>'
                                                        + ' <td id="total_votes_' + obs._id + '" style="padding: 3px 10px;"  align="left">' + (upVotes + downVotes) + '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-plus-circle" aria-hidden="true"> ' + upVotes + '</i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-minus-circle" aria-hidden="true"> ' + downVotes + '</i></td>'
                                                        );
                                            }
                                        });
                                        $('#div_' + temp_closure).html('');
                                        $('#but_' + temp_closure).hide();
                                    });
                                    $('#but_no_' + temp_closure).click(function () {

                                        api.downvoteObservation(temp_closure, function (success) {
                                            if (success) {
                                                var upVotes = parseInt($('#total_up_votes_' + temp_closure).text());
                                                var downVotes = parseInt($('#total_down_votes_' + temp_closure).text()) + 1;
                                                $('#tr_votes_' + temp_closure).html(
                                                        '   <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("votes_") + '</td>'
                                                        + ' <td id="total_votes_' + obs._id + '" style="padding: 3px 10px;"  align="left">' + (upVotes + downVotes) + '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-plus-circle" aria-hidden="true"> ' + upVotes + '</i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-minus-circle" aria-hidden="true"> ' + downVotes + '</i></td>'
                                                        );
                                            }
                                        });
                                        $('#div_' + temp_closure).html('');
                                        $('#but_' + temp_closure).hide();
                                    });
                                } else {
                                    $('#div_' + temp_closure).html('');
                                    $('#but_' + temp_closure).text(getLanguageValue("see_image_and_vote"));
                                }
                            };
                        })());
                    }//endi fi imagen no borrada
                }
            }, function () {
                $scope.vote_loading = false;
                $scope.vote_error = true;
                $scope.vote_empty = false;
                $scope.$apply();
            });
        };
        $scope.getObservationRand();
    }

    function initMyDetections() {
        var skip = 0;
        var limit = 50;
        var isLoading = false;
        $scope.myDetections_loading = true;
        $scope.myDetections_error = false;
        $scope.myDetections_empty = false;
        initApi();
        function addToHTMLList(data) {
            $("#my_detections_load_more_div").remove();
            for (i = 0; i < data.length; i++) {
                var detection = data[i];
                try {
                    var title = detection.image.name;
                    var upvotes = detection.info.votes;
                    var downvotes = detection.info.downvotes;
                    var ord = detection.info.ord;
                    var points = detection.info.points;
                    var detections = detection.info.detections;
                    var status = detection.status;
                    var imageUrl = api.server + "/obs/get_obs_gif/" + detection._id;
                    var color = '';
                    var state = getLanguageValue("state_pending");
                    var object_info = undefined;
                    var showPoints = true;
                    var showVotes = true;
                    var isRejected = false;
                    switch (status) {
                        case 'approved':
                            color = '#99cc99';
                            state = getLanguageValue("state_approved");
                            showPoints = false;
                            showVotes = false;
                            break;
                        case 'rejected':
                            color = '#ffb2b2';
                            state = getLanguageValue("state_rejected");
                            showPoints = false;
                            showVotes = false;
                            isRejected = true;
                            break;
                    }

//                        //https://ssd.jpl.nasa.gov/sbdb.cgi?name=K14J25O
                    if (detection.object) {
                        if (detection.object.mpcDesignation) {
                            object_info = ' <tr>'
                                    + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue("detection_objec_") + '</td>'
                                    + '             <td style="padding: 3px 10px;"  align="left"><a target="_blank" href="https://ssd.jpl.nasa.gov/sbdb.cgi?name=' + detection.object.mpcDesignation + '"><font color="4137A4">' + detection.object.mpcDesignation + '</font></a>' + '<br>' + getLanguageValue("detection_known") + '</td>'
                                    + '         </tr>';
                        } else if (detection.object.cazastDesignation) {
                            object_info = ' <tr>'
                                    + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue("detection_objec_") + '</td>'
                                    + '             <td style="padding: 3px 10px;"  align="left"><font color="4137A4">' + detection.object.cazastDesignation + '</font><br>' + getLanguageValue("detection_candidate") + '</td>'
                                    + '         </tr>';
                        }
                    }

                    var html = '<div id="row_' + detection._id + '" class="luke-text-box row" style="padding-bottom:5px;padding-top:5px; background-color:' + color + ' ;">'
                            + '     <h5 class="media-heading">' + title + '</h5>'
                            + '     <div class="col-xs-5 col-sm-6 col-md-6">'
                            + '         <img src="' + imageUrl + '" class="img-responsive centered" style="max-width:200px;float: right;">'
                            + '     </div>'
                            + '     <div class="col-xs-6 col-sm-6 col-md-6">'
                            + '     <table style="margin: 2px;table-layout: fixed;">'
                            + '         <tr>'
                            + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue('detection_state_') + '</td>'
                            + '             <td style="padding: 3px 10px;"  align="left">' + state + '</td>'
                            + '         </tr>'

                            + (isRejected && detection.rejectedCode ? (
                                    '         <tr>'
                                    + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue("rejected_reason_") + '</td>'
                                    + '             <td style="padding: 3px 10px;"  align="left">' + getLanguageValue("rejected_code_" + detection.rejectedCode, "rejected_no_code") + '</td>'
                                    + '         </tr>') : '')

                            + (object_info ? (object_info) : '')
                            + '         <tr>'
                            + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue("detection_ord_position_") + '</td>'
                            + '             <td style="padding: 3px 10px;"  align="left">' + ordinal_suffix_of(ord, getLanguageValue("language")) + ' ' + getLanguageValue("detection_of_") + ' ' + detections + '</td>'
                            + '         </tr>'

                            + (showPoints ? (' <tr>'
                                    + '             <td style="padding: 3px 10px;" align="right">' + getLanguageValue("points_") + '</td>'
                                    + '             <td style="padding: 3px 10px;"  align="left">' + points.toFixed(2) + '</td>'
                                    + '         </tr>') : '')
//                                + '         <tr>'
//                                + '           <td style="padding: 3px 10px;"  align="right">RA:&nbsp;</td>'
//                                + '            <td style="padding: 3px 10px;"  align="left">' + deg2hour(detection.info.Coord[0]) + '</td>'
//                                + '         </tr>'
//                                + '         <tr>'
//                                + '            <td style="padding: 3px 10px;"  align="right">DEC:&nbsp;</td>'
//                                + '            <td style="padding: 3px 10px;"  align="left">' + deg2degminsec(detection.info.Coord[1]) + '</td>'
//                                + '          </tr>'
                            + (showVotes ? ('          <tr id="tr_votes_' + detection._id + '" style="white-space: nowrap;">'
                                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("votes_") + '</td>'
                                    + '             <td id="total_votes_' + detection._id + '" style="padding: 3px 10px;"  align="left">' + (upvotes + downvotes) + '&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-plus-circle" aria-hidden="true"> ' + upvotes + '</i>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-minus-circle" aria-hidden="true"> ' + downvotes + '</i></td>'
                                    + '          </tr>') : '')
                            + '     </table>'
                            + ' </div>'
                            + '     <div >'
                            + '         <div style="float: right;width: 45px;height: 45px;position: relative; border-radius: 100%;" type="button" class="btn" id="but_' + detection._id + '">'
                            + '             <i class="fa fa-times" aria-hidden="true" style="position: absolute;    top: 50%;    left: 50%;    height: 50%;    transform: translate(-50%, -50%);    width: 20px;    height: 20px;    display: block;"></i>'
                            + '         </div>'
                            + ((window.plugins && window.plugins.socialsharing && /Android/.test(window.navigator.userAgent)) || (navigator.share !== undefined) ?
                                    '   <div style="float: right;width: 45px;height: 45px;position: relative; border-radius: 100%;" type="button" class="btn" id="but_share_' + detection._id + '">'
                                    + '     <i class="fa fa-share-alt" aria-hidden="true" style="position: absolute;    top: 50%;    left: 50%;    height: 50%;    transform: translate(-50%, -50%);    width: 20px;    height: 20px;    display: block;"></i>'
                                    + ' </div>'
                                    : '')
                            + '     </div>'
                            /**
                             + '<a class="a2a_dd" href="https://www.addtoany.com/share">Share</a>'
                             + '<script>'
                             + 'var a2a_config = a2a_config || {};'
                             + 'a2a_config.linkname = "page name";'
                             + 'a2a_config.linkurl = "page url";'
                             + 'a2a_config.onclick = 1;'
                             + '</script>'
                             + '<script async src="https://static.addtoany.com/menu/page.js"></script>'
                             */
                            + ' </div>'
                            + '<hr id="hr_' + detection._id + '" style="margin:1px">';
                    $('#detectionlist').append(html);
                    $('#but_' + detection._id).click((function () {
                        var temp_detection = detection;
                        return function () {
                            api.postHideObs(temp_detection._id, function () {
                                $("#row_" + temp_detection._id).remove();
                                $("#hr_" + temp_detection._id).remove();
                            });
                        };
                    })());
                    $('#but_share_' + detection._id).click((function () {
                        var temp_detection = detection;
                        return function () {

                            var imageUrl = api.server + "/obs/get_obs_gif/" + temp_detection._id;
                            var shareText = getLanguageValue('share_detection_text');
                            if (temp_detection.object) {
                                if (temp_detection.object.mpcDesignation) {
                                    shareText = getLanguageValue('share_detection_known_text').replace("{{name}}", temp_detection.object.mpcDesignation);
                                }
                            }

                            if (window.plugins && window.plugins.socialsharing && /Android/.test(window.navigator.userAgent)) {

                                var options = {
                                    message: shareText, // not supported on some apps (Facebook, Instagram)
                                    subject: 'Cazasteroides', // fi. for email
                                    //files: [imageUrl], // an array of filenames either locally or remotely
                                    url: imageUrl
                                };
                                var onSuccess = function (result) {
                                    console.log("Share completed? " + result.completed); // On Android apps mostly return false even while it's true
                                    console.log("Shared to app: " + result.app); // On Android result.app is currently empty. On iOS it's empty when sharing is cancelled (result.completed=false)
                                };
                                var onError = function (msg) {
                                    console.log("Sharing failed with message: " + msg);
                                };
                                window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);
                            } else if (navigator.share !== undefined) {
                                navigator.share({
                                    'title': 'Cazasteroides',
                                    'text': shareText,
                                    'url': imageUrl//'http://cazasteroides.org'
                                }).then(function () {
                                    console.log('Successful share');
                                }).catch(function (error) {
                                    console.log('Error sharing:', error);
                                });
                            }
                        };
                    })());
                } catch (e) {

                }
            }

            if (data.length == limit) {
                $('#detectionlist').append(
                        '   <div id="my_detections_load_more_div" class="luke-text-box row" style="padding-bottom:5px;padding-top:5px;">'
                        + '   <button type="button" class="btn" style="margin:5px;min-width:80px"  id="my_detections_load_more">' + getLanguageValue("load_more") + '</button>'
                        + '</div>');
                $('#my_detections_load_more').click(function () {
                    callServer(false);
                });
            }

        }

        function callServer(clearPrevious) {
            if (!clearPrevious && isLoading)
                return;
            if (clearPrevious) {
                skip = 0;
            }

            isLoading = true;
            api.detectionsMe({skip: skip, limit: limit}, function (data) {
                $scope.myDetections_loading = false;
                $scope.myDetections_error = false;
                if (clearPrevious && (!data || data.length <= 0)) {
                    $scope.myDetections_empty = true;
                }
                $scope.$apply();
                if (clearPrevious)
                    $('#detectionlist').html('');

                isLoading = false;
                if (!data || data.length < 1) {
                    return;
                }

                addToHTMLList(data);
                skip += data.length;


            }, function () {
                $scope.myDetections_loading = false;
                $scope.myDetections_error = clearPrevious && true;
                $scope.myDetections_empty = false;
                $scope.$apply();
                isLoading = false;
            });
        }

        $scope.refreshMyDetections = function () {
// console.log("LoadMyDetections");
            $scope.myDetections_loading = true;
            $scope.myDetections_error = false;
            $scope.myDetections_empty = false;
            callServer(true);
        };
        $scope.refreshMyDetections();
//        $(window).scroll(function () {
//            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
//                console.log('Add more content');
//            }
//        });

    }

    function initCompetition(name) {
        var skip = 0;
        var limit = 200;
        var isLoading = false;

        name = decodeURI(name);
        $scope.competition_loading = true;
        $scope.competition_error = false;
        $scope.competition_empty = false;
        function updateInfo(contest) {
            //console.log(JSON.stringify(contest));
            var isInRange = isDateInRage(new Date(contest.time_start), new Date(contest.time_end), new Date());
            var isFinished = (new Date() > new Date(contest.time_end));
            var divStyle = '';
            if (isInRange) {
                divStyle += 'background-color:#99cc99;';
            } else if (isFinished) {
                divStyle += 'background-color:#ffb2b2;';
            }

            var addButton = false;
            var buttonText = "contest_join";
            var canClickButton = false;
            if (!contest.registered && !isFinished) {
                addButton = true;
                canClickButton = true;
            } else if (contest.registered && !isFinished) {
                addButton = true;
                buttonText = "contest_leave"; // isInRange ? "contest_competing" : "contest_registered";
                canClickButton = true;
            }

            var html = ' <div id="row_' + contest._id + '" style="' + divStyle + '">'
                    + '     <table style="margin: 2px auto;table-layout: fixed;">'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("start_") + '</td>'
                    + '             <td style="padding: 3px 10px;"  align="left">' + new Date(contest.time_start).toLocaleString() + '</td>'
                    + '         </tr>'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("end_") + '</td>'
                    + '             <td style="padding: 3px 10px;"  align="left">' + new Date(contest.time_end).toLocaleString() + '</td>'
                    + '         </tr>'
                    + '         <tr>'
                    + '             <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("contest_rules_") + '</td>'
                    + '             <td style="padding: 3px 10px;"  align="left"><a target="_blank" href="' + addhttp(contest.link) + '">' + contest.link + '</td>'
                    + '         </tr>'
                    + '     </table>'
                    + (addButton ? '     <button type="button" class="btn" style="margin:5px;min-width:80px"  id="but_' + contest._id + '">' + getLanguageValue(buttonText) + '</button>' : '')
                    + '</div>';
            $("#competition_info").html(html);
            if (canClickButton) {
                $('#but_' + contest._id).click((function () {
                    var temp_contest = contest;
                    return function () {
                        if (temp_contest.registered) {
                            api.contestUnregister(temp_contest._id, function () {

                                //var isInRange = isDateInRage(new Date(temp_contest.time_start), new Date(temp_contest.time_end))(new Date());
                                var isFinished = (new Date() > new Date(temp_contest.time_end));
                                var buttonText = "contest_join";
                                if (!isFinished) {
                                    $('#but_' + temp_contest._id).text(getLanguageValue(buttonText));
                                } else {
                                    $("#but_" + temp_contest._id).remove();
                                }

                                var index = $rootScope.allContests.findIndex(function (element) {
                                    return element._id.toString() === temp_contest._id.toString();
                                });
                                if (index >= 0 && index < $rootScope.allContests.length) {
                                    $rootScope.allContests[index].registered = false;
                                }

                                temp_contest.registered = false;
                            });
                        } else {
                            api.contestRegister(temp_contest._id, function () {
                                //$("#but_" + temp_contest._id).remove();

                                //var isInRange = isDateInRage(new Date(temp_contest.time_start), new Date(temp_contest.time_end))(new Date());
                                var isFinished = (new Date() > new Date(temp_contest.time_end));
                                var buttonText = "contest_leave";
                                if (!isFinished) {
                                    $('#but_' + temp_contest._id).text(getLanguageValue(buttonText));
                                } else {
                                    $("#but_" + temp_contest._id).remove();
                                }

                                var index = $rootScope.allContests.findIndex(function (element) {
                                    return element._id.toString() === temp_contest._id.toString();
                                });
                                if (index >= 0 && index < $rootScope.allContests.length) {
                                    $rootScope.allContests[index].registered = true;
                                }

                                temp_contest.registered = true;
                            });
                        }
                    };
                })());
            }

        }

        $("#competition_title").text(name);
        if ($rootScope.allContests) {
            var index = $rootScope.allContests.findIndex(function (element) {
                return element.name == name;
            });
            if (index >= 0 && index < $rootScope.allContests.length) {
                updateInfo($rootScope.allContests[index]);
                var isStarted = (new Date() >= new Date($rootScope.allContests[index].time_start));
                if (!isStarted) {
                    $("#contest_description").text($rootScope.allContests[index].description);
                    $("#without_content_users").hide();
                }
            }
        }
//******************************************************************************
        initApi();

        function generateHtml(position, user) {

            var points = (user.contests[0] && user.contests[0].points ? user.contests[0].points.toFixed(0) : 0);
            switch (position) {
                case 0:
                    var html = '<div class="premios_winner" style="padding:8px;">';
                    //html = html + '<H1> Winner </H1>';
                    html = html + '<img src=\"images/winnercup_.png\" width=100 heigh=100 style=\"width:100px\" />';
                    html = html + '<H2>1. ' + user.username + '</H2>';
                    html = html + ' ' + points + ' Pts</p>';
                    html = html + '</div>';
                    return html;
                case 1:
                    var html =
                            '<div style="padding:8px;">'
//                                    + ' <table style="margin: 0 auto;">'
                            + '     <tr>'
                            + '         <th align="right">&nbsp;&nbsp;<img src="images/secondcup.png" width=40 heigh=40 style="width:40px"/>&nbsp;&nbsp;</td>'
                            + '         <th align="right">&nbsp;&nbsp;' + (position + 1) + '.&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.username) + '&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + points + '&nbsp;&nbsp;</td>'
                            + '     </tr>'
//                                    + ' </table>'
                            + '</div>'
                            ;
                    return html;
                case 2:
                    var html =
                            '<div style="padding:8px;">'
//                                    + ' <table style="margin: 0 auto;">'
                            + '     <tr>'
                            + '         <th align="right">&nbsp;&nbsp;<img src="images/third_cup.png" width=32 heigh=32 style="width:32px" />&nbsp;&nbsp;</td>'
                            + '         <th align="right">&nbsp;&nbsp;' + (position + 1) + '.&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.username) + '&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + points + '&nbsp;&nbsp;</td>'
                            + '     </tr>'
//                                    + ' </table>'
                            + '</div>'
                            ;
                    return html;
                default:
                    var html =
                            '<div style="padding:8px;">'
//                                    + ' <table style="margin: 0 auto;">'
                            + '     <tr>'
                            + '         <th align="right">&nbsp;&nbsp;' + '&nbsp;&nbsp;</td>'
                            + '         <th align="right">&nbsp;&nbsp;' + (position + 1) + '.&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + (user.username) + '&nbsp;&nbsp;</td>'
                            + '         <th align="left">&nbsp;&nbsp;' + points + '&nbsp;&nbsp;</td>'
                            + '     </tr>'
//                                    + ' </table>'
                            + '</div>'
                            ;
                    return html;
            }
        }


        function addToHTMLList(data) {
            $("#competition_load_more").remove();

            for (i = 0; i < data.length; i++) {
                $("#competitionlist").append(generateHtml(i + skip, data[i]));
            }
// $("#premioslist").html(html);

            if (data.length == limit) {
                $('#competitionlist').append(
                        //'   <div id="competition_load_more_div" class="luke-text-box row" style="padding-bottom:5px;padding-top:5px;">'
                        '   <button type="button" class="btn" style="margin:5px;min-width:80px"  id="competition_load_more">' + getLanguageValue("load_more") + '</button>'
                        //+ '</div>'
                        );
                $('#competition_load_more').click(function () {
                    callServer(false);
                });
            }
        }

        function callServer(clearPrevious) {
            if (!clearPrevious && isLoading)
                return;
            if (clearPrevious) {
                skip = 0;
            }

            isLoading = true;
            api.contestRanking({skip: skip, limit: limit}, name, function (data) {
                $scope.competition_loading = false;
                $scope.competition_error = false;
                if (!data || data.length <= 0) {
                    $scope.competition_empty = true;
                }
                $scope.$apply();
                if (clearPrevious)
                    $('#competitionlist').html('');

                isLoading = false;
                if (!data || data.length < 1) {
                    return;
                }

                addToHTMLList(data);
                skip += data.length;
            }, function () {
                $scope.competition_loading = false;
                $scope.competition_error = clearPrevious && true;
                $scope.competition_empty = false;
                $scope.$apply();
            });
        }

        $scope.refreshContest = function () {
            //console.log("LoadObservations");
            $scope.competition_loading = true;
            $scope.competition_error = false;
            $scope.competition_empty = false;
            callServer(true);
        };

        $scope.refreshContest();
    }


    $scope.$on('$routeChangeSuccess', function () {
        if (navigator.globalization) {
            navigator.globalization.getPreferredLanguage(
                    function (locale) {//Get Language from Settings
                        cLANGUAGE = locale.value;
                        languageControls(cLANGUAGE);
                    },
                    function () {//On Failure set language to english
                        cLANGUAGE = "en";
                    }
            );
        } else {
            cLANGUAGE = "en";
            try {
                cLANGUAGE = navigator.language || navigator.userLanguage;
            } catch (e) {
            }
            languageControls(cLANGUAGE);
        }

        var screenName = undefined;
        //console.log("route change " + $location.url());
        if (localStorage.isLogin != 'true' &&
                /*Rutas permitidas sin estar logueado*/
                !($location.url() == "/cazast-forgot" || $location.url() == "/cazast-signup" || $location.url() == "/cazast-login" || $location.url() == "/" || $location.url().indexOf("/cazast-competition") == 0)) {
//console.log("eeeeeeeeeeepa " + $location.url());
            event.preventDefault();
            window.history.forward();
        } else
        if ($location.url() == "/cazast-login" || $location.url() == "/") {
            initlogin();
            screenName = 'cazast-login';
        } else if ($location.url() == "/cazast-main") {
            initmain();
            screenName = 'cazast-main';
        } else if ($location.url() == "/cazast-signup") {
            initsignup();
            screenName = 'cazast-signup';
        } else if ($location.url() == "/cazast-ranking") {
            initpremios();
            screenName = 'cazast-ranking';
        } else if ($location.url() == "/cazast-forgot") {
            initforgot();
            screenName = 'cazast-fotgot';
        } else if ($location.url() == "/cazast-me") {
            initme();
            screenName = 'cazast-me';
        } else if ($location.url() == "/cazast-vote") {
            initvote();
            screenName = 'cazast-vote';
        } else if ($location.url() == "/cazast-my_detections") {
            initMyDetections();
            screenName = 'cazast-my_detections';
        } else if ($location.url().indexOf("/cazast-competition") == 0) {
            var url = $location.url();
            var name = url.substr(url.lastIndexOf('/') + 1);
            initCompetition(name);
            screenName = 'cazast-competition/' + name;
        }

//        if (screenName) {
//            console.log("screenName: " + screenName);
//            if (window.ga && window.ga.trackView) {
//                window.ga.trackView(screenName);
//            } else if (ga) {
//                console.log("Send: " + screenName);
//                ga('send', screenName);
//            }
//
//            if (window.FirebasePlugin) {
//                window.FirebasePlugin.setScreenName(screenName);
//            }
//        }
        registerScreenName(screenName);
        setCopyrightDate();
    });
    function registerScreenName(screenName) {
        try {
            if (screenName) {
//console.log("screenName: " + screenName);
                if (window.ga && window.ga.trackView) {
                    window.ga.trackView(screenName);
                } else if (ga) {
                    console.log("Send: " + screenName);
                    ga('send', screenName);
                }

                if (window.FirebasePlugin) {
                    window.FirebasePlugin.setScreenName(screenName);
                }
            }
        } catch (e) {
        }
    }

    function setCopyrightDate() {
        year = new Date().getYear();
        if (year < 1900)
            year += 1900;
        try {
            document.getElementById("copyrightYear").innerHTML = year;
        } catch (e) {
        }

    }

    function initApi() {
        try {
            if (!api)
                api = new Api();
        } catch (e) {
            api = new Api();
        }
    }

    $scope.$on('$stateChangeSuccess', function () {
//console.log("state change");
    });
//    var time = 8;
//  var $progressBar,$bar,$elem,isPause,tick,percentTime;
    var screen = $(window).height();
//var screen = $window.innerHeight;
//var screen = $.mobile.getScreenHeight();
    var header = $(".ui-header").hasClass("ui-header-fixed") ? $(".ui-header").outerHeight() - 1 : $(".ui-header").outerHeight();
    var footer = $(".ui-footer").hasClass("ui-footer-fixed") ? $(".ui-footer").outerHeight() - 1 : $(".ui-footer").outerHeight();
    /* content div has padding of 1em = 16px (32px top+bottom). This step
     can be skipped by subtracting 32px from content var directly. */
    var contentCurrent = $(".ui-content").outerHeight() - $(".ui-content").height();
    var content = screen - header - footer - contentCurrent;
    $("#animation").height(content - 15);
    $(".ui-content").height(content);
    if (window.ga && window.ga.startTrackerWithId) {
//window.ga.startTrackerWithId('UA-86527143-2');
        window.ga.startTrackerWithId('UA-96370001-2');
    }

    var resizeTimer;
    $(window).on('resize', function (e) {
//console.log("Resize!!!");
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            //console.log("Resize End");
            if ($location.url() == "/cazast-main")
                $route.reload();
        }, 250);
    });
    loadBrowserAnalytics();
    if (window.FirebasePlugin) {
        window.FirebasePlugin.subscribe("app_news");
        if (deviceDetect && deviceDetect.Android()) {
            window.FirebasePlugin.subscribe("app_android");
        }
    }
});
