var currendfield = "init"

var MAPFILE = "/var/opt/cazasteroides/server/mapfile.map";
var MAPSERVER = SERVER_URL_BASE + "/images/mapserv?map=" + MAPFILE + "&mode=map";
var scalemin = [2000, 2000, 2000, 2000, 2000];
var scalemax = [2500, 2500, 2500, 2500, 2500];
var SCALE = ["&MSCALE=2000,2500", "&MSCALE=2000,2500", "&MSCALE=2000,2500", "&MSCALE=2000,2500", "&MSCALE=2000,2500"];
var EXTENSION = "&mapsize=200%20200";//esto es el tamaño en pixeles de la imagen final
//var api.getMetadataValue('IMAGEW') = 1024;//FIXME1! esto hay que adaptarlo segun la imagen id
//var api.getMetadataValue('IMAGEH') = 1024;//FIXME

var recortesizex = 200;
var recortesizey = 200;

var xorig = 0;// Math.random() * (api.getMetadataValue('IMAGEW') - recortesizex);
var yorig = 0;// Math.random() * (api.getMetadataValue('IMAGEH') - recortesizey);

var randomext = "&MAPEXT=" + xorig + "%20" + yorig + "%20" + (xorig + recortesizex) + "%20" + (yorig + recortesizey);
var anim_interval;
//var resolution = 0.3;
var ref_resolution = 0.6; //Aimed resolution 0.3 aperture will result on 1:1 pixel scale//Setup.key = res_image_end 
var idx_img = 0;
//   Load the animation images
var JPEGN = [];

function change_img() {
    Math.seedrandom('added entropy.', {entropy: true});

    if (recortesizex >= api.getMetadataValue('IMAGEW')) {
        xorig = (api.getMetadataValue('IMAGEW') / 2) - (recortesizex / 2);
    } else {
        var rand = Math.random();
        xorig = rand * (api.getMetadataValue('IMAGEW') - recortesizex);

        if (xorig < 0) {
            xorig = 0;
            console.log("Fix xorig");
        }
        if (xorig + recortesizex > api.getMetadataValue('IMAGEW')) {
            xorig = api.getMetadataValue('IMAGEW') - recortesizex;
            console.log("Fix xmax");
        }
    }
    if (recortesizey >= api.getMetadataValue('IMAGEH')) {
        yorig = (api.getMetadataValue('IMAGEH') / 2) - (recortesizey / 2);
    } else {
        var rand = Math.random();
        yorig = rand * (api.getMetadataValue('IMAGEH') - recortesizey);

        if (yorig < 0) {
            yorig = 0;
            console.log("Fix yorig");
        }
        if (yorig + recortesizey > api.getMetadataValue('IMAGEH')) {
            yorig = api.getMetadataValue('IMAGEH') - recortesizey;
            console.log("Fix ymax");
        }

    }
    var xmax = xorig + recortesizex;
    var ymax = yorig + recortesizey;
    randomext = "&MAPEXT=" + xorig + "%20" + yorig + "%20" + xmax + "%20" + ymax;

    console.log("Animation: change_img: ImgSize: " + api.getMetadataValue('IMAGEW') + "x" + api.getMetadataValue('IMAGEH'));
    console.log("Animation: change_img: RectSize: " + recortesizex + "x" + recortesizey);
    console.log("Animation: change_img: Center: " + ((xorig + xmax) / 2) + "x" + ((yorig + ymax) / 2));

    console.log(JSON.stringify({
        "xmin": xorig,
        "ymin": yorig,
        "xmax": xmax,
        "ymax": ymax,
        "scalemin": scalemin,
        "scalemax": scalemax
    }));

    return {
        "xmin": xorig,
        "ymin": yorig,
        "xmax": xmax,
        "ymax": ymax,
        "scalemin": scalemin,
        "scalemax": scalemax
    };
}

function set_res_image_end(value) {
    ref_resolution = parseFloat(value);
    console.log("Animation: ref_resolution:" + ref_resolution);
}

function set_img_size(width, height) {
    console.log("Animation: set_img_size:" + width + " " + height);
    recortesizey = resolutionScale(height);// * ref_resolution / api.getMetadataValue('SCALE');
    recortesizex = resolutionScale(width);// * ref_resolution / api.getMetadataValue('SCALE');
    EXTENSION = "&mapsize=" + width + "%20" + height;
}

function set_img_scale(min, max, index) {
    console.log("Animation: set_img_scale(" + index + "):" + min + " -- " + max);
    scalemin[index] = min;
    scalemax[index] = max;
    SCALE[index] = "&MSCALE=" + min + "," + max;
}

function set_map_file(mapfile) {
    MAPFILE = mapfile;
    MAPSERVER = SERVER_URL_BASE + "/images/mapserv?map=" + MAPFILE + "&mode=map";
}

function initialize_images() {
    JPEGN = [];

    if (typeof api != 'undefined') {
        if (api && api.getMasterMetadata()) {
            clearInterval(anim_interval);
            idx_img = 0;

            var nuevo_orig = [xorig, yorig];
            var xmax = nuevo_orig[0] + recortesizex;
            var ymax = nuevo_orig[1] + recortesizey;
            var newext = "&MAPEXT=" + nuevo_orig[0] + "%20" + nuevo_orig[1] + "%20" + xmax + "%20" + ymax;

            //big extension shouldn't be greater than 2048 in any dir.
//            var factorx = 2;
//            var big_recortex = (recortesizex * 2);
//            if (big_recortex > 2048) {
//                factorx = 2048 / recortesizex;
//            }
//            var factory = 2;
//            var big_recortey = (recortesizey * 2);
//            if (big_recortey > 2048) {
//                factory = 2048 / recortesizey;
//            }
//
//            var factor = factorx < factory ? factorx : factory;
//            BIGEXTENSION = "&mapsize=" + (recortesizex * factor) + "%20" + (recortesizey * factor);
            for (var idx = 1; idx < 6; idx++) {

                var src = MAPSERVER + SCALE[idx - 1] + EXTENSION + newext + "&layer=img" + idx + "&nocache=" + Date.now();

                JPEGN[idx - 1] = new Image();
                JPEGN[idx - 1].id = "animation";
                JPEGN[idx - 1].alt = "Responsive";
                JPEGN[idx - 1].style = "width:unset;touch-action:none;pointer-events: none;";
                JPEGN[idx - 1].src = src;

                if (idx == api.quintentMaster + 1) {
                    var temp = new Image();
                    temp.id = "animation2";
                    temp.alt = "Responsive";
                    temp.style = "width:unset;visibility: hidden;position: absolute;bottom: 0px;left: 0px ;touch-action:none;pointer-events: none;";
                    temp.setAttribute("style", "width:unset;visibility: hidden;position: absolute;bottom: 0px;left: 0px ;touch-action:none;pointer-events: none;");
                    temp.className = "zoom";

                    temp.setAttribute("data-magnify-src", src);

                    temp.src = temp.getAttribute("data-magnify-src");
                    $("#animation2").replaceWith(temp);
                }
            }
            anim_interval = setInterval("anim_img(idx_img)", 500);
        }
    }
}

function anim_img(x) {
    if (idx_img == 6) {
        idx_img = 1;
    }

    if (JPEGN[idx_img - 1]) {
        $("#animation").replaceWith(JPEGN[idx_img - 1]);
    }

    idx_img++;
}
function hideinfo() {
    $("#infowindow").hide(1);
}

function showImgInfo(e) {

    var hms = $("#clock").text();   // your input string
    var a = hms.split(':'); // split it at the colons
    var ms = (+a[0]) * 60 * 1000 + (+a[1]) * 1000 + (+a[2]);

    var html = '<p>' + getLanguageValue("image") + currendfield + '</p>'
            + '<table style="margin: 0 auto;table-layout: fixed;">'
            + '  <tr>'
            + '     <td style="padding: 3px 10px;" align="right">' + getLanguageValue("probability_to_find") + '</td>'
            + '     <td style="padding: 3px 10px;overflow:hidden; white-space:nowrap"  align="left">' + api.getMetadataValue('PROBA') + '%</td>'
            + '  </tr>'
            + '  <tr>'
            + '     <td style="padding: 3px 10px;"  align="right">' + getLanguageValue("sky_turbulence_") + '</td>'
            + '     <td style="padding: 3px 10px;overflow:hidden; white-space:nowrap"  align="left">' + (api.getMetadataQuintet(0)['FWHM'] * api.getMetadataQuintet(0)['SCALE']).toFixed(2) + ' arcsec</td>'
            + '  </tr>'
            + '</table>'
            + '<br><p style="font-size:16px" >' + getLanguageValue("with_this_image_can_get_") + Math.floor(getPoints(ms, api.getMetadataQuintet(0))) + getLanguageValue("_points") + '</p>'
            + '<button type="button" class="ui-btn btn-block" onclick=\"hideinfo()\">OK</button>';

    $('#infowindow').html(html);
    $("#infowindow").show(1);

    $("#infowindow")
            .css({
                top: 50 + "px",
                left: 10 + "px"
            });

    return false;
}

function resolutionScale(size) {
    return size * ref_resolution / api.getMetadataValue('SCALE');
}

function updateGUIelements(nX, nY) {
    var header = 0;

    /**
     * Evitar mover el puntero cuando esté abierto el popup con información
     */
    if (!$('#infowindow').is(":visible")) {
        // Place element where the finger is
        var draggable = $("#draggable");

        draggable.css("left", "" + (nX - 25) + 'px');//+ oContainerOffset.left
        draggable.css("top", "" + (nY - header - 25) + 'px');
    }

    //xorig e yorig se definen en animation.js se actualizan con cada random
    var nX_scale = resolutionScale(nX);// * ref_resolution / api.getMetadataValue('SCALE');
    var nY_scale = resolutionScale(nY - header);// * ref_resolution / api.getMetadataValue('SCALE');

    var pixx = nX_scale + xorig;
    var pixy = api.getMetadataValue('IMAGEH') + nY_scale - yorig - recortesizey;//CHECK THIS!!!
    pixcoord = [nX_scale, nY_scale];
    pixcoordabs = [pixx, pixy];
    coord = pix2coord(pixx, pixy, api.getMasterMetadata());
//    repix = coord2pix(coord[0], coord[1], api.getMasterMetadata());
//    console.log(JSON.stringify(pixcoordabs));
//    console.log(JSON.stringify(repix));

    $("#coord_div").html(
            '   <div style="font-size:12px">'
            + '     <a href="#" onClick="showImgInfo();return false;">'
            + '         <span id="info" class="fa fa-info-circle" style="color: rgba(0,0,0,1);margin:3px"></span>'
            + currendfield
            + '     </a>'
            + ' </div>'
            + '<table style="font-size:	13px; margin: 0 auto;">'
            + '  <tr><td align="right">RA:&nbsp;</td><td>' + deg2hour(coord[0]) + '</td>'
            + '  <tr><td align="right">DEC:&nbsp;</td><td>' + deg2degminsec(coord[1]) + '</td>'
            + '</table>'
            );
}