//api.js

var Api = function () {
    this.server = SERVER_URL_BASE;
    this.metadata = [null, null, null, null, null];
    this.quintentMaster = 0;
};

Api.prototype.getHistogram = function (id_image, cb) {//Not in use

    $.ajax({
        url: this.server + '/images/' + id_image + '/histogram',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        timeout: REQUEST_TIMEOUT,
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        success: function (data) {
            //SERVER_TOKEN=data.token;
            //data.min = data.min + 32768;
            //data.max = data.max + 32768;
            m_hexcel = new Histogram();
            m_hexcel.setData(data);
            m_hexcel.draw();
            cb();
            //$.mobile.navigate("#mainpage");
        },
        error: function (jqXHR) {

            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.getHistogram(id_image, cb);
                    }
                });
            } else {//TODO parse error 400 error_gdal_[number]
                parseServerError(jqXHR);
//                console.log("Error connecting to server");
//                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
};

/**
 * Obtener Todas las imagenes activas
 * 
 * @param {function} cb -> Se le pasa como parametro una de las imagenes activas [Seleccionada aleatoriamente]
 * @returns {undefined}
 */
Api.prototype.getLastImageData = function (cb) {
    //console.log("getLastImageData");
    $.ajax({
        url: this.server + '/images/active',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        timeout: REQUEST_TIMEOUT,
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        success: function (data) {
            //console.log("getLastImageData success " + data.length);

            var totalWeight = 0;
            for (var i = 0; i < data.length; i++) {
                if (data[i].weight) {
                    totalWeight += parseInt(data[i].weight);
                    //console.log(i + ") " + data[i].weight + " total: " + totalWeight);
                }
            }

            if (totalWeight > 0) {

                Math.seedrandom();
                var w = Math.random() * totalWeight;
                //console.log("Select w: " + w);

                for (var i = 0; i < data.length; i++) {
                    if (data[i].weight && w - parseInt(data[i].weight) <= 0) {
                        //console.log("Select index: " + i);
                        var img = data[i];
                        if (img) {
                            console.log(img._id);
                        }
                        cb(img);
                        break;
                    } else if (data[i].weight) {
                        w -= parseInt(data[i].weight);
                    }
                }
            } else {
                Math.seedrandom();
                var img = data[Math.floor(Math.random() * data.length)];
                if (img) {
                    console.log(img._id);
                }
                cb(img);
            }
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            //console.log("getLastImageData error");
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.getLastImageData(cb);
                    }
                });
            } else {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log("getLastImageData failure");
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}

Api.prototype.getMe = function (fb) {
    $.ajax({
        url: this.server + '/users/me' + "?access_token=" + SERVER_TOKEN,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            //console.log(JSON.stringify(data));
            localStorage.user = data.username;
            fb(data);
            if (window.FirebasePlugin)
                window.FirebasePlugin.setUserId(data._id);

//            if (window.ga) {
//                window.ga.setUserId(data._id);
//            }
        },
        error: function (jqXHR) {
            //console.log(JSON.stringify(jqXHR));
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.getMe(fb);
                    }
                });
            } else {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}

Api.prototype.getUsers = function (pag, fb, CBerror) {

    var pagOptions = '?skip=' + pag.skip + '&limit=' + pag.limit;

    $.ajax({
        url: this.server + '/users' + pagOptions,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        timeout: REQUEST_TIMEOUT,
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        success: function (data) {
            fb(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.getUsers(pag, fb, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                //alert(getLanguageValue("server_error"));
                CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            CBerror();
        }
    }); //fin ajax
};

Api.prototype.setMeatdata = function (data, i) {
//console.log("obj" + obj);
    //console.log(data);

    if (!data.A_0_0)
        data.A_0_0 = "0";
    if (!data.A_1_0)
        data.A_1_0 = "0";
    if (!data.A_0_1)
        data.A_0_1 = "0";
    if (!data.A_1_1)
        data.A_1_1 = "0";
    if (!data.A_2_0)
        data.A_2_0 = "0";
    if (!data.A_0_2)
        data.A_0_2 = "0";
    if (!data.B_0_0)
        data.B_0_0 = "0";
    if (!data.B_1_0)
        data.B_1_0 = "0";
    if (!data.B_0_1)
        data.B_0_1 = "0";
    if (!data.B_1_1)
        data.B_1_1 = "0";
    if (!data.B_2_0)
        data.B_2_0 = "0";
    if (!data.B_0_2)
        data.B_0_2 = "0";
    if (!data.AP_0_0)
        data.AP_0_0 = "0";
    if (!data.AP_1_0)
        data.AP_1_0 = "0";
    if (!data.AP_0_1)
        data.AP_0_1 = "0";
    if (!data.AP_1_1)
        data.AP_1_1 = "0";
    if (!data.AP_2_0)
        data.AP_2_0 = "0";
    if (!data.AP_0_2)
        data.AP_0_2 = "0";
    if (!data.BP_0_0)
        data.BP_0_0 = "0";
    if (!data.BP_1_0)
        data.BP_1_0 = "0";
    if (!data.BP_0_1)
        data.BP_0_1 = "0";
    if (!data.BP_1_1)
        data.BP_1_1 = "0";
    if (!data.BP_2_0)
        data.BP_2_0 = "0";
    if (!data.BP_0_2)
        data.BP_0_2 = "0";

    if (!data.SCALE) {
        data.SCALE = 0.3;
    } else {
        data.SCALE = parseFloat(data.SCALE);
    }

    if (!data.SKY) {
        data.SKY = 2200;
    } else {
        var bzero = 0;
        if (data.BZERO) {
            bzero = parseFloat(data.BZERO);
        }
        data.SKY = parseFloat(data.SKY) - bzero; //FIXME ESTO ES PARA PASAR DE UNSIGNED A SIGNED PERO PUEDE DAR PROBLEMAS
    }

    if (!data.SIGMA) {
        data.SIGMA = 100;
    } else {
        data.SIGMA = parseFloat(data.SIGMA);
    }

    if (!data.FWHM) {
        if (data.FWHMORI) {
            data.FWHM = parseFloat(data.FWHMORI);
        } else {
            data.FWHM = 0;
        }
    } else {
        data.FWHM = parseFloat(data.FWHM);
    }

    if (data.IMAGEW) {
        data.IMAGEW = parseFloat(data.IMAGEW);
    }

    if (data.IMAGEH) {
        data.IMAGEH = parseFloat(data.IMAGEH);
    }

    this.metadata[i] = data;
    //console.log(this.metadata[i]);
//    if (i == this.quintentMaster) {
//        //variables defined on animation
//        //imgysize = parseFloat(data.IMAGEH);
//        //imgxsize = parseFloat(data.IMAGEW);
//        //resolution = data.SCALE;
//
////        if (this.metadata[this.quintentMaster] && this.metadata[this.quintentMaster].FWHM)
////            this.metadata[this.quintentMaster].FWHM = parseFloat(this.metadata[this.quintentMaster].FWHM);
//    }
};

Api.prototype.getMetadataValue = function (key) {
    //console.log("metadata[" + this.quintentMaster + "]." + key + ": " + this.metadata[this.quintentMaster][key]);
    return this.metadata[this.quintentMaster][key];
};
Api.prototype.getMasterMetadata = function () {
    //console.log("metadata[" + this.quintentMaster + "]: " + JSON.stringify(this.metadata[this.quintentMaster]));
    return this.metadata[this.quintentMaster];
};
Api.prototype.getMetadataQuintet = function (q) {
    return this.metadata[q];
};

Api.prototype.getMetadata = function (id_image, id_frame, cb) {
    //console.log("getMetadata");
    obj = this;
    $.ajax({
        url: this.server + '/images/' + id_image + '/' + id_frame + '/coeff',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        timeout: REQUEST_TIMEOUT,
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        success: function (data) {
            //console.log("getMetadata success");
            //if (id_frame == 0)
            obj.setMeatdata(data, id_frame);
            cb(obj.metadata[id_frame], id_frame);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            //console.log("getMetadata error");
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.getMetadata(id_image, id_frame, cb);
                    }
                });
            } else {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log("getMetadata failure");
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
};

Api.prototype.newRect = function (rect, id_image, cb) {

    var rect = {
        "image": id_image,
        "minx": rect.xmin,
        "miny": rect.ymin,
        "maxx": rect.xmax,
        "maxy": rect.ymax,
        "scalemin": rect.scalemin[0],
        "scalemax": rect.scalemax[0],
        "scaleminArr": rect.scalemin,
        "scalemaxArr": rect.scalemax
    };
    //console.log(JSON.stringify(rect));
    $.ajax({
        url: this.server + '/rectangles',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(rect),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            cb(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.newRect(rect, id_image, cb);
                    }
                });
            } else {
                console.log(getLanguageValue("server_error"));
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
};

Api.prototype.newObservation = function (id_image, id_rect, pixcoord, pixcoordabs, coords, clock, time, metadata, quintetn, cb) {

    var meta = {
        "PROBA": metadata.PROBA,
        "SCALE": metadata.SCALE,
        "FWHM": metadata.FWHM
    };
    var obs = {
        "image": id_image,
        "rect": id_rect,
        "pixCoord": pixcoord,
        "pixCoordAbs": pixcoordabs,
        "Coord": coords,
        "clock": clock,
        "time": time, //TODO add this param to server
        "meta": JSON.stringify(meta), //TODO add this param to server
        "status": "pending",
        "quintetn": quintetn
    };
    //console.log(JSON.stringify(obs));
    $.ajax({
        url: this.server + '/obs',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(obs),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            if (data.message == "existing") {
                console.log("observation already on the list");
                //cb(data);
            } else {
                console.log("observation not on the list");
                //cb(data);
            }
            cb(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.newObservation(id_image, id_rect, pixcoord, pixcoordabs, coords, clock, time, metadata, quintetn, cb);
                    }
                });
            } else {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}

Api.prototype.voteObservation = function (id_obs, cb) {
    var obs = {
        "_id": id_obs
    }
    $.ajax({
        url: this.server + '/obs/vote',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(obs),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            cb('true');
        },
        error: function (jqXHR) {
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.voteObservation(id_obs, cb);
                    }
                });
            } else if (!parseServerError(jqXHR)) {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}

Api.prototype.downvoteObservation = function (id_obs, cb) {
    var obs = {
        "_id": id_obs
    }
    $.ajax({
        url: this.server + '/obs/downvote',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(obs),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            cb('true');
        },
        error: function (jqXHR) {
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.downvoteObservation(id_obs, cb);
                    }
                });
            } else if (!parseServerError(jqXHR)) {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
}

Api.prototype.getObservationRand = function (cb, CBerror) {
    $.ajax({
        url: this.server + '/obs/rand',
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            cb(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
//                console.log('Invalid user or password');
//                alert('Invalid User or password');
                refreshToken(function (success) {
                    if (success) {

                        //api = new Api();
                        api.getObservationRand(cb, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                //alert(getLanguageValue("server_error"));
                CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            CBerror();
        }
    }); //fin ajax
}

Api.prototype.detectionsMe = function (pag, CBsuccess, CBerror) {

    var pagOptions = '?skip=' + pag.skip + '&limit=' + pag.limit;

    $.ajax({
        url: this.server + '/obs/me' + pagOptions,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.detectionsMe(pag, CBsuccess, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            CBerror();
        }
    });
};

Api.prototype.updateEmail = function (email) {
    $.ajax({
        url: this.server + '/users/updatemail/' + email,
        type: 'PUT',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT
    });
};

Api.prototype.contestActive = function (CBsuccess, CBerror) {
    $.ajax({
        url: this.server + '/contest/active',
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.contestActive(CBsuccess, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                if (CBerror)
                    CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            if (CBerror)
                CBerror();
        }
    });
};

Api.prototype.contestRegister = function (id, CBsuccess, CBerror) {

    var data = {
        "contest": id
    };

    $.ajax({
        url: this.server + '/contest/register',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(data),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.contestRegister(id, CBsuccess, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                if (CBerror)
                    CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            if (CBerror)
                CBerror();
        }
    });
};
Api.prototype.contestAll = function (CBsuccess, CBerror) {
    $.ajax({
        url: this.server + '/contest',
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.contestAll(CBsuccess, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                if (CBerror)
                    CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            if (CBerror)
                CBerror();
        }
    });
};

Api.prototype.contestRanking = function (pag, name, CBsuccess, CBerror) {
    var pagOptions = '?skip=' + pag.skip + '&limit=' + pag.limit;
    $.ajax({
        url: this.server + '/contest/ranking/' + encodeURI(name) + pagOptions,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.contestRanking(pag, name, CBsuccess, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                if (CBerror)
                    CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            if (CBerror)
                CBerror();
        }
    });
};

Api.prototype.contestUnregister = function (id, CBsuccess, CBerror) {
    $.ajax({
        url: this.server + '/contest/' + id,
        type: 'DELETE',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            parseServerError(jqXHR);
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.contestUnregister(id, CBsuccess, CBerror);
                    }
                });
            } else {
                console.log("Error connecting to server");
                if (CBerror)
                    CBerror();
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
            if (CBerror)
                CBerror();
        }
    });
};

Api.prototype.getSetupValue = function (key, CBsuccess, CBerror) {
    $.ajax({
        url: this.server + '/setup/' + key,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            CBsuccess(data);
        },
        error: function (jqXHR) {
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.getSetupValue(key, CBsuccess, CBerror);
                    }
                });
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
        }
    });
};

Api.prototype.postHideObs = function (id_obs, cb) {
    var obs = {
        "_id": id_obs
    }
    $.ajax({
        url: this.server + '/obs/hide',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(obs),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            cb();
        },
        error: function (jqXHR) {
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.postHideObs(id_obs, cb);
                    }
                });
            } else if (!parseServerError(jqXHR)) {
                console.log("Error connecting to server");
                alert(getLanguageValue("server_error"));
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
            alert(errMsg);
        }
    }); //fin ajax
};

Api.prototype.pushAdd = function (token) {
    PUSH_TOKEN = token;
    var push = {
        "token": token
    };
    $.ajax({
        url: this.server + '/gcm',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify(push),
        beforeSend: function (xhr) {
            if (SERVER_TOKEN)
                xhr.setRequestHeader("Authorization", "BEARER " + SERVER_TOKEN);
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {

        },
        error: function (jqXHR) {
            if (jqXHR.status == 401) {
                refreshToken(function (success) {
                    if (success) {
                        api.pushAdd(token);
                    }
                });
            } else if (!parseServerError(jqXHR)) {
                console.log("Error connecting to server");
            }
        },
        failure: function (errMsg) {
            console.log(errMsg);
        }
    }); //fin ajax
};

Api.prototype.pushDelete = function (server_token, push_token) {
    if (!server_token)
        return;

    if (push_token) {
        $.ajax({
            url: this.server + '/gcm?token=' + push_token,
            type: 'DELETE',
            dataType: 'json',
            beforeSend: function (xhr) {
                if (server_token)
                    xhr.setRequestHeader("Authorization", "BEARER " + server_token);
            },
            timeout: REQUEST_TIMEOUT,
            success: function (data) {
            },
            error: function (jqXHR) {
                parseServerError(jqXHR);
                if (jqXHR.status == 401) {
                    refreshToken(function (success) {
                        if (success) {
                            api.pushDelete(server_token, push_token);
                        }
                    });
                } else {
                    console.log("Error connecting to server");
                }
            },
            failure: function (errMsg) {
                console.log(errMsg);
            }
        });
    } else {
        if (window.FirebasePlugin) {
            window.FirebasePlugin.onTokenRefresh(function (token) {
                if (!token)
                    return;
                $.ajax({
                    url: this.server + '/gcm?token=' + token,
                    type: 'DELETE',
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        if (server_token)
                            xhr.setRequestHeader("Authorization", "BEARER " + server_token);
                    },
                    timeout: REQUEST_TIMEOUT,
                    success: function (data) {
                    },
                    error: function (jqXHR) {
                        parseServerError(jqXHR);
                        if (jqXHR.status == 401) {
                            refreshToken(function (success) {
                                if (success) {
                                    api.pushDelete(server_token, token);
                                }
                            });
                        } else {
                            console.log("Error connecting to server");
                        }
                    },
                    failure: function (errMsg) {
                        console.log(errMsg);
                    }
                });
                console.log(token);
            }, function (error) {
                console.error(error);
            });
        }
    }




};



/**
 * Si se modificó el Token por usar la app desde otro dispositivo, necesitamos actualizar
 * el token para poder continuar
 * 
 * @param {function(boolean)} callback
 * @returns {boolean} -> True if the token refreshed successfully
 */
var lastRefreshTokenCallback = null;
function refreshToken(callback) {
//    console.log('Invalid user or password');
//    alert('Invalid User or password');
//    callback(false);
//    return;
//    console.log("callBack? " + (lastRefreshTokenCallback === JSON.stringify(callback)));
    if (lastRefreshTokenCallback === JSON.stringify(callback)) {
        refreshTokenLoopError();
        return;
    } else {
        lastRefreshTokenCallback = JSON.stringify(callback);
    }

    console.log('refreshToken');
    if (!localStorage.isLogin) {
        refreshTokenError();
        return;
    }

//    //if (!localStorage.user && !localStorage.pass) {
//    if (!SERVER_TOKEN) {
//        refreshTokenError();
//        return;
//    }

    //Auto Facebook Login
    autoLogin(null, function (data) {
        SERVER_TOKEN = data.token;
        localStorage.isLogin = 'true';
        console.log("Token Updated [" + SERVER_TOKEN + "]");
        callback(true);
    }, function (jqXHR) {
        parseServerError(jqXHR);
        refreshTokenError();
    });

}

function refreshTokenError() {
    try {
        if (facebookConnectPlugin)
            facebookConnectPlugin.logout();
    } catch (e) {

    }
    api.pushDelete(SERVER_TOKEN, PUSH_TOKEN);
    SERVER_TOKEN = '';
    console.log("Token Updated [" + SERVER_TOKEN + "]");
    localStorage.isLogin = null;
    console.log('Invalid user or password');
    alert(getLanguageValue("credential_error")); //TODO change message
    window.window.location.href = window.location.pathname;
}

function refreshTokenLoopError() {
//alert(getLanguageValue("server_error"));
    refreshTokenError();
}

/**
 * 
 * @param {jqXHR} jqXHR
 * @returns {Boolean} true if was show alert
 */
function parseServerError(jqXHR) {
    if (!jqXHR)
        return false;
    console.log(JSON.stringify(jqXHR));
    if (jqXHR.status === 400) {
        if (jqXHR.responseText === 'NeedUpdateApp') {//TODO return this message from server when update app with new SERVER_URL_BASE -> res.send(403, { error: "NeedUpdateApp" });
//TODO show popup to force update
            if ($('#mainpage')) {
                $('#mainpage').html(
                        '     <div class="content" data-role="content">'
                        + '         <div data-role="none" class="luke-text-box luke-text-box-a align-center">'
                        + '             <div class="target align-center"  style="margin:10px; padding:5px; width:300px;  height:350px;   text-align:center;"  >'
                        + '                 <h3>' + getLanguageValue("app_name") + '</h3>'
                        + '                 <p>' + getLanguageValue("we_are_update") + '</p><br>'
                        + '                 <p>' + getLanguageValue("please_upgrade") + '</p><br>'
                        + '                 <button type="button" class="btn" style="margin:5px;min-width:75px" onclick="market://details?id=' + APP_PACKAGE_NAME + '">Android App</button>'
                        + '             </div>'
                        + '         </div>'
                        + '     </div>'
                        + ' <div data-role="footer" data-theme="none" data-border="false" data-inner="true" data-position="fixed">'
                        + '     <p>' + getLanguageValue("sorry_inconvenience") + '</p>'
                        + ' </div>'
                        );
                $('div.target').css({
                    "position": "absolute",
                    "top": ((($(parent).height() - $('div.target').outerHeight()) / 2) + $(parent).scrollTop() + "px"),
                    "left": ((($(parent).width() - $('div.target').outerWidth()) / 2) + $(parent).scrollLeft() + "px")
                });
            }
            return true;
        } else if (jqXHR.responseText.startsWith('error_gdal_')) {
            console.warn("Error GDAL " + jqXHR.responseText.substr(jqXHR.responseText.length - 1));
            switch (jqXHR.responseText.substr(jqXHR.responseText.length - 1)) {
                case '1':
                    alert(getLanguageValue("server_error_gdal_1"));
                    break;
                default:
                    alert(getLanguageValue("server_error"));
                    break;
            }
            return true;
        } else if (jqXHR.responseText === 'error_owner_obs') {
            alert(getLanguageValue("server_error_owner_obs"));
            return true;
        } else if (jqXHR.responseText === 'error_duplicate_obs_vote') {
            alert(getLanguageValue("server_error_duplicate_obs_vote"));
            return true;
        } else if (jqXHR.responseText === ('error_no_exist_obs')) {
            alert(getLanguageValue("server_error_no_exist_obs"));
            return true;
        } else {
            console.warn("Error: " + jqXHR.responseText);
        }
    }
    return false;
}