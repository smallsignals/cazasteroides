/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function alert(message) {
    dialog.alert({
        message: message
    });
}
function userAborted(xhr) {
    return !xhr.getAllResponseHeaders();
}

var loginServerRequest = null;
var loginServerFacebookRequest = null;

function loginServer($scope, user, pass, successCallback, errorCallback) {
    if ($scope) {
        $scope.inLoginProcess = true;
        $scope.$apply();
    }
    loginServerRequest = $.ajax({
        url: SERVER_URL_BASE + '/auth/basic',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (loginServerRequest) {
                loginServerRequest.abort('NoShowMessage');
            }
            if (loginServerFacebookRequest) {
                loginServerFacebookRequest.abort('NoShowMessage');
            }
            xhr.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass));
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            if ($scope) {
                $scope.inLoginProcess = false;
                $scope.$apply();
            }
            localStorage.user = user;
            localStorage.pass = pass;
            if (successCallback)
                successCallback(data);

            loginServerRequest = null;
            sendPushToken();
        },
        error: function (jqXHR) {
            if ($scope) {
                $scope.inLoginProcess = false;
                $scope.$apply();
            }
            if (errorCallback && !userAborted(jqXHR)) {
                errorCallback(jqXHR);
            }

            loginServerRequest = null;
        },
        failure: function (errMsg) {
            if ($scope) {
                $scope.inLoginProcess = false;
                $scope.$apply();
            }
            //console.log("Login failure");
            alert(errMsg);

            loginServerRequest = null;
        }
    });
}

function loginServerFacebook($scope, token, successCallback, errorCallback) {
    if ($scope) {
        $scope.inLoginProcess = true;
        $scope.$apply();
    }
    loginServerFacebookRequest = $.ajax({
        url: SERVER_URL_BASE + "/auth/facebook/token?access_token=" + token,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        beforeSend: function (xhr) {
            if (loginServerRequest) {
                loginServerRequest.abort('NoShowMessage');
            }
            if (loginServerFacebookRequest) {
                loginServerFacebookRequest.abort('NoShowMessage');
            }
        },
        timeout: REQUEST_TIMEOUT,
        success: function (data) {
            if ($scope) {
                $scope.inLoginProcess = false;
                $scope.$apply();
            }
            //localStorage.user = null;
            localStorage.pass = null;
            if (successCallback)
                successCallback(data);

            loginServerFacebookRequest = null;
            sendPushToken();
        },
        error: function (jqXHR) {
            if ($scope) {
                $scope.inLoginProcess = false;
                $scope.$apply();
            }
            if (errorCallback && !userAborted(jqXHR)) {
                errorCallback(jqXHR);
            }

            loginServerFacebookRequest = null;
        },
        failure: function (errMsg) {
            if ($scope) {
                $scope.inLoginProcess = false;
                $scope.$apply();
            }
            console.warn(errMsg);
            alert(errMsg);

            loginServerFacebookRequest = null;
        }
    });
}

function autoLogin($scope, successCallback, errorCallback) {
    function loginFacebook(param) {
        if (facebookConnectPlugin) {
            facebookConnectPlugin.getLoginStatus(function onLoginStatus(status) {
                facebookConnectPlugin.getAccessToken(function (token) {
                    loginServerFacebook($scope, token, successCallback, errorCallback);
                });
            });
        }
    }
    //Auto Login
    if (localStorage.user && localStorage.pass) {
        loginServer($scope, localStorage.user, localStorage.pass, successCallback, loginFacebook);
    } else {
        loginFacebook();
    }
}

function sendPushToken() {
    if (window.FirebasePlugin) {
        window.FirebasePlugin.onTokenRefresh(function (token) {
            if (!token)
                return;
            // save this server-side and use it to push notifications to this device
            try {
                if (!api)
                    api = new Api();
            } catch (e) {
                api = new Api();
            }
            api.pushAdd(token);
            console.log(token);
        }, function (error) {
            console.error(error);
        });
    }
}