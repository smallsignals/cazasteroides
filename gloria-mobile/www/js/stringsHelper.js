/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function replaceTokens(str, tokens, values) {
    console.log("str: " + str);
    console.log("tokens: " + tokens);
    console.log("values: " + values);

    for (var i = 0; i < tokens.length; i++) {
        //str = str.replace(new RegExp("[[" + tokens[i] + "]]", "g"), values[i]);
        str = str.split("[[" + tokens[i] + "]]").join(values[i]);
    }
    return str;
}

function ordinal_suffix_of(ord, language) {
    switch (language) {
        case 'es':
            return ord + "&ordm;";
        default:
            var j = ord % 10,
                    k = ord % 100;
            if (j == 1 && k != 11) {
                return ord + "st";
            }
            if (j == 2 && k != 12) {
                return ord + "nd";
            }
            if (j == 3 && k != 13) {
                return ord + "rd";
            }
            return ord + "th";
    }
}
;
