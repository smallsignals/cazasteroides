var toolbox = angular.module('toolbox', ['ngCookies', 'ngRoute', 'ngTouch',
    'ngSanitize', 'gloria.api', 'ui.bootstrap']);
toolbox.config(['$routeProvider', function ($routeProvider) {

        /**
         * Cazasteroides
         */
        $routeProvider.when('/', {
            templateUrl: "html/cazast-login.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-login', {
            templateUrl: "html/cazast-login.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-signup', {
            templateUrl: "html/cazast-signup.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-forgot', {
            templateUrl: "html/cazast-forgot.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-main', {
            templateUrl: "html/cazast-main.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-vote', {
            templateUrl: "html/cazast-vote.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-me', {
            templateUrl: "html/cazast-me.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-ranking', {
            templateUrl: "html/cazast-premios.html",
            controller: "CazasteroidesController"
        });
        $routeProvider.when('/cazast-my_detections', {
            templateUrl: "html/cazast-my_detections.html",
            controller: "CazasteroidesController"
        });

        $routeProvider.when('/cazast-competition/:name', {
            templateUrl: "html/cazast-competition.html",
            controller: "CazasteroidesController"
        });

        /**
         * Gloria Project
         */
        $routeProvider.when('/gloria', {
            templateUrl: "html/gloria-main.html",
            controller: "MainController"
        });
        $routeProvider.when('/home', {
            templateUrl: "html/gloria-home.html",
            controller: "LoginController"
        });
        $routeProvider.when('/login', {
            templateUrl: "html/gloria-login.html",
            controller: "LoginController"
        });
        $routeProvider.when('/register', {
            templateUrl: "html/gloria-register.html",
            controller: "RegisterController"
        });
        $routeProvider.when('/images', {
            templateUrl: "html/gloria-images.html",
            controller: "ImagesController"
        });
        $routeProvider.when('/reservations', {
            templateUrl: "html/gloria-reservations.html",
            controller: "ReservationsController"
        });
        $routeProvider.when('/reservations/new', {
            templateUrl: "html/gloria-reservations.html",
            controller: "ReservationsController"
        });
        $routeProvider.when('/reservations/pending', {
            templateUrl: "html/gloria-reservations.html",
            controller: "ReservationsController"
        });
        $routeProvider.when('/experiment/solar', {
            templateUrl: "html/gloria-solar-experiment.html",
            controller: "SolarExperimentController"
        });
        $routeProvider.when('/experiment/night', {
            templateUrl: "html/gloria-night-experiment.html",
            controller: "NightExperimentController"
        });
    }]);

toolbox.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    };
    return fallbackSrc;
});

