#!/bin/bash

for i in "$@" ; do gunzip $i.gz; done

gdalinfo $1 -mm -hist > hist.txt
mean=`cat hist.txt|grep STATISTICS_MEAN|awk -F = '{print $2}'`
echo $mean
stddev=`cat hist.txt|grep STATISTICS_STDDEV|awk -F = '{print $2}'`
echo $stddev
minimum=`cat hist.txt|grep STATISTICS_MINIMUM|awk -F = '{print $2}'`
#minimum=$(awk "BEGIN {print $mean-$stddev; exit}")
max=$(awk "BEGIN {print $mean+$stddev; exit}")
echo $minimum
echo $max

#for i in "$@" ; do gdal_translate -of JPEG -ot Byte -scale $minimum $max $i $i.jpg; done

#for i in "$@"
#do 
# /usr/local/astrometry/bin/solve-field --no-plots --downsample 2 --tweak-order 2 --scale-units degwidth --scale-low 0.1 --scale-high 180.0 --overwrite $i.jpg
#done

#for i in "$@";do gdalinfo $i.new|grep CRVAL; done
p1=${1//\//\\\/}
p2=${2//\//\\\/}
p3=${3//\//\\\/}
p4=${4//\//\\\/}
p5=${5//\//\\\/}

pp=$(dirname "${1}")

cat /var/opt/cazasteroides/data/mapfile.map |sed 's/AAAA1/'"$p1"'/g'|sed 's/AAAA2/'"$p2"'/g'|sed 's/AAAA3/'"$p3"'/g'|sed 's/AAAA4/'"$p4"'/g'|sed 's/AAAA5/'"$p5"'/g'> $pp/mapfile.map


echo '{"image":{"mapfile":"'"${pp}"'/mapfile.map","ypoly":[],"xpoly":[],"filename":["'"$1"'","'"$2"'","'"$3"'","'"$4"'","'"$5"'"],
"fecha":"2016/07/14 23:02"}}'

